<?php
//DESCRIPCION: VENTANA PRINCIPAL  DE SALIDAS PARA ADMINISTRADORES
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
    //CONEXION A LA BASE DE DATOS
    require_once '../database/conexion_pdo.php';
    include("../assets/encabezado.php");
    include("../Security/LevelSecurityModules.php");
    ?>
    <!doctype html>
    <html lang="es">
        <head>
            <link rel="shortcut icon" href="../imagenes/1.ico"/>
            <link href="../css/paginacion.css" type="text/css" rel="stylesheet"/>
            <link href="../css/estilos.css" rel="stylesheet" type="text/css" />
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Salidas</title>
            <style>	
                body {
                    background: #eeeeee no-repeat center top;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    background-size: cover;}
                #div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;	}
                .container > header h1,
                .container > header h2 {
                    color: #fff;
                    text-shadow: 0 1px 1px rgba(0,0,0,0.7);
                }
            </style>

        <body>
            <div id="centro2">
                <table class="botonesfila" >
                    <tr>
                        <td>  
                            <a href="../principal.php">
                                <img src="../imagenes/inicio6.png"  border="0" width="52" height="52">
                                INICIO
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="centro">
                <div id="div_bienvenido">
                    <?php echo "Bienvenido"; ?> <BR/>
                    <div id="div_usuarios">
                        <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
                    </div>
                    <?php echo "SALIR"; ?>
                    <a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="13" height="13" /></a>
                </div>
                <center>
                    <table width="65%" border="0">
                        <tr>
                            <td colspan="11" class="titulo">
                        <center>
                            SALIDAS
                        </center>
                        </td>
                        </tr>
                    </table>
                    <?php if (isset($_GET['creado'])) {
                        if ($_GET['creado'] == 5) { ?>
                            <div class="quitarok">
                                <img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado la Salida correctamente!
                            </div>
                        <?php }
                    } ?>
                    <?php if (isset($_GET['borrado'])) {
                        if ($_GET['borrado'] == 1) { ?>
                            <div class="quitarok">
                                <img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha anulado la Salida  correctamente!
                            </div>
                        <?php }
                    } ?>
                    <table class="menuprincipal" >
                        <tr class="menuactivos">
                            <td colspan="4">
                                <h3>
                                    Activos Devolutivos
                                </h3>
                            </td>
                        <tr class="menuactivos">

                            <td>
                                <?php
                                if (in_array("14", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="salida_usuario_add.php">
                                        <img src="../imagenes/salida2.png" title="Menu de Elementos" width="64" height="64"  />
                                        Crear salida a usuarios
                                    </a>
                                    <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (in_array("15", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu"  href="salida_entidades_seleccion.php">
                                        <img src="../imagenes/establecimientos.png" title="Menu de Elementos" width="64" height="64"  />
                                        Crear salida Entidades
                                    </a>
                                  <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>

                            <td>

                            </td>

                            <td>
                                <?php
                                if (in_array("16", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="salida_baja_add.php?">
                                        <img src="../imagenes/bajas.png" title="Menu de Elementos" width="64" height="64"  />
                                        Salida por Bajas
                                    </a>
                                <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>


                        </tr>
                        <tr class="menuconsumo" >
                            <td colspan="4" >
                                <h3>
                                    Elementos de Consumo
                                </h3>
                            </td>
                        </tr>
                        <tr class="menuconsumo">

                            <td>
                                <?php
                                if (in_array("17", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="salida_consumo_usuario_add.php">
                                        <img src="../imagenes/salida2.png" title="Menu de Elementos" width="64" height="64" />
                                        Crear salida a usuarios
                                    </a>
                                 <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (in_array("18", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="salida_consumo_entidad_seleccion.php">
                                        <img src="../imagenes/establecimientos.png" title="Menu de Elementos" width="64" height="64" />
                                        Crear salida Entidades
                                    </a>
                                 <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>

                            <td>&#160;</td>

                            <td>
                                <?php
                                if (in_array("19", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="salida_baja_1.php?">
                                        <img src="../imagenes/bajas.png" title="Menu de Elementos" width="64" height="64" />
                                        Baja de E. de Consumo
                                    </a>
                                <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>


                        </tr>
                        <tr class="menugral">
                            <td colspan="4">
                                <h3>
                                    Bajas
                                </h3>
                            </td>
                        </tr>
                        <tr class="menugral">

                            <td>
                                <?php
                                if (in_array("20", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="../reportes/elementos_bajas.php">
                                        <img src="../imagenes/elementosbaja.png" title="Editar" width="50" height="50" />
                                        Ver Elementos  dados de Baja
                                    </a>
                                 <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="menuactivos2">
                            <td colspan="4">

                                <h3>
                                    Anuladas
                                </h3>
                            </td>
                        </tr>
                        <tr class="menuactivos2">

                            <td>
                                <?php
                                if (in_array("21", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="salidas_anuladas.php?all1=1">
                                        <img src="../imagenes/anuladas.png" title="Menu de Elementos" width="64" height="64" />
                                        Salidas Activos Anuladas
                                    </a>
                                 <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (in_array("22", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="salidas_consumo_anuladas.php?all1=1">
                                        <img src="../imagenes/anuladas.png" title="Menu de Elementos" width="64" height="64" />
                                        Salidas Consumo Anuladas
                                    </a>
                                    <?php
                                }
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="menuactivos2">
                            <td colspan="4">
                                <h3>
                                    Reporte de Salidas
                                </h3>
                            </td>
                        </tr>
                        <tr class="menuactivos2">
                            <td>
                                <?php
                                if (in_array("23", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="ReportDepartures.php">
                                        <img src="../imagenes/documento.png" title="Reportes" width="64" height="64" align="left" />
                                        Reporte de Salidas
                                    </a>
                                 <?php
                                }else{
                                ?>
                                &#160;
                                 <?php
                                }
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </center>
            </div>
        </body>
    </html>
    <?php
    include ("../assets/footer.php");
    ?>
    <?php
    /*
      @Cerrar Sesion
     */
} else {
    header("location: ../403.php");
}
?>
<?php
session_start();
require("../database/conexion_pdo.php");
$KeyWord=$_POST['search'];
$ExtraParameter=$_POST['ExtraParameter'];
$start=$_POST['start'];
$length=$_POST['length'];
$draw=$_POST['draw'];
$KeyWord_End=$KeyWord["value"];

class Departures{
    
  private $KeyWord;
  private $Connection;
  private $Start;
  private $Length;
  private $Draw;
  
  public function __construct($conn,$ExtraParameter,$start,$length,$draw)
  {
       $this->Connection = $conn;
       $this->KeyWord=$ExtraParameter;
       $this->Start=$start;
       $this->Length=$length;
       $this->Draw=$draw;
  }
  public function FormatDateSql($infoDate){
      $DateFormat = new DateTime($infoDate);
      return $DateFormat->format('Y-m-d');
  }
   public function FormatDate($infoDate){
      $DateFormat = new DateTime($infoDate);
      return $DateFormat->format('d-m-Y');
  }
  public function SearchListDepartures(){ 
    
    $sql="SELECT * FROM salidas ";
    $sql.="LEFT JOIN usuarios ON (salidas.documentoid=usuarios.documentoid) ";
    $sql.="LEFT JOIN dependencias ON (salidas.dependencia=dependencias.codigodependencia) ";
    $sql.="LEFT JOIN entidades on salidas.documentoid=entidades.nitentidad ";
    $sql.="WHERE idsalida IS NOT NULL ";
  
    if($this->KeyWord["DateStart"]!=""&&$this->KeyWord["DateEnd"]!=""){
        
         $sql.="AND (fechasalida>=:datestart AND fechasalida<=:dateend) ";
    }
    if($this->KeyWord["TypeElement"]!="-1"){
        
         $sql.="AND tiposalida=:idtypelement ";
    }
    $sql.="ORDER BY idsalida ASC ";
    $sql_limit="LIMIT $this->Length OFFSET ".$this->Start;
    $stmt_total = $this->Connection->prepare($sql);
    $stmt = $this->Connection->prepare($sql.$sql_limit);
 
    
    if($this->KeyWord["DateStart"]!=""&&$this->KeyWord["DateEnd"]!=""){
   
        $DateStart_R=$this->FormatDateSql($this->KeyWord["DateStart"]);
        $DateEnd_R=$this->FormatDateSql($this->KeyWord["DateEnd"]);
        
        $stmt_total->bindParam(':datestart',$DateStart_R);
        $stmt_total->bindParam(':dateend',$DateEnd_R);
        $stmt->bindParam(':datestart',$DateStart_R);
        $stmt->bindParam(':dateend',$DateEnd_R);
        
        
    }
    if($this->KeyWord["TypeElement"]!="-1"){
        
        $TypeElement = $this->KeyWord["TypeElement"];
        $stmt_total->bindParam(':idtypelement',$TypeElement,PDO::PARAM_STR);
        $stmt->bindParam(':idtypelement',$TypeElement,PDO::PARAM_STR);
    }
    $stmt_total->execute();
    $total_datos = $stmt_total->rowCount();
    $stmt->execute();
    $total_limit = $stmt->rowCount();
  
    //$result = $stmt->fetchAll();
    //$obj->filmName;
    $jsondata = array();
        if($total_datos>0){
            $jsondata = array(
                    'success' => true,
                    'message' => sprintf("Se han encontrado %d resultados", $total_datos),
                    'draw'=> $this->Draw,
                    'recordsTotal'=> $total_limit,
                    'recordsFiltered'=> $total_datos,
                    'data'=>array()
            );
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
                  //$documento_user = (empty($row->documentoid) || is_null($row->documentoid)) ? "--" : $row->documentoid;
                    $detail="";
                    $action="";
                    $idpdf="";
                    $funcionario_entidad="";
                    $documento_nit="";
                    if($row->tiposalida=="Salida de Bienes Devolutivos"){
                        $detail='<a href="detalles_salida.php?salida='.$row->salida.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                        if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                        $action='<a href="anular_salida_entidad_mod.php?salida='.$row->salida.'"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>';
                        }       
                        $idpdf='<a href="../crear_pdf/crear_pdf_salida_devolutivos_usuarios.php?salida='.$row->salida.'"><img src="../imagenes/pdf.png" title="Ver PDF" width="28" height="28"/></a>';
                        $funcionario_entidad=htmlentities($row->nombres)." ".htmlentities($row->apellidos);
                        $documento_nit = $row->documentoid;
                    }
                    if($row->tiposalida=="Salida de Elementos de consumo"){
                        $detail='<a href="detalles_salida_consumo.php?salida='.$row->salida.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                        if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                        $action='<a href="anular_salida_consumo1.php?salida='.$row->salida.'"><img src="../imagenes/ANULAR.png" title="Anular Salidar" width="28" height="28"/></a>';
                        }
                        $idpdf='<a href="../crear_pdf/crear_pdf_salida_consumo_usuarios.php?salida='.$row->salida.'"><img src="../imagenes/pdf.png" title="Ver PDF" width="28" height="28"/></a>';
                        $funcionario_entidad=htmlentities($row->nombres)." ".htmlentities($row->apellidos);
                        $documento_nit = $row->documentoid;
                        
                    }
                    if($row->tiposalida=="Salida de Elementos de consumo a otra Entidad"){
                        $detail='<a href="detalles_salida_entidad_consumo.php?salida='.$row->salida.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                        if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                        $action='<a href="anular_salida_consumo1.php?salida='.$row->salida.'"><img src="../imagenes/ANULAR.png" title="Anular Salida" width="28" height="28"/></a>';
                        }
                        $idpdf='<a href="../crear_pdf/crear_pdf_salida_consumo_entidades.php?salida='.$row->salida.'"><img src="../imagenes/pdf.png" title="Ver PDF" width="28" height="28"/></a>';
                        $funcionario_entidad=$row->entidad;
                        $documento_nit=$row->nitentidad;
                    }
                    
                    if($row->tiposalida=="Salida activos a otra Entidad"){
                        $detail='<a href="detalles_salida_entidad.php?salida='.$row->salida.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                        if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                        $action='<a href="anular_salida_mod.php?salida='.$row->salida.'"><img src="../imagenes/ANULAR.png" title="Anular Salida" width="28" height="28"/></a>';
                        }
                        $idpdf='<a href="../crear_pdf/crear_pdf_salida_devolutivos_entidades.php?salida='.$row->salida.'"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>';
                        $funcionario_entidad=$row->entidad;
                        $documento_nit=$row->nitentidad;
                    }
                    
                    if($row->tiposalida=="Salida por baja de Bienes Devolutivos"){
                        
                        $detail='<a href="detalles_salida_bajas.php?salida='.$row->salida.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                        if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                        $action='<a href="anular_salida_entidad_mod.php?salida='.$row->salida.'"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>';
                        }       
                        $idpdf='<a href="../crear_pdf/crear_pdf_salida_devolutivos_usuarios.php?salida='.$row->salida.'"><img src="../imagenes/pdf.png" title="Ver PDF" width="28" height="28"/></a>';
                        $funcionario_entidad=htmlentities($row->nombres)." ".htmlentities($row->apellidos);
                        $documento_nit = $row->documentoid;
                        
                    }
                    
                    
                    
                  $jsondata['data'][]= array(
                    'idsalida' => $row->idsalida,
                    'salida' => $row->salida,
                    'fechasalida' => $this->FormatDate($row->fechasalida),
                    'tiposalida' => htmlentities($row->tiposalida),
                    'nombredependencia' => htmlentities(utf8_encode($row->nombredependencia)),
                    'documento_nit' => $documento_nit,
                    'funcionario_entidad' => $funcionario_entidad,
                    'numitems' => $row->numitems,
                    'valortotal' => number_format($row->valortotal,2,',','.'),
                    'elaboradopor' => htmlentities(utf8_encode($row->elaboradopor)),
                    'comentario' => htmlentities(utf8_encode($row->salcomentarios)),
                    'detalle' => $detail,
                    'acciones' => $action,
                    'pdf' => $idpdf
                );
           
            }
          
        }else {
            $jsondata = array(
                    'success' => true,
                    'message' => 'No se encontró ningún resultado.',
                    'draw'=> $this->Draw,
                    'recordsTotal'=> $total_limit,
                    'recordsFiltered'=> $total_datos,
                     'data'=>array()
            );
        }
        return json_encode($jsondata);
        $stmt = null;
        $stmt_total = null;
  }
}

$data = new Departures($conexion,$ExtraParameter,$start,$length,$draw);
$resultado = $data->SearchListDepartures();
echo $resultado;
/**
 * Close connection
 */
$conn = null;

?>

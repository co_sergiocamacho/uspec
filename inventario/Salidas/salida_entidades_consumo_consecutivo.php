<?php
//DESCRIPCION:  CREA CONSECUTIVO DE SALIDA PARA ELEMENTOS DE CONSUMO A SALIDAS PARA ENTIDADES-ACTUALIZA BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
include("../database/conexion.php");

    $identidad=$_POST['identidad'];
    $jsondata = array();
    $proceso_transaccion = true;
    $queryentidad="SELECT * FROM entidades WHERE identidad='$identidad'";
    $leerentidad=mysql_query($queryentidad,$conexion);	
    while($filenti=mysql_fetch_array($leerentidad))
    {
            $nitentidad=$filenti['nitentidad'];
    }
    mysql_free_result($leerentidad);
    
    /**
     * Generar Consecutivo
     */
    ////////////////////////////////////////////////
    $referencia="SAL-";
    $consecutivo="";
    $consulta="SELECT COUNT(idconsecsaldia) AS total_data FROM consecutivosalidas";
    $result=mysql_query($consulta,$conexion);
    $row=mysql_fetch_array($result,MYSQLI_ASSOC);
    $num_resultados=$row["total_data"];
    mysql_free_result($result);

    $tiposalida="Salida de elementos de consumo a otra entidad.";
    $consecutivo=$num_resultados+1;
    $salidanum=$referencia.$consecutivo;
    $fechasalida=date("Y-m-d");
    ///////////////////////////////////////////////
    $numcontrato="";
    $elaboradopor=$_SESSION['nombres']." ". $_SESSION['apellidos'];
    /* deshabilitar autocommit */
    mysql_query("SET AUTOCOMMIT=0");
    mysql_query("START TRANSACTION");
    $insertar_consecutivo="INSERT INTO consecutivosalidas (consecsalida) VALUES ('$salidanum')";
    $query_consecutivo=mysql_query($insertar_consecutivo,$conexion);
    $anterior=mysql_insert_id()-1;
    if (!$query_consecutivo){
        $proceso_transaccion = false;
    }
    $insertar_salida="INSERT INTO salidas (salida, fechasalida, tiposalida,  documentoid,  elaboradopor) VALUES ('$salidanum', '$fechasalida', '$tiposalida', '$nitentidad',  '$elaboradopor')";
    $query_salida=mysql_query($insertar_salida,$conexion);
    if (!$query_salida){
        $proceso_transaccion = false;
    }

    if ($proceso_transaccion) {
        mysql_query("COMMIT");
        $jsondata["data"] = array(
                'message' => "Consecutivo creado.",
                'internalid' => "3",
                'nitentidad' => $nitentidad,
                'numberconsec' => $consecutivo,
                'salidanum' => $salidanum,
                'anterior' => $anterior,
                'process' => "true"
        );
    } else {
        mysql_query("ROLLBACK");
        $jsondata["data"] = array(
                'message' => "Consecutivo no creado.",
                'process' => "false"
            );
    }           
    mysql_close(); 
   echo json_encode($jsondata);

//header ("location: salida_consumo_entidades_add.php?internalid=3&nitentidad=$nitentidad&numberconsec=$consecutivo&salidanum=$salidanum&anterior=$anterior");

} else {
header("location: ../403.php");
}
?>

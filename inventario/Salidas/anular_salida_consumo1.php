<?php
//DESCRIPCION:  METODO PARA ANULAR SALIDAS POR CONSUMO VENTANA 1
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	$numsalida=$_GET['salida'];
	$salida_a_borrar=$_REQUEST["salida"];

	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Anular entradas</title>



		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<body>





		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="ReportDepartures.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr></table></div>



				<div id="centro">

					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
					</div>   
					<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DE LAS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24



					$sqlquery="SELECT * FROM tabla_aux_consumo_salidas


					LEFT JOIN proveedores on tabla_aux_consumo_salidas.idproveedor_aux=proveedores.idproveedor
					LEFT JOIN consumo on tabla_aux_consumo_salidas.idelemento_aux=consumo.idelemento
					LEFT JOIN codigocontable on tabla_aux_consumo_salidas.codigocontable_aux=codigocontable.codigocontable

					WHERE numsalida_aux='$numsalida' 
					ORDER BY idelemento_aux

					";
					$t_consumo=mysql_query($sqlquery, $conexion);

					$num_registro=mysql_num_rows($t_consumo);

					if ($num_registro == 0){

						?>
						<center>
							<form name="anularcomentario" action="anular_salida_consumo3.php?salida=<?php echo $salida_a_borrar;?>" title="Anular" method="post">
								<table width="60%" class="tabla_2" border="0" style="width:580px">
									<tr>
										<TD class="fila1" colspan="3">      <H1 >ANULAR SALIDA</H1></TD>
									</tr>
									<tr>
										<td class="fila2" colspan="3" ><center>Por favor escriba el motivo por el cual anulará la Salida</center></td>
									</tr>
									<tr>
										<TD class="fila2"><strong>Comentario</strong></TD>  
										<td class="fila2" colspan="3"><input type="text" class="textinput"  name="comentario" size="100"  onChange="MAY(this)" value=""  required/></td>

										<tr>
											<td class="fila2">&nbsp;</td>
											<td class="fila2"><center><input type="submit" class="botonguardar" name="guardar" title="Aceptar" value="Aceptar"/>
											</p></center></td>


											<td class="fila2"><p align="center"> 
												<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar anular" value="cancelar"/>
											</form>

										</Tr>
									</table>
									<?php
								} else {
									?>


									<table  width="95%" id="tabla_activos">
										<tH class="fila1" colspan="12" style="font-size:14px;">SALIDA N°: <?PHP ECHO $numsalida;?> </tH>

										<TR>

											<th width="30px"class="fila1">ID</th>
											<th  class="fila1">ELEMENTO</th>
											<th width="30px" class="fila1">UNID. MEDIDA</th>
											<th class="fila1">CANTIDAD</th>
											<th class="fila1">FECHA</th>
											<th class="fila1">VALOR UNIT</th>
											<th class="fila1">VALOR TOTAL</th>
											<th width="30px" class="fila1">CODIGO CONTABLE</th>
											<th width="30px" class="fila1">CATEGORIA</th>
											<th width="60px" class="fila1">SALIDA N°</td>



												<th class="fila1" >OBSERVACIONES</th>
												<th class="fila1" >ELIMINAR</th>
											</tR>
											<?php



//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
//INNER JOIN usuarios on productos.documentoid=usuarios.documentoid

//ENLAZAR NOMBRES Y APELLIDOS

//<th class="fila1"  >NOMBRES</th>
// <th class="fila1"  >APELLIDOS</th>
//<td class="fila2"><?php echo $fila_consumo["nombres"]; cerrar PHP </td>
//     <td class="fila2"><?php echo $fila_consumo["apellidos"]; CERRAR PHP </td> 




//mysql_close($conexion);
//exit();
//}

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09 

//FIN PAGINADOR

//CONSULTA PARA UBICAR EL PAGINADOR
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09  

											?>  

											<?php
											while ($fila_consumo=mysql_fetch_array($t_consumo))
											{

												?>
												<tr>
													<td class="fila2"><?php echo $fila_consumo["idelemento_aux"];?></td>
													<td class="fila2"><?php echo $fila_consumo["elemento"];?></td>
													<td class="fila2"><?php echo $fila_consumo["unidmedida"];?></td>
													<td class="fila2"><?php echo $fila_consumo["cantidad_aux"];?></td>
													<td class="fila2"><?php echo $fila_consumo["fecha_salida_aux"];?></td>

													<td class="fila2" align="right">$<?php echo number_format($fila_consumo["precioadqui"],2,',','.');?></td>
													<td class="fila2" align="right">$<?php echo number_format(($fila_consumo["cantidad_aux"]*$fila_consumo["precioadqui"]),2,',','.');?></td>
													<td class="fila2"><?php echo $fila_consumo["codigocontable_aux"];?></td>
													<td class="fila2"><?php echo $fila_consumo["codigodescripcion"];?></td>
													<td class="fila2"><?php echo $fila_consumo["numsalida_aux"];?></td>

													<td class="fila2"><?php echo $fila_consumo["consobservaciones"];?></td>
													<td class="fila2" align="center">


														<a href="anular_salida_consumo2.php?idtabla_aux=<?php echo $fila_consumo['idtabla_aux'];?>&idelemento=<?php echo $fila_consumo['idelemento'];?>&numsalida=<?php echo $numsalida?>&cant=<?php echo $fila_consumo['cantidad_aux'];?>"><img src="../imagenes/delete.png" title="Modificar A" width="28" height="28"/></a>



														<?php
													}
													?> 

												</tr>
												<?php 

												$numsalida=$_GET['salida'];
												$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_salidas WHERE numsalida_aux='$numsalida'");   
												$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
												$valortotal=$valortotal1["total"];

												?>
												<tr>
													<TD class="fila2"colspan="4"> &nbsp;</TD>
													<TD class="fila2"colspan="2"><strong>VALOR TOTAL</strong></TD>
													<td class="fila3" colspan="1" align="right">$<?php echo number_format($valortotal,2,',','.');?></td>
												</tr>
											</table>



											<?php 
										}
										?>



									</div>


								</body>




								<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

<?php
   //DESCRIPCION:  VENTANA QUE PERMITE AGREGAR ELEMENTOS DE CONSUMO A LAS SALIDAS PARA FUNCIONARIOS
   //NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
   //FECHA: 2015-07-24
   //Unidad de Servicios Penitenciarios y Carcelarios
   //SOLUCIONES DE PRODUCTIVIDAD
   session_start();
   //Verificación de sesion
   if (isset($_SESSION['idpermiso'])) {
   include("../database/conexion.php");
   include("../assets/encabezado.php");
   include("../assets/global.php");
   ?>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <script src="../js/calendario/src/js/jscal2.js"></script>
      <script src="../js/calendario/src/js/lang/en.js"></script>
      <link href="../css/estilos.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
      <link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
      <link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
      <link rel="stylesheet" type="text/css" href="../css/paginacion.css"  >
      <link rel="shortcut icon" href="../imagenes/1.ico">
      <link rel="stylesheet" type="text/css" href="../css/loading.css"  >
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
      <script src="../js/jquery.mousewheel.js"></script>
      <script>
          $(document).ready(function () {
            $('#div_loading').hide();
            $('#guardar_producto').click(function () {
                var id_documento = $("#salida_documentoid").val();
                if (!$("#salida_documentoid").attr("value").match(/^[0-9a-zA-Z]+$/)) {
                    alert("ingrese un documento valido.");
                    return false;
                }
                $.ajax({
                    data: {"documento" : id_documento},
                    type: "POST",
                    dataType: "json",
                    url: "../General/search_user.php",
                    beforeSend: function(xhr) {
		        $('#div_loading').show();
                    }
                })
                .done(function(response, textStatus, jqXHR ) {
		        if(response.data.process=="true"){
                           setTimeout(function(){
                           CreateConsecutivo(id_documento);
                           }, 1000);
                        }
                        if(response.data.process=="false"){
                            $('#div_loading').hide();
                            alert(response.data.message);
                        }
                })
                .fail(function( jqXHR, textStatus, errorThrown ) {
			    $('#div_loading').hide();
                            alert(textStatus);
                     
                });
            });
            
            function CreateConsecutivo(id_documento){
                    $.ajax({
                       data: {"documento" : id_documento},
                       type: "POST",
                       dataType: "json",
                       url: "salida_consumo_usuarios_guardar.php"
                   })
                   .done(function(response, textStatus, jqXHR ) {
                            $('#div_loading').hide();
                            if(response.data.process=="true"){
                                $("#internalid").val(response.data.internalid);
                                $("#userid").val(response.data.userid);
                                $("#numberconsec").val(response.data.numberconsec);
                                $("#salidanum").val(response.data.salidanum);
                                $("#anterior").val(response.data.anterior);
                                $("#salida_elementos_add").submit();
                            }
                            if(response.data.process=="false"){
                               alert("No se ha podido crear la salida. Por favor intentelo nuevo m\u00E1s tarde.")
                            }
                   })
                   .fail(function( jqXHR, textStatus, errorThrown ) {
                               $('#div_loading').hide();
                               alert(textStatus);

                   });
                 
            }
            $("#horizontal").wrapInner("<table cellspacing='10'><tr>");
            $(".post").wrap("<td></td>");
            $("body").mousewheel(function(event, delta) {
                    this.scrollLeft -= (delta * 50);
                    event.preventDefault();
            });  
          });
      </script>
      <style>	
         <!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
         body {
         background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
         -webkit-background-size: cover;
         -moz-background-size: cover;
         background-size: cover;
         }
         .container > header h1,
         .container > header h2 {
         color: #fff;
         text-shadow: 0 1px 1px rgba(0,0,0,0.7);
         }
      </style>
      <title>Salida de elementos</title>
   <body>
    <form id="salida_elementos_add" method="get" action="salida_consumo_add.php">
        <input type="hidden" id="internalid" name="internalid" value=""/>
        <input type="hidden" id="userid" name="userid" value=""/>
        <input type="hidden" id="numberconsec" name="numberconsec" value=""/>
        <input type="hidden" id="salidanum" name="salidanum" value=""/>
        <input type="hidden" id="anterior" name="anterior" value=""/>
    </form>
    <div id="div_loading">
        <img id="img_loading" src="../imagenes/ajax-loader.gif"/>
    </div>
      <div id="centro2">
         <table class="botonesfila" >
            <tr>
               <td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
               <td><a href="salidas_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td>
            </tr>
         </table>
      </div>
      <div id="centro" >
         <div id="div_bienvenido">
            <?php echo "Bienvenido"; ?> <BR/>
            <div id="div_usuarios">
               <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
            </div>
            <?php echo "SALIR";?>
            '<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
         </div>
         <CENTER>
            SALIDA DE ELEMENTOS A USUARIOS
         </center>
         </td>
         </tr>
         </table>
         </CENTER>
         <div id="generador">
            <table width="70%">
               <div id="generador" width="30%" align="">
                  <center>
                     <form name="CrearSalida" action="nuevo_num_salida.php" title="Consecutivo de Entrada">
                  </center>
                  </tr>
                  </form>
               </div>
               </center>
            </table>
         </div>
         <CENTER>
         <table class="tabla_4">
            <TH class="fila1" COLSPAN="4">
               <H3>ASIGNACION DE ELEMENTOS A </H3>
            </TH>
            <TR>
               <form name="productos" id="productos" title="Datos de la Salida" method="post">
                  <th class="fila2" >Documento de Identificación</th>
                  <td class="fila2"><input type="text" class="textinput"  name="salida_documentoid" id="salida_documentoid" size="30" maxlength="50"  onChange="MAY(this)" value=""  onkeypress="return SoloNumeros(event)" placeholder="Escriba la CC del funcionario"/></td>
            </TR>
            <tr>
            <td class="fila2" colspan="2"><center><a href="buscar_usuario.php" target="_blank" align="center"><img align="center" src="../imagenes/buscarusuario.png" height="16" target="_blank" title="Buscar"
            </a>Buscar usuario</tr>
            </tr>
            <TR>
                <td  class="fila2"><p align="center"><input type="button" class="botonguardar" name="guardar_producto" id="guardar_producto" title="Guardar producto" value="Generar "/>
            </p></td>
            <td class="fila2"><p align="center">
            <a href="javascript:history.go(-1)">
            <input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="cancelar  "/></a>  
            <tr>
            </td>
            </tr>
            </form>
            </TR>
         </table>
         </CENTER>
      </div>
   </body>
</html>
<?php 
   include ("../assets/footer.php");
   ?>
<?php
   /*
   @Cerrar Sesion
   */
   } else {
   header("location: ../403.php");
   }
?>

	





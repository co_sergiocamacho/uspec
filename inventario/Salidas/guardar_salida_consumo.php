<?php
include("../database/conexion.php");
include("../assets/datosgenerales.php");
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


//PERMITE GUARDAR DATOS ADICIONALES  A LA salida DE CONSUMO
//EXISTEN DATOS CON CAMPOS AUTOMÁTICOS PARA GUARDAR COMO LA FECHA DE ELABORACION Y LA PERSONA QUE ELABORO LA salidaSALDIA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

$numsalida2=$_POST['salida_numsalida'];
$fechasalida=$_POST['f_date1'];
$dependencia=$_POST['salida_dependencia'];
$comentario=$_POST['salida_comentario'];
$tiposalida="Salida de Elementos de consumo";
$documentoid=$_GET['iduser'];
$elaboro=$_POST['salida_elaboro'];
$valortotal=$_POST['salida_valortotal'];
$cant_items= $_POST['salida_num_items'];
$observaciones="Salida de elementos creada correctamente";
$tiposalida_sel=$_POST['salida_tiposalida_sel'];
$activo=1;
$jsondata = array();
$proceso_transaccion = true;

/* Deshabilitar autocommit */
mysql_query("SET AUTOCOMMIT=0");
mysql_query("START TRANSACTION");

$update_salida="UPDATE salidas SET "
        . "fechasalida='$fechasalida', "
        . "tiposalida='$tiposalida',  "
        . "documentoid='$documentoid', "
        . "numitems='$cant_items', "
        . "valortotal='$valortotal',"
        . "dependencia='$dependencia', "
        . "elaboradopor='$elaboro', "
        . "salcomentarios='$comentario', "
        . "salobservaciones='$observaciones', "
        . "salactivo='$activo', "
        . "unidadejecutora='$unidadejecutora', "
        . "nitunidadejecutora='$nitunidadejecutora', "
        . "tiposalida_sel='$tiposalida_sel' "
        . "WHERE salida='$numsalida2'";
$query_update_salida=mysql_query($update_salida,$conexion) or exit(mysql_error()); 
if (!$query_update_salida){
    $proceso_transaccion = false;
}
$update_tablas_consumo = "UPDATE tabla_aux_consumo_salidas SET "
        . "fecha_salida_aux='$fechasalida', "
        . "documentoid_aux='$documentoid', "
        . "dependencia_aux='$dependencia', "
        . "tipoelementos_aux='$tiposalida', "
        . "comentario_aux='$comentario', "
        . "elaboro_aux='$elaboro', "
        . "unidadejecutora_aux='$unidadejecutora', "
        . "nitunidadejecutora_aux='$nitunidadejecutora' "
        . "WHERE numsalida_aux='$numsalida2'";

$query_update_tablas_consumo=mysql_query($update_tablas_consumo,$conexion)or exit(mysql_error());
if (!$query_update_tablas_consumo){
    $proceso_transaccion = false;
}

$update_kardex=" UPDATE tabla_aux_kardex SET "
        . "fechasalida_kardex= '$fechasalida', "
        . "elaboro_kardex='$elaboro', "
        . "documentoid_out_kardex='$documentoid', "
        . "dependencia_out_kardex='$dependencia', "
        . "fecha_disponible_kardex='$fechasalida', "
        . "comentario_kardex='$comentario' "
        . "WHERE salida_kardex='$numsalida2' ";
$query_update_kardex=mysql_query($update_kardex,$conexion)or exit(mysql_error()); 
if (!$query_update_kardex){
    $proceso_transaccion = false;
}

$justificacion="creacion de Salida de consumo a usuarios "." permiso usuario: " .$_SESSION['idpermiso'];
$observacionregistro="Salida numero: " .$numsalida2." Acceso por IP:".$ip;
$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
$fecha=date ("Y-m-d");
$hora=date("H:i:s");
$registro_sistema="INSERT INTO registro (registro, usuario, fecha, hora, observacion) "
        . "VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
$query_insert_registro=mysql_query($registro_sistema,$conexion)or exit(mysql_error()); 
if (!$query_insert_registro){
    $proceso_transaccion = false;
}
            if ($proceso_transaccion) {
                mysql_query("COMMIT");
                header("location:salidas_consumo_creadas.php?all1=1&all2=1");
                exit();
            } else {
                mysql_query("ROLLBACK");
            }           
            mysql_close(); 
} else {
header("location: ../403.php");
}
?>

<?php
//DESCRIPCION: VENTANA PARA VISUALIZAR LAS SALIDAS DE CONSUMO  GENERADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS

if(isset($_REQUEST['all1'])){$all1=1;}
if(isset($_REQUEST['all2'])){$all2=1;}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">



	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>SALIDAS GENERADAS</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />





</head>

<body>

	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="salidas_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>





			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> DETALLES DE LAS SALIDAS DE ELEMENTOS DE CONSUMO</STRONG></center>
						</td>


					</tr>
				</table>

				<form name="salidas" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
					<CENTER>
						<TABLE>
							<tr>
								Ordenar por fecha
							</tr>
							<tr>
								<td class="fila5a">DESDE</td>
								<td class="fila5a"><input size="10" id="f_date1" name="f_date1" value="" /><button id="f_btn1" class="botonmas">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>

	<td class="fila5a">HASTA</td>
	<td class="fila5a"><input size="10" id="f_date2" name="f_date2" value="" /><button class="botonmas" id="f_btn2">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date2",
		trigger    : "f_btn2",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script></td>

	<td><input type="submit" class="botonver" name="submit" value="Ver"><br></td>
</tr></form>
</TABLE></CENTER>

<table width="100%" border="0">
	<tr>
		<td  class="subtitulo">
			<center><STRONG> Salidas a Usuarios</STRONG></center>
		</td>


	</tr>
</table>




<div class="container-full">
	<div class="table-responsive">
		<table   class=" table"  cellspacing="0" width="100%" >
			<thead>
				<tr>

					<td  class="fila1">ID</td>
					<td  class="fila1">SALIDA N</td>
					<td  class="fila1">FECHA DE SALIDA</td>
					<td   class="fila1">DEPENDENCIA</td>
					<td class="fila1">DOCUMENTO ID</td>
					<td  class="fila1">NOMBRES </td>
					<td  class="fila1">APELLIDOS</td>
					<td  class="fila1">CANT ITEMS</td>
					<td  class="fila1">VALOR TOTAL</td>
					<td  class="fila1">ELABORO</td>

					<td  class="fila1">COMENTARIOS</td>
					<td class="fila1">OBSERVACIONES</td>
					<td class="fila1">PDF</td>
					<td class="fila1">DETALLES</td>
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

					<td class="fila1">ELIMINAR</td>
				</tr>
			</thead>
			<tbody id="myTable2">
				<?php }?>

				<?php


				if(isset($_REQUEST['all1'])){
					$fecha1=date("Y-m-d");
					$fecha2=date("Y-m-d");
					$query1="SELECT  * FROM salidas  
					LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
					LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
					WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo' ORDER BY fechasalida DESC"  ;
				}


				else {
					if(isset($_REQUEST['all1'])){
						$fecha1=$_REQUEST['f_date1'];
						$fecha2=$_REQUEST['f_date2'];
						$query1="SELECT  * FROM salidas  
						LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
						LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
						WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo' 
						AND fechasalida>= '$fecha1' AND fechasalida<='$fecha2' ORDER BY fechasalida DESC"  ;
					}}

					if (empty($fecha1) and empty($fecha2)){ 
						$query1="SELECT  * FROM salidas  
						LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
						LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
						WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo' ORDER BY fechasalida DESC"  ;
					}



					$t_salidas=mysql_query($query1,$conexion);

					while ($Fila_salidas=mysql_fetch_array($t_salidas)){

						?>
						<tr>
							<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["nombredependencia"];?></td>
							<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
							<td class="fila2"><?php echo $Fila_salidas["nombres"];?></td>
							<td class="fila2"><?php echo $Fila_salidas["apellidos"];?></td>
							<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
							<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
							<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>

							<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>



						
							<td class="fila2"><center>
								<a href="../crear_pdf/crear_pdf_salida_consumo_usuarios.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
							</center></td>

						



							<td class="fila2"><center>
								<a href="detalles_salida_consumo.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
							</center></td>





							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
							<td class="fila2"><center>
								<a href="anular_salida_consumo1.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
							</center></td>

							<?php }?>



							<?php
						}

						?>


					</tbody>
				</TABLE>


			</div>
			<div>
				<center>
					<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
					<div class="col-md-12 text-center">

					</div>




					<table width="100%" border="0">
						<tr>
							<td  class="subtitulo">
								<center><STRONG> Salidas a otras entidades</STRONG></center>
							</td>


						</tr>
					</table>



					<div class="container-full">
						<div class="table-responsive">
							<table   class=" table"  cellspacing="0" width="100%" >
								<thead>
									<tr>

										<td  class="fila1">ID</td>
										<td  class="fila1">SALIDA N</td>
										<td  class="fila1">FECHA DE SALIDA</td>
										<td  class="fila1">ENTIDAD</td>
										<td class="fila1">NIT</td>
										<td class="fila1">DIRECCIÓN</td>
										<td  class="fila1">TELÉFONO</td>
										<td  class="fila1">CANT ITEMS</td>
										<td  class="fila1">VALOR TOTAL</td>
										<td  class="fila1">ELABORO</td>
										<td  class="fila1">COMENTARIOS</td>
										<td class="fila1">OBSERVACIONES</td>
										<td class="fila1">PDF</td>
										<td class="fila1">DETALLES</td>
										<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
										<td class="fila1">ELIMINAR</td>
										<?php }?>

									</thead>
									<tbody id="myTable2a">

										<?php

										if(isset($_REQUEST['all1'])){
											$fecha1=date("Y-m-d");
											$fecha2=date("Y-m-d");
											$query1="SELECT  * FROM salidas  
											LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
											WHERE salactivo='1' AND tiposalida LIKE 'Salida de Elementos de consumo a otra Entidad%' ORDER BY fechasalida DESC"  ;
										}

										else {
											if(isset($_REQUEST['all1'])){
												$fecha1=$_REQUEST['f_date1'];
												$fecha2=$_REQUEST['f_date2'];
												$query1="SELECT  * FROM salidas  
												LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
												WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo a otra Entidad' 
												AND fechasalida>= '$fecha1' AND fechasalida<='$fecha2' ORDER BY fechasalida DESC"  ;
											}
											if (empty($fecha1) and empty($fecha2)){ 
												$query1="SELECT  * FROM salidas  
												LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
												WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo a otra Entidad' ORDER BY fechasalida DESC"  ;

											}}

											$t_salidas=mysql_query($query1,$conexion);
											while ($Fila_salidas=mysql_fetch_array($t_salidas)){


												?>
												<tr>
													<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
													<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
													<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
													<td class="fila2"><?php echo $Fila_salidas["entidad"];?></td>
													<td class="fila2"><?php echo $Fila_salidas["nitentidad"];?></td>
													<td class="fila2"><?php echo $Fila_salidas["direccion_entidad"];?></td>
													<td class="fila2"><?php echo $Fila_salidas["telefono_entidad"];?></td>
													<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
													<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
													<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>

													<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
													<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>




<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 


					

												<td class="fila2"><center>
												<a href="../crear_pdf/crear_pdf_salida_consumo_entidades.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
												</center></td>

												
						<?php }?>





													<td class="fila2"><center>
													<a href="detalles_salida_entidad_consumo.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
													</center></td>





													<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

													<td class="fila2"><center>
														<a href="anular_salida_consumo1.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
													</center></td>

													<?php }?>



													<?php
												}

												?>
											</tr>
										</tbody>
									</TABLE>


								</div>
								<div>
									<center>
										<ul class="pagination pagination-lg pager" id="myPager2a"></ul></center>
										<div class="col-md-12 text-center">

										</div>
									</div>
								</div>




<div id="centro">


<table width="100%" border="0">
	<tr>
		<td  class="subtitulo">
			<center><STRONG> Salidas por Baja</STRONG></center>
		</td>


	</tr>
</table>




<div class="container-full">
	<div class="table-responsive">
		<table   class=" table"  cellspacing="0" width="100%" >
			<thead>
				<tr>

					<td  class="fila1">ID</td>
					<td  class="fila1">SALIDA N</td>
					<td  class="fila1">FECHA DE SALIDA</td>
					<td  class="fila1">CANT ITEMS</td>
					<td  class="fila1">VALOR TOTAL</td>
					<td  class="fila1">ELABORO</td>
					<td  class="fila1">COMENTARIOS</td>
					<td class="fila1">OBSERVACIONES</td>
					<td class="fila1">PDF</td>
					<td class="fila1">DETALLES</td>
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

					<td class="fila1">ELIMINAR</td>
				</tr>
			</thead>
			<tbody id="myTable2">
				<?php }?>

				<?php


				if(isset($_REQUEST['all1'])){
					$fecha1=date("Y-m-d");
					$fecha2=date("Y-m-d");
					$query1="SELECT  * FROM salidas  
					LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
					LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
					WHERE salactivo='1' AND tiposalida='Salida Por baja de Elementos' ORDER BY fechasalida DESC"  ;
				}


				else {
					if(isset($_REQUEST['all1'])){
						$fecha1=$_REQUEST['f_date1'];
						$fecha2=$_REQUEST['f_date2'];
						$query1="SELECT  * FROM salidas  
						LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
						LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
						WHERE salactivo='1' AND tiposalida='SSalida Por baja de Elementos' 
						AND fechasalida>= '$fecha1' AND fechasalida<='$fecha2' ORDER BY fechasalida DESC"  ;
					}}

					if (empty($fecha1) and empty($fecha2)){ 
						$query1="SELECT  * FROM salidas  
						LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
						LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
						WHERE salactivo='1' AND tiposalida='Salida Por baja de Elementos' ORDER BY fechasalida DESC"  ;
					}



					$t_salidas=mysql_query($query1,$conexion);

					while ($Fila_salidas=mysql_fetch_array($t_salidas)){

						?>
						<tr>
							<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>


							<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
							<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
							<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>

							<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
							<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>



						
							<td class="fila2"><center>
								<a href="../crear_pdf/crear_pdf_salida_consumo_baja.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
							</center></td>

						



							<td class="fila2"><center>
								<a href="detalles_salida_consumo.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
							</center></td>





							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
							<td class="fila2"><center>
								<a href="anular_salida_consumo1.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
							</center></td>

							<?php }?>



							<?php
						}

						?>


					</tbody>
				</TABLE>


			</div>
			<div>
				<center>
					<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
					<div class="col-md-12 text-center">

					</div>







								<?php
								include ("../assets/footer.php");
								?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
//DESCRIPCION:  SELECCIONA LA SALIDA GENERADA Y LA ENTIDAD PARA  CONTINUAR CON EL PROCESO DE SALIDA A ENTIDADES
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");

if (isset($_GET['entrada'])){

	if (isset($_GET['consec'])){

		$buscarconsec=$_GET['consec'];

	}
	else {

		$consultanum="SELECT * FROM consecutivoentrada ORDER BY idconsecentrada";
		$leerconsec=mysql_query($consultanum);
		$num_entrada=mysql_num_rows($leerconsec);
// este es el numero para grabar los datos en el formulario
//echo $num_entrada;   
		$consultaconsecutivo= "SELECT * FROM consecutivoentrada WHERE  idconsecentrada=$num_entrada";
		$asignarconsecutivo=mysql_query($consultaconsecutivo);
//   echo $num_entrada;

		if ($consecutivo=mysql_fetch_array($asignarconsecutivo)){
			$buscarconsec=$consecutivo['consecentrada'];
//      echo $consecutivo['consecentrada'];
			echo $buscarconsec;

		}
	}

}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<link rel="stylesheet" type="text/css" href="../css/paginacion.css"  >
	<link rel="shortcut icon" href="../imagenes/1.ico">
        <link rel="stylesheet" type="text/css" href="../css/loading.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="../js/jquery.mousewheel.js"></script>
	<script>
		 $(document).ready(function () {
                 
                    $('#div_loading').hide();
                    $("#horizontal").wrapInner("<table cellspacing='10'><tr>");
                    $(".post").wrap("<td></td>");
                    $("body").mousewheel(function(event, delta) {
                            this.scrollLeft -= (delta * 50);
                            event.preventDefault();
                    }); 
                    $('#guardar_e').click(function () {
                        var id_documento = $("#identidad").val();
                        if (!$("#identidad").attr("value").match(/^[0-9a-zA-Z]+$/)) {
                            alert("Ingrese un documento valido.");
                            return false;
                        }
                        $.ajax({
                            data: {"identidad" : id_documento},
                            type: "POST",
                            dataType: "json",
                            url: "salida_entidades_consumo_consecutivo.php",
                            beforeSend: function(xhr) {
                                $('#div_loading').show();
                            }
                        })
                        .done(function(response, textStatus, jqXHR ) {
                            
                                if(response.data.process=="true"){
                                    
                                    $("#internalid").val(response.data.internalid);
                                    $("#nitentidad").val(response.data.nitentidad);
                                    $("#numberconsec").val(response.data.numberconsec);
                                    $("#salidanum").val(response.data.salidanum);
                                    $("#anterior").val(response.data.anterior);
                                    
                                    setTimeout(function(){
                                    SendFormOutputEntity();
                                    }, 1000);
                                }
                                if(response.data.process=="false"){
                                    $('#div_loading').hide();
                                    alert(response.data.message);
                                }
                        })
                        .fail(function( jqXHR, textStatus, errorThrown ) {
                                    $('#div_loading').hide();
                                    alert(textStatus);

                        });
                    });
                    function SendFormOutputEntity(){
                        $('#div_loading').hide();
                        $("#salida_consumo_entidades_add").submit();
                    }
             });
	</script>


	<style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>

	<title>Salida de elementos</title>

	<body>
        <form id="salida_consumo_entidades_add" method="get" action="salida_consumo_entidades_add.php">
           <input type="hidden" id="internalid" name="internalid" value=""/>
           <input type="hidden" id="nitentidad" name="nitentidad" value=""/>
           <input type="hidden" id="numberconsec" name="numberconsec" value=""/>
           <input type="hidden" id="salidanum" name="salidanum" value=""/>
           <input type="hidden" id="anterior" name="anterior" value=""/>
       </form>
         <div id="div_loading">
             <img id="img_loading" src="../imagenes/ajax-loader.gif"/>
        </div>

		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="salidas_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>




				<div id="centro" >
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   


					<CENTER>

					</center></td>
				</tr>
			</table>
		</CENTER>




		<div id="generador">
			<table width="70%">

				<div id="generador" width="30%" align="">
					<center>
						<form name="CrearSalida" action="nuevo_num_salida.php"       title="Consecutivo de Entrada" method="post">
						</center>
					</tr>
				</form>
				<?php


//solicitud de confirmacion de consecutivo
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24

//    if (isset($_GET['salida'])){
//     if ($_GET['salida']==3){
//Activar generador de Consecutivos
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24

				?> 

			</div>
		</center>
	</table>
</div>
<?php
//solicitud de confirmacion de consecutivo
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//FORMULARIO PARA ASIGNAR NOMBRE DE USUARIO DE SALIDA Y GENERAR EL COSECUTIVO DE LA SALIDA EN EL MISMO MOVIMIENTO CON LA FUNCION.

?>
<CENTER>
	<table class="tabla_4">

		<TH class="fila1" COLSPAN="4"><H3>SALIDA DE ELEMENTOS DE CONSUMO A ENTIDADES</H3></TH>
		<TR><form name="productos" action="salida_entidades_consumo_consecutivo.php" title="Datos de la Salida" method="post">
			<th class="fila2" >Entidad destino</th>

			<td class="fila2"><select name="identidad" id="identidad">
				<option value="">--</option>
				<?php
				$entidad=mysql_query("SELECT identidad, entidad FROM entidades  ORDER BY entidad");
				while($filenti=mysql_fetch_array($entidad))
				{
					?>
					<option value="<?php echo $filenti['identidad'];?>"><?php echo $filenti['entidad'];?></option>
					<?php
				}
				?>
			</select></td>
                        </td>
	</TR>
	<TR>
            <td  class="fila2"><p align="center"><input type="button" class="botonguardar" name="guardar_e" id="guardar_e" title="Guardar producto" value="Generar "/>
		</p></td>
		<td class="fila2"><p align="center">
			<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="cancelar  "/></a>  
			<tr>
			</td>
		</tr>
	</form>
</TR>
</table>
</CENTER>
<?php      
//   }
//}

?>



</div>
</body>
</html>

<?php 

include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>
	





<?php
//DESCRIPCION: VENTANA PARA VISUALIZAR LAS SALIDAS DE ELEMENTOS DE CONSUMO ANULADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
if(isset($_REQUEST['all1'])){$all1=1;}

$tipo1="Salida de Elementos de consumo";
$tipo2="Salida de Elementos de consumo a otra Entidad";

$tipo3="Salida Por baja de Elementos";
$tipo4="";

//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	
		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>SALIDAS GENERADAS</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />





</head>

<body>

	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="salidas_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>





			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>
				<center>
					<table width="100%" border="0">
						<tr>
							<td  class="titulo">
								<center><STRONG> DETALLES DE LAS SALIDAS ANULADAS</STRONG></center>
							</td>

						</tr>
					</table>
				</center>


				<form name="salidas" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
					<CENTER>
						<TABLE>
							<tr>
								Ordenar por fecha
							</tr>
							<tr>
								<td class="fila5a">DESDE</td>
								<td class="fila5a"><input size="10" id="f_date1" name="f_date1" value="" /><button id="f_btn1" class="botonmas">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>

	<td class="fila5a">HASTA</td>
	<td class="fila5a"><input size="10" id="f_date2" name="f_date2" value="" /><button class="botonmas" id="f_btn2">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date2",
		trigger    : "f_btn2",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script></td>

	<td><input type="submit" class="botonver" name="submit" value="Ver"><br></td>
</tr></form>
</TABLE></CENTER>


<center>
	<table width="100%" border="0">
		<tr>
			<td  class="subtitulo">
				<center><STRONG> Salidas a Usuarios</STRONG></center>
			</td>

		</tr>
	</table>
</center>

<table border="0" class="tabla_2" width="90%">

	<tr >
		<p></p>

		<td  class="fila1">ID</td>
		<td  class="fila1">SALIDA N</td>
		<td  class="fila1">FECHA DE SALIDA</td>
		<td  class="fila1">TIPO SALIDA</td>
		<td   class="fila1">DEPENDENCIA</td>
		<td class="fila1">DOCUMENTO ID</td>
		<td  class="fila1">NOMBRES </td>
		<td  class="fila1">APELLIDOS</td>
		<td  class="fila1">CANT ITEMS</td>
		<td  class="fila1">VALOR TOTAL</td>
		<td  class="fila1">ELABORO</td>
		<td  class="fila1">COMENTARIOS</td>
		<td class="fila1">OBSERVACIONES</td>
		<td class="fila1">DETALLES</td>
		<td class="fila1">PDF</td>

		<?php

		if(isset($_REQUEST['all1'])){
			$fecha1=date("Y-m-d");
			$fecha2=date("Y-m-d");	
			$query1="SELECT  * FROM salidas  
			LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
			LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
			WHERE salactivo='0' AND tiposalida='$tipo1'  ORDER BY  salida ASC"  ;
		}

		else {

			if(isset($_REQUEST['f_date1'])){
			$fecha1=$_REQUEST['f_date1'];
			$fecha2=$_REQUEST['f_date2'];
			$query1="SELECT  * FROM salidas  
			LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
			LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
			WHERE salactivo='0' AND tiposalida='$tipo1'  
			AND fechasalida>= '$fecha1' AND fechasalida<='$fecha2' ORDER BY  salida ASC"  ;
		}}

		if (empty($fecha1) and empty($fecha2)){ 
			$query1="SELECT  * FROM salidas  
			LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
			LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
			WHERE salactivo='0' AND tiposalida='$tipo1'  ORDER BY  salida ASC"  ;
		}
		$t_salidas=mysql_query($query1,$conexion);
		while ($Fila_salidas=mysql_fetch_array($t_salidas)){

			?>
			<tr>
				<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["tiposalida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["nombredependencia"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["nombres"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["apellidos"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
				<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
				<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
				<td class="fila2"><center>
					<a href="detalles_salida_anulada.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
				</center></td>

		
					<td class="fila2"><center>
					<a href="../crear_pdf/crear_pdf_salida_consumo_usuarios_anuladas.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
					</center></td>




				<?php
			}

			?>
		</tr>
	</table>

	<center>
		<table width="100%" border="0">
			<tr>
				<td  class="subtitulo">
					<center><STRONG> Salidas a Otras Entidades</STRONG></center>
				</td>

			</tr>
		</table>
	</center>

	<table border="0" class="tabla_2" width="90%">

		<tr >
			<p></p>

			<td  class="fila1">ID</td>
			<td  class="fila1">SALIDA N</td>
			<td  class="fila1">FECHA DE SALIDA</td>
			<td  class="fila1">TIPO SALIDA</td>
			<td class="fila1">NIT</td>
			<td  class="fila1">ENTIDAD </td>
			<td  class="fila1">DIRECCIÓN </td>
			<td  class="fila1">CORREO ELECTRÓNICO </td>
			<td  class="fila1">TELÉFONO</td>
			<td  class="fila1">CANT ITEMS</td>
			<td  class="fila1">VALOR TOTAL</td>
			<td  class="fila1">ELABORO</td>
			<td  class="fila1">COMENTARIOS</td>
			<td class="fila1">OBSERVACIONES</td>
			<td class="fila1">DETALLES</td>
			<td class="fila1">PDF</td>
			<?php

			if(isset($_REQUEST['all1'])){
				$fecha1=date("Y-m-d");
				$fecha2=date("Y-m-d");
				$query1="SELECT  * FROM salidas  
				LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
				WHERE salactivo='0' AND tiposalida='$tipo2'  ORDER BY  salida ASC"  ;
			}


			else {

				if(isset($_REQUEST['f_date1'])){
				$fecha1=$_REQUEST['f_date1'];
				$fecha2=$_REQUEST['f_date2'];
				$query1="SELECT  * FROM salidas  
				LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
				WHERE salactivo='0' AND tiposalida='$tipo2'  
				AND fechasalida>= '$fecha1' AND fechasalida<='$fecha2' ORDER BY  salida ASC"  ;
}			}


			if (empty($fecha1) and empty($fecha2)){ 
				$query1="SELECT  * FROM salidas  
				LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
				WHERE salactivo='0' AND tiposalida='$tipo2'  ORDER BY  salida ASC"  ;
			}


			$t_salidas=mysql_query($query1,$conexion);
			while ($Fila_salidas=mysql_fetch_array($t_salidas)){

				?>
				<tr>
					<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
					<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
					<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
					<td  class="fila2"><?php echo $Fila_salidas["tiposalida"];?></td>
					<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
					<td class="fila2"><?php echo $Fila_salidas["entidad"];?></td>
					<td class="fila2"><?php echo $Fila_salidas["direccion_entidad"];?></td>
					<td class="fila2"><?php echo $Fila_salidas["correo_entidad"];?></td>
					<td class="fila2"><?php echo $Fila_salidas["telefono_entidad"];?></td>
					<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
					<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
					<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>
					<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
					<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
					<td class="fila2"><center>
						<a href="detalles_salida_anulada_entidades.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
					</center></td>


				<td class="fila2"><center>
												<a href="../crear_pdf/crear_pdf_salida_consumo_entidades_anuladas.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
												</center></td>

					<?php
				}

				?>
			</tr>
		</table>








<table width="100%" border="0">
		<tr>
			<td  class="subtitulo">
				<center><STRONG> Salidas por Bajas</STRONG></center>
			</td>

		</tr>
	</table>
</center>

<table border="0" class="tabla_2" width="90%">

	<tr >
		<p></p>

		<td  class="fila1">ID</td>
		<td  class="fila1">SALIDA N</td>
		<td  class="fila1">FECHA DE SALIDA</td>
		<td  class="fila1">TIPO SALIDA</td>
		<td   class="fila1">DEPENDENCIA</td>
		<td class="fila1">DOCUMENTO ID</td>
		<td  class="fila1">NOMBRES </td>
		<td  class="fila1">APELLIDOS</td>
		<td  class="fila1">CANT ITEMS</td>
		<td  class="fila1">VALOR TOTAL</td>
		<td  class="fila1">ELABORO</td>
		<td  class="fila1">COMENTARIOS</td>
		<td class="fila1">OBSERVACIONES</td>
		<td class="fila1">DETALLES</td>
		<td class="fila1">PDF</td>

		<?php

		if(isset($_REQUEST['all1'])){
			$fecha1=date("Y-m-d");
			$fecha2=date("Y-m-d");	
			$query1="SELECT  * FROM salidas  
			LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
			LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
			WHERE salactivo='0' AND tiposalida='$tipo3'  ORDER BY  salida ASC"  ;
		}

		else {

			if(isset($_REQUEST['f_date1'])){
			$fecha1=$_REQUEST['f_date1'];
			$fecha2=$_REQUEST['f_date2'];
			$query1="SELECT  * FROM salidas  
			LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
			LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
			WHERE salactivo='0' AND tiposalida='$tipo3'  
			AND fechasalida>= '$fecha1' AND fechasalida<='$fecha2' ORDER BY  salida ASC"  ;
		}}

		if (empty($fecha1) and empty($fecha2)){ 
			$query1="SELECT  * FROM salidas  
			LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
			LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia
			WHERE salactivo='0' AND tiposalida='$tipo3'  ORDER BY  salida ASC"  ;
		}
		$t_salidas=mysql_query($query1,$conexion);
		while ($Fila_salidas=mysql_fetch_array($t_salidas)){

			?>
			<tr>
				<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["tiposalida"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["nombredependencia"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["nombres"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["apellidos"];?></td>
				<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
				<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
				<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
				<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
				<td class="fila2"><center>
					<a href="detalles_salida_anulada.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
				</center></td>

				<td class="fila2"><center>
				<a href="../crear_pdf/crear_pdf_salida_consumo_bajas_anuladas.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
				</center></td>				



				<?php
			}

			?>
		</tr>
	</table>



	</div>

	<?php
	include ("../assets/footer.php");
	?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
//DESCRIPCION: REPORTE CON EL HISTORIAL DE ELEMENTOS ASIGNADOS A UN USUARIO ESPECIFICO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Historial de Usuario</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />

</head>

<body>


	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="historial_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>




			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> HISTORIAL DE USUARIO</STRONG></center>
						</td>


					</tr>
				</table>


<?php 
if(isset($_REQUEST['docid'])){ 
$docid=$_REQUEST['docid'];
$query0="SELECT *	FROM usuarios WHERE documentoid='$docid'";					
$queryus=mysql_query($query0,$conexion);
$sinuser=mysql_num_rows($queryus);
}


	?>




<center>
<table width="50%" border="0">
<div id="centro3">
<form name="historialproducto" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<CENTER>


<tr>
<td > Numero de  Documento del usuario
<td class="fila5a"><input size="20"  name="docid" value="" required /><button id="buscar" class="botonmas" >Ir</button></td>
</tr>
</form>
</TABLE>
<?php 
if(isset($_REQUEST['docid'])){ 

$docid=$_REQUEST['docid'];
						$query0="SELECT *			FROM usuarios 
			LEFT JOIN dependencias on usuarios.iddependencia=dependencias.codigodependencia
			LEFT JOIN permisos on usuarios.idpermiso=permisos.idpermiso
			LEFT JOIN calidadempleador on usuarios.calidadempleado=calidadempleador.idcalidadempleador
			LEFT JOIN sexo on usuarios.idsexo=sexo.idsexo WHERE documentoid='$docid'
			";					
						$queryus=mysql_query($query0,$conexion);


	?>

	<?php 
if(isset($sinuser)){
	if($sinuser>0){

	?>

				<table border="0" class="tabla_2" >

					<tr >
						<p></p>
						<td class="fila1" colspan="10">INFORMACION DEL USUARIO</td>
						<tr>
						<td  class="fila1">ID</td>
						<td  class="fila1">NOMBRES</td>
						<td  class="fila1">APELLIDOS</td>
						<td  class="fila1">DEPENDENCIA </td>
						<td   class="fila1">EMAIL</td>
						<td class="fila1">TELEFONO</td>
						<td  class="fila1">SEXO</td>
						<td  class="fila1">CARGO</td>
						<td  class="fila1">PROFESION</td>
						<td  class="fila1">CALIDAD DE  EMPLEADO</td>
						<?php
	

						while ($f_us=mysql_fetch_array($queryus)){

								?>
							<tr>
								<td class="fila2"><?php echo $f_us["idusuario"];?></td>
								<td  class="fila2"><?php echo $f_us["nombres"];?></td>
								<td  class="fila2"><?php echo $f_us["apellidos"];?></td>
								<td  class="fila2"><?php echo $f_us["codigodependencia"]." ". $f_us["nombredependencia"];?></td>
								<td  class="fila2"><?php echo $f_us["email"];?></td>
								<td class="fila2"><?php echo $f_us["telefono"];?></td>
								<td  class="fila2"><?php echo $f_us["sexo"];?></td>
								<td  class="fila2"><?php echo $f_us["cargo"];?></td>
								<td  class="fila2"><?php echo $f_us["idprofesion"];?></td>
								<td  class="fila2"><?php echo $f_us["calidadempleador"];?></td>
								<?php }?>

							</tr>
						</table>

<DIV ID="CENTRO">


	<?php 
if(isset($sinuser)){
	if($sinuser!=0){

	?>


				<table border="0" class="tabla_2"width="95%" >

					<tr >
					
						<td class="fila1" colspan="17"><H3>ELEMENTOS ACTUALMENTE ASIGNADOS AL USUARIO</H3></td>
						<tr>
<th class="fila1"  >ID</th>
<th class="fila1"  >ELEMENTO</th>
							<th class="fila1"  >UNID MED</th>
							<th  class="fila1"  >CONDICION</th> 
							<th class="fila1"  >VALOR</th>
							<th class="fila1"  >CATEGORIA</th>
							<th class="fila1"  >FECHA ENTRADA</th>
							<th class="fila1"  >ENTRAD N°</td>
							<th class="fila1"  >FECHA SALIDA</th>
							<th  class="fila1"  >SALIDA N°</td>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th class="fila1"  >SERIE</th>
							<th class="fila1"  >CODEBAR</th>
							<th class="fila1"  >PROVEEDOR</th>
							<th class="fila1"  >CONTRATO</th>
							<th class="fila1"  >FACTURA</th>
<tr>
<?php


if(isset($_REQUEST['docid'])){
	$docid=$_REQUEST['docid'];
$selprod="SELECT *  FROM productos 
									LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
									LEFT  JOIN condicion on productos.idcondicion=condicion.idcondicion
									LEFT  JOIN proveedores on productos.idproveedor=proveedores.idproveedor
									LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
									LEFT  JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
									WHERE activo='1' AND documentoid='$docid' AND idubicacion=2";
$querypr=mysql_query($selprod,$conexion);
while ($f_pr=mysql_fetch_array($querypr))								{
										?>
<td class="fila2"><?php echo $f_pr["idelemento"];?></td>
<td class="fila2" width="35%"><?php echo $f_pr["elemento"];?></td>
<td class="fila2"><?php echo $f_pr["unidadmedida"];?></td>
<td class="fila2"><?php echo $f_pr["condicion"];?></td>
<td class="fila2">$<?php echo number_format($f_pr["precioadqui"],2,',','.');?></td>
<td class="fila2"><?php echo $f_pr["codigocontable"] ." ".$f_pr["codigodescripcion"];?></td>
<td class="fila2"><?php echo $f_pr["fechaing"];?></td>
<td class="fila2"><?php echo $f_pr["numentrada"];?></td>
<td class="fila2"><?php  $fechasalida=$f_pr["fechaasig"]; echo $f_pr["fechaasig"]; if ($fechasalida=='0000-00-00'){	echo "";} else{	echo $fechasalida;}?></td>
<td class="fila2"><?php echo $f_pr["numsalida"];?></td>
<td class="fila2"><?php echo $f_pr["marca"];?></td>
<td class="fila2"><?php echo $f_pr["modelo"];?></td>
<td class="fila2"><?php echo $f_pr["serie"];?></td>
<td class="fila3" align="center"><?php echo $f_pr["codebar"];?></td>

<td class="fila2"><?php echo $f_pr["proveedor"];?></td>
<td class="fila2"><?php echo $f_pr["prcontrato"];?></td>
<td class="fila2"><?php echo $f_pr["numfactura"];?></td>
<tr>

	


</div>



<?php } }}?>
<?php } }?>
</tABLE>			

<BR>
<BR>

<?php 

if(isset($_REQUEST['docid'])){  ?>


	<?php 
if(isset($sinuser)){
	if($sinuser!=0){
	?>
<table border="0" class="tabla_2"  width="95%">
<td class="fila1" colspan="17"><H3>ELEMENTOS ASIGNADOS AL USUARIO MEDIANTE TRASLADO</H3></td>
						<tr>
							<th class="fila1"  >ID</th>
							<th class="fila1"  >ELEMENTO</th>
							<th class="fila1"  >UNID MED</th>
							<th class="fila1"  >CONDICION</th> 
							<th class="fila1"  >CATEGORIA</th>
							<th class="fila1"  >FECHA TRASLADO</th>
							<th class="fila1"  >TRASLADO N°</td>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th class="fila1"  >SERIE</th>
							<th class="fila1"  >CODEBAR</th>
							<th class="fila1"  >VALOR</th>
<tr>
<?PHP
if(isset($_REQUEST['docid'])){
	$docid=$_REQUEST['docid'];
$sqlqueryd="SELECT * FROM tabla_aux_traslados  
					LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
					LEFT JOIN codigocontable on tabla_aux_traslados.codigocontable_tr_aux=codigocontable.codigocontable
					LEFT JOIN usuarios ON tabla_aux_traslados.docid_entrega_aux= usuarios.documentoid
					LEFT JOIN condicion on tabla_aux_traslados.condicion_aux=condicion.idcondicion
					left join unidadmedida on tabla_aux_traslados.id_unidadmedida_aux=unidadmedida.idunidadmedida
					WHERE docid_recibe_aux='$docid'

					ORDER BY idelemento_tr_aux";

$rt_data=mysql_query($sqlqueryd, $conexion);

while ($d_tra=mysql_fetch_array($rt_data)){
						?>



<td class="fila2"><?php echo $d_tra["idelemento_tr_aux"];?></td>
<td class="fila2"><?php echo $d_tra["elemento"];?></td>
<td class="fila2"><?php echo $d_tra["unidadmedida"];?></td>
<td class="fila2"><?php echo $d_tra["condicion"];?></td>
<td class="fila2"><?php echo $d_tra["codigocontable_tr_aux"]." ".$d_tra["codigodescripcion"];?></td>
<td class="fila2"><?php echo $d_tra["fecha_traslado_aux"];?></td>
<td class="fila2"><?php echo $d_tra["trasladonum_aux"];?></td>
	<td class="fila2"><?php echo $d_tra["marca"];?></td>
<td class="fila2"><?php echo $d_tra["modelo"];?></td>
<td class="fila2"><?php echo $d_tra["serie"];?></td>
<td class="fila3" align="center"><strong><?php echo $d_tra["codebar"];?></strong></td>
<td class="fila2" align="right">$<?php echo number_format($d_tra["precioadqui_tr_aux"],2,',','.');?></td>
<td class="fila2"><?php echo $d_tra["observaciones_tr_aux"];?></td>


<tr>



<?php }} }?>

</table>

<?php } }?>

<BR>
<BR>


<?php 
if(isset($_REQUEST['docid'])){ ?>


	<?php 
if(isset($sinuser)){
	if($sinuser!=0){
	?>
<table border="0" class="tabla_2"  width="95%">
<td class="fila1" colspan="17"><H3>ELEMENTOS ENTREGADOS POR EL USUARIO MEDIANTE TRASLADO</H3></td>
						<tr>
							<th class="fila1"  >ID</th>
							<th class="fila1"  >ELEMENTO</th>
							<th class="fila1"  >UNID MED</th>
							<th class="fila1"  >CONDICION</th> 
							<th class="fila1"  >CATEGORIA</th>
							<th class="fila1"  >FECHA TRASLADO</th>
							<th class="fila1"  >TRASLADO N°</td>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th class="fila1"  >SERIE</th>
							<th class="fila1"  >CODEBAR</th>
							<th class="fila1"  >VALOR</th>
<tr>
<?PHP
if(isset($_REQUEST['docid'])){
	$docid=$_REQUEST['docid'];
$sqlqueryd="SELECT * FROM tabla_aux_traslados  
					LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
					LEFT JOIN codigocontable on tabla_aux_traslados.codigocontable_tr_aux=codigocontable.codigocontable
					LEFT JOIN usuarios ON tabla_aux_traslados.docid_entrega_aux= usuarios.documentoid
					LEFT JOIN condicion on tabla_aux_traslados.condicion_aux=condicion.idcondicion
					left join unidadmedida on tabla_aux_traslados.id_unidadmedida_aux=unidadmedida.idunidadmedida
					WHERE docid_entrega_aux='$docid'

					ORDER BY idelemento_tr_aux";

$rt_data2=mysql_query($sqlqueryd, $conexion);

while ($d_tra=mysql_fetch_array($rt_data2)){
						?>



<td class="fila2"><?php echo $d_tra["idelemento_tr_aux"];?></td>
<td class="fila2"><?php echo $d_tra["elemento"];?></td>
<td class="fila2"><?php echo $d_tra["unidadmedida"];?></td>
<td class="fila2"><?php echo $d_tra["condicion"];?></td>
<td class="fila2"><?php echo $d_tra["codigocontable_tr_aux"]." ".$d_tra["codigodescripcion"];?></td>
<td class="fila2"><?php echo $d_tra["fecha_traslado_aux"];?></td>
<td class="fila2"><?php echo $d_tra["trasladonum_aux"];?></td>
	<td class="fila2"><?php echo $d_tra["marca"];?></td>
<td class="fila2"><?php echo $d_tra["modelo"];?></td>
<td class="fila2"><?php echo $d_tra["serie"];?></td>
<td class="fila3" align="center"><strong><?php echo $d_tra["codebar"];?></strong></td>
<td class="fila2" align="right">$<?php echo number_format($d_tra["precioadqui_tr_aux"],2,',','.');?></td>
<td class="fila2"><?php echo $d_tra["observaciones_tr_aux"];?></td>


<tr>



<?php }}}?>

</table>


<?php } }?>

<BR>
<BR>

<?php 
if(isset($_REQUEST['docid'])){ ?>

	<?php 
if(isset($sinuser)){
	if($sinuser!=0){
	?>

<table border="0" class="tabla_2"  width="95%">
<td class="fila1" colspan="17"><H3>ELEMENTOS ENTREGADOS POR EL USUARIO  A BODEGA MEDIANTE ENTRADA POR REINTEGRO</H3></td>
						<tr>
							<th class="fila1"  >ID</th>
							<th class="fila1"  >ELEMENTO</th>
							<th class="fila1"  >UNID MED</th>
							<th class="fila1"  >CONDICION</th> 
							<th class="fila1"  >CATEGORIA</th>
							<th class="fila1"  >FECHA ENTRADA</th>
							<th class="fila1"  >ENTRADA N°</td>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th class="fila1"  >SERIE</th>
							<th class="fila1"  >CODEBAR</th>
							<th class="fila1"  >VALOR</th>
<tr>

<?PHP
if(isset($_REQUEST['docid'])){
	$docid=$_REQUEST['docid'];
$selreint="SELECT * FROM tabla_aux_reintegros 
LEFT JOIN  productos on tabla_aux_reintegros.idelemento_r_aux=productos.idelemento
LEFT JOIN condicion on tabla_aux_reintegros.condicion_aux=condicion.idcondicion

WHERE documentoid_aux='$docid'";
$queryreint=mysql_query($selreint,$conexion);
while($d_re=mysql_fetch_array($queryreint)){

?>

<td class="fila2"><?php echo $d_re["idelemento_r_aux"];?></td>
<td class="fila2"><?php echo $d_re["elemento"];?></td>
<td class="fila2"><?php 
$selund="SELECT unidadmedida FROM productos left join unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
where idelemento='$d_re[idelemento_r_aux]' ";$querund=mysql_query($selund,$conexion);
while ($unidm=mysql_fetch_array($querund)){$unidme=$unidm['unidadmedida'];}	echo $unidme;?></td>
<td class="fila2"><?php echo $d_re["condicion"];?></td>


<td class="fila2"><?php 
$selcat="SELECT * FROM productos left join codigocontable on productos.codigocontable=codigocontable.codigocontable
where idelemento='$d_re[idelemento_r_aux]' ";
$querund2=mysql_query($selcat,$conexion);
while ($codc=mysql_fetch_array($querund2)){
$codcont=$codc['codigocontable']; 
$coddesc=$codc['codigodescripcion'];
$marca=$codc['marca'];
$modelo=$codc['modelo'];
$serie=$codc['serie'];
$codebar=$codc['codebar'];
$precioadqui=$codc['precioadqui'];

}	echo $codcont. " ". $coddesc;?></td>
</td>

<td class="fila2"><?php echo $d_re["fecha_entrada_aux"];?></td>
<td class="fila2"><?php echo $d_re["entrada_r_aux"];?></td>
<td class="fila2"><?php echo $marca;?></td>
<td class="fila2"><?php echo $modelo;?></td>
<td class="fila2"><?php echo $serie;?></td>
<td class="fila3"><?php echo $codebar;?></td>
<td class="fila2" align="right">$<?php echo number_format($precioadqui,2,',','.');?></td>

<tr>





<?php } }}?>


<?php } }?>

</tABLE>
	</div>	</div>	</div>	</div>

	<?php } else {


	} }?>


	<?php 
if(isset($sinuser)){
	if($sinuser==0){
	?>

<div id="centro3"
	<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> No hay Datos de usuario </STRONG></center>
						</td>


					</tr>
				</table>
				</div>

		<?php  } } ?>

<?php
if(isset($sinuser)){
	if($sinuser!=0){
	?>
<div id="centro">
<div id="centro3">
<td>Opciones Adicionales:</td><a href="../crear_pdf/crear_pdf_historial_usuarios.php?documentoid=<?php echo $docid;?>">Descargar PDF<img src="../imagenes/pdf.png" title="Ver Detalles " width="18" height="18"/></a>
</div>
</div>
		<?php  } } ?>

	</BODY>
 
<?php
include ("../assets/footer.php");
?>

<?php  ?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>




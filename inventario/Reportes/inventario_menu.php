
<?php 
//DESCRIPCION: VENTANA PARA VISUALIZAR EL INVENTARIO TOTAL DE ELEMENTOS 
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");

?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript" src="../js/buscar.js"></script>
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../bootstrap/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" href="../css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="../js/jquery-ui.css" />
        <script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/FilterSearch.js"></script>
        <link href="../css/estilos.css" type="text/css" rel="stylesheet">
        

	
	
        <style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>

	<title>INVENTARIO</title>




	<script type="text/javascript">
		$(document).ready(function() {
			$(".search").keyup(function () {
				var searchTerm = $(".search").val();
				var listItem = $('.results tbody').children('tr');
				var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
				
				$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
					return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
				}
			});
				
				$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
					$(this).attr('visible','false');
				});

				$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
					$(this).attr('visible','true');
				});

				var jobCount = $('.results tbody tr[visible="true"]').length;
				$('.counter').text(jobCount + ' Elementos');

				if(jobCount == '0') {$('.no-result').show();}
				else {$('.no-result').hide();}
			});
		});

	</script>
        
        <script>
 $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
     $( function() {
       $("#DateStart" ).datepicker({
         showWeek: true,
         firstDay: 1,
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd',
         onClose: function (selectedDate) {
          $("#DateEnd").datepicker("option", "minDate", selectedDate);
         }
       });
       
       $("#DateEnd" ).datepicker({
         showWeek: true,
         firstDay: 1,
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd',
         onClose: function (selectedDate) {
        $("#DateStart").datepicker("option", "maxDate", selectedDate);
        }
       });
  } );
  </script>
</head>

<body>


	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
		</tr></table></div>






		<div id="centro">
			<div id="div_bienvenido">
				<?php echo "Bienvenido"; ?> <BR/>



				<div id="div_usuarios">
					<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
				</div>
				<?php echo "SALIR";?>
				<a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
			</div>




<?php

$selmax="SELECT * FROM responsable_bodega WHERE id_responsable=(SELECT MAX(id_responsable) FROM responsable_bodega)";
$queryselmax=mysql_query($selmax,$conexion); while($fmax=mysql_fetch_array($queryselmax))
{ $fechadesde=$fmax['fecha_desde']; $fechahasta=$fmax['fecha_hasta']; $nombre_resp=$fmax['nombres']; $apellidos_resp=$fmax['apellidos']; $cometnario=$fmax['resp_comentario'];} ?>

<table  >
<tr>
<td class="bodega">Responsable de Bodega: </td>
<td>
    <?php echo $nombre_resp . " " . $apellidos_resp?>
</td>
</tr>
</table>
<center>
				<table width="65%" border="0">
			<tr>
			<td colspan="11" class="titulo">
                        <center>
			INVENTARIO
			</center></td>
	
				</table>

				<?php
				?>

	

			</center>
	<?php

	?>
                    
    <!-- Start Modal -->
  <div class="modal fade" id="ModalUser" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="FormFilterForm">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4>Buscar</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="#" method="post" id="FilterForm">
            <div class="form-group">
              <label>Placa</label>
              <input type="text" class="form-control" id="placa" name="placa" placeholder="Placa.">
              <label>Serie</label>
              <input type="text" class="form-control" id="serie" name="serie" placeholder="Serie.">
               <label>Fecha de inicio</label>
               <input type="text" class="form-control" id="DateStart" name="DateStart">
               <label>Fecha de fin</label>
               <input type="text" class="form-control" id="DateEnd" name="DateEnd">
            </div>
            <div class="alert alert-warning" role="alert" id="Message_AlertSearch"></div>
          </form>
        </div>
         <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            <button type="button" id="ButtonSearchFilter" class="btn btn-primary">Buscar</button>
          </div>
      </div>
      <!-- End modal content-->
    </div>
  </div>
  <!-- End Modal content-->                
  <div style="width: 200px;padding: 25px 0;margin: 0; ">
      <button type="button" id="button_ModalSearch" class="btn btn-sm btn-primary btn-create"><em class="glyphicon glyphicon-search"></em>  Buscar</button>
      <a href="registro_excel_inventario.php"><input type="image" src="../imagenes/excel.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar"></a>
  </div>                   

	<div id="centro">
		<!--<div class="form-group pull-left">
			<h4>Escriba su busqueda aqui</h4>
			<input type="text" class="search form-control" placeholder="Consulta por cedula, id, serial, placa, entrada, salida etc..." size="80">
		</div>-->
                
		<br>
		<span class="counter pull-left"></span>
		<div class="container-full">
		<br>
			<div class="table-responsive " >
					
				<table   class=" table table-bordered results"  cellspacing="0" width="100%" >
					<thead>
							
							<?PHP 
							//<th class="fila1"  >ID</th> ?>
							<th class="fila1"  >ELEMENTO</th>
							<th class="fila1"  >UNID MED</th>
							<th  class="fila1"  >CONDICION</th> 
							<th class="fila1"  >VALOR</th>
							<th class="fila1"  >CATEGORIA</th>
							<th class="fila1"  >FECHA ENTRADA</th>
							<th class="fila1"  >ENTRAD N°</td>
							<th class="fila1"  >FECHA SALIDA</th>
							<th  class="fila1"  >SALID N°</td>
							<th class="fila1"  >FECHA TRASL</th>
							<th class="fila1"  >TRASL N°</th>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th class="fila1"  >SERIE</th>
							<th class="fila1"  >PLACA</th>
							<th  class="fila1"  >DEPENDENCIA</hd>      
							<th class="fila1"  >DOC. ID</th>
							<th class="fila1"  >FUNCIONARIO</th>
							<th class="fila1"  >UBICACION</th>
							<th class="fila1"  >PROVEEDOR</th>
							<th class="fila1"  >CONTRATO</th>
							<th class="fila1"  >FACTURA</th>
							<th class="fila1" >OBSERVACIONES</th>
										
									</th>
								</thead>
								<tbody id="myTable2">


									<?php


//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
//Rutina: Si los campos estan Vacios Muestra todos los Activos



									$sqlquery="SELECT *  FROM productos 

									LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
									LEFT  JOIN condicion on productos.idcondicion=condicion.idcondicion
									LEFT  JOIN proveedores on productos.idproveedor=proveedores.idproveedor
									LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
									LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
									LEFT  JOIN CODIGOCONTABLE on productos.codigocontable=codigocontable.codigocontable
									LEFT JOIN  dependencias on productos.dependencia=dependencias.codigodependencia
									WHERE activo='1'
									";

//ECHO $sqlquery;
									$t_activos=mysql_query($sqlquery, $conexion);
									$total=mysql_num_rows($t_activos);

									while ($fila_activos=mysql_fetch_array($t_activos))
									{
										?>

										<tr>

												
<td class="fila2" width="35%"><?php echo $fila_activos["elemento"];?></td>
<td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
<td class="fila2"><?php echo $fila_activos["condicion"];?></td>
<td class="fila2">$<?php echo number_format($fila_activos["precioadqui"],2,',','.');?></td>
<td class="fila2"><?php echo $fila_activos["codigocontable"] ." ".$fila_activos["codigodescripcion"];?></td>
<td class="fila2"><?php echo $fila_activos["fechaing"];?></td>
<td class="fila2"><?php echo $fila_activos["numentrada"];?></td>
<td class="fila2"><?php if ($fila_activos["fechaasig"]=='0000-00-00'){	echo "";} else{	echo $fila_activos["fechaasig"];}?></td>
<td class="fila2"><?php echo $fila_activos["numsalida"];?></td>
<td class="fila2"><?php $fechatrasl=$fila_activos["fechaultimanov"]; if($fechatrasl=='0000-00-00'){echo "";}else {echo $fechatrasl;}?></td>
<td class="fila2"><?php echo $fila_activos["numtraspaso"];?></td>
<td class="fila2"><?php echo $fila_activos["marca"];?></td>
<td class="fila2"><?php echo $fila_activos["modelo"];?></td>
<td class="fila2"><?php echo $fila_activos["serie"];?></td>
<td class="fila3" align="center"><?php echo $fila_activos["codebar"];?></td>
<td class="fila2"><?php echo  $fila_activos["codigodependencia"] ." ".$fila_activos["nombredependencia"];?></td>
<td class="fila2"><?php echo $fila_activos["documentoid"];?></td>
<td class="fila2"><?php echo $fila_activos["nombres"] ." ".$fila_activos["apellidos"] ;?></td>
<td class="fila2"><?php echo $fila_activos["nombreubicacion"];?></td>
<td class="fila2"><?php echo $fila_activos["proveedor"];?></td>
<td class="fila2"><?php echo $fila_activos["prcontrato"];?></td>
<td class="fila2"><?php echo $fila_activos["numfactura"];?></td>
<td class="fila2"><?php echo $fila_activos["probservaciones"];?></td>

											<?php  }

											?>  
										</tr>

									</tbody>
									<tr class="warning no-result">
										<td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
									</tr>
								</TABLE>
							</div>
							<center>
								<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
								<div class="col-md-12 text-center">

								</div>

							</div>
						</div>

					</center>
				</table>

			</div>
		</body>

		<?php
		include("../assets/footer.php");
		?>

		<?php
					} else {
						header("location: ../403.php");
					}
					?>


<?php
//DESCRIPCION: MENU DE REPORTES 
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");

include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>BOLETIN</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />



</head>

<body>
	<div id="centro2">
		<table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
						
		</table>
	</div>

	<div id="centro">
		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
			<?php echo "SALIR";?>
			'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
		</div>   


		<center>

			<table  border="0">
				<tr>
					<td colspan="11" class="titulo"><center>
						<STRONG> REPORTES MENU</STRONG>
					</center></td>
				</tr>
			</table>
			<table class="menu2" > 
					<tr class="menuactivos">
						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
						<?php 
						/*
						<TD><a  class="titulos_menu"href="boletin_menu.php"><img src="../imagenes/boletin.png" title="Activos"width="64" height="64" align="LEFT" />Boletín</TD>
						*/
						?>
						<?php }?>
						<TD><a class="titulos_menu" href="historial_menu.php"><img src="../imagenes/boletin2.png" title="Eliminar"width="64" height="64" align="left" />Historial</a></TD>
					</tr>
								</tr>
							</table>
						</table>
					</div>
				</body>
				</html>
				<?php 
				include ("../assets/footer.php");
				?>
    <?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

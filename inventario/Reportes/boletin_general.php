<?php
//DESCRIPCION: BOLETIN GENERAL **SIN TERMINAR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>

<?php
if (isset($_SESSION['idpermiso'])) {
$fecha_anterior1=$_REQUEST['f_date1'];
$fecha_anterior2=$_REQUEST['f_date2'];

$fecha_boletin1=$_REQUEST['f_date3'];
$fecha_boletin2=$_REQUEST['f_date4'];


/*
echo "<br>";
echo $fecha_anterior1;
echo "<br>";
echo $fecha_anterior2;
echo "<br>";
echo $fecha_boletin1;
echo "<br>";
echo $fecha_boletin2;
echo "<br>";

*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	
		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>BOLETIN</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<?php
	if (isset($_GET['creado'])){ if ($_GET['creado']==5){?>
	<div class="quitarok">
		<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado el elemento correctamente!
	</div>
	<?php   }    } ?>
<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
<td>  <a href="boletin_general_fecha.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			</tr></table></div>
			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   
				<center>
					<table width="70%" border="0">
						<tr>
							<td class="titulo"><center>
								BOLETIN</center></td>
							</tr>
						</table>
						<?php
						if (isset($_GET['add'])){ if ($_GET['add']==1){?>
						<div class="quitarok">
							<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Elemento creado correctamente!
						</div>
						<?php   }    } ?> 
					</center></td>
				</tr>
				<tr>
					<td class="texto">
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1){
		?>
		<td>
			<?php
		}
		?>

		<center>
			<table border="0" class="tabla_2" style="width:90%">
				
				<tr>
				<td class="fila1" colspan="7"><h4>Fecha de Boletin: <?php 
									
					$fecha_boletin2=$_REQUEST['f_date2'];
					echo "Del ". $fecha_boletin1 . " Hasta " . $fecha_boletin2;
				
				?>	
</h4></td>

<tr>
				<td class="fila1" colspan="7"><h5>Fecha saldo anterior: <?php 
									
					$fecha_boletin2=$_REQUEST['f_date2'];
					echo "Del ". $fecha_anterior1 . " Hasta " . $fecha_anterior2;
				
				?>	
</h5></td>




				</tr>
				<tr>
					<td class="fila1">ID</td>
					<td  class="fila1">CODIGO CATEGORIA</td>
					<td  class="fila1">SALDO INICIAL</td>
					<td  class="fila1">ENTRADAS</td>
					<td class="fila1">SALIDAS</td>
					<td  class="fila1">BAJAS</td>
					<td  class="fila1">SALDO FINAL</td>
				</tr>
		<?php
//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
					
$t_consumo2="SELECT * FROM codigocontable";
$t_consumo=mysql_query($t_consumo2, $conexion);
while ($fila_consumo=mysql_fetch_array($t_consumo))
{		
$codigodelelemento=$fila_consumo["codigocontable"];
?>
<tr>
<td class="fila2"><?php echo $fila_consumo["idcodigocontable"];?></td>
<td class="fila2"><?php echo $fila_consumo["codigocontable"]. " - " .$fila_consumo["codigodescripcion"];  ?></td>
<td class="fila2" align="right">							<?php
$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_entradas WHERE codigocontable_aux='$codigodelelemento' AND fecha_entrada_aux>='$fecha_anterior1' AND  fecha_entrada_aux<='$fecha_anterior2' ");   
$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
$valortotalentradas=$valortotal1["total"];
echo number_format($valortotalentradas,2,',','.');							
?>
        
</td>
<td class="fila2"align="right">$

<?php
$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_entradas WHERE codigocontable_aux='$codigodelelemento' AND fecha_entrada_aux>='$fecha_boletin1' AND  fecha_entrada_aux<='$fecha_boletin2' ");   
$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
$valortotalentradas=$valortotal1["total"];
echo number_format($valortotalentradas,,2,',','.');							
?>
</td> 
<td class="fila2" align="right"> $         
<?php
$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_salidas WHERE codigocontable_aux='$codigodelelemento'");   
$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
$valortotalsalidas=$valortotal1["total"];
echo number_format($valortotalsalidas,2,',','.'));							
?>
</td>
<td class="fila2"align="right"></td>
<td class="fila2"align="right"> $ <?php 	echo  number_format(($valortotalentradas-$valortotalsalidas),2,',','.');	?> 
</td> 
<?php } 	?> 
</tr>
<?php
$querysuma = mysql_query("SELECT SUM(precioadqui*cantidad) as total FROM consumo WHERE consactivo='1'");   
$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
$valortotal=$valortotal1["total"];
?>
<TR>
<TD class="fila2"colspan="5"> &nbsp;</TD>
<TD class="fila2"colspan="1" align="right"><STRONG>VALOR TOTAL</STRONG></TD>
<td  class="fila2"align="right"><strong>$<?php echo number_format($valortotal,2,',','.'));?></strong></td>
<TD class="fila2"colspan="3"> &nbsp;</TD>
<tr>
<td colspan="7" class="fila2"><center>
</center> </td>	
</tr>
</table>
</center>

</div>
	
	<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
	include ("../assets/footer.php");

	?>
</body>
</html>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

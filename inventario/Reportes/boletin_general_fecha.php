<?php
//DESCRIPCION: BOLETIN GENERAL POR FECHA *SIN TERMINAR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="../js/dataTables.min.js"></script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/tablas.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<script src="../js/calendario/src/js/jscal2.js"></script>
<script src="../js/calendario/src/js/lang/en.js"></script>
<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
<link href="../css/styles.css" type="text/css" rel="stylesheet">
<link href="../css/estilos.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
<link rel="shortcut icon" href="../imagenes/1.ico">
<style>	
body {
background: #eaeaea no-repeat center top;
-webkit-background-size: cover;
-moz-background-size: cover;
background-size: cover;
}
.container > header h1,
.container > header h2 {
color: #fff;
text-shadow: 0 1px 1px rgba(0,0,0,0.7);
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BOLETIN GENERAL</title>
<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>


<div id="centro2"><table class="botonesfila" >
<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
<td>  <a href="BOLETIN_MENU.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td>
</tr></table></div>
<div id="centro">
<div id="div_bienvenido">
<?php echo "Bienvenido"; ?> <BR/>
<div id="div_usuarios">
<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
</div>
<?php echo "SALIR";?>
'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
</div>   
<center>
<table width="70%" border="0">
<tr>
<td class="titulo"><center>
BOLETIN GENERAL</center></td>
</tr>
</table>
<?php
if (isset($_GET['add'])){ if ($_GET['add']==1){?>
<div class="quitarok">
<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Elemento creado correctamente!
</div>
<?php   }    } ?> 
</center></td>
</tr>
<tr>
<td class="texto">
<!--Andres Montealegre Giraldo
IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
<?php
if ($_SESSION['idpermiso']==1){
?>
<td>
<?php
}
?>
<form name="entradasc" method="post" action="boletin_general.php">
<CENTER>
<TABLE class="menu3">
<tr >


<tr >
<td class="fila5a" colspan="2"><h4> <style > h4{ color:#4a5caf; }</style>Seleccione la Fecha de Boletin Anterior</td></h4><tr>
<tr>
<td class="fila1"  colspan="1">Desde </td><td class="fila1"  colspan="1">Hasta </td>
<tr>
<td class="fila5a"><input size="10" id="f_date1" name="f_date1" value="" required /><button id="f_btn1" class="botonmas">+</button></td>
<script type="text/javascript">//<![CDATA[
Calendar.setup({
inputField : "f_date1",
trigger    : "f_btn1",
onSelect   : function() { this.hide() },
showTime   : 12,
dateFormat : "%Y-%m-%d"
});
//]]></script></td>
<td class="fila5a"><input size="10" id="f_date2" name="f_date2" value="" required /><button id="f_btn2" class="botonmas">+</button></td>
<script type="text/javascript">//<![CDATA[
Calendar.setup({
inputField : "f_date2",
trigger    : "f_btn2",
onSelect   : function() { this.hide() },
showTime   : 12,
dateFormat : "%Y-%m-%d"
});
//]]></script></td>

<tr >
<td class="fila5a" colspan="2" > <h4> <style > h4{ color:#4a5caf; }</style>Seleccione la Fecha de Boletin</td></h4><tr>

<td class="fila1"  colspan="1">Desde </td><td class="fila1"  colspan="1">Hasta </td>
<tr>
<td class="fila5a"><input size="10" id="f_date3" name="f_date3" value="" required/><button class="botonmas" id="f_btn3">+</button></td>
<script type="text/javascript">//<![CDATA[
Calendar.setup({
inputField : "f_date3",
trigger    : "f_btn3",
onSelect   : function() { this.hide() },
showTime   : 12,
dateFormat : "%Y-%m-%d"
});
//]]></script></td>

<td ><input size="10" id="f_date4" name="f_date4" value="" required/><button class="botonmas" id="f_btn4">+</button></td>
<script type="text/javascript">//<![CDATA[
Calendar.setup({
inputField : "f_date4",
trigger    : "f_btn4",
onSelect   : function() { this.hide() },
showTime   : 12,
dateFormat : "%Y-%m-%d"
});
//]]></script></td>

<tr >
<td  colspan="2"><input type="submit" class="botonver" name="submit" value="Consultar"><br></td>
</tr>
</TABLE></CENTER>

</center>

</div>
<?php  ?>
<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
include ("../assets/footer.php");
?>
</body>
</html>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

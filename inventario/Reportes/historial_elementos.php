<?php
//DESCRIPCION: VENTANA PARA VISUALIZAR HISTORIAL DE ELEMENTOS DESDE SU ENTRADA HASTA LOS USUARIOS QUE HA TENIDO ASIGNADOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
<link href="../css/styles.css" type="text/css" rel="stylesheet">
<link href="../css/estilos.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="../imagenes/1.ico">
<style>	

body {
background: #ddd no-repeat center top;
-webkit-background-size: cover;
-moz-background-size: cover;
background-size: cover;
}
.container > header h1,
.container > header h2 {
color: #fff;
text-shadow: 0 1px 1px rgba(0,0,0,0.7);
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Historial de productos</title>
<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="centro2"><table class="botonesfila" >
<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
<td><a href="historial_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>

<div id="centro">
<div id="div_bienvenido">
<?php echo "Bienvenido"; ?> <BR/>
<div id="div_usuarios">
<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
<br>
<?php echo "SALIR";?>
'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
</div></div>
<table width="100%" border="0">

<DIV ID="CENTRO3	">
					<tr>
						<td  class="titulo">
							<center><STRONG> HISTORIAL DE ELEMENTOS</STRONG></center>
						</td>


					</tr>
				</table>
</DIV>

<div id="centro3">
<table width="100%" border="0">

<form name="historialproducto" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<CENTER>


<tr>
<tr>
<TD>Seleccione el tipo de búsqueda </TD>
<td class="fila2"> <select onclick="check()" name="criterio1">
<option value="codebar">Numero de Placa</option>
<option value="serie">Numero de serie</option>
</select></td>
<td class="fila5a"><input size="20"  name="criterio2" value="" required /><button id="buscar" class="botonmas" >Ir</button></td>
</tr>
</form>
</TABLE>
</div>
</CENTER>
<div id="centro">


<?php 
if(isset($_REQUEST['criterio1'])){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];
?>
<tr>
<td  class="titulo">
<center></center>
</td>
<?php
$query2="SELECT  * FROM productos  
LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia
WHERE activo='1'  AND  $criterio1='$criterio2'";
$t_prod1=mysql_query($query2,$conexion);
$inicial=mysql_num_rows($t_prod1);
//echo $inicial;

?>


<?php 

if(isset($inicial)){
if($inicial!=0){
	?>
<table border="0" class="tabla_2" >
<td  class="fila1" colspan="12"><STRONG><h3> DETALLES DE ELEMENTO</STRONG><h3></td>
<TR>
<td  class="fila1"> ID</td>	
<td  class="fila1"> Elemento</td>		
<td  class="fila1"> Categoria</td>		
<td  class="fila1">Fecha Ingreso </td>		
<td  class="fila1"> Entrada</td>	
<td  class="fila1"> Placa</td>	
<td  class="fila1"> Marca</td>		
<td  class="fila1"> Modelo</td>		
<td  class="fila1"> Serie</td>		

    <td  class="fila1"> Ubicacion</td>	
    <td  class="fila1"> Documento ID</td>	
        <td  class="fila1"> Nombre Funcionario</td>	

</TR>			
<tr>	




<?php



while ($f_us=mysql_fetch_array($t_prod1)){

$elemento=$f_us['elemento'];
$entrada_elem=$f_us['numentrada_origen'];
$idelemento_general=$f_us['idelemento'];    
$codebar_general=$f_us['codebar'];

$serie_general=$f_us['serie'];
$nombre=$f_us['nombres'] ." ". $f_us['apellidos'];
$docid=$f_us['documentoid'];
$coddependencia=$f_us['codigodependencia'];
$nombredependencia=$f_us['nombredependencia'];

?>


<td  class="fila2"> <?php echo $f_us['idelemento'];?></td>
<td  class="fila2"> <?php echo $elemento;?></td>
<td  class="fila2"> <?php echo $f_us['codigocontable'] ." - ".$f_us['codigodescripcion'];?></td>
<td  class="fila2"> <?php echo $f_us['fecha_origen'];?></td>
    <td  class="fila2"> <?php echo $f_us['numentrada_origen'];?></td>   
    
<td  class="fila3"> <?php echo $f_us['codebar'];?></td>
<td  class="fila2"> <?php echo $f_us['marca'];?></td>
<td  class="fila2"> <?php echo $f_us['modelo'];?></td>
<td  class="fila3"> <?php echo $f_us['serie'];?></td>
    
    <td  class="fila3"> <?php echo $f_us['nombreubicacion'];?></td>
    <td  class="fila2"> <?php echo $f_us['documentoid'];?></td>
        <td  class="fila2"> <?php echo $f_us['nombres']." ".$f_us['apellidos'] ;?></td>
</TR>


</table>

    
<?PHP

if(isset($_REQUEST['criterio1'])){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];
$query2="SELECT  * FROM historial_productos  

LEFT JOIN codigocontable on historial_productos.codigocontable=codigocontable.codigocontable
LEFT JOIN dependencias on historial_productos.dependencia=dependencias.codigodependencia	
LEFT  JOIN usuarios on historial_productos.documentoid=usuarios.documentoid
LEFT  JOIN ubicacion on historial_productos.idubicacion=ubicacion.id_ubicacion
WHERE activo='1'  AND  $criterio1= '$criterio2' ORDER BY  numsalida DESC";
$t_prod=mysql_query($query2,$conexion);

?>

</tr>
</table>
<br>

<table border="0" class="tabla_2" WIDTH="700PX">

<td  class="fila1" colspan="7"><h3>HISTORIAL DE SALIDAS</h3></td>
<tr >

<td  class="fila1">ID</td>

<td  class="fila1">FECHA DE SALIDA</td>
<td  class="fila1">SALIDA N°</td>
<td  class="fila1">DEPENDENCIA</td>
<td  class="fila1">DOCUMENTO ID</td>
<td  class="fila1">NOMBRE FUNCIONARIO</td>

<tr>

<?php
while ($f_productos=mysql_fetch_array($t_prod)){
?>
<td class="fila2"><?php echo $f_productos["idelemento"];?></td>
<td  class="fila2"><?php echo $f_productos["fechaasig"];?></td>
<td  class="fila2"><?php echo $f_productos["numsalida"];?></td>
<td  class="fila2"><?php echo $f_productos["dependencia"]. " -".$f_productos["nombredependencia"];?></td>
<td  class="fila2"><?php echo $f_productos["documentoid"];?></td>
<td  class="fila2"><?php echo $f_productos["nombres"];?></td>
</tr>
<tr>
<?php } }

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
?>
</tr>
</table>
</div>
</div>
<div id="centro">
<div id="centro">

<?php
if(isset($_REQUEST['criterio1'])){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];

$query3="SELECT  * FROM productos  
LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia	
WHERE activo='1'  and  '$criterio1'= '$criterio2' ORDER BY  fechaasig DESC"  ;
$t_prod3=mysql_query($query3,$conexion);
while ($f_us1=mysql_fetch_array($t_prod3)){
$nombre=$f_us1['nombres'] ." ". $f_us['apellidos'];
$docid=$f_us1['documentoid'];
$coddependencia=$f_us1['codigodependencia'];
$nombredependencia=$f_us1['nombredependencia'];
$idelemento_tr=$f_us1['idelemento'];
$elemeto_tr=$f_us1['elemento'];
}			
}
?>


<?php
if(isset($idelemento_general)){
if(isset($_REQUEST['criterio1'])){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];
?>
<table border="0" class="tabla_2" WIDTH="700PX" >

<td  class="fila1" colspan="9"> <H3> HISTORIAL DE TRASLADOS ENTREGA</H3></td>
<tr >	
<td  class="fila1">ID</td>
<td  class="fila1">FECHA DE TRASLADO</td>
<td  class="fila1">TRASLADO N°</td>
<td  class="fila1">DEPENDENCIA Entrega</td>
<td  class="fila1">DOCUMENTO ID Entrega</td>
<td  class="fila1">NOMBRE FUNCIONARIO Entrega</td>


<tr>
<?php		
if(isset($idelemento_general)){
if(isset($_REQUEST['criterio1'] )){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];

$query2tr="SELECT  * FROM tabla_aux_traslados  
LEFT JOIN usuarios on tabla_aux_traslados.docid_entrega_aux=usuarios.documentoid
LEFT JOIN dependencias on tabla_aux_traslados.iddependencia_entrega_aux=dependencias.codigodependencia
WHERE activo_aux_tr='1' AND idelemento_tr_aux='$idelemento_general'  ORDER BY  trasladonum_aux DESC"  ;
$t_prod=mysql_query($query2tr,$conexion);


if (mysql_errno()!=0)
{
	echo "ERROR en la busqueda.". mysql_errno(). "-". mysql_error();
	$error=1;
	mysql_close($conexion);
}

while ($f_trasl=mysql_fetch_array($t_prod)){
?>
<td class="fila2"><?php echo $f_trasl["idtabla_tr_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["fecha_traslado_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["trasladonum_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["iddependencia_entrega_aux"] . " ".$f_trasl["nombredependencia"] ;?></td>
<td class="fila2"><?php echo $f_trasl["docid_entrega_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["nombres"]." ".$f_trasl["apellidos"];?></td>
	
<td class="fila2"><?php echo $f_trasl["observaciones_tr_aux"];?></td>
<tr>
<?php } } }?>
</tr>
</table>

<?php }  }?>

<?php
if(isset($_REQUEST['criterio1'])){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];

$query3="SELECT  * FROM productos  
LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia	
WHERE activo='1'  and  '$criterio1'= '$criterio2' ORDER BY  fechaasig DESC"  ;
$t_prod3=mysql_query($query3,$conexion);
while ($f_us1=mysql_fetch_array($t_prod3)){
$nombre=$f_us1['nombres'] ." ". $f_us['apellidos'];
$docid=$f_us1['documentoid'];
$coddependencia=$f_us1['codigodependencia'];
$nombredependencia=$f_us1['nombredependencia'];
$idelemento_tr=$f_us1['idelemento'];
$elemeto_tr=$f_us1['elemento'];
}			
}
?>

<?php		
if(isset($idelemento_general)){
if(isset($_REQUEST['criterio1'] )){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];
?>
<table border="0" class="tabla_2" WIDTH="700PX" >

<td  class="fila1" colspan="9"> <H3> HISTORIAL DE TRASLADOS RECIBE</H3></td>
<tr >	
<td  class="fila1">ID</td>
<td  class="fila1">FECHA DE TRASLADO</td>
<td  class="fila1">TRASLADO N°</td>
<td  class="fila1">DEPENDENCIA RECIBE</td>
<td  class="fila1">DOCUMENTO ID RECIBE</td>
<td  class="fila1">NOMBRE FUNCIONARIO RECIBE</td>

<tr>
<?php		
if(isset($idelemento_general)){
if(isset($_REQUEST['criterio1'] )){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];

$query2tr="SELECT  * FROM tabla_aux_traslados  
LEFT JOIN usuarios on tabla_aux_traslados.docid_recibe_aux=usuarios.documentoid
LEFT JOIN dependencias on tabla_aux_traslados.iddependencia_recibe_aux=dependencias.codigodependencia
WHERE activo_aux_tr='1' AND idelemento_tr_aux='$idelemento_general'  ORDER BY  trasladonum_aux DESC"  ;
$t_prod=mysql_query($query2tr,$conexion);


if (mysql_errno()!=0)
{
	echo "ERROR en la busqueda.". mysql_errno(). "-". mysql_error();
	$error=1;
	mysql_close($conexion);
}

while ($f_trasl=mysql_fetch_array($t_prod)){
?>
<td class="fila2"><?php echo $f_trasl["idtabla_tr_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["fecha_traslado_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["trasladonum_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["iddependencia_recibe_aux"] . " ".$f_trasl["nombredependencia"] ;?></td>
<td class="fila2"><?php echo $f_trasl["docid_recibe_aux"];?></td>
<td class="fila2"><?php echo $f_trasl["nombres"]." ".$f_trasl["apellidos"];?></td>
	
<td class="fila2"><?php echo $f_trasl["observaciones_tr_aux"];?></td>
<tr>
<?php } } }?>
</tr>
</table>
</div>

<?php } } ?>



<?php		
if(isset($idelemento_general)){
if(isset($_REQUEST['criterio1'] )){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2']; ?>
<table border="0" class="tabla_2"  WIDTH="700PX">

<td  class="fila1" colspan="9"> <H3> HISTORIAL DE REINTEGROS</H3></td>
<tr >	
<td  class="fila1">ID</td>
<td  class="fila1">FECHA ENTRADA</td>
<td  class="fila1">ENTRADA N°</td>
<td  class="fila1">DEPENDENCIA </td>
<td  class="fila1">DOCUMENTO ID</td>
<td  class="fila1">NOMBRE FUNCIONARIO </td><tr>
<?php		

if(isset($idelemento_general)){
if(isset($_REQUEST['criterio1'])){
$criterio1=$_REQUEST['criterio1'];
$criterio2=$_REQUEST['criterio2'];

$query2tr="SELECT  * FROM tabla_aux_reintegros  
LEFT JOIN usuarios on tabla_aux_reintegros.documentoid_aux=usuarios.documentoid
LEFT JOIN dependencias on tabla_aux_reintegros.dependencia_aux=dependencias.codigodependencia
WHERE  idelemento_r_aux='$idelemento_general'  ORDER BY  fecha_entrada_aux DESC" ;

$t_prod=mysql_query($query2tr,$conexion);
while ($f_rein=mysql_fetch_array($t_prod)){
?>

<td class="fila2"><?php echo $f_rein["idtabla_aux_reintegro"];?></td>
<td class="fila2"><?php echo $f_rein["fecha_entrada_aux"];?></td>
<td class="fila2"><?php echo $f_rein["entrada_r_aux"];?></td>.
<td class="fila2"><?php echo $f_rein["dependencia_aux"];?></td>
<td class="fila2"><?php echo $f_rein["documentoid_aux"];?></td>
<td class="fila2"><?php echo $f_rein["nombres"]." ".$f_rein["apellidos"];?></td>
<tr>
<?php } }  }?>
</table>
</div>
<?php } } ?>
<div id="centro3">
<td>Opciones Adicionales:</td>
<a href="../crear_pdf/crear_pdf_historial_elementos.php?idelemento=<?php echo $idelemento_general;?>">Descargar PDF<img src="../imagenes/pdf.png" title="Ver Detalles " width="18" height="18"/></a>

<?php } 		}?>
<?php }
if ($inicial==0){

?>
<DIV ID="CENTRO">					<tr>					<td  class="titulo">						<center><STRONG> NO HAY DATOS</STRONG></center>				</td>
					</tr>			</table></DIV>
<div id="centro">

<?php

}

 } ?>
<?php //Comentario del elemento vacio
 ?>
</DIV>
</DIV>
</DIV>
</DIV>
</body>
<?php
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");
//--------------------------------------------------------------------
//ANULAR TRASLADOS-------
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-15-09
//----------------------------------------------------------------------------------------------------
$traslado3=$_REQUEST['traslado'];
$comentario=$_REQUEST['comentario'];
//--------------------------------------------------
/* PASOS PARA REALIZAR ANULACION DE TRASLADO
------------------------------------------------------------------------------------------------------------------------------------
1)Copiar a una tabla AUX de traslados anulados los items que se anularon con la informacion relacionados con numero de Traslado (OK)
*/
$copiar_tabla_anulada= "INSERT INTO tabla_aux_traslados_anulados SELECT * FROM tabla_aux_traslados WHERE trasladonum_aux='$traslado3'";
$query_copiar_tabla_anulada=mysql_query($copiar_tabla_anulada,$conexion);
//---------------------------------------------------------------------------------------------------------------------------
/*
//---------------------------------------------------------------------------------------------------------------------------
2)Devolver los datos originales del Propietario del elemento (restablecer tabla producto )
	    -Cambiar documentoid, dependencia
		-Obtener los datos generales del traslado elaborado
		-Obtener los datos del producto agregadosa la tabla Aux de traslado elaborado
		-Copiar los datos del traslado a los productos (update)
//------------INFORMACION DE LA TABLA DE TRASLADO + INFORMACION TABLA AUX DEL TRASLADO-------------------------------------------------------
*/
		$sel_traslado="SELECT docid_entrega, docid_recibe, dependencia_entrega, dependencia_recibe FROM traslados WHERE traslado='$traslado3'";
		$query_traslado=mysql_query($sel_traslado,$conexion);
		while ($datos_traslado=mysql_fetch_array($query_traslado)){
//--DATOS DE LA PERSONA QUE ENTREGA-------------> DATOS QUE HAY QUE REESTABLECER EN EL PRODUCTO **-----docid Y Dependencia
			$docid_entrega=$datos_traslado['docid_entrega'];
			$dependencia_entrega=$datos_traslado['dependencia_entrega'];
//--DATOS DE LA PERSONA QUE RECIBE-------------> PERSONA QUIEN HAY QUE DESCARGARLE LOS ARTICULOS PARA QUE NO QUEDEN A SU NOMBRE
			$docid_recibe=$datos_traslado['docid_recibe'];
			$dependencia_recibe=$datos_traslado['dependencia_recibe'];
		}
//--rutina que permite Obtener de la tabla Aux de traslado de productos, los datos del ultimo traslado realizado a cada producto
		$sel_tabla_aux_traslado="SELECT * FROM tabla_aux_traslados WHERE trasladonum_aux='$traslado3' ";
		$query_tabla_aux_traslado=mysql_query($sel_tabla_aux_traslado,$conexion);
//-------------cantiadd de elementos registrados en el traslado NUM--------------
		$num_datos=mysql_num_rows($query_tabla_aux_traslado);
//------------Guarda en 3 Arrays (cada uno de mismo tamaño) los valores de idelemento, ultimo traslado y fecha de ultimo traslado
//-------------------OBTENER LOS DATOS DEL ULTIMO TRASLADO GUARDADOS EN la tabla aux de traslado


		for ($j=0; $j<$num_datos;$j++){
			while ($datos_tabla_aux_traslado=mysql_fetch_array($query_tabla_aux_traslado)){

				$traslado2[]=$datos_tabla_aux_traslado['ultimotraslado2_aux'];
				$fecha_traslado2[]=$datos_tabla_aux_traslado['fechaultimo_traslado2_aux'];

				$traslado1[]=$datos_tabla_aux_traslado['ultimotraslado_aux'];
				$fecha_traslado1[]=$datos_tabla_aux_traslado['fechaultimotraslado_aux'];

				$idelemento_aux[]=$datos_tabla_aux_traslado['idelemento_tr_aux'];

			}
		}

//------------------Ciclo que permite actualizar los registros de los productos a sus valores originales Segun la tabla auxiliar de traslados
		for ($j=0; $j<$num_datos;$j++){
	//echo "<br/>"; ECHO "fecha ultima: ". $fechaultimo[$j] ; echo "<br/>";
			$upd="UPDATE productos SET numtraspaso='$traslado2[$j]', fechaultimanov='$fecha_traslado2[$j]', 
			ultimotraslado='$traslado1[$j]', fecha_ultimo_traspaso='$fecha_traslado1[$j]', documentoid='$docid_entrega',    dependencia='$dependencia_entrega'  WHERE idelemento= '$idelemento_aux[$j]' ";
			$query_tabla_aux_traslado=mysql_query($upd,$conexion);

		}


//----------------------------------------------------------------------------------------------------------------------------------------
/*
3) Poner en estado inactivo el traslado (NO ELIMINAR) (OK)
	-Agregar observacion al traslado
*/
	$upd_traslado_inactivo="UPDATE traslados SET traslado_activo='0', traslado_comentario='$comentario' WHERE traslado='$traslado3'";
	$query_tabla_aux_traslado=mysql_query($upd_traslado_inactivo,$conexion);
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
--PROBAR UTILIZANDO "ECHO" EN TODOS LOS CAMPOS QUE SE CAMBIO INFORMACION,
--ANALIZAR EN SIMULTANEO A PHPMYADMIN Y REVISAR PRODUCTOS
--REVISAR TABLAS AUXILIAR DE TRASPASOS
--REVISAR TABLAS AUXILIAR DE TRASPASOS_ANULADOS
--REVISAR TABLA DE PRODUCTOS
--REVISAR TABLA TRASPASOS  DONDE ANULADOS=0 Y COMENTARIO.
-----------------------------------------------------------------------------------------------
-Tener en cuenta que los traslados anulados anuladas cargan una tabla diferente la cual es una tabla de elementos anulados.
-La Tabla de traslados anuladoos con activo=0 llama los elementos de la tabla aux de traslados....
-Revisar copia de traslados
--------------------------------------------------------------------------------------------------------------------------------------------
*/
//------------si es por cambio de dependencia, devolver el usuario a la dependencia anterior tomando los datos del numero de traslado----------------------
$tipo1="Traslado por Cambio de Dependencia";
$sel_tipo_traslado="SELECT * FROM traslados WHERE traslado='$traslado3'";
$query_tipo_traslado=mysql_query($sel_tipo_traslado,$conexion);

while ($fila_tipo_traslado=mysql_fetch_array($query_tipo_traslado)){
	$tipo_t=$fila_tipo_traslado['tipo_traslado'];
}
if ($tipo_t==$tipo1){
	ECHO "<BR/>";	
	echo "TIPOT: ".$tipo_t;

	ECHO "<BR/>";
	echo "DEP ENTREGA(ANTI) ". $dependencia_entrega;ECHO "<BR/>";
	echo "DEP RECIBE (NUEVA)". $dependencia_recibe;ECHO "<BR/>";
	echo $docid_entrega;
	$upd_user="UPDATE usuarios SET
	iddependencia='$dependencia_entrega'
	WHERE documentoid='$docid_entrega'";
	$query_actualizar_user= mysql_query($upd_user,$conexion); 
/*
for ($j=0; $j<$num_datos;$j++){
$upd="UPDATE productos SET numtraspaso='$traslado3anterior[$j]', fechaultimanov='$fechaultimo[$j]', dependencia='$dependencia_entrega' WHERE documentoid= '$docid_entrega' AND  ";
$query_tabla_aux_traslado=mysql_query($upd,$conexion);
}
*/
}
//---------------------------------------------------------------------------------------------------------------------------------------------//4) Eliminar del sistema  tablas Aux con el numero de traslado
$borrar_auxiliares="DELETE FROM tabla_aux_traslados WHERE trasladonum_aux='$traslado3'";
$query_tabla_aux_traslado=mysql_query($borrar_auxiliares,$conexion);


$delhistorial="DELETE FROM historial_productos WHERE numtraspaso='$traslado3'";
mysql_query($delhistorial,$conexion);

header("location: traslados_menu.php?borrado=1");
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

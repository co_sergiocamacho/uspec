<?php
//VARIABLE PARA LA IDENTIFICACION DEL USUARIO
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
    include("../database/conexion.php");
    include("../assets/encabezado.php");
    include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <link href="../css/paginacion.css" type="text/css" rel="stylesheet"/>
            <link href="../css/styles.css" type="text/css" rel="stylesheet"/>
            <link href="../css/estilos.css" type="text/css" rel="stylesheet"/>
            <link rel="shortcut icon" href="../imagenes/1.ico"/>
            <style> 

                body {
                    background: #eaeaea no-repeat center top;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    background-size: cover;
                }
                .container > header h1,
                .container > header h2 {
                    color: #fff;
                    text-shadow: 0 1px 1px rgba(0,0,0,0.7);
                }

            </style>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Detalles de Taslados</title>
            <link href="../estilos/estilos.css" rel="stylesheet" type="text/css" />
        </head>

        <body>
            <div id="centro2"><table class="botonesfila" >
                    <tr><td>  <a href="traslados_menu.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
                        <td><a href="ver_traslados.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr></table></div>

            <div id="centro">
                <div id="div_bienvenido">
    <?php echo "Bienvenido"; ?> <BR/>
                    <div id="div_usuarios">
                    <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
                    </div>
                        <?php echo "SALIR"; ?>
                    <a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
                </div>

                <table width="100%" border="0">
                    <tr>
                        <td  class="titulo">
                            <center><STRONG> DETALLES DEL TRASLADO</STRONG></center>
                        </td>

                    </tr>
    <?php
    $traslado = $_GET['traslado'];


    $seltraslado = "SELECT * FROM traslados where traslado='$traslado'";
    $queryseltraslado = mysql_query($seltraslado, $conexion);
    while ($datos_traslado = mysql_fetch_array($queryseltraslado)) {

        $numitems = $datos_traslado['num_items'];
    }

    if ($numitems != 0) {


//-----------------separar datos de entrega y recibe para evitar cruce de datos en los detalles de la tabla
//-------------------Se separan haciendo 2 Query (peticiones) diferentes ya que la misma tabla incluye doc id y nombres


        $select_usuarios = "SELECT * FROM tabla_aux_traslados  
					LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
					LEFT JOIN usuarios on tabla_aux_traslados.docid_entrega_aux= usuarios.documentoid
					LEFT JOIN condicion on tabla_aux_traslados.condicion_aux=condicion.idcondicion
					WHERE trasladonum_aux='$traslado' ";
        $query_usuarios = mysql_query($select_usuarios, $conexion);
        while ($datos_user_entrega = mysql_fetch_array($query_usuarios)) {
            $nombres_entrega = $datos_user_entrega['nombres'];
            $apellidos_entrega = $datos_user_entrega['apellidos'];
            $docid_entrega = $datos_user_entrega['documentoid'];
            $dependencia_entrega = $datos_user_entrega['iddependencia_entrega_aux'];
        }
//-------------------------------------------------------------------------------------------
        $select_usuarios = "SELECT * FROM tabla_aux_traslados  
					LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
					LEFT JOIN usuarios ON tabla_aux_traslados.docid_recibe_aux= usuarios.documentoid
					WHERE trasladonum_aux='$traslado' ";
        $query_usuarios = mysql_query($select_usuarios, $conexion);
        while ($datos_user_recibe = mysql_fetch_array($query_usuarios)) {
            $nombres_recibe = $datos_user_recibe['nombres'];
            $apellidos_recibe = $datos_user_recibe['apellidos'];
            $docid_recibe = $datos_user_recibe['documentoid'];
            $dependencia_recibe = $datos_user_recibe['iddependencia_recibe_aux'];
        }

//------------------------------------------------------------------------------------

        $sqlqueryd = "SELECT * FROM tabla_aux_traslados  
					LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
					LEFT JOIN codigocontable on tabla_aux_traslados.codigocontable_tr_aux=codigocontable.codigocontable
					LEFT JOIN usuarios ON tabla_aux_traslados.docid_entrega_aux= usuarios.documentoid
					LEFT JOIN condicion on tabla_aux_traslados.condicion_aux=condicion.idcondicion
					left join unidadmedida on tabla_aux_traslados.id_unidadmedida_aux=unidadmedida.idunidadmedida
					WHERE trasladonum_aux='$traslado'

					ORDER BY idelemento_tr_aux";
        $t_data = mysql_query($sqlqueryd, $conexion);

        $query_traslado = "SELECT * FROM traslados WHERE traslado='$traslado'";
        $d_traslado = mysql_query($query_traslado, $conexion);
        while ($datos_traslado = mysql_fetch_array($d_traslado)) {

            $elaboro = $datos_traslado['elaboradopor'];
        }
        ?>
                    </table>


                    <table  width="90%" id="tabla_activos">
                        <th class ="fila1" COLSPAN="7"><H3> TRASLADO N° <?PHP ECHO "  " . $traslado; ?></H3> </th>

                        <th class ="fila1"COLSPAN="7"> ELABORADO POR<?PHP ECHO "  " . $elaboro; ?></th> 
                        <tr>
                            <th class="fila1" colspan="2" >Datos del Usuario que entrega</th>
                            <td class="fila2" colspan="1" >Nombre:</td><td class="fila2" colspan="2" ><strong><?php echo utf8_decode($nombres_entrega); ?></strong></td>
                            <td class="fila2" colspan="1" >Apellido:</td><td class="fila2" colspan="2" ><strong><?php echo utf8_decode($apellidos_entrega); ?></strong></td>
                            <td class="fila2" colspan="1" >Documento Id:</td><td class="fila2" colspan="1" ><strong><?php echo $docid_entrega; ?></strong></td>
                            <td class="fila2" colspan="1" >Dependencia:</td><td class="fila2" colspan="2"><strong><?php echo utf8_decode($dependencia_entrega); ?></strong></td>



                        </tr>
                        <th class="fila1" colspan="2" >Datos del Usuario que recibe</th>
                        <td class="fila2" colspan="1" >Nombre:</td><td class="fila2" colspan="2" ><strong><?php echo utf8_decode($nombres_recibe); ?></strong></td>
                        <td class="fila2" colspan="1" >Apellido:</td><td class="fila2" colspan="2" ><strong><?php echo utf8_decode($apellidos_recibe); ?></strong></td>
                        <td class="fila2" colspan="1" >Documento Id:</td><td class="fila2" colspan="1" ><strong><?php echo $docid_recibe; ?></strong></td>
                        <td class="fila2" colspan="1" >Dependencia:</td><td class="fila2"colspan="2"><strong><?php echo utf8_decode($dependencia_recibe); ?></strong></td>
                        <tr><th class="fila1" colspan="7" >Detalles del Elemento</th>
                            <th class="fila1" colspan="7" >Detalles del traslado</th>
                        </tr>
                        <th class="fila1">ID</th>
                        <th class="fila1">FECHA TRASLADO</th>
                        <th class="fila1">PLACA</th>
                        <th class="fila1">CONDICION</th>
                        <th class="fila1" width="70"  >UNIDAD MEDIDA</th>
                        <th  class="fila1">COD CATEGORIA</th>
                        <th  class="fila1">DESCRIPCION CATEGORIA</td>
                        <th  class="fila1">DESCRIPCIÓN</td>
                        <th  class="fila1">MARCA</td>
                        <th class="fila1">MODELO</th>
                        <th  class="fila1">SERIE</th>
                        <th  class="fila1">VALOR </th>
                        <th class="fila1">OBSERVACIONES</th>

        <?php
//LOS DATOS DE ENTREGA Y RECIBE EN LA CABECERA PARA NO REPETIR LOS MISMOS DATOS EN TODA LA TABLA

        while ($fila_activos = mysql_fetch_array($t_data)) {
            ?>
                                        <tr>

                                            <td class="fila2"><?php echo $fila_activos["idelemento_tr_aux"]; ?></td>
                                            <td class="fila2"><?php echo $fila_activos["fecha_traslado_aux"]; ?></td>
                                            <td class="fila3" align="center"><strong><?php echo $fila_activos["codebar"]; ?></strong></td>
                                            <td class="fila2"><?php echo $fila_activos["condicion"]; ?></td>
                                            <td class="fila2"><?php echo $fila_activos["unidadmedida"]; ?></td>
                                            <td class="fila2"><?php echo $fila_activos["codigocontable_tr_aux"]; ?></td>
                                            <td class="fila2"><?php echo $fila_activos["codigodescripcion"]; ?></td>
                                            <td class="fila2"><?php echo utf8_decode($fila_activos["elemento"]); ?></td>
                                            <td class="fila2"><?php echo $fila_activos["marca"]; ?></td>
                                            <td class="fila2"><?php echo $fila_activos["modelo"]; ?></td>
                                            <td class="fila2"><?php echo $fila_activos["serie"]; ?></td>
                                            <td class="fila2" align="right">$<?php echo number_format($fila_activos["precioadqui_tr_aux"], 2, ',', '.'); ?></td>
                                            <td class="fila2"><?php echo utf8_decode($fila_activos["observaciones_tr_aux"]); ?></td>

            <?php
        }
        ?> 
</tr>
 <?php
        $querysuma = mysql_query("SELECT SUM(precioadqui_tr_aux) as total FROM tabla_aux_traslados  WHERE trasladonum_aux='$traslado'");
        $valortotal1 = mysql_fetch_array($querysuma, MYSQL_ASSOC);
        $valortotal = $valortotal1["total"];
        ?>
                        <tr>
                            <TD class="fila2"colspan="9"> &nbsp;</TD>
                            <TD class="fila2"colspan="2"><strong>VALOR TOTAL</TD>
                            <td class="fila3" colspan="1" align="right">$<?php echo number_format($valortotal, 2, ',', '.'); ?></strong></td>
                            <TD class="fila2"colspan="1"> &nbsp;</TD>
                        </tr>
                        <table>
                                    </center>
                                    </DIV>

                                    <?php
                                }
                                ?> 

    <?php
    if ($numitems == 0) {


        echo " 
<div id='centro'>

<table width='100%' border='0'>
					<tr>
						<td  >
							<center><STRONG> Movimiento sin elementos trasladados</STRONG></center>
						</td>

					</tr></table></div>
					";
    }
    ?> 
</body>
<?php
include ("../assets/footer.php");
?>
<?php
/*
  @Cerrar Sesion
 */
} else {
header("location: ../403.php");
}
?>


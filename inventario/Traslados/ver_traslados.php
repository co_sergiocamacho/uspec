<?php
//VARIABLE PARA LA IDENTIFICACION DEL USUARIO
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
if(isset($_REQUEST['all'])){$all=1;}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Ver Traslados</title>
	<link href="../estilos/estilos.css" rel="stylesheet" type="text/css" />



</head>

<body>
	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="traslados_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> TRASLADOS DE ACTIVOS DEVOLUTIVOS</STRONG></center>
						</td>


					</tr>
				</table>

				<form name="salidas" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
					<CENTER>
						<TABLE>
							<tr>
								Ordenar por fecha
							</tr>
							<tr>
								<td class="fila5a">DESDE</td>
								<td class="fila5a"><input size="10" id="f_date1" name="f_date1" value="" /><button id="f_btn1" class="botonmas">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>

	<td class="fila5a">HASTA</td>
	<td class="fila5a"><input size="10" id="f_date2" name="f_date2" value="" /><button class="botonmas" id="f_btn2">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date2",
		trigger    : "f_btn2",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script></td>

	<td><input type="submit" class="botonver" name="submit" value="Ver"><br></td>
</tr></form>
</TABLE></CENTER>


<table border="0" class="tabla_2" >
	<tr >
		<td  class="fila1" COLSPAN="3">Datos del traslado</td>
		<td  class="fila1" COLSPAN="4">Datos de usuario que Entrega</td>
		<td  class="fila1" COLSPAN="4">Datos de usuario que Recibe</td>
		<td  class="fila1" COLSPAN="8">Datos Adicionales del traslado</td>
		<tr >
			<p></p>

			<td  class="fila1">ID</td>
			<td  class="fila1">TRASLADO N</td>
			<td  class="fila1">FECHA DE TRASLADO</td>
			<td  class="fila1">DOC. ID</td>
			<td  class="fila1">NOMBRES</td>
			<td  class="fila1">APELLIDOS</td>
			<td class="fila1">DEPENDENCIA</td>
			<td  class="fila1">DOC. ID</td>
			<td  class="fila1">NOMBRES</td>
			<td  class="fila1">APELLIDOS</td>
			<td class="fila1">DEPENDENCIA</td>
			<td  class="fila1">CANT ITEMS</td>
			<td  class="fila1">ELABORADO POR</td>
			<td  class="fila1">VALOR TOTAL</td>
			<td  class="fila1">OBSERVACIONES</td>
			<td  class="fila1">COMENTARIO</td>
			<td  class="fila1">PDF</td>
			<td  class="fila1">DETALLES</td>
			<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
			<td class="fila1">ANULAR</td>

			<?php }?>
			<?php




			if(isset($_REQUEST['all'])){
				$fecha1=date("Y-m-d");
				$fecha2=date("Y-m-d");	
				$query1="SELECT  * FROM traslados  
				WHERE traslado_activo='1' ORDER BY idtraslado DESC"  ;
			}

			else {
				if(isset($_REQUEST['all1'])){
					$fecha1=$_REQUEST['f_date1'];
					$fecha2=$_REQUEST['f_date2'];
					$query1="SELECT  * FROM traslados  
					WHERE traslado_activo='1' 	 
					AND fecha_traslado>= '$fecha1' AND fecha_traslado<='$fecha2' ORDER BY idtraslado DESC"  ;
				}}

				if (empty($fecha1) and empty($fecha2)){ 
					$query1="SELECT  * FROM traslados  
					WHERE traslado_activo='1' ORDER BY idtraslado DESC"  ;
				}


				$t_traslado=mysql_query($query1,$conexion);
				while ($fila_traslado=mysql_fetch_array($t_traslado)){

					?>
					<tr>
						<td class="fila2"><?php echo $fila_traslado["idtraslado"];?></td>
						<td  class="fila2"><?php echo $fila_traslado["traslado"];?></td>
						<td  class="fila2"><?php echo $fila_traslado["fecha_traslado"];?></td>
						<td  class="fila2"><?php echo $fila_traslado["docid_entrega"];?></td>
                                                <td  class="fila2"><?php echo utf8_decode($fila_traslado["nombres_entrega"]);?></td>
						<td class="fila2"><?php echo utf8_decode($fila_traslado["apellidos_entrega"]);?></td>

						<td  class="fila2" ALIGN="CENTER"><?php echo $fila_traslado["dependencia_entrega"];?></td>
						<td  class="fila2"><?php echo $fila_traslado["docid_recibe"];?></td>
						<td  class="fila2"><?php echo utf8_decode($fila_traslado["nombres_recibe"]);?></td>
						<td class="fila2"><?php echo utf8_decode($fila_traslado["apellidos_recibe"]);?></td>

						<td  class="fila2" ALIGN="CENTER"><?php echo $fila_traslado["dependencia_recibe"];?></td>
						<td  class="fila2" ALIGN="CENTER"><?php echo $fila_traslado["num_items"];?></td>
						<td  class="fila2"><?php echo $fila_traslado["elaboradopor"];?></td>
						<td  class="fila2" ALIGN="RIGHT">$<?php echo number_format($fila_traslado["valor_total_traslado"],2,',','.');?></td>
						<td  class="fila2"><?php echo utf8_decode($fila_traslado["tipo_traslado"]);?></td>
						<td  class="fila2"><?php echo utf8_decode($fila_traslado["traslado_comentario"]);?></td>



						<td class="fila2"><center>
							<a href="../crear_pdf/crear_pdf_traslados_usuarios.php?traslado=<?php echo $fila_traslado['traslado'];?>"><img src="../imagenes/pdf.png" title="Descarga PDF " width="28" height="28"/></a>
						</center></td>


						<td class="fila2"><center>
							<a href="detalles_TRASLADO_USUARIO.php?traslado=<?php echo $fila_traslado['traslado'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
						</center></td>


						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
						<td class="fila2"><center>
							<a href="anular_TRASLADO_USUARIO.php?traslado=<?php echo $fila_traslado['traslado'];?>"><img src="../imagenes/ANULAR.png" title="Anular Traslado " width="28" height="28"/></a>
						</center></td>
						<?php }?>

						<?php
					}
					?>
				</tr>
			</table>
		</div>
	</body>
	<?php
	include ("../assets/footer.php");
	?>
	</html>

	<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

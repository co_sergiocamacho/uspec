<?php
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");
/*
CANCELA  LOS DATOS DEL TRASLADO EN UNA TABLA AUXILIAR PARA SER GUARDADA EN LOS HISTORICOS, ACTUALIZA LOS PRODUCTOS Y CREA UNA TABLA AUX DE LOS TRASLADOS LA CUAL SE COPIA 
AL FINAL A LA TABLA DE TRASLADOS.
ELIMINA COPIA DEL TRASLADO
NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
FECHA: 2015-09-14
Unidad de Servicios Penitenciarios y Carcelarios
SOLUCIONES DE PRODUCTIVIDAD
*/
//----------VARIABLES DE TRASLADO_USUARIO_CREAR.PHP
$idelemento=$_REQUEST['idelemento'];
$docid_recibe=$_REQUEST['docid_rec'];
$docid_entrega=$_REQUEST['docid_ent'];
$trasladonum3=$_REQUEST['traslado'];
$anterior=$_REQUEST['anterior'];
$fecha_traslado= date("Y-m-d");
$elaboro=$_SESSION['nombres'] ."  ". $_SESSION['apellidos'];
//-------------------------------------------------------------------------------------------------------------------------
//------------------DATOS DEL USUARIO ENTREGA ( PARA REGRESARLOS AL USUARIO)
$datos_user_entrega="SELECT usuario, nombres, apellidos, iddependencia, documentoid FROM usuarios

WHERE documentoid='$docid_entrega' ";
$queryusuarioentrega=mysql_query($datos_user_entrega,$conexion);
while ($datos_entrega=mysql_fetch_array($queryusuarioentrega)){
	$nombre_entrega=$datos_entrega['nombres']. " ". $datos_entrega['apellidos'];
	$dependencia_entrega=$datos_entrega['iddependencia'];

}

//----------------------------------------DATOS USUARIO RECIBE------------------------------------------------------------------
$datos_user_recibe="SELECT usuario, nombres, apellidos, iddependencia, documentoid FROM usuarios

WHERE documentoid='$docid_recibe' ";
$queryusuariorecibe=mysql_query($datos_user_recibe,$conexion);
while ($datos_recibe=mysql_fetch_array($queryusuariorecibe)){
	$nombre_recibe=$datos_recibe['nombres']. " ". $datos_recibe['apellidos'];
	$dependencia_recibe=$datos_recibe['iddependencia'];
}
//----------------------------------------DATOS DEL PRODUCTO--------------------------------------------------------------------

$sel_producto="SELECT * FROM productos WHERE idelemento='$idelemento'";
$query_producto=mysql_query($sel_producto,$conexion);
while ($datos_producto=mysql_fetch_array($query_producto)){
	$unidadmedida=$datos_producto['idunidadmedida'];
	$precioadqui=$datos_producto['precioadqui'];
	$salidanum=$datos_producto['numsalida'];
	$codigocontable=$datos_producto['codigocontable'];
}

//RECONFIGURAR EL PRODUCTO CON LOS DATOS AUTENTICOS, ELIMINANDO TRASLADO Y ELIMINANDO LA FECHA DE ULTIMA NOVEDAD, QUITANDO LA ANOTACION DE OBSERVACIONES
//----------------------------------------ACTUALIZAR PRODUCTO A NUEVO USUARIO-------------------------------------------------------------
$sel_tabla_ultimo="SELECT * FROM tabla_aux_traslados WHERE (idelemento_tr_aux='$idelemento' AND trasladonum_aux='$trasladonum3') ";
$query_tabla_ultimo=mysql_query($sel_tabla_ultimo,$conexion);

while ($datos_tabla_ultimo=mysql_fetch_array($query_tabla_ultimo)){
	$traslado2=$datos_tabla_ultimo['ultimotraslado2_aux'];
	$fecha_traslado2=$datos_tabla_ultimo['fechaultimo_traslado2_aux'];

	$traslado1=$datos_tabla_ultimo['ultimotraslado_aux'];
	$fecha_traslado1=$datos_tabla_ultimo['fechaultimotraslado_aux'];
}

//--------------------------UPD PRODUCTOS CON LA ESTRUCTURA |3| |2| |1|--------------- 3=aCTUAL 2=Ultimo  1=Penultimo
$upd_producto="UPDATE productos SET 
documentoid='$docid_entrega',
dependencia='$dependencia_entrega',

numtraspaso='$traslado2',
fechaultimanov='$fecha_traslado2',

ultimotraslado='$traslado1',
fecha_ultimo_traspaso='$fecha_traslado2'

WHERE idelemento='$idelemento'";
$query_upd_producto=mysql_query($upd_producto,$conexion);

//-------------------------QUITAR LOS DATOS  QUE SE INSERTARON A LA TABLA AUXILIAR DE TRASLADOSOS----------------------------------------------/*
$insertar_tabla_aux="DELETE FROM tabla_aux_traslados WHERE (idelemento_tr_aux='$idelemento' AND trasladonum_aux='$trasladonum3')";
$query_tabla_aux=mysql_query($insertar_tabla_aux,$conexion);

//------------------------------------------------------------------------------------------------------------------------------------------------------
header ("location: traslado_usuarios_crear.php?internalid=6&docid_ent=$docid_entrega&docid_rec=$docid_recibe&traslado=$trasladonum3&anterior=$anterior&idelem=$idelemento");
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

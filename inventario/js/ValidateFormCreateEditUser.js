$(document).ready(function() {
     $("#LayerMessageSuccess").hide();
     $("#LayerForm").show();
    /**
     * Service Create User
     */
    
    $("#Form_Create_Edit_User").validate({
         error: function(label) {
            $(this).addClass("error");
        },
        rules: {
            IdentityCard: {required: true, minlength: 6},
            NameUser: { required: true, minlength: 3},
            LastNameUser: { required: true, minlength: 3},
            EmailUser: {required: true, email: true},
            OccupationUser: {minlength: 3},
            GradeUser: {minlength: 1},
            TypeContractUser: {required: true},
            OfficeUser: {required: true},
            StatusUser: {required: true},
            TypeLevelUser: {required: true}
           
        },
        messages: {
            NameUser: "Ingrese el nombre del usuario.",
            LastNameUser: "Ingrese el apellido del usuario.",
            EmailUser : "Ingrese un email válido del usuario.",
            TypeContractUser: "Seleccione la calidad del empleador.",
            OfficeUser: "Seleccione la dependencia.",
            StatusUser: "Seleccione si el usuario esta activo o no",
            TypeLevelUser: "Seleccione un tipo de usuario"
        },
        submitHandler: function(form){
           var SecurityLevelUser=$("#SecurityLevelUser").val();
       
            /*if(SecurityLevelUser==""){
                alert("Seleccione el tipo de permiso");
                return false;
            }*/
            $.ajax({
                type: "POST",
                url: "ControllerManagementUser.php",
                data: $("#Form_Create_Edit_User").serialize(),
                dataType: "json",
                success: function(data)
                {
                    var complete = data.complete;
                    var message = data.message;
                    if(complete==true){
                        
                        $("#LayerForm").hide();
                        
                        /**
                         * Message
                         */
                        $("#LayerMessageSuccess").show();
                        $("#LayerMessageSuccess").html("<strong>Registro completo!</strong><br><br>"+message);
                        
                    }
                }
              });

        }
    });
});




$(document).ready(function () {
    /**
     * List of Users
     * @type type
     */
    var ObjectSearch = new Object ();
    ObjectSearch.DateStart = "";
    ObjectSearch.DateEnd = "";
    ObjectSearch.TypeElement = "-1";
    var table = $('#ListEntrySearch').DataTable( {
        language: {
            processing:     "Busqueda en curso...",
            search:         "Buscar:",
            lengthMenu:     "Mostrar _MENU_ Entradas",
            info:           "_START_ a _END_ de _TOTAL_ Entradas",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: "Buscando datos",
            zeroRecords:    "No hay resultados en la b&#250;squeda",
            emptyTable:     "No hay resultados en la b&#250;squeda",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Último"
            },
            aria: {
                sortAscending:  ": Habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": Habilitado para ordenar la columna en orden descendente"
            }
        },
        "searching": false,
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": "MainSearchListEntry.php",
            "data": function ( d ) {
                d.ExtraParameter = ObjectSearch;
            },
            "type": "POST"
            
         },
        "columns": [
            { "data": "entrada" },
            { "data": "fecha" },
            { "data": "tipoelementos" },
            { "data": "proveedor" },
            { "data": "numitems" },
            { "data": "valortotal" },
            { "data": "elaboradopor" },
            { "data": "comentario" },
            { "data": "detalle" },
            { "data": "acciones" },
            { "data": "pdf" }
        ]
    } );
    /**
     * Event Select table
     * @returns {Boolean}
     */
    
     $('#ListEntrySearch tbody').on( 'click', 'tr', function () {
        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else{
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    
    
    /**
     * Search List of Entry
     */
    $("#Button_SearchListEntry").click(function(){ 
           var DateStart =$("#DateStart").val();
           var DateEnd = $("#DateEnd").val();
           var TypeElement = $("#TypeElement").val();
           $("#Message_AlertSearchListEntry").html("");
           
           ObjectSearch.DateStart = DateStart;
           ObjectSearch.DateEnd = DateEnd;
           ObjectSearch.TypeElement = TypeElement;
           
           if(DateStart!=""){
               if(DateEnd==""){
                   $("#Message_AlertSearchListEntry").html("Seleccione una fecha de fin");
                   return false;
               }
           }
           
           if(DateEnd!=""){
               if(DateStart==""){
                   $("#Message_AlertSearchListEntry").html("Seleccione una fecha de inicio");
                   return false;
               }
           }
           
           if(DateStart!=""||DateEnd!=""||TypeElement){
               console.log(ObjectSearch);
               table.ajax.reload();
           }

    });
   
    
});

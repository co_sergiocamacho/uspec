$(document).ready(function() {
    
            /**
             * Load modules for permissions
             * @type jQuery
             */
            var IdSerialNumber = $("#IdSerialNumber").val();
         
            $.ajax({
                type: "POST",
                url: "Node_Module.php",
                 data: {IdSerialData:IdSerialNumber},
                dataType: "json",
                success: function(data)
                {
                    $('#ContainerTree').jstree({
                        "plugins" : ["checkbox"],
                        'core' : {
                            'data' : data
                        }
                    });
                }
            });
            /**
             * Evaluate which models have been selected and saved
             */
            $('#ContainerTree').on("changed.jstree", function (e, data) {
               
                $("#SecurityLevelUser").val(data.selected);
            });
 
});


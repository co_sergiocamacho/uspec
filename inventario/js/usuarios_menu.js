$(document).ready(function () {
    /**
     * List of Users
     * @type type
     */
    var table = $('#ListUserSearch').DataTable( {
        language: {
            processing:     "Busqueda en curso...",
            search:         "Buscar:",
            lengthMenu:     "Mostrar _MENU_ usuarios",
            info:           "_START_ a _END_ de _TOTAL_ Usuarios",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: "Buscando datos",
            zeroRecords:    "No hay resultados en la b&#250;squeda",
            emptyTable:     "No hay resultados en la b&#250;squeda",
            paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
            },
            aria: {
                sortAscending:  ": Habilitado para ordenar la columna en orden ascendente",
                sortDescending: ": Habilitado para ordenar la columna en orden descendente"
            }
        },
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": "MainSearchListUser.php",
            "type": "POST"
         },
        "columns": [
            { "data": "documentoid" },
            { "data": "nombres" },
            { "data": "apellidos" },
            { "data": "usuario" },
            { "data": "permiso" },
            { "data": "usuactivo" }
        ]
    } );
    /**
     * Event Select table
     * @returns {Boolean}
     */
    
     $('#ListUserSearch tbody').on( 'click', 'tr', function () {
        if ($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else{
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    /**
     * Hide LoadScreen
     * @returns {Boolean}
     */
    function HideLoadScreen(){
        $("#loading").hide();
    }
    /**
     * Validate User
     */
    $("#button_ModalCreateUser").click(function(){ 
            $('#ModalUser').modal({
                show:true,
                backdrop:'static'
            });
            $("#FormValidUser").show();
            $("#FormCreateUser").hide();
            $("#FormEditUser").hide();
            $('#Load_Form_Create_User').html("");
            $('#Load_Form_Edit_User').html("");

    });
    /**
     * Button Edit User
     */
    $("#button_ModalEditUser").click(function(){ 
            var data = table.row('.selected').data();
            if(data==undefined){
                alert("Seleccione un usuario a editar");
                return false;
            }else{
                var id_usuario =data.documentoid;
                var idserial = data.idusuario;
                $('#ModalUser').modal({
                    show:true,
                    backdrop:'static'
                });
                $("#FormValidUser").hide();
                $("#FormCreateUser").hide();
                $("#FormEditUser").show();
                $('#Load_Form_Edit_User').html('<iframe src="../Ajustes/CreateEditUser.php?IdentityCard='+id_usuario+'&IdSerialNumber='+idserial+'&EditUser=1" frameborder="0" scrolling="yes" height="350px" width ="100%"></iframe>'); 
            }
            
    });
    
    
    /**
     * Event hide of window modal 
     */
    $("#ModalUser").on('hide.bs.modal', function () {
          $('#Create_User')[0].reset();
          $("#Message_AlertSearchUser").html("");
          $("#idusuario").val("");
          $("#FormValidUser").show();
          $("#FormCreateUser").hide();
          $("#FormEditUser").hide();
          $('#Load_Form_Create_User').html("");
          $('#Load_Form_Edit_User').html("");
          table.ajax.reload();

    });

    /**
     * lock key enter
     */
    $("#Create_User").keypress(function(e) {
            if (e.which == 13) {
                    return false;
            }
    });
    /***
     * Service Validate User
     */
    $("#button_validate").click(function(){ 
        var  RegExPattern = /^[0-9a-zA-Z]+$/;
        var id_usuario = $("#idusuario").val();
        $("#Message_AlertSearchUser").html("");
        if ((RegExPattern.test(id_usuario)) && (id_usuario!="")){

            $.ajax({
                beforeSend: function(){
                    $("#loading").show();
                },
                complete: function(jqXHR, estado){
                    $("#loading").hide();
                },
                data: {"iduser" : id_usuario},
                type: "POST",
                dataType: "json",
                url: "Service_Valide_User.php",
                success:  function (data) {

                    var code = data[0].code;
                    var message =data[0].message;
                    if(code=="1"){
                       $("#Message_AlertSearchUser").html(message);
                    }
                    if(code=="2"){

                        $("#FormValidUser").hide();
                        $("#FormCreateUser").show();
                        $("#FormEditUser").hide();
                        $('#Load_Form_Create_User').html('<iframe src="../Ajustes/CreateEditUser.php?IdentityCard='+id_usuario+'&IdSerialNumber=0&EditUser=0" frameborder="0" scrolling="yes" height="350px" width ="100%"></iframe>');
                    }
                }
            });
        }else{
            alert("Por favor ingrese el n\u00FAmero de identificaci\u00F3n del usuario.");
            return false;
        }
    });
        HideLoadScreen();
});


<?php
session_start();
if (isset($_SESSION['idpermiso'])) {
    include("../database/conexion_pdo.php");
    $jsondata = array();
    $documentoid=$_POST['documento'];
    $documentoid=htmlspecialchars($documentoid, ENT_QUOTES);
    try{
	$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = $conexion->prepare('SELECT documentoid FROM usuarios WHERE documentoid = ? AND usuactivo=?');
	$sql->execute(array($documentoid,"1"));
        $resultado = $sql->rowCount();
        if($resultado>0){
            $jsondata["data"] = array(
                'message' => "El usuario si existe",
                'process' => "true"
            );
        }else{
            $jsondata["data"] = array(
                'message' => "El usuario no existe. Por favor ingrese otro.",
                'process' => "false"
             );   
        }
        
        
    }catch(PDOException $e){
        $jsondata["data"] = array(
          'message' => $e->getMessage(),
          'process' => "false"
        );   
    }
    $conexion = null;
    echo json_encode($jsondata);
}
?>

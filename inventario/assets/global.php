<?PHP

//DESCRIPCION: FUNCIONES GLOBALES PARA TODO EL APLICATIVO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


function req_login()
{
	global $user;
}


?>
<script language="javascript">
//FUNCION DE CONVERTIR A MAYUSCULAS EL TEXTO
//ANDRES MONTEALEGRE GIRALDO
function MAY(campo)
{
	campo.value=campo.value.toUpperCase();
}
function mins(campo)
{
	campo.value=campo.value.toLowerCase();
}
function Reloj()
{
	theTime = window.setTimeout("Reloj()",1000);
	var today = new Date();
	var display= today.toLocaleString();
	document.freloj.reloj.value=display;
}
//FIN FUNCION MAYUSCULAS
</script>
<script language="JavaScript">
//Código para colocar los indicadores de miles mientras se escribe
//ANDRES MONTEALEGRE GIRALDO
//FECHA: 2015-03-27
function puntos(donde,caracter){
	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/
	valor = donde.value
	largo = valor.length
	crtr = true
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			//caracter = "\" + caracter
		}
		carcter = new RegExp(caracter,"g")
		valor = valor.replace(carcter,"")
		donde.value = valor
		crtr = false
	}
	else{
		var nums = new Array()
		cont = 0
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{continue;}
			else{
				nums[cont] = valor.charAt(m)
				cont++
			}
		}
	}
	var cad1="",cad2="",tres=0
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k]
			cad2 = cad1 + cad2
			tres++
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2
				}
			}
		}
		donde.value = cad2
	}
}
//FIN VALIDACION SEPARADOR MILES	
</script>
<script type="text/javascript">
	function SoloNumeros(e)
	{
		var keynum = window.event ? window.event.keyCode : e.which;
		if ((keynum == 8) || (keynum == 46))
			return true;
		
		return /\d/.test(String.fromCharCode(keynum));
	}
</script>
<?php
//FUNCION DE FORMATO DE MILES PARA VALORES EN PHP
//ANDRES MONTEALEGRE GIRALDO
function Formato($campo)
{
	$valor=number_format($campo,0,',','.');
	return $valor;
}

//FUNCION SUMA DIAS A UNA FECHA
//ANDRES MONTEALEGRE GIRALDO
function SumaDias($fecha,$dia)
{   list($year,$mon,$day) = explode('-',$fecha);
return date('Y-m-d',mktime(0,0,0,$mon,$day+$dia,$year));        
}

function RestaDias($fnueva,$fechahoy)
{
	$fnueva = strtotime($fnueva);
	$fechahoy = strtotime($fechahoy);
	$dif = $fechahoy - $fnueva;
	$diasFalt = (( ( $dif / 60 ) / 60 ) / 24);
	return ceil($diasFalt);
}
?>
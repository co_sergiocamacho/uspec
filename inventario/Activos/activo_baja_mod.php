<?php
//MODIFICAR ACTIVO EN BAJA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");

	$idelemento=$_REQUEST["idelemento"];

	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Bajas de Activos</title>



		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
		</div>   
		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<body>


		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="editar_activos.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


				<div id="centro">

					<center>
						<form name="anularcomentario" action="activo_baja.php?idelemento=<?php echo $idelemento;?>" title=" Entrada a Borrar" method="post">
							<table width="60%" class="tabla_2" border="0" style="width:580px">
								<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DE LAS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24


/*
CUANDO LA TABLA DE LA ENTRADA ESTE VACIA SE PUEDE MOSTRAR LA BARRA PARA ELIMINAR LA ENTRADA
*/
?>

<tr>
	<TD class="fila1" colspan="3">      <H1 >BAJA DE ACTIVOS</H1></TD>
</tr>
<tr>
	<td class="fila2" colspan="3" ><center>Por favor escriba el motivo por el cual desea dar de baja el activo</center></td>
</tr>
<tr>
	<TD class="fila2"><strong>Comentario</strong></TD>  
	<td class="fila2" colspan="3"><input type="text" class="textinput"  name="comentario" size="100"  onChange="MAY(this)" value=""  required/></td>
	<tr>
		<TD class="fila2"><strong>Condición</strong></TD>  
		<td class="fila2"colspan="2"><select name="condicion" required> <option value="" required></option>
			<?php
			$unidadmedida=mysql_query("SELECT idcondicion,condicion FROM condicion WHERE (condactivo = '1') ORDER BY idcondicion");
			while($filpro=mysql_fetch_array($unidadmedida))
			{
				?>
				<option   size="50"value="<?php echo $filpro['idcondicion'];?>"><?php echo $filpro['condicion'];?></option>
				<?php
			}?>  </td>


		</tr>



		<tr>
			<td class="fila2">&nbsp;</td>
			<td class="fila2"><center><input type="submit" class="botonguardar" name="guardar" title="Aceptar" value="Aceptar"/>
			</p></center></td>


			<td class="fila2"><p align="center"> 
				<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="cancelar"/>
			</form>

		</Tr>
	</table>


	<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

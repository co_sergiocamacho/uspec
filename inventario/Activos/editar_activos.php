<?php
//EDICION DE ACTIVOS FIJOS DEVOLUTIVOS
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
	include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript" src="../js/buscar.js"></script>

		<script type="text/javascript" src="../js/dataTables.min.js"></script>
		<script type="text/javascript" src="../js/jquery.js"></script>

		<?php
// <script type="text/javascript" src="../js/tablas.js"></script>
		?>


		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
		<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
		<script src="../js/calendario/src/js/jscal2.js"></script>
		<script src="../js/calendario/src/js/lang/en.js"></script>
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}
			
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Activos Fijos</title>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript">
			$(document).ready(function() {
				$(".search").keyup(function () {
					var searchTerm = $(".search").val();
					var listItem = $('.results tbody').children('tr');
					var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
					
					$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
						return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
					}
				});
					
					$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','false');
					});

					$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','true');
					});

					var jobCount = $('.results tbody tr[visible="true"]').length;
					$('.counter').text(jobCount + ' Elementos');

					if(jobCount == '0') {$('.no-result').show();}
					else {$('.no-result').hide();}
				});
			});

		</script>
	</head>

	<body>
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="../entradas/elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr></table></div>
				
				<div id="centro" width="100%">
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
						
						
						<?php

						$selmax="SELECT * FROM responsable_bodega WHERE id_responsable=(SELECT MAX(id_responsable) FROM responsable_bodega)";
						$queryselmax=mysql_query($selmax,$conexion); while($fmax=mysql_fetch_array($queryselmax))
						{ $fechadesde=$fmax['fecha_desde']; $fechahasta=$fmax['fecha_hasta']; $nombre_resp=$fmax['nombres']; $apellidos_resp=$fmax['apellidos']; $cometnario=$fmax['resp_comentario'];} ?>

						<table  >
							<tr>
								<td class="bodega">Responsable de Bodega: </td>
								<td ><?php echo $nombre_resp . " " . $apellidos_resp?>	 </td>
							</tr>
						</table>
						
						
					</div> 
					
					<center>
						
						<table width="100%" border="0">
							<tr>
								<td colspan="11" class="titulo"> <center><STRONG> Activos Fijos </STRONG>      </td>      </center>


							</tr>
						</table>
						<TABLE width="70%" >

						</TR>
					</TABLE>



					<table width="50%" border="0">
						<tr>
							
						</tr>
						<tr>
							
						</tr>
						<tr>
							<td class="texto">
      <!--Andres Montealegre Giraldo
      IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
      <input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idelemento]"; ?>" /> 

  </td>
</tr>
</table>

<div class="form-group pull-left">
	<h4>Escriba su busqueda aqui</h4>
	<input type="text" class="search form-control" placeholder="Escribe aqui el nombre, apellido, dependencia   o el documento de identificación del usuario a buscar..." size="80">
</div>

</center>

<center>

	<div class="table-responsive " >
		<table   class=" table table-bordered results"  cellspacing="0" width="100%" >
			<thead>
				<th width="30px"class="fila1"  >ID</th>
				<th class="fila1"  >ELEMENTO</th>
				<th class="fila1"  >PLACA</th>
				<th  class="fila1"  >ENTRADA ORIGEN</td>
					<th  class="fila1"  >FECHA ENTRADA ORIG.</td>

						<th class="fila1"  >UNID MED</th>
						<th  class="fila1"  >CONDICION</th>	
						<th class="fila1" width="70" >FECHA ENT</th>
						<th class="fila1"  >VALOR</th>
						<th  class="fila1"  >CATEGORIA</th>
						<th  class="fila1"  >ENTRADA N°</td>
							<th class="fila1"  >PROVEEDOR</th>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th  class="fila1"  >SERIE</th>

							<th  class="fila1"  >DEPENDENCIA</hd>    
								<th class="fila1"  >DOCUMENTO</th>
								<th class="fila1"  >USUARIO</th>

								<th class="fila1"  >UBICACION</th>
								<th class="fila1"  >SALIDA N°</th>
								<th class="fila1" >OBSERVACIONES</th>


								<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
								<th width="79" class="fila1">EDITAR</th>
								<?php }?>

								
							</thead>
							<tbody id="myTable2">


								<?php
								
		//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
		//NOMBRE: Andres Montealegre Giraldo
		//FECHA: 2015-01-09			
	//INNER JOIN usuarios on productos.documentoid=usuarios.documentoid

				//ENLAZAR NOMBRES Y APELLIDOS

				//<th class="fila1"  >NOMBRES</th>
				// <th class="fila1"  >APELLIDOS</th>
				//<td class="fila2"><?php echo $fila_activos["nombres"]; cerrar PHP </td>
				//     <td class="fila2"><?php echo $fila_activos["apellidos"]; CERRAR PHP </td> 
								$sqlquery="SELECT * FROM productos  
								left JOIN condicion on productos.idcondicion=condicion.idcondicion 
								LEFT JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
								LEFT JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
								LEFT JOIN proveedores on productos.idproveedor=proveedores.idproveedor
								LEFT JOIN usuarios on  productos.documentoid=usuarios.documentoid
								LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
								LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia
								WHERE activo='1' OR activo ='3'
								ORDER BY idelemento
								";
								$t_activos=mysql_query($sqlquery, $conexion);
								
								$num_registro=mysql_num_rows($t_activos);
								if ($num_registro == 0)
								{
									
									mysql_close($conexion);
									exit();
								}

		//INICIO DEL PAGINADOR DE LA CONSULTA
		//NOMBRE: Andres Montealegre Giraldo
		//FECHA: 2015-01-09	
								$registros=55;
								$pagina=0;
		//DECLARACION DE VARIABLE PARA PASAR EL PAGINADOR
								
								if(isset($_GET['num']))
								{
									$pagina= $_GET['num'];
									$inicio=(($pagina-1)* $registros);
									
								}
								else
								{
				//SI NO DIGO Q ES LA PRIMERA PÁGINA
									$inicio=1;
								}
								
		//FIN PAGINADOR
								
		//CONSULTA PARA UBICAR EL PAGINADOR
		//NOMBRE: Andres Montealegre Giraldo
		//FECHA: 2015-01-09	 

								?>	
								
								<?php
								while ($fila_activos=mysql_fetch_array($t_activos))
								{

									?>
									<tr>
										<td class="fila2"><?php echo $fila_activos["idelemento"];?></td>
										<td class="fila2"><?php echo $fila_activos["elemento"];?></td>
										<td class="fila2"><?php echo $fila_activos["codebar"];?></td>
										<td class="fila2"><?php echo $fila_activos["numentrada_origen"];?></td>
										<td class="fila2"><?php echo $fila_activos["fecha_origen"];?></td>
										<td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
										<td class="fila2"><?php echo $fila_activos["condicion"];?></td>
										<td class="fila2"><?php echo $fila_activos["fechaing"];?></td>
										<td class="fila2" align="right" width="150px">$<?php echo number_format($fila_activos["precioadqui"],2,',','.');?></td>
										<td class="fila2"><?php echo $fila_activos["codigocontable"] . " ".$fila_activos["codigodescripcion"];?></td>
										
										<td class="fila2"><?php echo $fila_activos["numentrada"];?></td>
										<td class="fila2"><?php echo $fila_activos["proveedor"];?></td>
										<td class="fila2"><?php echo $fila_activos["marca"];?></td>
										<td class="fila2"><?php echo $fila_activos["modelo"];?></td>
										<td class="fila2"><?php echo $fila_activos["serie"];?></td>
										<td class="fila2"><?php echo $fila_activos["dependencia"] ." " .$fila_activos["nombredependencia"];?></td>
										
										<td class="fila2"><?php echo $fila_activos["documentoid"];?></td>
										<td class="fila2"><?php echo $fila_activos["nombres"]. " ".$fila_activos["apellidos"] ;?></td>
										<td class="fila2"><?php echo $fila_activos["nombreubicacion"];?></td>
										<td class="fila2"><?php echo $fila_activos["numsalida"];?></td>
										<td class="fila2"><?php echo $fila_activos["probservaciones"];?></td>

										<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
									</center></td>
									<td class="fila2"><center>
										<a href="../activos/activos_modificar.php?idelemento=<?php echo $fila_activos['idelemento'];?>"><img src="../imagenes/editar1.png" title="Modificar Activos	" width="28" height="28"/></a>
									</center></td>
									<?php }?>


									<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
								</center></td>
								
								<?php }?>


								<?php
								
							}
							?> 




						</tr>

						<?php
						$querysuma = mysql_query("SELECT SUM(precioadqui) as total FROM productos WHERE activo='1'");   
						$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
						$valortotal=number_format($valortotal1["total"],2,',','.');



						?>

						<tr>

						</tbody>

						<td class="fila2"colspan="7" align="RIGHT"><strong>VALOR TOTAL ACTIVOS</strong></td>
						
						<td class="fila2" align="right" colspan="2"><strong>$<?php  echo $valortotal;;?></strong></td>
						<TD class="fila2"colspan="15"> &nbsp;</TD>

					</tr>

					<tr class="warning no-result">
						<td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
					</tr>
				</TABLE>
			</div>
			<center>


			</div>

		</div>
	</div>

</div>
</div>

<table width="100%" border="0">
	<tr>
		<td colspan="11" class="titulo"> <center><STRONG> Activos Fijos Otra Entidad</STRONG>      </td>      </center>


	</tr>
</table>



</center>
</DIV>

<DIV ID="CENTRO">

	<div class="container-full">
		<table   class=" table table-hoover"  cellspacing="0" width="95%" >
			<thead>
				<th width="30px"class="fila1"  >ID</th>
				<th class="fila1"  >ELEMENTO</th>
				<th class="fila1"  >PLACA</th>
				<th class="fila1"  >UNID MED</th>
				<th  class="fila1"  >CONDICION</th>	
				<th class="fila1" width="70" >FECHA ENT</th>
				<th class="fila1"  >VALOR</th>
				<th  class="fila1"  >COD. CONTABLE</th>
				<th  class="fila1"  >CATEGORIA</th>
				<th  class="fila1"  >ENTRADA N°</td>
					<th class="fila1"  >PROVEEDOR</th>
					<th class="fila1"  >MARCA</th>
					<th class="fila1"  >MODELO</th>
					<th  class="fila1"  >SERIE</th>
					<th class="fila1"  >DOCUMENTO</th>
					<th class="fila1"  >USUARIO</th>
					
					<th class="fila1"  >UBICACION</th>
					<th class="fila1"  >SALIDA N°</th>
					<th class="fila1" >OBSERVACIONES</th>
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<th width="79" class="fila1">EDITAR</th>			<?php }?>

					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<th width="79" class="fila1">DAR DE BAJA</th>			<?php }?>
				</thead>
				<tbody id="myTable2a">
					<?php
					
		//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
		//NOMBRE: Andres Montealegre Giraldo
		//FECHA: 2015-01-09			
	//INNER JOIN usuarios on productos.documentoid=usuarios.documentoid

				//ENLAZAR NOMBRES Y APELLIDOS

				//<th class="fila1"  >NOMBRES</th>
				// <th class="fila1"  >APELLIDOS</th>
				//<td class="fila2"><?php echo $fila_activos["nombres"]; cerrar PHP </td>
				//     <td class="fila2"><?php echo $fila_activos["apellidos"]; CERRAR PHP </td> 
					$sqlquery="SELECT * FROM productos  
					left JOIN condicion on productos.idcondicion=condicion.idcondicion 
					LEFT JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
					LEFT JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
					LEFT JOIN proveedores on productos.idproveedor=proveedores.idproveedor
					LEFT JOIN usuarios on  productos.documentoid=usuarios.documentoid
					LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
					LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia
					WHERE activo='2'
					ORDER BY idelemento
					";
					$t_activos=mysql_query($sqlquery, $conexion);
					
					$num_registro=mysql_num_rows($t_activos);
					if ($num_registro == 0)
					{
						
						mysql_close($conexion);
						exit();
					}

		//INICIO DEL PAGINADOR DE LA CONSULTA
		//NOMBRE: Andres Montealegre Giraldo
		//FECHA: 2015-01-09	
					$registros=55;
					$pagina=0;
		//DECLARACION DE VARIABLE PARA PASAR EL PAGINADOR
					
					if(isset($_GET['num']))
					{
						$pagina= $_GET['num'];
						$inicio=(($pagina-1)* $registros);
						
					}
					else
					{
				//SI NO DIGO Q ES LA PRIMERA PÁGINA
						$inicio=1;
					}
					
		//FIN PAGINADOR
					
		//CONSULTA PARA UBICAR EL PAGINADOR
		//NOMBRE: Andres Montealegre Giraldo
		//FECHA: 2015-01-09	 

					?>	
					
					<?php
					while ($fila_activos=mysql_fetch_array($t_activos))
					{
						$valor_u=($fila_activos["precioadqui"]);

						?>
						<tr>
							<td class="fila2"><?php echo $fila_activos["idelemento"];?></td>
							<td class="fila2"><?php echo $fila_activos["elemento"];?></td>
							<td class="fila2"><?php echo $fila_activos["codebar"];?></td>
							<td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
							<td class="fila2"><?php echo $fila_activos["condicion"];?></td>
							<td class="fila2"><?php echo $fila_activos["fechaing"];?></td>
							<td class="fila2" align="right">$<?php echo number_format($valor_u,2,',','.');?></td>
							<td class="fila2"><?php echo $fila_activos["codigocontable"];?></td>
							<td class="fila2"><?php echo $fila_activos["codigodescripcion"];?></td>
							<td class="fila2"><?php echo $fila_activos["numentrada"];?></td>
							<td class="fila2"><?php echo $fila_activos["proveedor"];?></td>
							<td class="fila2"><?php echo $fila_activos["marca"];?></td>
							<td class="fila2"><?php echo $fila_activos["modelo"];?></td>
							<td class="fila2"><?php echo $fila_activos["serie"];?></td>
							<td class="fila2"><?php echo $fila_activos["documentoid"];?></td>
							<td class="fila2"><?php echo $fila_activos["nombres"]. " ".$fila_activos["apellidos"] ;?></td>
							<td class="fila2"><?php echo $fila_activos["nombreubicacion"];?></td>
							<td class="fila2"><?php echo $fila_activos["numsalida"];?></td>
							<td class="fila2"><?php echo $fila_activos["probservaciones"];?></td>


							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
						</center></td>
						<td class="fila2"><center>
							<a href="activos_modificar.php?idelemento=<?php echo $fila_activos['idelemento'];?>"><img src="../imagenes/editar1.png" title="Modificar Activos	" width="28" height="28"/></a>
						</center></td>



						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					</center></td>
					<td class="fila2"><center>
						<a href="../activos/activo_baja_mod.php?idelemento=<?php echo $fila_activos['idelemento'];?>"><img src="../imagenes/bajaelemento.png" title="Modificar Activos	" width="28" height="28"/></a>
					</center></td>
					<?php }?>


					<?php }?>
					<?php
					
				}
				?> 




			</tr>

			<?php
			$querysuma = mysql_query("SELECT SUM(precioadqui) as total FROM productos WHERE activo='2'");   
			$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
			$valortotal=number_format($valortotal1["total"],2,',','.');



			?>

			<tr>
				<td class="fila2"colspan="6" align="RIGHT"><strong>VALOR TOTAL ACTIVOS $</strong></td>
				
				<td class="fila2" alighn="right"><strong><?php echo $valortotal;?></strong></td>
				<TD class="fila2"colspan="15"> &nbsp;</TD>

			</tr>

		</tbody>
	</TABLE>


</div>
<div>
	<center>
		<ul class="pagination pagination-lg pager" id="myPager2a"></ul></center>
		<div class="col-md-12 text-center">

		</div>
	</div>



</DIV>

</body>

</html>


</div>

<?php 
include ("../assets/footer.php");
?>

<?php
/*
@Cerrar Sesion
*/
} 

else {
	header("location: ../403.php");
}
?>


<?php 
include ("../assets/footer.php");
?>
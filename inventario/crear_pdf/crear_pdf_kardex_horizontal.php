
<?php
/* --------------------------------------------------------------------------
//DESCRIPCION: CREACION DE PDF DE KARDEX DE ELEMENTOS DE CONSUMO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//---------------------------------------------------------------------------
*/
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	require("../pdf/fpdf.php");
	include ("../assets/datosgenerales.php");
	include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------
	$idelemento=$_REQUEST['idelemento'];
//$valortotal=$_REQUEST['valortotal'];
//$cant_items=$_REQUEST['cant_items'];
//$tipoelementos=$_REQUEST['tipoelementos'];
//$new=$_REQUEST['new'];
	$justificacion="Descarga de PDF de kardex de elem de consumo  "."-  permiso usuario: " .$_SESSION['idpermiso'];
	$observacionregistro="Elemento ID : " .$idelemento." Acceso por IP:".$ip;
	$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
	$fecha=date ("Y-m-d");
	$hora=date("H:i:s");
	$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
	$queryreg2=mysql_query($registro2,$conexion);







	$fecha=date('Y-m-d');

	$seltablakardex="SELECT * FROM tabla_aux_kardex
	LEFT  JOIN codigocontable on tabla_aux_kardex.codigocontable_kardex=codigocontable.codigocontable
	LEFT  JOIN unidadmedida on tabla_aux_kardex.unidadmedida_kardex=unidadmedida.idunidadmedida

	WHERE idelemento_kardex='$idelemento'";
	$querykardex=mysql_query($seltablakardex,$conexion);
	while ($f_kpdf=mysql_fetch_array($querykardex)){


		$elemento_kardex=utf8_decode($f_kpdf['elemento_kardex']);
		$codigo_contable_kardex2=$f_kpdf['codigocontable_kardex'];
		$unidadmedida_kardex=$f_kpdf['unidadmedida_kardex'];
		$entrada_kardex=$f_kpdf['entrada_kardex'];
		$fechaentrada_kardex=$f_kpdf['fechaentrada_kardex'];
		$cantidad_in_kardex=$f_kpdf['cantidad_in_kardex'];
		$valorunitario_kardex=$f_kpdf['valorunit_in_kardex'];
		$valortotal_kardex=$f_kpdf['valortotal_in_kardex'];

		$salida_kardex=$f_kpdf['salida_kardex'];
		$fechasalida_kardex=$f_kpdf['fechasalida_kardex'];
		$cantidad_out_kardex=$f_kpdf['cantidad_out_kardex'];
		$dependencia_out_kardex=$f_kpdf['dependencia_out_kardex'];

		$cantidad_disponible_kardex2=$f_kpdf['cantidad_disponible_kardex'];
		$fecha_disponible_kardex=$f_kpdf['fecha_disponible_kardex'];

		$elaboro_kardex=$f_kpdf['elaboro_kardex'];
		$nombrecodigocontable=$f_kpdf['codigodescripcion'];
		$nombreunidadmedida=$f_kpdf['unidadmedida'];

	}

	$valortotaldisp=$cantidad_disponible_kardex2*$valorunitario_kardex;
//--------------------------OBTENER DATOS DEL PROVEEDOR SEGUN LA INFORMACION ALMACENADA EN LA ENTRADA--
//$descripcion="COMPUTADOR PORTATIL DE 17 PULGADAS PROCESADOR INTEL MEMORIA 4GB DISCO DURO DE 1 GB CON CARGADOR, CABLES TECLADO Y MOUSE SE ENTREGA PARA CAMBIO DE EQUIPO
//POR REPOSICION Y ACTUALIZACION DE SISTEMA  OPERATIVO, SE DEBE INSTALAR Y CONFIGURAR ADAPTADORES DE RED";
//$valorunitario=1900000;

//-----------OBTENER DATOS DE LOS PRODUCTOS ANEXOS A LA ENTRADA------------------------

//----------------------------

	class PDF extends FPDF
	{
//Page header
		function Header()
		{
			global $elemento_kardex;
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(40, -5, "  ", 0, 0, 'C');
$this->Cell(185, -5, " TABLA DE KARDEX DE ELEMENTOS", 0, 0, 'C');
$this->Ln(5);
$this->SetFont('Helvetica','B',11);
$this->Cell(185, -5,"Elemento: ". utf8_decode($elemento_kardex), 0, 0, 'C');
$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,240,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);
$this->Line(10, 25 , 285, 25); 
}


function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',8);
    //Pagina numero
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}
$pdf=new PDF('L');
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(35);

//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(40, 5, "Fecha :", 0);
$pdf->Cell(50, 5, $fecha,0 , 'C');
$pdf->Cell(45, 5, "ID del elemento:", 0);
$pdf->SetFont('Helvetica','B',11);
$pdf->Cell(45, 5, $idelemento, 0, 'B');
$pdf->Cell(50, 5, "Unidad de Medida:", 0);
$pdf->SetFont('Helvetica','B',10);
$pdf->Cell(40, 5, $nombreunidadmedida, 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Ln(5);
$pdf->Cell(40, 5, "Tipo elemento:", 0);
$pdf->SetFont('Helvetica','B',10);
$pdf->Cell(50, 5, utf8_decode($elemento_kardex), 0);
$pdf->Ln(5);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(40, 5, "Categoria:", 0);
$pdf->SetFont('Helvetica','B',10);
$pdf->Cell(40, 5, $codigo_contable_kardex2, 0);
$pdf->Cell(10, 5, "   ", 0);
$pdf->Cell(40, 5, $nombrecodigocontable, 0);
$pdf->SetFont('Helvetica','',9);


//$pdf->Cell(45, 5, "Tipo de Movimiento", 0);
$pdf->SetFont('Helvetica','B',10);
//$pdf->Cell(60, 5, $tipomovimiento, 0);
$pdf->Ln(3);

//---------------DATOS DE PROVEEDOR
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(30, 5, "Responsable:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(120, 5, $responsable,0 , 'C');
//$pdf->Cell(15, 5, "NIT", 0);
//$pdf->Cell(30, 5,$nit, 0);
$pdf->Ln( 2);
//------------------| DATOS UNIDAD EJECUTORA------------>
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(40, 5, "U. Ejecutora:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(140, 5, $unidadejecutora,0 , 'C');
$pdf->Cell(15, 5, "NIT", 0);
$pdf->Cell(30, 5,$nitunidadejecutora, 0);
$pdf->Ln(2);
//-----------------DATOS DE CONTACTO Y FACTURA  
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(30, 5, "Contrato No:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(60, 5,$contrato, 0);
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(45, 5, "Factura No:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(60, 5,$factura, 0);
$pdf->Ln(2);
//-------------------COMENTARIO----------------
$pdf->Ln(1);
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(0, 5, "Comentario:",0);
$pdf->Ln(1);
$pdf->SetFont('Helvetica','',7);
//$pdf->Multicell(195, 3,$comentario, 0, "L");
$pdf->Ln(2);
$yl = $pdf->GetY();
$pdf->Line(10,  $yl, 295-10, $yl);

$pdf->Ln(2);
$pdf->SetFont('Helvetica','B',8, 'C');


$pdf->Cell(25 , 5, " ", 0, 'C');
$pdf->Cell(40 , 5, " ", 0, 'C');
$pdf->Cell(40 , 5, " ", 0, 'C');


$pdf->Cell(10 , 5, " ", 0, 'C');

$pdf->Cell(8 , 5, " ", 0, 'C');
$pdf->Cell(29 , 5, "ENTRADAS", 0, 'C');
$pdf->Cell(20 , 5, " ", 0, 'C');


$pdf->Cell(16 , 5, " ", 0, 'C');
$pdf->Cell(40 , 5, "SALIDAS", 0, 'C');
$pdf->Cell(12 , 5, " ", 0, 'C');


$pdf->Cell(40 , 5, "SALDO", 0, 'C');
$pdf->Cell(30 , 5, " ", 0, 'C');


$pdf->Ln(5);
//$pdf->setFillColor(100, 100, 100);
$pdf->Cell(10 , 5, " ", 0, 'C');
$pdf->Cell(50 , 5, " COMENTARIO", 0, 'C');
$pdf->Cell(18, 5, " VR UNIT ", 0, 'C');
$pdf->Cell(22, 5, " VR TOTAL", 0, 'C');

$pdf->Cell(5 , 5, " ", 0, 'C');

$pdf->Cell(15, 5, " No.", 0, 'C');
$pdf->Cell(18, 5, " FECHA ", 0, 'C');
$pdf->Cell(15, 5, " CANTIDAD ", 0, 'C');

$pdf->Cell(7 , 5, " ", 0, 'C');

$pdf->Cell(15, 5, " No.", 0, 'C');
$pdf->Cell(18, 5, " FECHA ", 0, 'C');
$pdf->Cell(18, 5, " CANTIDAD ", 0, 'C');
$pdf->Cell(12, 5, " DEPEND. ", 0, 'C');

$pdf->Cell(13 , 5, " ", 0, 'C');
$pdf->Cell(18, 5, " FECHA ", 0, 'C');
$pdf->Cell(15, 5, " CANTIDAD ", 0, 'C');


$pdf->SetFont('Helvetica','',8, 'C');
$pdf->Ln(5);  
$pdf->Ln(3); 
$seltablakardex="SELECT * FROM tabla_aux_kardex where idelemento_kardex='$idelemento'";
$querykardex=mysql_query($seltablakardex,$conexion);

while ($f_kpdf=mysql_fetch_array($querykardex)){

//---------------------------------------------
	$y = $pdf->GetY();
	$pdf->SetFont('Helvetica','',7, 'C');
	$pdf->MultiCell(60,3,$f_kpdf['comentario_kardex'],0,'J'); 
	$y1=$pdf->GetY();
	$x1=$pdf->GetX();
	$pdf->SetXY(71,$y);
	$pdf->Line(10, $y-1, 295-10, $y-1);
	$pdf->Cell(2,3,"$",0,'L'); 
	$pdf->Cell(19, 3, number_format($f_kpdf['valorunit_in_kardex'],2,',','.'), 0, 'C');
	$pdf->Cell(2,3,"$",0,'L'); 
	$pdf->Cell(21,3,number_format($f_kpdf['valortotal_in_kardex'],2,',','.'),0,'L'); 

	$pdf->Cell(16, 3, $f_kpdf['entrada_kardex'], 0, 'C');
	$pdf->Cell(18, 3, $f_kpdf['fechaentrada_kardex'], 0, 'C');
	$pdf->Cell(4 , 5, " ", 0, 'C');
	$pdf->Cell(14, 3, $f_kpdf['cantidad_in_kardex'], 0, 'C');


	$pdf->Cell(4 , 5, " ", 0, 'C');

	$pdf->Cell(16, 3, $f_kpdf['salida_kardex'], 0, 'C');
	$pdf->Cell(18, 3, $f_kpdf['fechasalida_kardex'], 0, 'C');
	$pdf->Cell(7 , 5, " ", 0, 'C');
	$pdf->Cell(18, 3, $f_kpdf['cantidad_out_kardex'], 0, 'C');
	$pdf->Cell(18, 3, $f_kpdf['dependencia_out_kardex'], 0, 'C');

	$pdf->Cell(4 , 5, " ", 0, 'C');


	$pdf->Cell(18, 3, $f_kpdf['fechasalida_kardex'], 0, 'C');
	$pdf->Cell(7 , 5, " ", 0, 'C');
	$pdf->Cell(14, 3, $f_kpdf['cantidad_disponible_kardex'], 0, 'C');



//$pdf->MultiCell(22,5,$y,0,'L')  ;
//$pdf->MultiCell(22,5,$y1,0,'L')  ;

	$pdf->SetY($y1);
	$pdf->Ln();  
	$y2 = $pdf->GetY();

	if ($y2>155){
		$pdf-> AddPage();

	}
}
$y3 = $pdf->GetY();
if ($y2<150){
	//----------------------FIRMA Y --VALOR TOTAL--------------------------
	$pdf->SetY($y3);
	$pdf->SetFont('Helvetica','B',8, 'C');
	$pdf->Ln(1);  
	$yl = $pdf->GetY();
	$pdf->SetY($yl+1);
	$pdf->Line(10, $yl-0.9, 295-10, $yl-0.9);
	$pdf->Line(10, $yl-1, 295-10, $yl-1);
	$pdf->SetFont('Helvetica','B',9, 'C');
	$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
	$pdf->Cell(22,5,"   ".$elaboro_kardex,0,'L'); 
//$pdf->Cell(190,1,'    ',1,'L'); 
	$pdf->SetFont('Helvetica','B',9, 'C');
	$pdf->Ln(2); 
	$pdf->Cell(200,5,'    ',0,'L'); 
	$pdf->Cell(32,5,'   VALOR TOTAL  ',0,'L'); 

	$pdf->Cell(7,5,'  $',0,'L'); 
	$pdf->Cell(32,5,"$ ". number_format($valortotaldisp,2,',','.'),0,'L'); 

	$pdf->Ln(10);  
	$pdf->SetFont('Helvetica','B',1, 'C');

	$pdf->Cell(27,5,'                         ',0,'L'); 

	$pdf->Ln(12);  
	$pdf->Cell(5,5,'    ',0,'L'); 
	$pdf->SetFont('Helvetica','B',8, 'C');

//----------------------FIRMA Y --VALOR TOTAL--------------------------
} else {
	$pdf-> AddPage();

	$pdf->SetY(100);
	$pdf->SetFont('Helvetica','B',8, 'C');
	$pdf->Ln(2);  

	$pdf->SetFont('Helvetica','B',8, 'C');
	$pdf->Ln(2); 
	$pdf->SetFont('Helvetica','B',9, 'C');
	$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
	$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 	
	$pdf->Cell(80,5,'    ',0,'L'); 
	$pdf->Cell(37,5,'   VALOR TOTAL  ',0,'L'); 
	$pdf->Cell(37,5,"$ ".number_format($valortotal,2,',','.'),0,'L'); 

	$pdf->Ln(10);  
	$pdf->SetFont('Helvetica','B',1, 'C');

	$pdf->Cell(27,5,'                         ',0,'L'); 
	$pdf->Cell(77,20,'                         ',1,'L'); 
	$pdf->Ln(22);  
	$pdf->Cell(5,5,'    ',0,'L'); 
	$pdf->SetFont('Helvetica','B',8, 'C');

}
$nombresalida="kardex-".$idelemento."-".$fecha.".pdf";


$pdf->Output($nombresalida,'D');
//header("location:../elementos_menu.php?");




} else {
	header("location: ../403.php");
}
?>

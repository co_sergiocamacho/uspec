
<?php
/* -----------------------------------------------------------------------------------
//DESCRIPCION: CREACION DE PDF DE SALIDA DE ELEMENTOS DE CONSUMO A USUARIOS-ANULADA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//------------------------------------------------------------------------------------
*/
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
	require("../pdf/fpdf.php");
	include ("../assets/datosgenerales.php");
	include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------
	$numsalida=$_REQUEST['salida'];
//$valortotal=$_REQUEST['valortotal'];
//$cant_items=$_REQUEST['cant_items'];
//$tipoelementos=$_REQUEST['tipoelementos'];
//$new=$_REQUEST['new'];


	$justificacion="Descarga de PDF de Salida de elementos de consumo a Usuarios  "."-  permiso usuario: " .$_SESSION['idpermiso'];
	$observacionregistro="Salida Num: " .$numsalida." Acceso por IP:".$ip;
	$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
	$fecha=date ("Y-m-d");
	$hora=date("H:i:s");
	$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
	$queryreg2=mysql_query($registro2,$conexion);


	$selentrada="SELECT * FROM salidas 
	LEFT  JOIN tiposalida on salidas.tiposalida_sel=tiposalida.idtiposalida
	where salida='$numsalida'";
	$queryselentrada=mysql_query($selentrada,$conexion);
	while ($datos_salida=mysql_fetch_array($queryselentrada)){
		$fechasalida=$datos_salida['fechasalida'];
		$tipomovimiento=$datos_salida['tiposalida'];
		$fecha=$datos_salida['fechasalida'];
		$documentoid=$datos_salida['documentoid'];
		$valortotal=number_format($datos_salida['valortotal'],2,',','.');
		$cant_items=$datos_salida['numitems'];
		$comentario=$datos_salida['salcomentarios'];
		$contrato=$datos_salida['salcontrato'];
		$factura=$datos_salida['salnumfactura'];
		$tipoentrada=$datos_salida['tiposalida'];
		$elaboradopor=$datos_salida['elaboradopor'];
		$tiposalida_sel=$datos_salida['tiposalida_sel'];
	}
	$entrada=$numsalida;

//--------------------------OBTENER DATOS DEL PROVEEDOR SEGUN LA INFORMACION ALMACENADA EN LA ENTRADA--
	$selproveedor="SELECT * FROM usuarios where documentoid='$documentoid'";
	$queryselproveedor=mysql_query($selproveedor,$conexion);
	while ($datos_usuario=mysql_fetch_array($queryselproveedor)){

		$nombres=$datos_usuario['nombres'];
		$apellidos=$datos_usuario['apellidos'];
		$iddependencia=$datos_usuario['iddependencia'];

		$profesion=$datos_usuario['idprofesion'];
		$cargo=$datos_usuario['cargo'];
		$grado=$datos_usuario['grado'];
		$codcalidadempleado=$datos_usuario['calidadempleado'];


	}

	$selcalidad="SELECT * FROM calidadempleador where idcalidadempleador='$codcalidadempleado'";
	$queryselc=mysql_query($selcalidad,$conexion);
	while ($cal=mysql_fetch_array($queryselc)){
		$calidadempleado=$cal['calidadempleador'];

	}



	$seldependencia="SELECT nombredependencia, sigladependencia FROM dependencias where codigodependencia='$iddependencia'";
	$queryseldependencia=mysql_query($seldependencia,$conexion);
	while ($datos_dependencia=mysql_fetch_array($queryseldependencia)){

		$nombredependencia=$datos_dependencia['nombredependencia'];
		$sigladependencia=$datos_dependencia['sigladependencia'];
	}




	$nombres_apellidos=$nombres. "  " .$apellidos;


//$descripcion="COMPUTADOR PORTATIL DE 17 PULGADAS PROCESADOR INTEL MEMORIA 4GB DISCO DURO DE 1 GB CON CARGADOR, CABLES TECLADO Y MOUSE SE ENTREGA PARA CAMBIO DE EQUIPO
//POR REPOSICION Y ACTUALIZACION DE SISTEMA  OPERATIVO, SE DEBE INSTALAR Y CONFIGURAR ADAPTADORES DE RED";
//$valorunitario=1900000;

//-----------OBTENER DATOS DE LOS PRODUCTOS ANEXOS A LA ENTRADA------------------------

//----------------------------

	class PDF extends FPDF
	{
//Page header
		function Header()
		{

			global $fecha;
			global $numsalida;
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(185, -5, "COMPROBANTE DE SALIDA-ANULADA ", 0, 0, 'C');

$this->Ln(5);
$this->SetFont('Helvetica','B',12);
$this->Cell(185, -5, $numsalida ."    Fecha: " . $fecha, 0, 0, 'C');


$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,160,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);
$this->Line(10, 28 , 200, 28);  
}


function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',8);
    //Pagina numero
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}
$pdf=new PDF();
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(35);

//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(40, 5, "FECHA", 0);
$pdf->Cell(50, 5, $fecha,0 , 'C');
$pdf->Cell(45, 5, "No", 0);
$pdf->SetFont('Helvetica','B',11);
$pdf->Cell(60, 5, $entrada, 0, 'B');
$pdf->SetFont('Helvetica','',9);
$pdf->Ln(5);
$pdf->Cell(40, 5, "Tipo Comprobante", 0);
$pdf->SetFont('Helvetica','B',10);
$pdf->Cell(50, 5, $tipoentrada, 0);
$pdf->Ln(5);
$pdf->Cell(40, 5, "Tipo de Movimiento: ", 0);
$pdf->Cell(50, 5, $tiposalida_sel, 0);


$pdf->SetFont('Helvetica','',9);

//$pdf->Cell(45, 5, "Tipo de Movimiento", 0);
$pdf->SetFont('Helvetica','B',10);
//$pdf->Cell(60, 5, $tipomovimiento, 0);
$pdf->Ln(5);

//---------------DATOS DE PROVEEDOR
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "Funcionario:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(100, 5, utf8_decode($nombres_apellidos),0 , 'C');
$pdf->Cell(35, 5, "DOCUMENTO ID:", 0);
$pdf->Cell(30, 5,$documentoid, 0);
$pdf->Ln( 5);
$pdf->Cell(30, 5, "Dependencia:", 0);
$pdf->Cell(30, 5,$nombredependencia, 0);
$pdf->Ln( 5);
$pdf->Cell(30, 5, "Cod. Dependencia:", 0);
$pdf->Cell(30, 5,$iddependencia, 0);

$pdf->Cell(20, 5, "Sigla:", 0);
$pdf->Cell(30, 5,$sigladependencia, 0);
$pdf->Cell(20, 5, "Grado:", 0);
$pdf->Cell(30, 5,$grado, 0);
$pdf->Ln(5);

$pdf->Cell(30, 5, "Calidad de emp.:  ", 0);
$pdf->Cell(65, 5, $calidadempleado, 0);

$pdf->Cell(20, 5, "Profesion:", 0);
$pdf->Cell(50, 5,$profesion, 0);

$pdf->Ln(5);
$pdf->Cell(30, 5, "Cargo: ", 0);
$pdf->Cell(40, 5,utf8_decode($cargo), 0);
$pdf->Ln(5);	
//------------------| DATOS UNIDAD EJECUTORA------------>
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "U Ejecutora:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(120, 5, $unidadejecutora,0 , 'C');
$pdf->Cell(15, 5, "NIT:", 0);
$pdf->Cell(30, 5,$nitunidadejecutora, 0);
$pdf->Ln(5);
//-----------------DATOS DE CONTACTO Y FACTURA  
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(30, 5, "Contrato No:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(60, 5,$contrato, 0);
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(45, 5, "Factura No:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(60, 5,$factura, 0);
$pdf->Ln(1);
//-------------------COMENTARIO----------------
$pdf->Ln(1);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(0, 5, "Comentario:",0);
$pdf->Ln(5);
$pdf->SetFont('Helvetica','',7);
$pdf->Multicell(195, 3,$comentario, 0, "L");
$pdf->Ln(2);
$yl = $pdf->GetY();
$pdf->Line(10,  $yl, 210-10, $yl);
$pdf->Line(10,  $yl+0.2, 210-10, $yl+0.2);

$pdf->Ln(6);
if($cant_items!=0){
	$pdf->SetFont('Helvetica','B',8, 'C');
//$pdf->setFillColor(100, 100, 100);
	$pdf->Cell(93 , 5, "                  DESCRIPCION DE LA MERCANCIA", 0, 'C');
	$pdf->Cell(20, 5, " CATEG.", 0, 'C');
	$pdf->Cell(20, 5, " UNIDAD", 0, 'C');
	$pdf->Cell(20, 5, " CANTIDAD", 0, 'C');
	$pdf->Cell(17, 5, " VR UNIT ", 0, 'C');
	$pdf->Cell(20, 5, " VR TOTAL", 0, 'C');
	$pdf->SetFont('Helvetica','',8, 'C');
	$pdf->Ln(5);  
	$pdf->Ln(3); 
	$query1="SELECT  * FROM tabla_aux_consumo_salidas_anuladas 
	LEFT  JOIN unidadmedida on tabla_aux_consumo_salidas_anuladas.unidadmedida_aux=unidadmedida.idunidadmedida
	LEFT  JOIN consumo on tabla_aux_consumo_salidas_anuladas.idelemento_aux=consumo.idelemento
	WHERE numsalida_aux='$numsalida'";
	$t_salidas=mysql_query($query1,$conexion);

	while ($fen=mysql_fetch_array($t_salidas)){
//---------------------------------------------
		$y = $pdf->GetY();
		$pdf->SetFont('Helvetica','',7, 'C');
		$pdf->MultiCell(95,3,utf8_decode($fen['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(110,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(15, 3, $fen['codigocontable_aux'], 0, 'C');
		$pdf->Cell(22,3,$fen['unidadmedida'],0,'L'); 	
		$pdf->Cell(18, 3, $fen['cantidad_aux'], 0, 'C');
		$pdf->Cell(2,3,"$",0,'L'); 
		$pdf->Cell(14,3,number_format($fen['valorunit_aux'],2,',','.'),0,'R'); 
		$pdf->Cell(2,3,"$",0,'L'); 
		$pdf->Cell(25,3,number_format($fen['valortotal_aux'],2,',','.'),0,'R'); 

//$pdf->MultiCell(22,5,$y,0,'L')  ;
//$pdf->MultiCell(22,5,$y1,0,'L')  ;

		$pdf->SetY($y1);
		$pdf->Ln();  
		$y2 = $pdf->GetY();
		if ($y2>245){
			$pdf-> AddPage();
		}

	}

	$y3 = $pdf->GetY();
	if ($y2<245){
	//----------------------FIRMA Y --VALOR TOTAL--------------------------

		$pdf->SetY($y3);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2);  
		$yl = $pdf->GetY();
		$pdf->SetY($yl+1);
		$pdf->Line(10, $yl-0.9, 210-10, $yl-0.9);
		$pdf->Line(10, $yl-1, 210-10, $yl-1);
//$pdf->Cell(190,1,'    ',1,'L'); 
		$pdf->SetFont('Helvetica','B',9, 'C');
		$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
		$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 
		$pdf->Ln(2); 
		$pdf->Cell(134,5,'    ',0,'L'); 
		$pdf->Cell(32,5,'   VALOR TOTAL  ',0,'L'); 

		$pdf->Cell(7,5,'  $',0,'L'); 
		$pdf->Cell(32,5,$valortotal,0,'L'); 
		$pdf->Ln(10);  
		$pdf->SetFont('Helvetica','B',1, 'C');

//-------------------------------------------------
		$pdf->Ln(12);  

		$pdf->SetX(130);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(30,5,'RECIBIDO POR:',0,'L'); 
		$pdf->Ln(12);  
		$pdf->SetX(130);$pdf->SetX(130);
		$pdf->SetFont('Helvetica','',8, 'C');
		$pdf->Cell(30,5,'FIRMA:',0,'L'); 
		$pdf->Ln(7);  

//-------------FIRMAS*---------------------------
		$pdf->Cell(35,5,'                         ',0,'L'); 
		$pdf->Cell(67,5,'   RESPONSABLE RECURSOS FISICOS ',0,'L'); 
		$pdf->SetX(130);
		$pdf->Cell(20,5,'NOMBRE: _____________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'CC: __________________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'FECHA: _____ - _____ - _________',0,'L'); 
		$pdf->SetX(10);


		$pdf->Cell(35,5,'                         ',0,'L'); 
		$pdf->Cell(67,5,'   COORDINADOR    GRUPO ADMINISTRATIVO  ',0,'L'); 
	    $pdf->Cell(45,5,'                         ',0,'L'); 
	    $pdf->Cell(67,5,'  JULLY VIVIANA BUSTOS BOHORQUEZ  ',0,'L');
//----------------------FIRMA Y --VALOR TOTAL--------------------------
	} else {
		$pdf-> AddPage();

		$pdf->SetY(50);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2);  

		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
		$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 
		$pdf->Ln(2); 

		$pdf->Cell(37,5,'   VALOR TOTAL  ',0,'L'); 
		$pdf->Cell(37,5,"$ ".$valortotal,0,'L'); 

//-------------FIRMAS*---------------------------
		$pdf->SetX(130);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(30,5,'RECIBIDO POR:',0,'L'); 
		$pdf->Ln(12);  
		$pdf->SetX(130);$pdf->SetX(130);
		$pdf->SetFont('Helvetica','',8, 'C');
		$pdf->Cell(30,5,'FIRMA:',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);

		$pdf->Cell(35,5,'                         ',0,'L'); 
		$pdf->Cell(67,5,'   RESPONSABLE RECURSOS FISICOS ',0,'L'); 
		$pdf->Cell(20,5,'NOMBRE: _____________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'CC: __________________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'FECHA: _____ - _____ - _________',0,'L'); 
		$pdf->SetX(10);

		$pdf->Ln(10);  
		$pdf->SetFont('Helvetica','B',1, 'C');
		$pdf->Cell(77,20,'  ',1,'L'); 
		$pdf->Cell(27,5,'                         ',0,'L'); 
		$pdf->Cell(87,20,'                         ',1,'L'); 
		$pdf->Ln(22);  
		$pdf->SetX(10);
		$pdf->Cell(5,5,'    ',0,'L'); 
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(67,5,'   COORDINADOR    GRUPO ADMINISTRATIVO  ',0,'L'); 
		$pdf->Cell(35,5,'                         ',0,'L'); 
	    $pdf->Cell(67,5,'   JULLY VIVIANA BUSTOS BOHORQUEZ ',0,'L');

	}
}

if($cant_items==0){
	$pdf->SetFont('Helvetica','B',10, 'C');
	$pdf->Ln(20); 
	$pdf->Cell(70,5,'',0,'L'); 
	$pdf->Cell(67,5,'SALIDA SIN ELEMENTOS ASIGNADOS  ',0,'L'); 
	$pdf->Ln(12); 
	$pdf->Ln(10);  
	$pdf->SetFont('Helvetica','B',1, 'C');
	$pdf->Ln(22);  
	$pdf->Cell(5,5,'    ',0,'L'); 
}



$nombresalida=$numsalida."-".$fecha.".pdf";


$pdf->Output($nombresalida,'D');
//header("location:../elementos_menu.php?");



} else {
	header("location: ../403.php");
}
?>

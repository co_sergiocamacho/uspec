<?php

if(isset ($_REQUEST['exit'])){

header("location: Security/Logout.php");
}

?>

<!DOCTYPE html>
<!--
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
   -->
<html>
   <head>
      <title>Inventarios</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="css/estilos.css" rel="stylesheet" type="text/css" />
      <link href="css/style.css" rel="stylesheet" type="text/css"/>
      <link href="css/Login.css" rel="stylesheet" type="text/css"/>
      <script src="js/jquery.js"></script>
      <script src="Security/js/EncryptCode.js"></script>
      <script src="Security/js/FunctionsValidate.js"></script>
   </head>
   <body>
      <div class="container">
         <header>
            <h1>Sistema de Administracion de <strong>USPEC</strong> Unidad de Servicios Penitenciarios</h1>
            <h2>Bienvenido esperamos que sea util tu visita en este lugar</h2>
            <div class="support-note">
               <span class="note-ie">Lo sentimos, por favor actualiza tu navegador.</span>
            </div>
         </header>
      </div>
      
       
 <center>
               <img src="imagenes/logo.png" width="577"  />
            </center>
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" id="formlogin" name="form1" method="POST">
                <input type="hidden" name="data_password" id="data_password" value="-1">
              <input type="hidden" name="data_user" id="data_user" value="-1">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" name="txt_usuario" id="inputUser" class="form-control" placeholder="Usuario" required autofocus>
                <input type="password" name="txt_password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
                <span role="alert" class="error-msg" id="errormessage">
                </span>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="button" name="btn_login" id="btn_login">Ingresar</button>
            </form><!-- /form -->
            <!--<a href="#" class="forgot-password">
                Forgot the password?
            </a>-->
        </div><!-- /card-container -->
    </div><!-- /container -->
   </body>
</html>
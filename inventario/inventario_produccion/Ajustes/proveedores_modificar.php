<?php

//DESCRIPCION:VENTANA DE MOFICICACIÓN DE DATOS DEL PROVEEDOR EN BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">

		<style>	



			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Proveedor Modificar</title>





		<body>

			<div id="centro2"><table class="botonesfila" >
				<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
					<td><a href="proveedores_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


					<div id="centro">

						<div id="div_bienvenido">
							<?php echo "Bienvenido"; ?> <BR/>
							<div id="div_usuarios">
								<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
							</div>
						</div> 






						<center>
							<table width="70%" class="tabla_2" border="0" style="width:580px">
								<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DELproveedor  A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24

								$rst_proveedores=mysql_query ("SELECT
									proveedores.idproveedor,
									proveedores.proveedor,
									proveedores.nit,
									proveedores.direccion,
									proveedores.telefono,
									proveedores.email,
									proveedores.fechacrea,
									proveedores.provactivo,
									proveedores.provobservaciones
									FROM proveedores
									WHERE idproveedor=". $_REQUEST["id_proveedor"].";",$conexion);
//echo $rst_clientes;
								while($filusu=mysql_fetch_array($rst_proveedores))
								{

									?>

									<tr>
										<td class="fila1">MODIFICAR REGISTRO DE PROVEEDORES</td>
									</tr>
									<tr>
										<td class="fila2">Realizar la modificacion del proveedor seleccionado...</td>
									</tr>
								</table>

								<br>
								<form name="proveedores" action="proveedores_update.php?id_proveedor=<?php echo $_REQUEST["id_proveedor"];?>" title="Nuevo PROVEEDOR" method="post">

									<table width="70%" class="tabla_2" border="0" style="width:580px">
										<tr>
											<td colspan="5" class="fila1">DATOS DEL PROVEEDOR</td>
										</tr>
										<tr>
											<td class="fila2">NOMBRES PROVEEDOR</td>
											<td class="fila2"><input type="text" class="textinput"  name="proveedor_nombre" size="40" maxlength="80"  onChange="MAY(this)" value="<?php echo $filusu["proveedor"] ?>"  /></td>
											<td class="fila2">NIT</td>
											<td class="fila2"><input type="text" class="textinput"  name="proveedor_nit" size="30" maxlength="80" value="<?php echo $filusu["nit"] ?>"  /></td>
										</tr>
										<tr>

											<td class="fila2">DIRECCION:</td>
											<td class="fila2"><input type="text" class="textinput"  name="proveedor_direccion" size="40" maxlength="80" onChange="MAY(this)" value="<?php echo $filusu["direccion"] ?>" /></td>
											<td class="fila2">EMAIL</td>
											<td class="fila2"><input type="text" class="textinput"  name="proveedor_email" size="30" maxlength="80" value="<?php echo $filusu["email"] ?>"  /></td>
										</tr>
										<tr>

											<td class="fila2">TELEFONO:</td>
											<td class="fila2"><input type="text" class="textinput"  name="proveedor_telefono" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $filusu["telefono"] ?>" /></td>
											<td class="fila2">FECHA CREACION</td>
											<td class="fila2"><input type="text" class="textinput"  name="proveedor_fechacrea" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $filusu["fechacrea"]?>" />
												[aaaa-mm-dd]</td>
											</tr>
										</select></td>

									</tr>

									<tr>
										<td class="fila2">OBSERVACIONES:</td>
										<td colspan="3" class="fila2"><input type="text" class="textinput"  name="proveedor_observaciones" size="63" maxlength="300"  onChange="MAY(this)" value="<?php echo $filusu["provobservaciones"] ?>" /></td>
									</tr>
									<tr>
										<td class="fila2">&nbsp;</td>
										<td colspan="1" class="fila2"><p align="right"> ACTIVO:</p></td>
										<td colspan="2" class="fila2">
											<input type="radio" name="proveedor_activo" value="'1'" class="check" checked="checked"/>
											SI
											<input type="radio" name="proveedor_activo" value="'0'" class="check" />NO 



											<?php

										}
										?>

									</td>

								</tr>
								<tr>
									<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
										<input type="hidden" name="idusu" value="<?php echo "ID proveedor para modificar $_REQUEST[id_proveedor]"; ?>" /></td>
										<td class="fila2"><p align="center">
											<input type="submit" class="botonguardar" name="guardar_proveedor" title="Guardar proveedor" value="GUARDAR"/>
										</p></td>
										<td class="fila2">&nbsp;</td>
										<td class="fila2"><p align="center"> <a href="javascript:history.go(-1)">
											<input type="submit" class="botoncancelar" name="cancelar_modiprov" title="Cancelar Proovedor" value="CANCELAR"/>
										</a> </td>
									</tr>
								</table>
							</form></center>

							<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
							include ("../assets/footer.php");
							?>
						</body>
						</html>


						<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>
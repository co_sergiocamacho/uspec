<?php

//DESCRIPCION: MENU DE PROVEEDORES PARA EL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript" src="../js/buscar.js"></script>

		<script type="text/javascript" src="../js/dataTables.min.js"></script>
		<script type="text/javascript" src="../js/jquery.js"></script>

		<?php
// <script type="text/javascript" src="../js/tablas.js"></script>
		?>


		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
		<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
		<script src="../js/calendario/src/js/jscal2.js"></script>
		<script src="../js/calendario/src/js/lang/en.js"></script>
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>PROVEEDORES</title>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />





		<script type="text/javascript">
			$(document).ready(function() {
				$(".search").keyup(function () {
					var searchTerm = $(".search").val();
					var listItem = $('.results tbody').children('tr');
					var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
					
					$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
						return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
					}
				});
					
					$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','false');
					});

					$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','true');
					});

					var jobCount = $('.results tbody tr[visible="true"]').length;
					$('.counter').text(jobCount + ' Elementos');

					if(jobCount == '0') {$('.no-result').show();}
					else {$('.no-result').hide();}
				});
			});

		</script>

	</head>

	<body>

		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="ajustes_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


				<div id="centro">

					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   






					<center>
						<table width="50%" border="0">
							<tr>
								<td class="titulo"><center>
									PROVEEDORES</center>
									<?php
									if (isset($_GET['add'])){ if ($_GET['add']==1){?>
									<div class="quitarok">
										<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Proveedor creado correctamente!
									</div>

									<?php   }    } ?> 


									<center>      <td class="texto"></td>
									</tr>
									<tr>
										<td class="texto">
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN HIDDEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
		?>






		<div>
			<table class="menuactivosA" >

				<td coslpan="1">   <a  class="titulos_menu" href="proveedores_new.php">
					<img src="../imagenes/add.png" title="Nuevo proveedor" width="38" height="38" alt="nuevo" />Agregar proveedor</a>
				</table>
			</div>
			<?php
		}
		?>
	</td>

</tr>
</center></td>

</center>


<center>
</div>
<div class="form-group pull-left">
	<h4>Escriba su busqueda aqui</h4>
	<input type="text" class="search form-control" placeholder="Escribe aqui el nombre, apellido, dependencia   o el documento de identificación del usuario a buscar..." size="80">
</div>
<div class="table-responsive " >

	<table   class=" table table-bordered results"  cellspacing="0" width="100%" >
		<tr>
			<thead>
				<th width="37" class="fila1">ID </th>
				<th width="197" class="fila1">PROEVEDOR</th>
				<th width="92" class="fila1">NIT</th>
				<th width="170" class="fila1">DIRECCION</th>
				<th width="90" class="fila1">TELEFONO</th>
				<th width="135" class="fila1">EMAIL</th>

				<th width="150" class="fila1">OBSERVACIONES</th>

				<th width="89" class="fila1">ACTIVO</th>
				<th width="89" class="fila1">ACCIONES</th>
			</tr>
		</thead>
		<tbody id="myTable2">
			<?php

//CONSULTA DE LOS PROVEEDORES ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09			
			$t_proveedores=mysql_query("SELECT idproveedor, proveedor, nit, 
				direccion, telefono, email, provactivo, 
				provobservaciones
				FROM proveedores", $conexion);

//echo $t_clientes;
			$num_registro=mysql_num_rows($t_proveedores);
			if ($num_registro == 0)
			{
				echo "No se han encontrado proveedores...";
				mysql_close($conexion);
				exit();
			}

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	
			$registros=25;
			$pagina=0;
//DECLARACION DE VARIABLE PARA PASAR EL PAGINADOR

			if(isset($_GET['num']))
			{
				$pagina= $_GET['num'];
				$inicio=(($pagina-1)* $registros);

			}
			else
			{
//SI NO DIGO Q ES LA PRIMERA PÁGINA
				$inicio=1;
			}

//FIN PAGINADOR

//CONSULTA PARA UBICAR EL PAGINADOR
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	 

			?>	

			<?php
			while ($fila_proveedores=mysql_fetch_array($t_proveedores))
			{

				?>

				<tr>
					<td class="fila2"><?php echo $fila_proveedores["idproveedor"];?></td>
					<td class="fila2"><?php echo $fila_proveedores["proveedor"];?></td>
					<td class="fila2"><?php echo $fila_proveedores["nit"];?></td>
					<td class="fila2"><?php echo $fila_proveedores["direccion"];?></td>
					<td class="fila2"><?php echo $fila_proveedores["telefono"];?></td>
					<td class="fila2"><?php echo $fila_proveedores["email"];?></td>

					<td class="fila2"><?php echo $fila_proveedores["provobservaciones"];?></td>
					<td class="fila2"><center><?php


						if($fila_proveedores['provactivo']=="0")

						{
							?>

							<a href="proveedores_act.php?id_proveedor=<?php echo $fila_proveedores['idproveedor'];?>"><input type="image" src="../imagenes/desactivado.png" width="28" height="28" alt="activo" name="desactivar" value="Desactivar" class="botonf" /></a>
							<input type="hidden" name="activo" value="<?php echo $fila_proveedores['provactivo'];?>" />
							<input type="hidden" name="idproveedor" value="<?php echo $fila_proveedores['idproveedor'];?>" />



							<?php
						}
						else
						{
							?>


							<a href="proveedores_act.php?id_proveedor=<?php echo $fila_proveedores['idproveedor'];?>"><input type="image" src="../imagenes/activo.png" width="28" height="28" alt="activo" name="desactivar" value="Desactivar" class="botonf" /></a>
							<input type="hidden" name="activo" value="<?php echo $fila_proveedores['provactivo'];?>" />
							<input type="hidden" name="idproveedor" value="<?php echo $fila_proveedores['idproveedor'];?>" />



							<?php
						}
						?></center></td>
						<td class="fila2"><center>
							<a href="proveedores_modificar.php?id_proveedor=<?php echo $fila_proveedores['idproveedor'];?>"><img src="../imagenes/editar1.png" title="Modificar proveedor" width="28" height="28"/></a>
						</center></td>
						<?php

					}
					?>  
					
				</tr>

				<tr class="warning no-result">
					<td colspan="20"><i class="fa fa-warning"></i>¡¡ No se encontraron proveedores!!</td>
				</tr>
			</TABLE>
		</div>
	</br>
	<?php  //<div id="div_paginar"> ?>
<!--*******/////INICIO PAGINAR POR NUMERO*******///////
NOMBRE: Andres Montealegre Giraldo
FECHA: 2015-01-09
LISTAR PHP CONSULTA MYSQL POR NUMERO   -->

</tr>
</table>
</div>
<?php

//   NOMBRE: Andres Montealegre Giraldo
//  FECHA: 2015-01-09
//   LISTAR PHP CONSULTA MYSQL POR NUMERO   -->
?>



</div>
<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

</body>
</html>
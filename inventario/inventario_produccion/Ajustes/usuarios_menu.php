<?php
//DESCRIPCION: MENU DE USUARIOS PARA EL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript" src="../js/buscar.js"></script>

		<script type="text/javascript" src="../js/dataTables.min.js"></script>
		<script type="text/javascript" src="../js/jquery.js"></script>

		<?php
// <script type="text/javascript" src="../js/tablas.js"></script>
		?>


		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
		<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
		<script src="../js/calendario/src/js/jscal2.js"></script>
		<script src="../js/calendario/src/js/lang/en.js"></script>
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuarios</title>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />


		<script type="text/javascript">
			$(document).ready(function() {
				$(".search").keyup(function () {
					var searchTerm = $(".search").val();
					var listItem = $('.results tbody').children('tr');
					var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
					
					$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
						return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
					}
				});
					
					$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','false');
					});

					$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','true');
					});

					var jobCount = $('.results tbody tr[visible="true"]').length;
					$('.counter').text(jobCount + ' Elementos');

					if(jobCount == '0') {$('.no-result').show();}
					else {$('.no-result').hide();}
				});
			});

		</script>



	</head>

	<body>


		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="ajustes_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>




				<div id="centro">


					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   


					<center>
						<table width="50%" border="0">
							<tr>
								<td class="titulo"><center>USUARIOS</td></center>

							</tr>
						</center>
					</table>
					<?php
					if (isset($_GET['add'])){ if ($_GET['add']==1){?>
					<div class="quitarok">
						<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Usuario creado correctamente!
					</div>

					<?php   }    } ?> 
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
		?>


		<?php  }	  ?>
	</center>
	<center>
	</table>
	<table class="menuactivosA" >

		<tr class="menuactivosA">
			<td>&nbsp;</td>
			<td coslpan="1"> <a class="titulos_menu"  href="usuarios_new.php">      <img src="../imagenes/add.png" title="Nuevo Usuario" width="48" height="48" alt="nuevo"  align="left">Agregar</a></td>
			<td colspan="4">&nbsp;</td>
		</tr>

		<tr class="menuactivos2">
			<TD ><strong>Tipos de Permiso: </strong></TD>
			<td> 1: SuperAdmin </td> <td> 2: Administradores G-Admin </td><td> 3: Contabilidad</TD><td> 4: Funcionarios Gral</TD>
			<span class="fila1"></span>
		</table>

	</center>

	<center>

		<div class="form-group pull-left" ALIGN="center">
			<h4>Escriba su busqueda aqui</h4>
			<input type="text" class="search form-control" placeholder="Escribe aqui el nombre, apellido, dependencia o el nro de documento  del usuario a buscar..." size="130">
		</div></center>

		<div class="table-responsive " >
			<table   class=" table table-bordered results"  cellspacing="0" width="90%" >
				<tr>
					<thead>
						<th class="fila1">NOMBRES</th>
						<th class="fila1">APELLIDOS</th>
						<th class="fila1">DOCUMENTO ID</th>
						<th class="fila1">USUARIO</th>
						<th class="fila1">DEPENDENCIA</th>
						<th class="fila1">CARGO</th>
						<th class="fila1">EMAIL</th>
						<th class="fila1">DIRECCION</th>
						<th class="fila1">TELEFONO</th>
						<th class="fila1">FECHA NACIMIENTO</th>

						<th  class="fila1">TIPO DE PERMISO </th>
						<th width="142" class="fila1">PERFL  PERMISO</th>
						<th width="79" class="fila1">ACTIVO</th>
						<th width="79" class="fila1">ACCIONES</th></tr>
					</thead>
					<tbody>
						

						<?php

//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	
						$query0="SELECT
						usuarios.idusuario,
						usuarios.nombres,
						usuarios.apellidos,
						usuarios.usuario,
						usuarios.cargo,
						usuarios.iddependencia,
						usuarios.email,
						usuarios.direccion,
						usuarios.telefono,
						usuarios.fechanac
						FROM usuarios 
						LEFT JOIN dependencias on usuarios.iddependencia=dependencias.codigodependencia
						LEFT JOIN permisos on usuarios.idpermiso=permisos.idpermiso
						";
						$t_usuarios=mysql_query($query0,$conexion);		

//echo $t_clientes;
						$num_registro=mysql_num_rows($t_usuarios);
						if ($num_registro == 0)
						{
							echo "No se han encontrado usuarios...";
							mysql_close($conexion);
							exit();
						}

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	
						$registros=55;
						$pagina=0;
//DECLARACION DE VARIABLE PARA PASAR EL PAGINADOR

						if(isset($_GET['num']))
						{
							$pagina= $_GET['num'];
							$inicio=(($pagina-1)* $registros);

						}
						else
						{
//SI NO DIGO Q ES LA PRIMERA PÁGINA
							$inicio=1;
						}

//FIN PAGINADOR

//CONSULTA PARA UBICAR EL PAGINADOR
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	 
						$t_usuarios=mysql_query("SELECT * FROM usuarios LEFT JOIN dependencias on usuarios.iddependencia=dependencias.codigodependencia LEFT JOIN permisos on usuarios.idpermiso=permisos.idpermiso", $conexion);
//echo $t_clientes;

						?>	

						<?php
						while ($fila_usuarios=mysql_fetch_array($t_usuarios))
						{
							?>

							<tr>
								<td class="fila2"><?php echo $fila_usuarios["nombres"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["apellidos"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["documentoid"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["usuario"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["nombredependencia"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["cargo"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["email"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["direccion"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["telefono"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["fechanac"];?></td>
								<td class="fila2"><?php echo $fila_usuarios["idpermiso"];
									if($fila_usuarios["idpermiso"]==1 ){
										echo " " . "Admin OTEC";

									}


									if($fila_usuarios["idpermiso"]==2 ){
										echo " " . "Administrador G-ADMIN";
									}

									if($fila_usuarios["idpermiso"]==3 ){
										echo " " . "Usuario Contabilidad";
									}

									if($fila_usuarios["idpermiso"]>3 ){
										echo " " . "Funcionario";
									}
									?></td>
									<td class="fila2"><?php echo $fila_usuarios["permiso"];?></td>

									<td class="fila2"><center><?php


										if($fila_usuarios['usuactivo']=="0")

										{
											?>
											<a href="usuarios_act.php?id_usu=<?php echo $fila_usuarios['idusuario'];?>"><input type="image" src="../imagenes/desactivado.png" width="28" height="28" alt="activo"  name="activar"  value="Activar" class="botonf" /></a>
											<input type="hidden" name="activo" value="<?php echo $fila_usuarios['usuactivo'];?>" />
											<input type="hidden" name="idcliente" value="<?php echo $fila_usuarios['idusuario'];?>" />

											<?php
										}
										else
										{
											?>
											<a href="usuarios_act.php?id_usu=<?php echo $fila_usuarios['idusuario'];?>"><input type="image" src="../imagenes/activo.png" width="28" height="28" alt="activo" name="desactivar" value="Desactivar" class="botonf" /></a>
											<input type="hidden" name="activo" value="<?php echo $fila_usuarios['usuactivo'];?>" />
											<input type="hidden" name="idcliente" value="<?php echo $fila_usuarios['idusuario'];?>" />

											<?php
										}
										?></center></td>
										<td class="fila2"><center>
											<a href="usuario_modificar.php?id_usu=<?php echo $fila_usuarios['idusuario'];?>"><img src="../imagenes/editar1.png" title="Modificar cliente" width="28" height="28"/></a>
										</center></td>
									</tbody>
									<?php
								}
								?> 
							</tr>
							<tr>
								<td colspan="14" class="fila2"><center>

								</center> </td>	

								<tr class="warning no-result">
									<td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
								</tr>
							</TABLE>
						</div>
					</br>
				</center>
			</div>
			<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
			include ("../assets/footer.php");
			?>
			<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

</body>
</html>
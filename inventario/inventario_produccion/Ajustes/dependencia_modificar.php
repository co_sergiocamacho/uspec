<?php
//DESCRIPCION: MODIFICAR DEPENDENCIAS EN BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS



session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	



			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuario Modificar</title>



		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
		</div>   
		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<body>
		<br>
		<div id="centro">
			<a href="../principal.php?num=1"><input type="image" src="../imagenes/inicio6.png" width="38" height="38" name="regresar" title="Inicio" value="Regresar" class="botonf" /></a>
			<center>
				<table width="60%" class="tabla_2" border="0" style="width:580px">
					<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24

					$rst_dependencias=mysql_query ("SELECT *
						FROM dependencias
						WHERE iddependencia=". $_REQUEST["iddependencia"].";",$conexion);
//echo $rst_clientes;
					while($fildependencia=mysql_fetch_array($rst_dependencias))
					{

						?>

						<tr>
							<td class="fila1">MODIFICAR DEPENDENCIASS</td>
						</tr>
						<tr>
							<td class="fila2">Realizar la modificacion de la dependencia...</td>
						</tr>
					</table>

					<br>
					<form name="clientes" action="dependencia_update.php?iddependencia=<?php echo $_REQUEST["iddependencia"];?>" title="Nueva Dependencia" method="post">
						<table width="60%" class="tabla_2" border="0" style="width:580px">
							<tr>

							</tr>
							<tr>
								<td class="fila2">CÓDIGO DE DEPENDENCIA</td>
								<td class="fila2"><input type="text" class="textinput"  name="codigodependencia" size="30" maxlength="80"  onChange="MAY(this)" value="<?php echo $fildependencia["codigodependencia"] ?>"  /></td>
								<td class="fila2">SIGLA DE DEPENDENCIA</td>
								<td class="fila2"><input type="text" class="textinput"  name="sigladependencia" size="30" maxlength="80"  onChange="MAY(this)" value="<?php echo $fildependencia["sigladependencia"] ?>"  /></td>

							</tr>

							<tr>
								<td class="fila2">NOMBRE DE DEPENDENCIA</td>
								<td colspan="4" class="fila2"><input type="text" class="textinput"   name="nombredependencia" size="80" maxlength="80"  onChange="MAY(this)" value="<?php echo $fildependencia["nombredependencia"] ?>"  /></td>

							</tr>

							<tr>

								<td colspan="1" class="fila2"><p align="right"> ACTIVO:</p></td><td class="fila2">&nbsp;</td><td class="fila2">&nbsp;</td>  
								<td colspan="2" class="fila2"><input type="radio" name="activo" value="'1'" class="check" checked="checked"/>
									SI
									<input type="radio" name="activo" value="'0'" class="check" />
									NO 
									<?php
								}
								?></td>
							</tr>
							<tr>
								<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
									<input type="hidden" name="idusu" value="<?php echo "ID usuario modificar $_REQUEST[id_usu]"; ?>" /></td>
									<td class="fila2"><p align="center">
										<input type="submit" class="botonguardar" name="guardar_usuario" title="Guardar Dependencia" value="GUARDAR"/>
									</p></td>
									<td class="fila2">&nbsp;</td>
									<td class="fila2"><p align="center"> 
										<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="cancelar"/>
									</a> </td>
								</tr>
							</table>
						</form></center>

						<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
						include ("../assets/footer.php");
						?>
					</body>
					</html>

					<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

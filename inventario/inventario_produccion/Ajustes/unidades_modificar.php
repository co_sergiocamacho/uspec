<?php
//DESCRIPCION: VENTANA PARA MODIFICAR UNIDAD DE MEDIDA EN BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	



			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuario Modificar</title>



		
		

	</div>   
	<!--FIN IDENTIFICACION USUARIO LOGEADO -->
</head>
<body>
	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="unidades_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>



			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   
				<center>
					<table width="60%" class="tabla_2" border="0" style="width:580px">
						<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24

						$rst_unidades=mysql_query ("SELECT *
							FROM unidadmedida
							WHERE idunidadmedida=". $_REQUEST["idunidadmedida"].";",$conexion);
//echo $rst_clientes;
						while($filunidad=mysql_fetch_array($rst_unidades))
						{

							?>

							<tr>
								<td class="fila1">MODIFICAR UNIDADES</td>
							</tr>
							<tr>
								<td class="fila2">Realizar la modificacion de la unidad de medida...</td>
							</tr>
						</table>

						<br>
						<form name="clientes" action="unidad_update.php?idunidadmedida=<?php echo $_REQUEST["idunidadmedida"];?>" title="Nueva Unidad" method="post">
							<table width="60%" class="tabla_2" border="0" style="width:580px">
								<tr>

								</tr>
								<tr>
									<td class="fila2">UNIDAD DE MEDIDA:</td>
									<td class="fila2"><input type="text" class="textinput"  name="unidadmedida" size="30" maxlength="80"  onChange="MAY(this)" value="<?php echo $filunidad["unidadmedida"] ?>"  /></td>



								</tr>
								<tr>


									<td colspan="1" class="fila2"><p align="right"> ACTIVO:</p></td><td class="fila2">&nbsp;</td><td class="fila2">&nbsp;</td>  
									<td colspan="2" class="fila2"><input type="radio" name="activo" value="'1'" class="check" checked="checked"/>
										SI
										<input type="radio" name="activo" value="'0'" class="check" />
										NO 
										<?php
									}
									?></td>
								</tr>
								<tr>
									<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
										<input type="hidden" name="idusu" value="<?php echo "ID usuario modificar $_REQUEST[id_usu]"; ?>" /></td>
										<td class="fila2"><p align="center">
											<input type="submit" class="botonguardar" name="guardar_usuario" title="Guardar Unidad" value="GUARDAR"/>
										</p></td>
										<td class="fila2">&nbsp;</td>
										<td class="fila2"><p align="center"> 
											<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="cancelar"/>
										</a> </td>
									</tr>
								</table>
							</form></center>

							<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
							include ("../assets/footer.php");
							?>
						</body>
						</html>

						<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

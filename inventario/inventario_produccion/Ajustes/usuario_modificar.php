<?php

//DESCRIPCION: MODIFICAR USUARIOS-FUNCIONARIOS EN LA BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>


	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>


			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuario Modificar</title>



		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
		</div>   
		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<body>
		<br>
		<div id="centro">
			<a href="../principal.php?num=1"><input type="image" src="../imagenes/inicio6.png" width="38" height="38" name="regresar" title="Inicio" value="Regresar" class="botonf" /></a>
			<center>
				<table width="60%" class="tabla_2" border="0" style="width:580px">
					<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24
					$idusuariosx=$_REQUEST['id_usu'];
					$idusuario=$_REQUEST['id_usu'];
					$rst_usuarios=mysql_query ("SELECT *
						FROM usuarios
						WHERE idusuario=". $_REQUEST["id_usu"].";",$conexion);
//echo $rst_clientes;
					while($filusu=mysql_fetch_array($rst_usuarios))
					{

						$idsexo_usu=$filusu['idsexo'];
						$iddependencia_usu=$filusu['iddependencia'];

						?>

						<tr>
							<td class="fila1">MODIFICAR USUARIOS</td>
						</tr>
						<tr>
							<td class="fila2">Realizar la modificacion del cliente seleccionado...</td>
						</tr>
					</table>

					<br>
					<form name="clientes" action="usuario_update.php?id_usu= <?php echo $_REQUEST["id_usu"];?>" title="Nuevo Usuario" method="post">
						<table width="60%" class="tabla_2" border="0" style="width:580px">
							<tr>
								<td colspan="5" class="fila1">&nbsp;</td>
							</tr>
							<tr>
								<td class="fila2">NOMBRES:</td>
								<td class="fila2"><input type="text" class="textinput"  name="nomusu" size="30" maxlength="80"  onChange="MAY(this)" value="<?php echo $filusu['nombres']; ?> " required/></td>
								<td class="fila2">APELLIDOS:</td>
								<td class="fila2"><input type="text" class="textinput"  name="apeusu" size="40" maxlength="80" onChange="MAY(this)" value="<?php echo $filusu['apellidos']; ?>" required/></td>
							</tr>
							<tr>
								<td class="fila2">USUARIO:</td>
								<td class="fila2"><input type="text" class="textinput"  name="usuario" size="30" maxlength="80" value="<?php echo $filusu['usuario']; ?>"  /></td>
								<td class="fila2">PASSWORD:</td>



								<td class="fila2"><input type="password" name="password" size="40" maxlength="80" value="<?php echo $filusu['password']; ?>"  /></td>
							</tr>
							<tr>
								<td class="fila2">EMAIL</td>
								<td class="fila2"><input type="text" class="textinput"  name="emailusu" size="30" maxlength="80" value="<?php echo $filusu['email']; ?>"  /></td>
								<td class="fila2">DIRECCION:</td>
								<td class="fila2"><input type="text" class="textinput"  name="dirusu" size="40" maxlength="80" onChange="MAY(this)" value="<?php echo $filusu['direccion']; ?>" /></td>
							</tr>
							<tr>
								<td class="fila2">TELEFONO:</td>
								<td class="fila2"><input type="text" class="textinput"  name="telusu" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $filusu['telefono']; ?>" /></td>
								<td class="fila2">FECHA NACIMIENTO:</td>
								<td class="fila2"><input size="10" id="f_date1" name="f_date1" value="<?php echo $filusu['fechanac']; ?>" /><button id="f_btn1">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>
</tr>
<tr>
	<td class="fila2">DOCUMENTO:</td>
	<td class="fila2"><input type="text" class="textinput"  name="docusu" size="30" maxlength="80" onChange="MAY(this)" value="<?php echo $filusu['documentoid']; ?>"  required/></td>
	<td class="fila2">PROFESION:</td>
	<td class="fila2"><input type="text" class="textinput"  name="profesion" size="30" maxlength="80" onChange="MAY(this)" value="<?php echo $filusu['idprofesion']; ?>"  /></td>
	<tr>
		<td class="fila2">GRADO:</td>
		<td class="fila2" colspan="1"><input type="text" class="textinput"  name="grado" size="30" maxlength="80" onChange="MAY(this)" value="<?php echo $filusu['grado']; ?>" /></td>


		<td class="fila2">CARGO:</td>
		<td class="fila2"><input type="text" class="textinput"  name="carusu" size="30" maxlength="80"  onChange="MAY(this)" value="<?php echo $filusu['cargo']; ?>"  /></td>




	</tr>
	<tr>
		<td class="fila2">SEXO:</td>
		<td class="fila2"><select name="sexo">

			<?php

			if($idsexo_usu==""){

				$sexos=mysql_query("SELECT idsexo,sexo FROM sexo WHERE (activo = '1') ORDER BY sexo");
				while($filsex=mysql_fetch_array($sexos))
				{
					?>
					<option value="<?php echo $filsex['idsexo'];?>"><?php echo $filsex['sexo'];?></option>
					<?php
				}

			} else {

				$sexos2=mysql_query("SELECT idsexo FROM usuarios WHERE idusuario ='$idusuariosx' ");
				while($filsex2=mysql_fetch_array($sexos2))
				{
					$idsexo_usu_sel=$filsex2['idsexo'];
				}

				$sexos3=mysql_query("SELECT idsexo,sexo FROM sexo WHERE idsexo='$idsexo_usu_sel' AND activo = '1' ORDER BY sexo");		
				while($filsex3=mysql_fetch_array($sexos3))
				{


					?>



					<option value="<?php echo $filsex3['idsexo'];?>"><?php echo $filsex3['sexo'];?></option>
					<?php

				}}

				?>
			</select></td>

			<td class="fila2">FECHA FIN CONTRATO:</td>
			<td class="fila2" colspan="3"><input size="10" id="f_fin" name="f_fin" value="<?php echo $filusu['fechafincontrato']; ?>" /><button id="f_btn2">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_fin",
		trigger    : "f_btn2",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"	
	});
	//]]></script></td>


</tr>
<tr>
	<td class="fila2">CALIDAD DEL EMPLEADOR:</td>
	<td class="fila2"><select name="calidadempleado">

		<?php
		$calidad=mysql_query("SELECT idcalidadempleador, calidadempleador FROM calidadempleador WHERE (activo = '1') ORDER BY idcalidadempleador");
		while($filcalidad=mysql_fetch_array($calidad))
		{
			?>
			<option value="<?php echo $filcalidad['idcalidadempleador'];?>"><?php echo $filcalidad['calidadempleador'];?></option>
			<?php
		}
		?>
	</select></td>

	<td class="fila2">PERMISO:</td>
	<td class="fila2"><select name="permiso">

		<?php
		$permisos=mysql_query("SELECT idpermiso,permiso FROM permisos WHERE (activo = '1') ORDER BY permiso");
		while($filper=mysql_fetch_array($permisos))
		{
			?>
			<option  value="<?php echo $filper['idpermiso'];?>"><?php echo $filper['permiso'];?></option>
			<?php
		}
		?>
	</select></td>


</tr>

<tr>
	<td class="fila2">DEPENDENCIA:</td>

	<td class="fila2" colspan="3"><select name="dependencia">
		<?php



		if($iddependencia_usu==""){

			$dependencias_s=mysql_query("SELECT * FROM dependencias WHERE (activo = '1') ORDER BY codigodependencia");
			while($fildep_1=mysql_fetch_array($dependencias_s))
			{
				?>
				<option value="<?php echo $fildep_1['iddependencia'];?>"><?php echo $fildep_1['nombredependencia'];?></option>
				<?php
			}

		} else {

			$dependencias_2=mysql_query("SELECT iddependencia FROM usuarios WHERE idusuario ='$idusuariosx' ");
			while($fildep2=mysql_fetch_array($dependencias_2))
			{
				$iddep_usu_sel=$fildep2['iddependencia'];
			}

			$dep3=mysql_query("SELECT * FROM dependencias WHERE codigodependencia='$iddep_usu_sel' ");		
			while($fildep2=mysql_fetch_array($dep3))
			{

				?>
				<option value="<?php echo $fildep2['codigodependencia'];?>"><?php echo $fildep2['codigodependencia']."  ".$fildep2['nombredependencia'];?></option>
				<?php 
			} 

			$dependencias_3=mysql_query("SELECT * FROM dependencias  ORDER BY codigodependencia");
			while($fildep_4=mysql_fetch_array($dependencias_3))
			{
				?>
				<option value="<?php echo $fildep_4['iddependencia'];?>"><?php echo $fildep_4['codigodependencia'] ." ". $fildep_4['nombredependencia'];?></option>
				<?php
			}
		}
		?>


	</select></td></tr>


	<tr>
		<td class="fila2">OBSERVACIONES:</td>
		<td colspan="3" class="fila2"><input type="text" class="textinput"  name="obsusu" size="63" maxlength="300"  onChange="MAY(this)" /></td>
	</tr>
	<tr>
		<td class="fila2">&nbsp;</td>
		<td colspan="1" class="fila2"><p align="right"> ACTIVO:</p></td>
		<td colspan="2" class="fila2"><input type="radio" name="activo" value="'1'" class="check" checked="checked"/>
			SI
			<input type="radio" name="activo" value="'0'" class="check" />
			NO </td>
		</tr>
		<tr>
			<td class="fila2"><input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
			<td class="fila2"><p align="center">
				<input type="submit" class="botonguardar" name="guardar_producto" title="Guardar producto" value="GUARDAR"/>
			</p></td>
			<td class="fila2">&nbsp;</td>
			<td class="fila2"><p align="center">


			</td>
		</tr>
	</table>
</form></center>

<?php
}
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

</body>
</html>
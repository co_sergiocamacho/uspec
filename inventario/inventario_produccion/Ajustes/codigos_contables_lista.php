<?php

//DESCRIPCION: VENTANA PRINCIPAL CODIGOS CATEGORIAS CONTABLES EN LISTA PARA ADMINISTRADORES
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	header('Content-Type: text/html; charset=UTF-8'); 
//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//include("../assets/global.php");

	$_SESSION['idpermiso'];
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		
		//Verificación de sesion
		if (isset($_SESSION['idpermiso'])) {

		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>Codigos Contables Lista</title>
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	

			body {
				background: #eeeeee no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;}
				#div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;	}
				.container > header h1,
				.container > header h2 {
					color: #fff;
					text-shadow: 0 1px 1px rgba(0,0,0,0.7);
				}

			</style>

<!--IDENTIFICACION USUARIO LOGEADO
<!--NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
	<!--FECHA: 2015-07-24	 -->

	<!--FIN IDENTIFICACION USUARIO LOGEADO -->

	<body>
		<BR />
		<BR />
		<BR />
		<div id="centro">
			<div id="div_bienvenido">
				<?php echo "Bienvenido"; ?> <BR/>
				<div id="div_usuarios">
					<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
				</div>
				<?php echo "SALIR";?>
				<a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="13" height="13" /></a>
			</div>   
			<center>
				<h1>LISTA DE CODIGOS</h1>


				<table class="tabla_3"   border="0" id="tablaresult"  width="30%">
					<tr >

						<td  class="fila1" width="20">ID</td>
						<td  class="fila1" WIDTH="30">CODIGO CONTABLE</td>
						<td  class="fila1" width="50">DESCRIPCION </td>

					</tr>

					<?php



					$querycodigos="SELECT  * FROM codigocontable   WHERE (codactivo='1')"; 

					$t_codigos=mysql_query($querycodigos,$conexion);

					while($filacodigo=mysql_fetch_array($t_codigos))
					{
						?>

						<TR> 
							<td class="fila2" width="20px"><?php echo $filacodigo["idcodigocontable"];?></td>
							<td class="fila2" width="50px"><?php echo $filacodigo["codigocontable"];?></td>
							<td class="fila2" width="50px"><?php echo $filacodigo["codigodescripcion"];?></td>



							<?php
						}
						?>

					</TR>
				</Table>
			</center>

		</div>
	</body>














	<?php 

	include ("../assets/footer.php");
	?>

	</html>


	<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

<?php


//DESCRIPCION:  MENU DE  DEPENDENCIAS PAR AADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
	?>

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<style>	

			body {
				background: #FBFDF8 no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>DEPENDENCIAS</title>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />


	</head>

	<body>
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="ajustes_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>

				<div id="centro" width="100%">
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div> 
					<div>

						<div>
							<center>

								<table width="100%" border="0">
									<tr>
										<td colspan="11" class="titulo">

											<center><STRONG>DEPENDENCIAS</STRONG>
											</center>
										</td>
									</tr>
								</table>

								<table width="50%" border="0">

									<tr>
										<td class="texto">
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idelemento]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
		?>


		<?php
	}
	?>
</td>
</tr>
</table>
</center>
</div>
<center>


	<?php
	if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2){
		?>

		<table class="menuactivosA" >

			<td coslpan="1"> <a  class="titulos_menu" href="dependencia_new.php"><img src="../imagenes/add.png" title="Nueva Dependencia" width="38" height="38" alt="nuevo" align="center" />Agregar Dependencia</a>
				<?php
			}
			?>


		</table>

		<table  width="60%" id="tabla_activos">

			<th width="30px"class="fila1"  >ID</th>
			<th class="fila1"  width="100" >CODIGO DEPENDENCIA</th>
			<th class="fila1"   width="500">NOMBRE DEPENDENCIA</th>
			<th class="fila1"  >SIGLA</th>
			<th width="79" class="fila1">EDITAR</th>

			<?php

//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09			
//INNER JOIN usuarios on productos.documentoid=usuarios.documentoid

//ENLAZAR NOMBRES Y APELLIDOS

//<th class="fila1"  >NOMBRES</th>
// <th class="fila1"  >APELLIDOS</th>
//<td class="fila2"><?php echo $fila_dependencia["nombres"]; cerrar PHP </td>
//     <td class="fila2"><?php echo $fila_dependencia["apellidos"]; CERRAR PHP </td> 
			$sqlquery="SELECT * FROM dependencias 

			ORDER BY CODIGOdependencia 
			";
			$t_dependencias=mysql_query($sqlquery, $conexion);

			$num_registro=mysql_num_rows($t_dependencias);
			if ($num_registro == 0)
			{

				mysql_close($conexion);
				exit();
			}

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	
			$registros=55;
			$pagina=0;
//DECLARACION DE VARIABLE PARA PASAR EL PAGINADOR

			if(isset($_GET['num']))
			{
				$pagina= $_GET['num'];
				$inicio=(($pagina-1)* $registros);

			}
			else
			{
//SI NO DIGO Q ES LA PRIMERA PÁGINA
				$inicio=1;
			}

//FIN PAGINADOR

//CONSULTA PARA UBICAR EL PAGINADOR
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	 

			?>	
			<?php
			while ($fila_dependencia=mysql_fetch_array($t_dependencias))
			{

				?>
				<tr>
					<td class="fila2"><?php echo $fila_dependencia["iddependencia"];?></td>
					<td class="fila2" align="center"><?php echo $fila_dependencia["codigodependencia"];?></td>
					<td class="fila2"><?php echo $fila_dependencia["nombredependencia"];?></td>
					<td class="fila2" ><?php echo $fila_dependencia["sigladependencia"];?></td>
					<?php

					?>
					<?php

					?>


				</center></td>
				<td class="fila2"><center>
					<a href="dependencia_modificar.php?iddependencia=<?php echo $fila_dependencia['iddependencia'];?>"><img src="../imagenes/editar1.png" title="Modificar Dependencia" width="28" height="28"/></a>
				</center></td>
				<?php

			}
			?> 

		</tr>
	</table>
</center>
</DIV>
</body>

</html>

</div>
<?php
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>



<?php

//DESCRIPCION: VENTANA DEL RESPONSABLE DE BODEGA PARA EL ADMNIISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/encabezado.php");

	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="../js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>	
			<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
			body {
				background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Nuevo Usuario</title>



		<?php 
//include('menu.php');

		?>
	</head>
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<body>

		<div id="centro2">
			<table class="botonesfila" >
				<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
					<td><a href="usuarios_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr>
				</table>




			</div>

			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   





				<center>
					<table width="60%" class="tabla_2" border="0" style="width:580px">
						<tr>
							<td class="fila1">Persona responsable de elementos en la Bodega</td>
						</tr>

					</table>


					<?php

					$selmax="SELECT * FROM responsable_bodega WHERE id_responsable=(SELECT MAX(id_responsable) FROM responsable_bodega)";
					$queryselmax=mysql_query($selmax,$conexion); while($fmax=mysql_fetch_array($queryselmax))
					{ $fechadesde=$fmax['fecha_desde']; $fechahasta=$fmax['fecha_hasta']; $nombre_resp=$fmax['nombres']; $apellidos_resp=$fmax['apellidos']; $cometnario=$fmax['resp_comentario'];} ?>

					<table width="60%" class="tabla_2" border="0" style="width:580px">
						<tr>
							<td colspan="4" class="fila1">datos del Funcionario</td>
						</tr>
						<tr>
							<td class="fila2">Responsable:</td>
							<td class="fila2"><strong><?php echo $nombre_resp . "  " .$apellidos_resp; ?></strong></td>
							<td class="fila2">Fecha Inicio:</td>
							<td class="fila2"><strong><?php echo  $fechadesde; ?><strong> </td>
						</tr>
						<tr>
							<td colspan="4" class="fila1">OPCIONES</td>
						</tr>
						<tr class="menuactivos2" align="center">

							<TD><a href="tabla_responsable_bodega.php"><img src="../imagenes/reintegro.png" title="Inventario " width="36" height="36"  />Ver listado</TD>


							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
							<TD><a href="cambiar_responsable_bodega.php"><img src="../imagenes/responsable.png" title="Inventario " width="36" height="36"  />Cambiar Responsable</TD>
							<?php } ?>
							<td class="fila2" colspan="2"></td>
						</table>

					</div>



					<?php 
					include ("../assets/footer.php");
					?>
					<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

<?php
//DESCRIPCION: VENTANA PARA CAMBIO DE RESPONSABLE DE BODEGA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">

		<style>	



			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cambiar responsable</title>





		<body>

			<div id="centro2"><table class="botonesfila" >
				<tr><td>  <a href="ajustes_menu.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
					<td><a href="responsable_bodega.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


					<div id="centro">

						<div id="div_bienvenido">
							<?php echo "Bienvenido"; ?> <BR/>
							<div id="div_usuarios">
								<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
							</div>
						</div> 
						<center>
							<table width="70%" class="tabla_2" border="0" style="width:580px">


								<tr>
									<td class="fila1">MODIFICAR RESPONSABLE DE BODEGA</td>
								</tr>
								<tr>
									
								</tr>

							</table>
							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

							<br>
							<form name="responsable" action="responsable_add.php" title="Cambio de responsable" method="post">

								<table width="70%" class="tabla_2" border="0" style="width:580px">
									<tr>
										<td colspan="7" class="fila1">DATOS DEL RESPONSABLE</td>
									</tr>
									<tr>
										<td class="fila2" colspan="1">FUNCIONARIO</td>
										<td class="fila2" colspan="3"><select name="idusuario">

											<?php
											$responsable=mysql_query("SELECT idusuario, nombres, apellidos  FROM usuarios WHERE (idpermiso='2') ORDER BY nombres");
											while($filsex=mysql_fetch_array($responsable))
											{
												?>
												<option value="<?php echo $filsex['idusuario'];?>"><?php echo $filsex['nombres']. " ".$filsex['apellidos'] ;?></option>
												<?php
											}
											?>
										</select></td>
									</tr>
									<td class="fila2">OBSERVACIONES:</td>
									<td colspan="3" class="fila2"><input type="text" class="textinput"  name="comentario" size="50"  onChange="MAY(this)" value="" required placeholder="Escribe tu comentario"/></td>

								</tr>
								<tr>
									<td class="fila2" colspan="3"><p align="center">
										<input type="submit" class="botonguardar" name="guardar_producto" title="Guardar producto" value="GUARDAR"/>
									</p></td>
								</table>
							</form></center>
							<?php } ?>
							<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
							include ("../assets/footer.php");
							?>
						</body>
						</html>

						<?php
					} else {
						header("location: ../403.php");
					}
					?>

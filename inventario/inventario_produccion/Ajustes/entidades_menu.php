<?php
//DESCRIPCION:MENU DE ENTIDADES PARA EL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Entidades</title>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />





	</head>

	<body>

		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="ajustes_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>




				<div id="centro">


					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   


					<center>
						<table width="50%" border="0">
							<tr>
								<td class="titulo"><center>ENTIDADES</td></center>

							</tr>
						</center>
					</table>
					<?php
					if (isset($_GET['add'])){ if ($_GET['add']==1){?>
					<div class="quitarok">
						<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Entidad creada correctamente!
					</div>

					<?php   }    } ?> 
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[identidad]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
		?>



		<?php  }	  ?>
	</center>
	<center>
	</table>
	<table class="menuactivosA" >
		<tr>
			<td>&nbsp;</td>
			<td > <a class="titulos_menu" href="entidad_new.php"> <img src="../imagenes/add.png" title="Nueva Entidad" width="38" height="38" alt="nuevo"  align="left" />Agregar Entidad</a></td>
			<td>&nbsp;</td>
		</tr>
	</table>

</center>
<center>

	<table border="0" class="tabla_2" style="width:100%"><center>
		<tr>
			<td  class="fila1">ID</td>
			<td  class="fila1">NOMBRE DE LA ENTIDAD</td>
			<td  class="fila1">NIT</td>

			<td  class="fila1">DIRECCION</td>
			<td  class="fila1">TELEFONO</td>

			<td  class="fila1">CORREO</td>

			<td  class="fila1">ACCIONES</td>
		</tr>
		<?php


		$t_entidades=mysql_query("SELECT * FROM entidades WHERE entiactivo='1'", $conexion);
//echo $t_clientes;

		?>	

		<?php
		while ($fila_entidades=mysql_fetch_array($t_entidades))
		{

			?>

			<tr>
				<td class="fila2"><?php echo $fila_entidades["identidad"];?></td>
				<td class="fila2"><?php echo $fila_entidades["entidad"];?></td>
				<td class="fila2"><?php echo $fila_entidades["nitentidad"];?></td>
				<td class="fila2"><?php echo $fila_entidades["direccion_entidad"];?></td>
				<td class="fila2"><?php echo $fila_entidades["telefono_entidad"];?></td>
				<td class="fila2"><?php echo $fila_entidades["correo_entidad"];?></td>

				<?php

				?></center></td>
				<td class="fila2"><center>
					<a href="entidad_modificar.php?id_entidad=<?php echo $fila_entidades['identidad'];?>"><img src="../imagenes/editar1.png" title="Modificar cliente" width="28" height="28"/></a>
				</center></td>
				<?php

			}
			?> 
		</tr>
		<tr>
			<td colspan="14" class="fila2"><center>

			</center> </td>	
		</tr>
	</table>

</br>


</center>

</div>
<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

</body>
</html>


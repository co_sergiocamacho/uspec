<?php
session_start();
//DESCRIPCION: VETNANA PARA CREAR  NUEVA ENTIDAD POR PARTE DEL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/encabezado.php");

	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
		<script language="javascript">

			var cursor;
			if (document.all) {
// Está utilizando EXPLORER
cursor='hand';
} else {
// Está utilizando MOZILLA/NETSCAPE
cursor='pointer';
}

var miPopup
function nueva_categoria(){
	miPopup = window.open("categoria_new.php","miwin","width=900,height=380,scrollbars=yes");
	miPopup.focus();
}
</script>

<style>	
	<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
	body {
		background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		background-size: cover;
	}
	.container > header h1,
	.container > header h2 {
		color: #fff;
		text-shadow: 0 1px 1px rgba(0,0,0,0.7);
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Entidad</title>



<?php 
//include('menu.php');

?>
</head>
<script src="js/calendario/src/js/jscal2.js"></script>
<script src="js/calendario/src/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

<body>

	<div id="centro2">
		<table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="entidades_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr>
			</table>




		</div>

		<div id="centro">
			<div id="div_bienvenido">
				<?php echo "Bienvenido"; ?> <BR/>
				<div id="div_usuarios">
					<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
				</div>
				<?php echo "SALIR";?>
				'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
			</div>   





			<center>
				<table width="60%" class="tabla_2" border="0" style="width:580px">
					<tr>
						<td class="fila1">NUEVA ENTIDAD</td>
					</tr>
					<tr>
						<td class="fila2"> Permite el ingreso una nueva Entidad al sistema.</td>
					</tr>
				</table>

				<br>
				<form name="productos" action="entidad_guardar.php" title="Nuevo producto" method="post">
					<table width="60%" class="tabla_2" border="0" style="width:580px">
						<tr>
							<td colspan="5" class="fila1">&nbsp;</td>
						</tr>
						<tr>
							<td class="fila2">ENTIDAD</td>
							<td class="fila2" colspan="3"><input type="text" class="textinput"  name="entidad" size="150" maxlength="80"  onChange="MAY(this)" value=""  required/></td>

						</tr>

						<tr>
							<td class="fila2">NIT</td>
							<td class="fila2"><input type="text" class="textinput"  name="nit" size="80" maxlength="80" onChange="MAY(this)" value="" required/></td>
							<td class="fila2">EMAIL</td>
							<td class="fila2"><input type="text" class="textinput"  name="email" size="50" maxlength="80" value=""   required/></td>

						</tr>
						<tr>
							<td class="fila2">DIRECCION:</td>
							<td class="fila2"><input type="text" class="textinput"  name="dir" size="80" maxlength="80" onChange="MAY(this)" value="" /></td>
							<td class="fila2">TELEFONO:</td>
							<td class="fila2"><input type="text" class="textinput"  name="tel" size="20" maxlength="60" onChange="MAY(this)" value="" required/></td>

							<tr>
								<td class="fila2"><input type="hidden" name="identidad" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
								<td class="fila2"><p align="center">
									<input type="submit" class="botonguardar" name="guardar_producto" title="Guardar producto" value="GUARDAR"/>
								</p></td>

								<td class="fila2" colspan="2"><p align="center">

									<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="CANCELAR"/></a>	
								</td>
							</tr>
						</table>
					</form>
				</center>
			</div>
		</body>
		</html>
		<?php

		include ("../assets/footer.php");
		?>
		<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

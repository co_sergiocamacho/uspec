<?php


//DESCRIPCION: VENTANA PRINCIPAL PARA ADMINISTRADORES AJUSTES DEL PROGRAMA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//include("../assets/global.php");

	$_SESSION['idpermiso'];
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">



		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Principal</title>
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	

			body {
				background: #eeeeee no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;}
				#div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;	}
				.container > header h1,
				.container > header h2 {
					color: #fff;
					text-shadow: 0 1px 1px rgba(0,0,0,0.7);
				}

			</style>

<!--IDENTIFICACION USUARIO LOGEADO
<!--NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
	<!--FECHA: 2015-07-24	 -->

	<!--FIN IDENTIFICACION USUARIO LOGEADO -->

	<body>

		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td></tr></table></div>





			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="13" height="13" /></a>
				</div>   
				<center>
					<center><h1 CLASS="TITULO">      AJUSTES    </h1></center>
					<table class="menuprincipal" border="0">

						<tr class="menuactivos" align="center">
							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

							<TD><a href="categorias_menu.php"><img src="../imagenes/codigos.png" title="Menu de Elementos" width="36" height="36"  />Categorias contables</a></TD>
							<TD> <a href="dependencias_menu.php"><img src="../imagenes/dependencias.png" title="Inventario " width="36" height="36"  />Dependencias</a></TD>
							<TD><a class="titulos_menu" href="proveedores_menu.php"><img src="../imagenes/proveedores.png" title="PROVEEDORES" width="36" height="36"  />Proveedores</a></TD>

						</tr>
						<tr class="menuactivos2" align="center">
							<TD><a href="entidades_menu.php"><img src="../imagenes/entidades.png" title="Inventario " width="36" height="36"  />Entidades</TD>
							<TD><a href="unidades_menu.php"><img src="../imagenes/medida.png" title="Unidades de Medida" width="36" height="36"  />Unidades de Medida</TD>
							<TD><a class="titulos_menu" href="usuarios_menu.php"><img src="../imagenes/users.png" title="Inventario " width="36" height="36"  />Usuarios</a></TD>

						</tr>
						<tr class="menuactivos2" align="center">
							<TD><a href="Responsable_bodega.php"><img src="../imagenes/bodega.png" title="Inventario " width="36" height="36"  />Resp. de Bodega</TD>

							<?php }?>
						</table>

					</center>
				</div>
			</body>
			</html>

			<?php 
			include ("../assets/footer.php");
			?>
			<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

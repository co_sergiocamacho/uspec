<?php
//DESCRIPCION: VENTANA- NUEVA UNIDAD DE MEDIDA EN BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/encabezado.php");

	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>	
			<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
			body {
				background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Nueva Unidad</title>


		<?php 
//include('menu.php');

		?>
	</head>


	<body>

		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="unidades_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


				<div id="centro">


					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   

					<center>
						<table width="40%" class="tabla_2" style="width:480px">
							<tr>
								<td colspan="3" class="fila1">NUEVA UNIDAD</td>
							</tr>
							<form name="Unidades" action="unidad_guardar.php" title="Guardar Unidad" method="post">


								<tr>
									<td class="fila2" COLSPAN="1" ALIGN="CENTER">UNIDAD</td>    
									<td class="fila2"><input type="text" class="textinput"  name="unidadmedida" size="30" maxlength="80"  onChange="MAY(this)" value=""  required></td>
								</tr>
								<tr>

									<td class="fila2"><p align="center">
										<input type="submit" class="botonguardar" name="guardar_unidad" title="Guardar Unidad" value="GUARDAR"/>
									</p></td>


									<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
									<td class="fila2"><p align="center"><input type="submit" class="botoncancelar" name="cancelar" title="Cancelar Unidad" value="cancelar"/></a>  
									</td>

									<tr>

										<tr>
										</table>

									</body>
									</html>
									<?php

									include ("../assets/footer.php");
									?>
									<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>


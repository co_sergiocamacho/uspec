<?php

//DESCRIPCION:  REGISTRO DE ACTIVIDAD EN BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
require("../pdf/fpdf.php");
include ("../assets/datosgenerales.php");
include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------



$justificacion="Descarga de PDF de REGISTRO DE ACTIVIDADES"."-  permiso usuario: " .$_SESSION['idpermiso'];
$observacionregistro="Registro actividad". "Acceso por IP:".$ip;
$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
$fecha=date ("Y-m-d");
$hora=date("H:i:s");
$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
$queryreg2=mysql_query($registro2,$conexion);


$fecha=date('Y-m-d');
class PDF extends FPDF
{
//Page header
	function Header()
	{
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(185, -5, "    REGISTRO DEL SISTEMA - 	Control Super Admin", 0, 0, 'C');
$this->Ln(5);
$this->SetFont('Helvetica','',10);
$this->Cell(185, -5, "Elaborado por: Andres Montealegre", 0, 0, 'C');
$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,160,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);
$this->Line(10, 35 , 200, 35); 
}


function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',7);
    //Pagina numero
	$this->Cell(0,10,"Control Super Admin de registro - ".'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}


$pdf=new PDF();
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(30);

//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(40, 5, "FECHA DE REPORTE: ", 0);
$pdf->Cell(50, 5, $fecha,0 , 'C');
$pdf->Cell(30, 5, "USUARIO : ", 0);
$pdf->Cell(60, 5, $usuarioregistro , 0);

$pdf->Ln(7);




//-------------------COMENTARIO----------------

$query1="SELECT  *  FROM registro  ORDER BY fecha"  ;
$t_entradas33=mysql_query($query1,$conexion);
$inicial=mysql_num_rows($t_entradas33);

if(!empty($inicial)){
//$pdf->setFillColor(100, 100, 100);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(60, 5, " ", 0, 'C');
$pdf->Cell(105 , 5, "             REGISTRO DE ACTIVIDADES", 0, 'C');
$pdf->Ln(5);  

$pdf->SetFont('Helvetica','B',8);
$pdf->Cell(10, 5, " No", 0, 'C');
$pdf->Cell(65, 5, " REGISTRO", 0, 'C');
$pdf->Cell(33, 5, " USUARIO ", 0, 'C');
$pdf->Cell(15, 5, "  FECHA ", 0, 'C');
$pdf->Cell(15, 5, " HORA", 0, 'C');
$pdf->Cell(50, 5, " OBSERVACION", 0, 'C');
$pdf->SetFont('Helvetica','',8, 'C');
$pdf->Ln(5);  

$pdf->SetFont('Helvetica','',8);


while ($fen=mysql_fetch_array($t_entradas33)){
//---------------------------------------------
	$y = $pdf->GetY();
	$pdf->SetFont('Helvetica','',6, 'C');
	$pdf->Cell(6, 3, $fen['idregistro'], 0, 'C');
	$pdf->MultiCell(65,3,utf8_encode($fen['registro']),0,'J'); 
	$y1=$pdf->GetY();
	$x1=$pdf->GetX();
	$pdf->SetXY(85,$y);
	
	$pdf->Line(10, $y-1, 210-10, $y-1);

	$pdf->Cell(35, 3, $fen['usuario'], 0, 'C');
	$pdf->Cell(14, 3, $fen['fecha'], 0, 'C');
	$pdf->Cell(13,3,$fen['hora'],0,'C'); 
	$pdf->Cell(55,3,$fen['observacion'],0,'C'); 
	$pdf->Ln(4);  
	$pdf->SetY($y1);
			$pdf->Ln(2); 	
			$y2 = $pdf->GetY();
			$y3 = $pdf->GetY();
			if ($y2>265){
				$pdf-> AddPage();
				}
	}


	$y1=$pdf->GetY();
	$x1=$pdf->GetX();

		$pdf->Line(10, $y+8, 210-10, $y+8);



$pdf->Ln(7);




$nombresalida="Registro"."$fecha".".pdf";
$pdf->Output($nombresalida,'D');


} 


else {
	$pdf->Ln(18);




$nombresalida="Registro"."$fecha".".pdf";
$pdf->Output($nombresalida,'D');

}

}
?>



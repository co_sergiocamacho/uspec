<?php
//DESCRIPCION: MENU PRINCIPAL DE LAS ENTRADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");

include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Menu Elementos</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />



</head>

<body>
	<div id="centro2">
		<table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td></tr>
		</table>
	</div>

	<div id="centro">
		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
			<?php echo "SALIR";?>
			'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
		</div>   


		<center>

			<table width="100%" border="0">
				<tr>
					<td colspan="11" class="titulo"><center>
						<STRONG> ENTRADAS DE ELEMENTOS ACTIVOS Y DE CONSUMO</STRONG>

					</center></td>
				</tr>

			</table>
			<?php
			if (isset($_GET['creado'])){ if ($_GET['creado']==5){?>
			<div class="quitarok">
				<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado la Entrada correctamente!
			</div>

			<?php   }    } ?> 

			<?php

			if (isset($_GET['borrado'])){   if ($_GET['borrado']==1){ ?>
			<div class="quitarok">
				<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha Borrado la Entrada de correctamente!
			</div>
			<?php   }  }	 ?>



			<table class="menu2"> 

				<tr class="menuactivos">
					<TD colspan="4"><a  class="titulos_menu" align="center"> <h3><center>Activos Devolutivos</center></h3></a></TD>
					<tr class="menuactivos">
						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3){?> 
						<TD><a  class="titulos_menu"href="entradas_add.php"><img src="../imagenes/add_file.png" title="Activos" width="64" height="64" align="LEFT" />Entrada de Activos</TD>
						<?php }?>


						<TD><a class="titulos_menu" href="anular_entradas.php?all=1"><img src="../imagenes/verentradas.png" title="Eliminar" width="64" height="64" align="left" />Ver Entradas de Activos</a></TD>

						<td><a  class="titulos_menu"href="../activos/editar_activos.php"><img src="../imagenes/documento.png" title="Editar" width="64" height="64" align="left" />Ver Activos</a></td>
                            <td><a  class="titulos_menu"></a></td>
						
					</tr>
					<tr class="menuconsumo">



						<TD colspan="4"><a  class="titulos_menu" align="center"> <h3><center>Elementos de consumo</center></h3></a></TD>
						<tr class="menuconsumo">
							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3){?> 
							<TD><a  class="titulos_menu" href="entrada_consumo_add.php"><img src="../imagenes/add_file.png" title="Activos" width="64" height="64" align="LEFT" />Entrada Elem. de consumo</td>
								<?php }?>
								<TD><a  class="titulos_menu"href="ver_entradas_consumo.php?all=1"><img src="../imagenes/verentradas.png" title="Eliminar" width="64" height="64" align="left" />Ver Entradas de Consumo</a></TD>
								
								<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3 ){?> 
								<td><a  class="titulos_menu" href="../consumo/consumo_menu.php"><img src="../imagenes/add.png" title="Eliminar" width="64" height="64" align="left" />Agregar elementos de consumo</a><?php ?></td>
								<?php }?>
								<?php if ($_SESSION['idpermiso']==1  or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3){?> 
								<td><a  class="titulos_menu" href="../consumo/consumo_menu.php"><img src="../imagenes/documento.png" title="Editar" width="64" height="64" align="left" />Ver elementos de consumo</a><?php ?></td>
								<?php }?>
							</tr>



							<TR class="menugral" >

								<TD colspan="4"><a  class="titulos_menu" align="center"> <h3><center>GENERAL</center></h3></a></TD>
								<TR class="menugral" >
									<?php
//<TD><a  class="titulos_menu"href="ver_todas_entradas.php"><img src="../imagenes/verentradas.png" title="Activos" width="64" height="64" align="LEFT" />Ver todas las entradas</TD>
									?>
									<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3 ){?> 
									<TD><a class="titulos_menu" href="../Reintegros/reintegros_menu.php"><img src="../imagenes/reintegro.png" title="Salidas" width="50" height="50"/>Reintegros</a></TD>
									<?php }?>
									<td><a  class="titulos_menu" href="entradas_anuladas.php?all=1"><img src="../imagenes/anuladas.png" title="Eliminar" width="64" height="64" align="left" />Entradas Anuladas</a><?php ?></td>
							
								<TD><a  class="titulos_menu"href="ver_entradas_incompletas.php?all=1"><img src="../imagenes/verentradas.png" title="Eliminar" width="64" height="64" align="left" />Ver Entradas sin Terminar</a></TD>


								</tr>

							</table>
						</table>
					</div>
				</body>
				</html>

				<?php 
				include ("../assets/footer.php");
				?>


    <?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

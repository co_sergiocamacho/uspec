<?php
//DESCRIPCION: VENTANA PARA VER DETALLES DE ENTRADAS DE CONSUMO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS

$numentrada=$_GET['entrada'];
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Anular Entradas</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />

</head>

<body>




	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="ver_entradas_consumo.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>



			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>  

				</div>
			</div>

		</div>
		<div id="centro">

			<table  width="95%" id="tabla_activos">
				<tH class="fila1" colspan="12" style="font-size:14px;">ENTRADA N°: <?PHP ECHO $numentrada;?> </tH>

				<TR>

					<th width="30px"class="fila1">ID</th>
					<th width="50px" class="fila1">ELEMENTO</th>
					<th width="30px" class="fila1">UNID. MEDIDA</th>
					<th class="fila1">CANTIDAD</th>
					<th class="fila1">FECHA</th>
					<th class="fila1">VALOR UNIT</th>
					<th class="fila1">VALOR TOTAL</th>
					<th width="30px" class="fila1">CODIGO CONTABLE</th>
					<th width="30px" class="fila1">CATEGORIA</th>
					<th width="70px" class="fila1">ENTRADA N°</td>
						<th class="fila1">PROVEEDOR</th>


						<th class="fila1" >OBSERVACIONES</th>
					</tR>
					<?php



//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
//INNER JOIN usuarios on productos.documentoid=usuarios.documentoid

//ENLAZAR NOMBRES Y APELLIDOS

//<th class="fila1"  >NOMBRES</th>
// <th class="fila1"  >APELLIDOS</th>
//<td class="fila2"><?php echo $fila_activos["nombres"]; cerrar PHP </td>
//     <td class="fila2"><?php echo $fila_activos["apellidos"]; CERRAR PHP </td> 


//L

//
					$sqlquery="SELECT * FROM tabla_aux_consumo_entradas  


					LEFT JOIN proveedores on tabla_aux_consumo_entradas.idproveedor_aux=proveedores.idproveedor
					LEFT JOIN consumo on tabla_aux_consumo_entradas.idelemento_aux=consumo.idelemento
					LEFT JOIN codigocontable on tabla_aux_consumo_entradas.codigocontable_aux=codigocontable.codigocontable
					LEFT JOIN unidadmedida on tabla_aux_consumo_entradas.unidadmedida_aux=unidadmedida.idunidadmedida

					WHERE numentrada_aux='$numentrada' 
					ORDER BY idelemento_aux

					";
					$t_consumo=mysql_query($sqlquery, $conexion);

					$num_registro=mysql_num_rows($t_consumo);
//if ($num_registro == 0)
// {

//mysql_close($conexion);
//exit();
//}

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09 
					$registros=55;
					$pagina=0;
//DECLARACION DE VARIABLE PARA PASAR EL PAGINADOR

					if(isset($_GET['num']))
					{
						$pagina= $_GET['num'];
						$inicio=(($pagina-1)* $registros);

					}
					else
					{
//SI NO DIGO Q ES LA PRIMERA PÁGINA
						$inicio=1;
					}

//FIN PAGINADOR

//CONSULTA PARA UBICAR EL PAGINADOR
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09  

					?>  

					<?php
					while ($fila_activos=mysql_fetch_array($t_consumo))
					{

						?>
						<tr>
							<td class="fila2"><?php echo $fila_activos["idelemento_aux"];?></td>
							<td class="fila2"><?php echo $fila_activos["elemento"];?></td>
							<td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
							<td class="fila2"><?php echo $fila_activos["cantidad_aux"];?></td>
							<td class="fila2"><?php echo $fila_activos["fecha_entrada_aux"];?></td>

							<td class="fila2" align="right">$<?php echo number_format($fila_activos["precioadqui"],2,',','.');?></td>
							<td class="fila2" align="right">$<?php echo number_format(($fila_activos["cantidad_aux"]*$fila_activos["precioadqui"]),2,',','.');?></td>
							<td class="fila2"><?php echo $fila_activos["codigocontable_aux"];?></td>
							<td class="fila2"><?php echo $fila_activos["codigodescripcion"];?></td>
							<td class="fila2"><?php echo $fila_activos["numentrada_aux"];?></td>
							<td class="fila2"><?php echo $fila_activos["proveedor"];?></td>        
							<td class="fila2"><?php echo $fila_activos["consobservaciones"];?></td>

							<?php
						}
						?> 

					</tr>
					<?php 

					$numentrada=$_GET['entrada'];
					$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_entradas WHERE numentrada_aux='$numentrada'");   
					$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
					$valortotal=$valortotal1["total"];

					?>
					<tr>
						<TD class="fila2"colspan="4"> &nbsp;</TD>
						<TD class="fila2"colspan="2"><strong>VALOR TOTAL</strong></TD>
						<td class="fila3" colspan="1" align="right">$<?php echo number_format($valortotal,2,',','.');?></td>
					</tr>
				</table>
			</center>
		</DIV>
	</body>
</div>

<?php

include ('../assets/footer.php');
?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
//VARIABLE PARA LA IDENTIFICACION DEL USUARIO
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
	include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
	if(!isset($_REQUEST['all'])){$fecha1=""; $fecha2="";}

	if(isset($_REQUEST['all'])){$all=1;}
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript" src="../js/dataTables.min.js"></script>
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/tablas.js"></script>
		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
		<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
		
		
		<script src="../js/calendario/src/js/jscal2.js"></script>
		<script src="../js/calendario/src/js/lang/en.js"></script>
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Ver Entradas</title>






	</head>

	<body>
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>



				<div id="centro">
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>

					<table width="100%" border="0">
						<tr>
							<td  class="titulo">
								<center><STRONG> DETALLES DE LA ENTRADA</STRONG></center>
							</td>
						</tr>
					</table>



					<form name="entradas" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						<CENTER>
							<TABLE>
								<tr>
									Ordenar por fecha
								</tr>
								<tr>
									<td class="fila2">DESDE</td>
									<td class="fila2"><input size="10" id="f_date1" name="f_date1" value="" /><button id="f_btn1">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>


	<td class="fila2">HASTA</td>
	<td class="fila2"><input size="10" id="f_date2" name="f_date2" value="" /><button id="f_btn2">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date2",
		trigger    : "f_btn2",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script></td>

	<td><input type="submit" class="botonver" name="submit" value="Ver"><br></td>
</tr>
</TABLE></CENTER>

<div class="container-full">
	<div class="table-responsive">
		<table   class=" table"  cellspacing="0" width="100%" >
			<thead>
				<tr>
					<th  class="fila1">ID</th>
					<th  class="fila1">ENTRADA N</th>
					<th  class="fila1">FECHA DE ENTRADA</th>
					<th  class="fila1">TIPO DE ELEMENTOS</th>
					<th  class="fila1">PROVEEDOR </th>
					<th  class="fila1">CANT ITEMS</th>
					<th  class="fila1">VALOR TOTAL</th>
					<th  class="fila1">ELABORADO POR</th>
					<th  class="fila1">COMENTARIO</th>
					<th  class="fila1">OBSERVACIONES</th>
					<th  class="fila1">DETALLES</th>
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3 ){?> 
					<th class="fila1">ACCIONES</th>
					<?php }?>
					<th class="fila1">PDF</th>
				</tr>
			</thead>
			<tbody id="myTable">
				<?php

				if(isset($_REQUEST['all'])){
					$fecha1=date("Y-m-d");
					$fecha2=date("Y-m-d");
					$query1="SELECT  * FROM entradas  
					LEFT  JOIN proveedores on entradas.idproveedor=proveedores.idproveedor   
					WHERE activo='1' AND tipoelementos='ACTIVOS DEVOLUTIVOS' AND entobservaciones<>'Entrada por reintegro' ORDER BY identrada  DESC"  ;
				}

 /*if (empty($fecha1) and empty($fecha2)){ 
$query1="SELECT  * FROM entradas  LEFT  JOIN proveedores on entradas.idproveedor=proveedores.idproveedor   WHERE activo='1' AND tipoelementos='ACTIVOS DEVOLUTIVOS' AND entobservaciones<>'Entrada por reintegro' ORDER BY identrada  DESC"  ;
} */

else {

	if(isset($_REQUEST['f_date1'])){

		$fecha1=$_REQUEST['f_date1'];
		$fecha2=$_REQUEST['f_date2'];
		$query1="SELECT  * FROM entradas  
		LEFT  JOIN proveedores on entradas.idproveedor=proveedores.idproveedor   
		WHERE activo='1' AND tipoelementos='ACTIVOS DEVOLUTIVOS' AND entobservaciones<>'Entrada por reintegro' 
		AND fecha>= '$fecha1' AND fecha<='$fecha2'  ORDER BY identrada  DESC";
	}
}
if (empty($fecha1) and empty($fecha2)){ 
	$query1="SELECT  * FROM entradas  LEFT  JOIN proveedores on entradas.idproveedor=proveedores.idproveedor   WHERE activo='1' AND tipoelementos='ACTIVOS DEVOLUTIVOS' AND entobservaciones<>'Entrada por reintegro' ORDER BY identrada  DESC"  ;
}

$t_entradas=mysql_query($query1,$conexion);
while ($fila_entradas=mysql_fetch_array($t_entradas)){
	?>
	<tr>
		<td class="fila2"><?php echo $fila_entradas["identrada"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["entrada"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["fecha"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["tipoelementos"];?></td>

		<td  class="fila2"><?php echo $fila_entradas["proveedor"];?></td>
		<td class="fila2" align="center"><?php echo $fila_entradas["numitems"];?></td>
		<td  class="fila2" align="right">$<?php echo number_format($fila_entradas["valortotal"],2,',','.');?></td>
		<td  class="fila2"><?php echo $fila_entradas["elaboradopor"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["comentario"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["entobservaciones"];?></td>
		


		<td class="fila2"><center><a href="detalles_entrada.php?entrada=<?php echo $fila_entradas['entrada'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a></center></td>


		<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

		<td class="fila2"><center>
			<a href="anular_entrada_mod.php?entrada=<?php echo $fila_entradas['entrada'];?>"><img src="../imagenes/ANULAR.png" title="Anular Entrada " width="28" height="28"/></a>
		</center></td>
		<?php }?>



		<td class="fila2"><center>
			<a href="../crear_pdf/crear_pdf_entrada_devolutivos.php?entrada=<?php echo $fila_entradas['entrada'];?>"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>
		</center></td>



	</tr>


	<?php
}
?>
</tbody>
</TABLE>


</div>
<div>
	<center>
		<ul class="pagination pagination-lg pager" id="myPager"></ul></center>
		<div class="col-md-12 text-center">
		</div>
		
	</div>
</div>
</div>
</div>




<?php
include ('../assets/footer.php');
?>

<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>




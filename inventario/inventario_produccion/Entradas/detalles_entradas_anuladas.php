<?php

//DESCRIPCION: VENTANA PARA VER LOS DETALLES DE ENTRADAS ANULADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//VARIABLE PARA LA IDENTIFICACION DEL USUARIO
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
$numentrada=$_GET['entrada'];
//echo $numentrada  ;
//echo "<br/>";
$tipo1="ACTIVOS DEVOLUTIVOS";
$tipo2="ELEMENTOS DE CONSUMO";

//***********************************************************************************************************
$querytipoent="SELECT tipoelementos FROM ENTRADAS WHERE entrada='$numentrada'";
$peticiontipo=mysql_query($querytipoent,$conexion );
while ($filatipoentrada=mysql_fetch_array($peticiontipo )){

	$tipoentrada=$filatipoentrada['tipoelementos'];
}

//***********************************************************************************************************

//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Anular Entradas</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />


</head>

<body>

	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="entradas_anuladas.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr></table></div>



			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>


				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG>DETALLE DE DE ELEMENTOS DE ENTRADAS ANULADAS</STRONG></center>
						</td>

					</tr>
				</table>


				<?php
//*************************************************************************************************************************************************************
//INSERTAR CODIGO PARA CLASIFICAR LOS TIPOS DE ENTRADAS ANULADAS

				if($tipoentrada==$tipo1){
					?>

					<table  width="95%" id="tabla_activos">

						<th width="30px"class="fila1"  >ID</th>
						<th class="fila1"  >ELEMENTO</th>
						<th width="30px" class="fila1"  >CATEGORIA</th>
						<th class="fila1"  >UNID MED</th>
						<th width="40px" class="fila1"  >CONDICION</th> 
						<th class="fila1"  >FECHA ENT</th>
						<th class="fila1"  >VALOR</th>
						<th width="30px" class="fila1"  >CODIGO CONTABLE</th>
						<th width="70px" class="fila1"  >ENTRADA N°</td>
							<th class="fila1"  >PROVEEDOR</th>
							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th  class="fila1"  >SERIE</th>
							<th class="fila1" >OBSERVACIONES</th>


							<?php
//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
//INNER JOIN usuarios on productos_anulados.documentoid=usuarios.documentoid

//ENLAZAR NOMBRES Y APELLIDOS

//<th class="fila1"  >NOMBRES</th>
// <th class="fila1"  >APELLIDOS</th>
//<td class="fila2"><?php echo $fila_activos["nombres"]; cerrar PHP </td>
//     <td class="fila2"><?php echo $fila_activos["apellidos"]; CERRAR PHP </td> 
							$sqlquery="SELECT * FROM productos_anulados 
							left JOIN condicion on productos_anulados.idcondicion=condicion.idcondicion 
							LEFT JOIN ubicacion on productos_anulados.idubicacion=ubicacion.id_ubicacion
							LEFT JOIN unidadmedida on productos_anulados.idunidadmedida=unidadmedida.idunidadmedida
							LEFT JOIN proveedores on productos_anulados.idproveedor=proveedores.idproveedor
							LEFT JOIN codigocontable on productos_anulados.codigocontable=codigocontable.codigocontable
							WHERE numentrada='$numentrada'

							ORDER BY idelemento
							";
							$t_activos=mysql_query($sqlquery, $conexion);

							$num_registro=mysql_num_rows($t_activos);
							if ($num_registro == 0)
							{

								mysql_close($conexion);
								exit();
							}
							?>  

							<?php
							while ($fila_activos=mysql_fetch_array($t_activos))
							{

								?>
								<tr>
									<td class="fila2"><?php echo $fila_activos["idelemento"];?></td>
									<td class="fila2"><?php echo $fila_activos["elemento"];?></td>
									<td class="fila2"><?php echo $fila_activos["codigocontable"]. " " .$fila_activos["codigodescripcion"];?></td>
									<td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
									<td class="fila2"><?php echo $fila_activos["condicion"];?></td>
									<td class="fila2"><?php echo $fila_activos["fechaing"];?></td>
									<td class="fila2">$<?php echo number_format($fila_activos["precioadqui"],2,',','.');?></td>
									<td class="fila2"><?php echo $fila_activos["codigocontable"];?></td>
									<td class="fila3"><?php echo $fila_activos["numentrada"];?></td>
									<td class="fila2"><?php echo $fila_activos["proveedor"];?></td>
									<td class="fila2"><?php echo $fila_activos["marca"];?></td>
									<td class="fila2"><?php echo $fila_activos["modelo"];?></td>
									<td class="fila2"><?php echo $fila_activos["serie"];?></td>

									<td class="fila2"><?php echo $fila_activos["probservaciones"];?></td>
									<?php      } ?>
								</tr>
								<?php 
								$numentrada=$_GET['entrada'];
								$querysuma = mysql_query("SELECT SUM(precioadqui) as total FROM productos_anulados WHERE numentrada='$numentrada'");   
								$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
								$valortotal=$valortotal1["total"];

								?>


								<tr>
									<TD class="fila2"colspan="4"> &nbsp;</TD>
									<TD class="fila2"colspan="2"><strong>VALOR TOTAL</strong></TD>
									<td class="fila3" colspan="1">$<?php echo number_format($valortotal,2,',','.');?></td>
								</tr>

							</table>

							<?PHP
//**********************************************************************************************************************************************
// CARGA DE CODIGO PARA ELEMENTOS DEVOLUTIVOS
						} else if($tipoentrada==$tipo2){
							?>


							<table border="0" class="tabla_2" >

								<tr >
									<p></p>

									<th class="fila1"  >ID</th>
									<th class="fila1"  >ELEMENTO</th>
									<th class="fila1"  >UNID MED</th>
									<th class="fila1"  >CANTIDAD</th>
									<th class="fila1"  >FECHA ENTRADA</th>
									<th class="fila1" width="70"  >VALOR UNIT </th>
									<th class="fila1" width="70"  >VALOR TOTAL </th>
									<th  class="fila1"  >COD. CONT.</th>
									<th  class="fila1"  >CATEGORIA</th>
									<th  class="fila1"  >ENTRADA N°</td>

										<?PHP
										$sqlquery="SELECT * FROM tabla_aux_consumo_entradas_anuladas

										LEFT JOIN consumo on tabla_aux_consumo_entradas_anuladas.idelemento_aux=consumo.idelemento
										LEFT JOIN unidadmedida on tabla_aux_consumo_entradas_anuladas.unidadmedida_aux=unidadmedida.idunidadmedida
										LEFT JOIN codigocontable on tabla_aux_consumo_entradas_anuladas.codigocontable_aux=codigocontable.codigocontable
										WHERE numentrada_aux='$numentrada' 
										ORDER BY idelemento_aux";

										$t_consumo=mysql_query($sqlquery, $conexion);
										while ($fila_consumo=mysql_fetch_array($t_consumo))
										{
											?>
											<tr>
												<td class="fila2"><?php echo $fila_consumo["idelemento_aux"];?></td>
												<td class="fila2"><?php echo $fila_consumo["elemento"];?></td>
												<td class="fila2"><?php echo $fila_consumo["unidadmedida"];?></td>
												<td class="fila2"><?php echo $fila_consumo["cantidad_aux"];?></td>
												<td class="fila2"><?php echo $fila_consumo["fecha_entrada_aux"];?></td>

												<td class="fila2" align="right">$<?php echo number_format($fila_consumo["precioadqui"],2,',','.');?></td>
												<td class="fila2" align="right">$<?php echo number_format(($fila_consumo["cantidad_aux"]*$fila_consumo["precioadqui"]),2,',','.');?></td>
												<td class="fila2"><?php echo $fila_consumo["codigocontable_aux"];?></td>
												<td class="fila2"><?php echo $fila_consumo["codigodescripcion"];?></td>
												<td class="fila2"><?php echo $fila_consumo["numentrada_aux"];?></td>
												<td class="fila2"><?php echo $fila_consumo["consobservaciones"];?></td>   
											</tr>
											<?php      } ?>

											<?php 

//valor total de la entrada anulada
											$numentrada=$_GET['entrada'];
											$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_entradas_anuladas WHERE numentrada_aux='$numentrada'");   
											$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
											$valortotal=$valortotal1["total"];

											?>


											<tr>
												<TD class="fila2"colspan="4"> &nbsp;</TD>
												<TD class="fila2"colspan="2"><strong>VALOR TOTAL</strong></TD>
												<td class="fila3" colspan="1" align="right">$<?php echo number_format($valortotal,2,',','.');?></td>
											</tr>


										</table>

										<?php

									}

									?>


								</DIV> 
							</DIV> 

						</DIV> 

					</body>

					<?php

					include("../assets/footer.php");
					?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

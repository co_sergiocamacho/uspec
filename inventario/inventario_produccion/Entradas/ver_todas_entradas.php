<?php
//DESCRIPCION: PERMITE VER TODAS LAS ENTRADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Ver Entradas</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />


</head>

<body>

	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>




			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> DETALLES DE LA ENTRADA</STRONG></center>
						</td>


					</tr>
				</table>


				<table border="0" class="tabla_2" >

					<tr >
						<p></p>

						<td  class="fila1">ID</td>
						<td  class="fila1">ENTRADA N</td>
						<td  class="fila1">FECHA DE ENTRADA</td>
						<td  class="fila1">TIPO DE ELEMENTOS</td>
						<td   class="fila1">PROVEEDOR </td>
						<td class="fila1">CANT ITEMS</td>
						<td  class="fila1">VALOR TOTAL</td>
						<td  class="fila1">ELABORADO POR</td>
						<td  class="fila1">COMENTARIO</td>
						<td  class="fila1">OBSERVACIONES</td>
						<td  class="fila1">DETALLES</td>



						<?php
						$query1="SELECT  * FROM entradas  LEFT  JOIN proveedores on entradas.idproveedor=proveedores.idproveedor   WHERE activo='1'  and entobservaciones<>'Entrada por reintegro'  ORDER BY  entrada DESC"  ;
						$t_entradas=mysql_query($query1,$conexion);

						while ($fila_entradas=mysql_fetch_array($t_entradas)){


							?>
							<tr>
								<td class="fila2"><?php echo $fila_entradas["identrada"];?></td>
								<td  class="fila2"><?php echo $fila_entradas["entrada"];?></td>
								<td  class="fila2"><?php echo $fila_entradas["fecha"];?></td>
								<td  class="fila2"><?php echo $fila_entradas["tipoelementos"];?></td>
								<td  class="fila2"><?php echo $fila_entradas["proveedor"];?></td>
								<td class="fila2"><?php echo $fila_entradas["numitems"];?></td>
								<td  class="fila2">$<?php echo number_format($fila_entradas["valortotal"],2,',','.');?></td>
								<td  class="fila2"><?php echo $fila_entradas["elaboradopor"];?></td>
								<td  class="fila2"><?php echo $fila_entradas["comentario"];?></td>
								<td  class="fila2"><?php echo $fila_entradas["entobservaciones"];?></td>
								<td class="fila2"><center>
									<a href="detalles_entrada_todas.php?entrada=<?php echo $fila_entradas['entrada'];?>&tipo=<?php echo $fila_entradas['tipoelementos'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
								</center></td>




								<?php }?>

							</tr>
						</table>





					</div>




					<?php
					include ("../assets/footer.php");
					?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

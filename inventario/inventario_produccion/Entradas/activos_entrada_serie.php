<<?php
//DESCRIPCION: MODIFICAR SERIE DE ACTIVO FIJO DE UNA ENTRADA -VENATA DE ADMIN
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");


	$idelemento=$_REQUEST['idelemento'];
	$entrada=$_REQUEST['entrada'];
	$consec=$_REQUEST['consec'];
	$anterior=$_REQUEST['anterior'];

	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Activos Modificar</title>

		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
		</div>   
		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<body>

		<div id="centro2">
			<table class="botonesfila" >
				<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
					<td><a href="editar_activos.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr>
				</table>
			</div>

			<div id="centro">

				<center>
					<table width="60%" class="tabla_2" border="0" style="width:580px">

						<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL ACTIVO A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24
						$rst_activos= mysql_query("SELECT * FROM productos WHERE idelemento=". $_REQUEST['idelemento'].";",$conexion);
						while($filactivos=mysql_fetch_array($rst_activos))
						{
							?>

							<tr>

							</table>

							<form name="activo_modificar" action="activo_update_serie.php?idelemento=<?php echo $_REQUEST["idelemento"];?>&entrada=<?php echo $entrada;?>&consec=<?php echo $consec;?>&anterior=<?php echo $anterior;?> " title="Nuevo Activo" method="post">
								<table width="90%" class="tabla_2" border="0" style="width:580px">
									<tr>
										<td colspan="7" class="fila1">DATOS DEL ACTIVO</td>


									</tr>
									<tr>
										<td class="fila2">ELEMENTO:</td>
										<td class="fila2" colspan="5" ><input type="text" class="textinput"  name="activo_elemento"  onChange="MAY(this)"  size="130" value="<?php echo $filactivos["elemento"] ?>"  /></td>

									</tr>
									<TR>

										<td class="fila2">ID CODIGO CONTABLE</td>
										<td class="fila2" colspan="1" ><select name="activo_codigocontable" >
											<option colspan="2"></option>
											<?php 
											$condicion=mysql_query("SELECT codigocontable, codigodescripcion FROM codigocontable WHERE (codactivo = '1') ORDER BY codigocontable asc ");
											while($filcondicion=mysql_fetch_array($condicion)){?>
											<option selected='1' value= "<?php echo $filcondicion['codigocontable'];?>"> <?php echo $filcondicion['codigocontable']. "  ". $filcondicion['codigodescripcion'] ;?> </option><?php  echo $filcondicion['codigodescripcion'];}?></select></td> 

											<td class="fila2">UNIDAD DE MEDIDA</td>

											<td class="fila2" colspan="3" ><select name="activo_unidadmedida" >
												<option selected="" value= ""colspan="2"></option>
												<?php
												$unidades=mysql_query("SELECT idunidadmedida, unidadmedida FROM unidadmedida WHERE (	unidactivo = '1') ORDER BY idunidadmedida desc",$conexion);
												while($filunidades=mysql_fetch_array($unidades))
													{?>
												<option selected='' value="<?php echo $filunidades['idunidadmedida'];?>" > <?php echo $filunidades['unidadmedida'];?> </option><?php echo $filunidades['unidadmedida'];} ?></select></td>


												<tr>
													<td class="fila2">MARCA:</td>
													<td class="fila2"><input type="text" class="textinput"  name="activo_marca" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $filactivos["marca"] ?>" /></td>
													<td class="fila2">MODELO:</td>
													<td class="fila2"><input type="text" class="textinput"  name="activo_modelo" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $filactivos["modelo"] ?>" /></td>
													<td class="fila2">SERIE:</td>
													<td class="fila2"><input type="text" class="textinput"  name="activo_serie" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $filactivos["serie"] ?>" /></td>
												</tr>
												<tr>
													<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
														<input type="hidden" name="idelemento" value="<?php echo "$_REQUEST[idelemento]"; ?>" /></td>
														<td class="fila2"><p align="center">
															<input type="submit" class="botonguardar" name="guardar" title="Guardar Activo" value="GUARDAR"/>


														</a> </td>
														<td class="fila2" colspan="4">       &nbsp;       </td>
													</form>

												</tr>

											</table>
										</center>

									</div>

								</body>
							</HTML>

							<?php

						}

						?>

						<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
						include ('../assets/footer.php');
						?>


						<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

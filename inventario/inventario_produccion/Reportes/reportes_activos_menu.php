
<?php 
session_start();
//DESCRIPCION: VENTANA PARA VISUALIZAR ELEMENTOS ACTIVOS DE LA UNIDAD
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");

?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<script type="text/javascript" src="../js/buscar.js"></script>

	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>

	<?php
// <script type="text/javascript" src="../js/tablas.js"></script>
	?>


	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>BUSQUEDA DE ACTIVOS</title>




	<script type="text/javascript">
		$(document).ready(function() {
			$(".search").keyup(function () {
				var searchTerm = $(".search").val();
				var listItem = $('.results tbody').children('tr');
				var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
				
				$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
					return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
				}
			});
				
				$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
					$(this).attr('visible','false');
				});

				$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
					$(this).attr('visible','true');
				});

				var jobCount = $('.results tbody tr[visible="true"]').length;
				$('.counter').text(jobCount + ' Elementos');

				if(jobCount == '0') {$('.no-result').show();}
				else {$('.no-result').hide();}
			});
		});

	</script>
</head>

<body>


	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
		</tr></table></div>






		<div id="centro">
			<div id="div_bienvenido">
				<?php echo "Bienvenido"; ?> <BR/>



				<div id="div_usuarios">
					<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
				</div>
				<?php echo "SALIR";?>
				<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
			</div>

<?php

$selmax="SELECT * FROM responsable_bodega WHERE id_responsable=(SELECT MAX(id_responsable) FROM responsable_bodega)";
$queryselmax=mysql_query($selmax,$conexion); while($fmax=mysql_fetch_array($queryselmax))
{ $fechadesde=$fmax['fecha_desde']; $fechahasta=$fmax['fecha_hasta']; $nombre_resp=$fmax['nombres']; $apellidos_resp=$fmax['apellidos']; $cometnario=$fmax['resp_comentario'];} ?>

<table  >
<tr>
<td class="bodega">Responsable de Bodega: </td>
<td ><?php echo $nombre_resp . " " . $apellidos_resp?>	 </td>
</tr>
</table>

			<center>
				<table width="65%" border="0">
					<tr>
						<td colspan="11" class="titulo"><center>
							BUSQUEDA DE ACTIVOS
						</center></td>
	
				</table>

				<?php
				?>

			</center>
	<?php

	?>

	<div id="centro">
		<div class="form-group pull-left">
			<h4>Escriba su busqueda aqui</h4>
			<input type="text" class="search form-control" placeholder="Consulta por cedula, id, serial, placa, entrada, salida etc..." size="80">
		</div>
		<br>
		<span class="counter pull-left"></span>
		<div class="container-full">
		<br>
			<div class="table-responsive " >
				<table   class=" table table-bordered results"  cellspacing="0" width="100%" >
					<thead>
							
							<?PHP 
							//<th class="fila1"  >ID</th> ?>
							<th class="fila1"  >ELEMENTO</th>
							<th class="fila1"  >UNID MED</th>
							<th  class="fila1"  >CONDICION</th> 
							<th  class="fila1"  >FECHA ENTRADA ORIGEN</th> 
							<th  class="fila1"  >ENTRADA ORIGEN</th> 	
							<th class="fila1"  >VALOR</th>
							<th class="fila1"  >CATEGORIA</th>
							<th class="fila1"  >FECHA ENTRADA</th>
							<th class="fila1"  >ENTRAD N°</td>
							<th class="fila1"  >FECHA SALIDA</th>
														<th  class="fila1"  >SALID N°</td>
							<th class="fila1"  >FECHA TRASL</th>
							<th class="fila1"  >TRASL N°</th>

							<th class="fila1"  >MARCA</th>
							<th class="fila1"  >MODELO</th>
							<th class="fila1"  >SERIE</th>
							<th class="fila1"  >PLACA</th>

			
	
							<th  class="fila1"  >DEPENDENCIA</hd>      
							<th class="fila1"  >DOC. ID</th>
							<th class="fila1"  >FUNCIONARIO</th>
							<th class="fila1"  >UBICACION</th>
							<th class="fila1"  >PROVEEDOR</th>
							<th class="fila1"  >CONTRATO</th>
							<th class="fila1"  >FACTURA</th>
							<th class="fila1" >OBSERVACIONES</th>
										
									</th>
								</thead>
								<tbody id="myTable2">


									<?php


//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
//Rutina: Si los campos estan Vacios Muestra todos los Activos



									$sqlquery="SELECT *  FROM productos 

									LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
									LEFT  JOIN condicion on productos.idcondicion=condicion.idcondicion
									LEFT  JOIN proveedores on productos.idproveedor=proveedores.idproveedor
									LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
									LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
									LEFT  JOIN CODIGOCONTABLE on productos.codigocontable=codigocontable.codigocontable
									LEFT JOIN  dependencias on productos.dependencia=dependencias.codigodependencia
									WHERE activo='1';
									";

//ECHO $sqlquery;
									$t_activos=mysql_query($sqlquery, $conexion);

									$total=mysql_num_rows($t_activos);

									while ($fila_activos=mysql_fetch_array($t_activos))
									{
										?>

										<tr>

												<?PHP  //<td class="fila2"><?php echo $fila_activos["idelemento"];?> <?PHP // </td>  ?>
<td class="fila2" width="35%"><?php echo $fila_activos["elemento"];?></td>
<td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
<td class="fila2"><?php echo $fila_activos["condicion"];?></td>
<td class="fila2"><?php echo $fila_activos["fecha_origen"];?></td>
<td class="fila2"><?php echo $fila_activos["numentrada_origen"];?></td>
<td class="fila2" align="right">$<?php echo number_format($fila_activos["precioadqui"],2,',','.');?></td>
<td class="fila2"><?php echo $fila_activos["codigocontable"] ." ".$fila_activos["codigodescripcion"];?></td>
<td class="fila2"><?php echo $fila_activos["fechaing"];?></td>
<td class="fila2"><?php echo $fila_activos["numentrada"];?></td>
<td class="fila2"><?php if ($fila_activos["fechaasig"]=='0000-00-00'){	echo "";} else{	echo $fila_activos["fechaasig"];}?></td>
<td class="fila2"><?php echo $fila_activos["numsalida"];?></td>
<td class="fila2"><?php $fechatrasl=$fila_activos["fechaultimanov"]; if($fechatrasl=='0000-00-00'){echo "";}else {echo $fechatrasl;}?></td>
<td class="fila2"><?php echo $fila_activos["numtraspaso"];?></td>
<td class="fila2"><?php echo $fila_activos["marca"];?></td>
<td class="fila2"><?php echo $fila_activos["modelo"];?></td>
<td class="fila2"><?php echo $fila_activos["serie"];?></td>
<td class="fila3" align="center"><?php echo $fila_activos["codebar"];?></td>

<td class="fila2"><?php echo  $fila_activos["codigodependencia"] ." ".$fila_activos["nombredependencia"];?></td>
<td class="fila2"><?php echo $fila_activos["documentoid"];?></td>
<td class="fila2"><?php echo $fila_activos["nombres"] ." ".$fila_activos["apellidos"] ;?></td>
<td class="fila2"><?php echo $fila_activos["nombreubicacion"];?></td>
<td class="fila2"><?php echo $fila_activos["proveedor"];?></td>
<td class="fila2"><?php echo $fila_activos["prcontrato"];?></td>
<td class="fila2"><?php echo $fila_activos["numfactura"];?></td>


<td class="fila2"><?php echo $fila_activos["probservaciones"];?></td>

											<?php  }

											?>  
										</tr>

									</tbody>
									<tr class="warning no-result">
										<td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
									</tr>
								</TABLE>
							</div>
							<center>
								<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
								<div class="col-md-12 text-center">

								</div>

							</div>
						</div>

					</center>
				</table>
			</div>
		</body>

		<?php
		include("../assets/footer.php");
		?>

		<?php
					} else {
						header("location: ../403.php");
					}
					?>


<?php
//DESCRIPCION: VENTANA PRINCIPAL DEL APLICATIVO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

// Verificar sesion Usuario y permiso
session_start();
//Verificación de sesion

if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("database/conexion.php");
include("encabezado.php");
//include("global.php");
$_SESSION['idpermiso'];
$idusuario=$_SESSION['idusuario'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="shortcut icon" href="imagenes/1.ico">


		<link href="css/paginacion.css" type="text/css" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Principal</title>
		<link href="css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	

			body {
				background: #eeeeee no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;}
				#div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;	}
				.container > header h1,
				.container > header h2 {
					color: #fff;
					text-shadow: 0 1px 1px rgba(0,0,0,0.7);
				}
			</style>
<!--IDENTIFICACION USUARIO LOGEADO
<!--NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
	<!--FECHA: 2015-07-24	 -->
	<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	<body>
		<header>  
			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"."   "; ?><a  href="cambio/usuario_clave.php?idusuario=<?php echo $idusuario;?>"><img src="imagenes/clave.png" title="Activos" width="15" height="15"  /> </a>
					<br >
					<?php echo "SALIR";?>
					<a href="index.php?exit=1"><img src="imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>
					



				</div>   
				<center>
					<table width="65%" border="0">
						<tr>
							<td colspan="11" class="titulo"><center>
								PRINCIPAL
							</center></td>
						</tr>

					</table>
				</center>
				<CENTER>
					<TABLE class="menuprincipal" width="55%" PADDING="50">
						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3  ){



							?> 
						<tr class="menuactivos" align="center"> 

							<TD><a class="titulos_menu" href="Entradas/elementos_menu.php"><img src="imagenes/entrada.png" title="Menu de Elementos" width="50" height="50"  />Entradas</TD>
							<TD><a class="titulos_menu" href="Salidas/salidas_menu.php"><img src="imagenes/inventario3.png" title="Salidas" width="50" height="50"/>Salidas</a></TD>
							<TD><a class="titulos_menu" href="Traslados/traslados_menu.php"><img src="imagenes/traspaso.png" title="Salidas" width="50" height="50"/>Traslados</a></TD>
							<TD><a class="titulos_menu" href="Reportes/inventario_menu.php"><img src="imagenes/inventario2.png" title="Salidas" width="50" height="50"/>Inventario</a></TD>
							
						</tr>
						<?php }?>

						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 or $_SESSION['idpermiso']==3  ){?> 

						<TR class="menuactivos" align="center">
							<TD><a class="titulos_menu" href="kardex/kardex_menu.php"><img src="imagenes/kardex2.png" title="Inventario " width="50" height="50"  />KARDEX</a></TD>
							<TD><a  class="titulos_menu"href="Reportes/reportes_activos_menu.php"><img src="imagenes/activos1.png" title="Activos" width="50" height="50"  />Consulta de Activos </a></TD>
							

							<TD><a  class="titulos_menu" href="consumo/consumo_menu.php"><img src="imagenes/consumo.png" title="Activos" width="50" height="50"  />Elem. de Consumo</a></TD>
						<TD><a  class="titulos_menu" href="reportes/reportes_menu.php"><img src="imagenes/boletin.png" title="Activos" width="50" height="50"  />Reportes</a></TD>
			
							<?php //<TD><a  class="titulos_menu" href="reportes/historial_Menu.php"><img src="imagenes/historial.png" title="Activos" width="50" height="50"/>Historial</a></TD> ?>
							<?php 
//<TD><a  class="titulos_menu" href="principal.php"><img src="imagenes/bajas.png" title="Activos" width="50" height="50"  />Bajas</a></TD>
							?>

							<?php }?>

						</TR>

						<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2){?> 
						<tr class="menuactivos" align="center"> 
							

							<td><a class="titulos_menu" href="ajustes/ajustes_menu.php"><img src="imagenes/ajustes.png" title="Editar" width="50" height="50"  />Ajustes</a></td>
								<?php }?>
								<?php if ($_SESSION['idpermiso']==1 ){?> 
							<td><a class="titulos_menu" href="ajustes/registro.php"><img src="imagenes/log.png" title="Editar" width="50" height="50"  />Registro</a></td>
							<?php }?>
							<TD><a  class="titulos_menu" href="#"></a></TD>
						
</tr>

						</center>
					</table>
				</div>
				
			</body>
        
       	
			</html>

			<?php 
			include ("footer.php");
			?>







<?php
/*
@Cerrar Sesion
*/
} else {
header("location: 403.php");
}
?>
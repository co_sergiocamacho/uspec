<?php
//DESCRIPCION: VENTANA DE ERROR 403-PAGINA ERROR, O NO HAY CUENTA CON SESION INICIADA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js sidebar-large">
<!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>Usuario no autorizado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
   <link rel="shortcut icon" href="imagenes/1.ico">
    


    <script type="text/javascript" src="/js/dataTables.min.js"></script>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/tablas.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <link  type="text/css" href="/css/dataTables.min.css"  rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/datatables.css"  >
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">

    <script src="/js/calendario/src/js/jscal2.js"></script>
    <script src="/js/calendario/src/js/lang/en.js"></script>

    <link href="/css/estilos.css" type="text/css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/js/calendario/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="/js/calendario/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="/js/calendario/src/css/steel/steel.css" />


        <link href="css/paginacion.css" type="text/css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Principal</title>
        <link href="css/estilos.css" rel="stylesheet" type="text/css" />
        <style> 

            body {
        
background: #ffffff; /* Old browsers */
background: -moz-radial-gradient(center, ellipse cover,  #ffffff 14%, #bfbfbf 61%); /* FF3.6+ */
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(14%,#ffffff), color-stop(61%,#bfbfbf)); /* Chrome,Safari4+ */
background: -webkit-radial-gradient(center, ellipse cover,  #ffffff 14%,#bfbfbf 61%); /* Chrome10+,Safari5.1+ */
background: -o-radial-gradient(center, ellipse cover,  #ffffff 14%,#bfbfbf 61%); /* Opera 12+ */
background: -ms-radial-gradient(center, ellipse cover,  #ffffff 14%,#bfbfbf 61%); /* IE10+ */
background: radial-gradient(ellipse at center,  #ffffff 14%,#bfbfbf 61%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#bfbfbf',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */



                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;}
                #div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px; }
                .container > header h1,
                .container > header h2 {
                    color: #fff;
                    text-shadow: 0 1px 1px rgba(0,0,0,0.7);
                }
            </style>
    <!-- END  MANDATORY STYLE -->
</head>

<body class="error-page">
<center>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-offset-1 col-xs-10">
            <div class="error-container">
                <div class="error-main">

                    <img class="oops2" src="imagenes/logos/logo1.png">

                    <h1 class="oops"> Oops.</h1>
                    <h3> Lo sentimos, no se encuentra autorizado para ingresar a esta página. </h3>
                    <h4> Por favor <a href="index.php">Inicie sesión</a>, o comuníquese  </h4>
					<h4> con el administrador para solucionar el problema.</h4>
                    <h4> </h4>
                    <br>
                    </div>
                    </div>
                    </div>
                    </div>
                    </center>
                    </body>

</body>

<?php
$ip=$_SERVER['REMOTE_ADDR'];
include("database/conexion.php");
$justificacion="Error 403 -Intento de Acceso no permitido";
$observacionregistro="Acceso No permitido mediante IP No: " .$ip ;
$fecha=date ("Y-m-d");
$hora=date("H:i:s");
$registro2="INSERT INTO registro (registro,  fecha, hora, observacion) VALUES ('$justificacion' ,   '$fecha' ,'$hora' ,'$observacionregistro' )   ";
$queryreg2=mysql_query($registro2,$conexion);

?>

     <?php
     include ("footer.php");

     ?>
    <!-- Placed at the end of the document so the pages load faster -->
  


</html>

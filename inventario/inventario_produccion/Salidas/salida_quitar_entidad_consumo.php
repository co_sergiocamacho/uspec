<?php
//DESCRIPCION: QUITA ELEMENTOS ACTIVOS DE LA SALIDA PARA ENTIDADES -ACTUALIZA BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");

// para quitar los elementos de la salida primero se debe comprobar las cantidades, devolverlas al sistema, luego borrar el item de la tabla aux de elem de salida

// 

//CREAR VARIABLES PARA RESTABLECER LAS CANTIDADES Y LUEVO ELIMINAR LA TABLA AUX CON EL ITEM..........
$anterior=$_GET['anterior'];
$idelemento=$_GET['idelemento']; //NUMERO DE SALIDA COMPLETO
$salidanum=$_GET['salidanum'];
$nitentidad=$_GET['nitentidad'];
$fechaasig="0000-00-00";
$idtabla_aux=$_REQUEST['idtabla_aux'];
//dependencia no requiere

echo "elemento ID ". $idelemento;echo "<br/>";

echo "CONSECUTIVO SALIDA " .$salidanum;	echo "<br/>";
echo "FECHA ASIGNACION ". $fechaasig;echo "<br/>";


echo "-------------------------------------------------------";echo "<br/>";

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//obtener Cantidad de elementos de la tabla de salida y guardarlos en una variable

$queryverificar="SELECT cantidad_aux FROM tabla_aux_consumo_salidas WHERE idelemento_aux='$idelemento' AND numsalida_aux='$salidanum' AND idtabla_aux='$idtabla_aux' ";
$pedirdatostabla=mysql_query($queryverificar,$conexion);

while($tabla_salida=mysql_fetch_array($pedirdatostabla)){

	$cantidadquitar=$cantidadquitar=$tabla_salida["cantidad_aux"];

}

echo "CANTIDAD PA QUITAR :". $cantidadquitar;
echo "<br/>";
echo "id tabla AUX";
echo "<br/>";
echo $idtabla_aux;
echo "<br/>";
echo "salida";

echo "<br/>";
echo $salidanum;

//devolver la cantidad de elementos a  Bodega

$querysumarabodega="UPDATE consumo SET cantidad=cantidad+'$cantidadquitar' WHERE idelemento='$idelemento'";
$pedirdatostabla=mysql_query($querysumarabodega,$conexion);


	//Borrar del Kardex
$queryborrar_kardex="DELETE FROM tabla_aux_kardex WHERE salida_kardex='$salidanum' AND idtabla_aux_ent_sal='$idtabla_aux'";
$pedirdatostabla=mysql_query($queryborrar_kardex,$conexion);


//Borrar de la tabla de salidas El item con el elemento Seleccionado
$queryborrardetabla="DELETE FROM tabla_aux_consumo_salidas WHERE numsalida_aux='$salidanum' AND idtabla_aux='$idtabla_aux'";
$pedirdatostabla=mysql_query($queryborrardetabla,$conexion);

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

header("location: salida_consumo_entidades_add.php?internalid=3&nitentidad=$nitentidad&salidanum=$salidanum&quitarok=4&anterior=$anterior");
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

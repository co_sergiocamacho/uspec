<?php
//DESCRIPCION:  VENTANA QUE MUESTRA LOS DETALLES DE LA SALIDA DE ELEMENTOS A ENTIDADES
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link href="../css/paginacion.css" type="text/css" rel="stylesheet">
  <link href="../css/styles.css" type="text/css" rel="stylesheet">
  <link href="../css/estilos.css" type="text/css" rel="stylesheet">
  <link rel="shortcut icon" href="../imagenes/1.ico">
  <style> 

    body {
      background: #eaeaea no-repeat center top;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
    }
    .container > header h1,
    .container > header h2 {
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,0.7);
    }
    
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Anular Entradas</title>
  <link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
  


</head>

<body>
  <div id="centro2"><table class="botonesfila" >
    <tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
      <td><a href="salidas_activos_creadas.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>

      <div id="centro">
        <div id="div_bienvenido">
          <?php echo "Bienvenido"; ?> <BR/>
          <div id="div_usuarios">
            <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
          </div>
          <?php echo "SALIR";?>
          '<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
        </div>

        <table width="100%" border="0">
          <tr>
            <td  class="titulo">
              <center><STRONG> DETALLES DE LAS SALIDAS</STRONG></center>
            </td>


          </tr>

          <?php

          $salida=$_GET['salida'];

          ?>



        </table>


        <table  width="90%" id="tabla_activos">
         
          <th class="fila1"  >ID</th>
          <th class="fila1"  >ELEMENTO</th>
          <th class="fila1"  >UNID MED</th>
          <th class="fila1"  >FECHA ENT</th>
          <th class="fila1" width="70"  >VALOR</th>
          <th  class="fila1"  >COD. CONT.</th>
          <th  class="fila1"  >ENTRADA N°</td>
            <th  class="fila1"  >SALIDA N°</td>
              <th class="fila1"  >PROVEEDOR</th>
              <th class="fila1"  >MARCA</th>
              <th class="fila1"  >MODELO</th>
              <th  class="fila1"  >SERIE</th>
              
              <th class="fila1"  >NIT</th>

              <th class="fila1"  >UBIC.</th>
              <th class="fila1" >OBSERVACIONES</th>

              <?php

              $numsalida=$_GET['salida'];
              
    //CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
    //NOMBRE: Andres Montealegre Giraldo
    //FECHA: 2015-01-09     
  //INNER JOIN usuarios on productos.documentoid=usuarios.documentoid

        //ENLAZAR NOMBRES Y APELLIDOS

        //<th class="fila1"  >NOMBRES</th>
        // <th class="fila1"  >APELLIDOS</th>
        //<td class="fila2"><?php echo $fila_activos["nombres"]; cerrar PHP </td>
        //     <td class="fila2"><?php echo $fila_activos["apellidos"]; CERRAR PHP </td> 
              $sqlquery="SELECT * FROM productos  
              left JOIN condicion on productos.idcondicion=condicion.idcondicion 
              LEFT JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
              LEFT JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
              LEFT JOIN proveedores on productos.idproveedor=proveedores.idproveedor
              LEFT JOIN  entidades on productos.documentoid=entidades.nitentidad
              WHERE numsalida='$numsalida'

              ORDER BY idelemento ";
              $t_activos=mysql_query($sqlquery, $conexion);
              

  //if ($num_registro == 0)
 // {
              
  //mysql_close($conexion);
  //exit();
//}

              while ($fila_activos=mysql_fetch_array($t_activos))
              {

                ?>
                <tr>
                  <td class="fila2"><?php echo $fila_activos["idelemento"];?></td>
                  <td class="fila2" width="150"><?php echo $fila_activos["elemento"];?></td>
                  <td class="fila2"><?php echo $fila_activos["unidadmedida"];?></td>
                  <td class="fila2"><?php echo $fila_activos["fechaing"];?></td>
                  <td class="fila2" align="right">$<?php echo number_format($fila_activos["precioadqui"],2,',','.');?></td>
                  <td class="fila2"><?php echo $fila_activos["codigocontable"];?></td>
                  <td class="fila2" align="center" ><?php echo $fila_activos["numentrada"];?></td>
                  <td class="fila2" align="center"><?php echo $fila_activos["numsalida"];?></td>
                  <td class="fila2"><?php echo $fila_activos["proveedor"];?></td>

                  <td class="fila2"><?php echo $fila_activos["marca"];?></td>
                  <td class="fila2"><?php echo $fila_activos["modelo"];?></td>
                  <td class="fila2"><?php echo $fila_activos["serie"];?></td>
                  
                  <td class="fila2"><?php echo $fila_activos["documentoid"];?></td>
                  <td class="fila2"><?php echo $fila_activos["entidad"];?></td>
                  <td class="fila2"><?php echo $fila_activos["nombreubicacion"];?></td>
                  <td class="fila2"><?php echo $fila_activos["probservaciones"];?></td>

                  
                  <?php
                  
                }
                ?> 




              </tr>

              <?php 

              $numentrada=$_GET['salida'];
              $querysuma = mysql_query("SELECT SUM(precioadqui) as total FROM productos WHERE numsalida='$numsalida'");   
              $valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
              $valortotal=$valortotal1["total"];

              ?>

              
              <tr>
                <TD class="fila2"colspan="2"> &nbsp;</TD>
                <TD class="fila2"colspan="2"><strong>VALOR TOTAL</strong></TD>
                <td class="fila3" colspan="1" align="right">$<?php echo number_format($valortotal,2,',','.');?></td>
                <TD class="fila2"colspan="14"> &nbsp;</TD>
              </tr>


              
            </table>
          </center>
        </DIV>
      </body>




      <?php

      include ("../assets/footer.php");
      ?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

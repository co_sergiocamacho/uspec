<?php
//DESCRIPCION: VENTANA PARA VISUALIZAR LAS SALIDAS  GENERADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>SALIDAS GENERADAS</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />

</head>

<body>

	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="salidas_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr></table></div>


			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> DETALLES DE LAS SALIDAS DE ACTIVOS</STRONG></center>
						</td>


					</tr>
				</table>


				<table width="100%" border="0">
					<tr>
						<td  class="subtitulo">
							<center><STRONG> Salidas de Activos devolutivos a Usuarios</STRONG></center>
						</td>

						<?php
						$query1="SELECT  * FROM salidas  

						LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
						LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia

						WHERE salactivo='1'  AND tiposalida='Salida de Bienes Devolutivos' ORDER BY  salida DESC"  ;
						$t_salidas=mysql_query($query1,$conexion);


						?>
						<table border="0" class="tabla_2" >
							<td  class="fila1">ID</td>
							<td  class="fila1">SALIDA N</td>
							<td  class="fila1">FECHA DE SALIDA</td>
							<td   class="fila1">DEPENDENCIA</td>
							<td class="fila1">DOCUMENTO ID</td>
							<td  class="fila1">NOMBRES </td>
							<td  class="fila1">APELLIDOS</td>
							<td  class="fila1">CANT ITEMS</td>
							<td  class="fila1">VALOR TOTAL</td>
							<td  class="fila1">ELABORO</td>
							<td  class="fila1">COMENTARIOS</td>
							<td class="fila1">OBSERVACIONES</td>
							<td class="fila1">DETALLES</td>
							<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
							<td class="fila1">ELIMINAR</td>

							<?php }?>
							<tr>
								<?php
								while ($Fila_salidas=mysql_fetch_array($t_salidas)){
									?>

									<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
									<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
									<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
									<td  class="fila2"><?php echo $Fila_salidas["nombredependencia"];?></td>
									<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
									<td class="fila2"><?php echo $Fila_salidas["nombres"];?></td>
									<td class="fila2"><?php echo $Fila_salidas["apellidos"];?></td>
									<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
									<td  class="fila2" align="right">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
									<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>
									<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
									<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
									<td class="fila2"><center>
										<a href="detalles_salida.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
									</center></td>

									<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
									<td class="fila2"><center>
										<a href="anular_salida_entidad_mod.php?salida=<?php echo $Fila_salidas['salida'];?>&tiposalida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
									</center></td>
								</tr>

								<?php }?>
								<?php
							}

							?>

						</table>






						<table width="100%" border="0">
							<tr>
								<td  class="subtitulo">
									<center><STRONG> Salidas de Activos a Otras entidades</STRONG></center>
								</td>

								<DIV>

									<?php
									$query1="SELECT  * FROM salidas  

									LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad
									WHERE salactivo='1'  AND tiposalida='Salida activos a otra Entidad' ORDER BY  salida DESC"  ;
									$t_salidas=mysql_query($query1,$conexion);
									?>
									<table border="0" class="tabla_2" >
										<td  class="fila1">ID</td>
										<td  class="fila1">SALIDA N</td>
										<td  class="fila1">FECHA DE SALIDA</td>
										<td class="fila1">NIT ENTIDAD</td>
										<td  class="fila1">ENTIDAD </td>
										<td  class="fila1">CANT ITEMS</td>
										<td  class="fila1">VALOR TOTAL</td>
										<td  class="fila1">ELABORO</td>

										<td  class="fila1">COMENTARIOS</td>
										<td class="fila1">OBSERVACIONES</td>
										<td class="fila1">DETALLES</td>
										<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
										<td class="fila1">ELIMINAR</td>

										<?php }?>
										<tr>


											<?php
											while ($Fila_salidas=mysql_fetch_array($t_salidas)){

												?>
												<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
												<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
												<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
												<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
												<td  class="fila2"><?php echo $Fila_salidas["entidad"];?></td>
												<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
												<td  class="fila2" align="right">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
												<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>

												<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
												<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
												<td class="fila2"><center>
													<a href="detalles_salida_entidad.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
												</center></td>


												<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
												<td class="fila2"><center>
													<a href="anular_salida_mod.php?salida=<?php echo $Fila_salidas['salida'];?>&tiposalida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
												</center></td>
											</tr>
											<?php }?>

											<tr>

												<?php } ?>
											</table>




											<table width="100%" border="0">
												<tr>
													<td  class="subtitulo">
														<center><STRONG> Salida de elementos de consumo a Usuarios</STRONG></center>
													</td>


												</tr>
											</table>




											<table border="0" class="tabla_2" >

												<tr >
													<p></p>

													<td  class="fila1">ID</td>
													<td  class="fila1">SALIDA N</td>
													<td  class="fila1">FECHA DE SALIDA</td>
													<td   class="fila1">DEPENDENCIA</td>
													<td class="fila1">DOCUMENTO ID</td>
													<td  class="fila1">NOMBRES </td>
													<td  class="fila1">APELLIDOS</td>
													<td  class="fila1">CANT ITEMS</td>
													<td  class="fila1">VALOR TOTAL</td>
													<td  class="fila1">ELABORO</td>

													<td  class="fila1">COMENTARIOS</td>
													<td class="fila1">OBSERVACIONES</td>
													<td class="fila1">DETALLES</td>
													<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?>
													<td class="fila1">ELIMINAR</td>
													<?php } ?>


													<?php
													$query1="SELECT  * FROM salidas  

													LEFT  JOIN usuarios on salidas.documentoid=usuarios.documentoid
													LEFT  JOIN dependencias on salidas.dependencia=dependencias.codigodependencia




													WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo' ORDER BY  salida DESC"  ;
													$t_salidas=mysql_query($query1,$conexion);

													while ($Fila_salidas=mysql_fetch_array($t_salidas)){

														?>
														<tr>
															<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
															<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
															<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
															<td  class="fila2"><?php echo $Fila_salidas["nombredependencia"];?></td>
															<td class="fila2"><?php echo $Fila_salidas["documentoid"];?></td>
															<td class="fila2"><?php echo $Fila_salidas["nombres"];?></td>
															<td class="fila2"><?php echo $Fila_salidas["apellidos"];?></td>
															<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
															<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
															<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>

															<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
															<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
															<td class="fila2"><center>
																<a href="detalles_salida_consumo.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
															</center></td>

															<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?>

															<td class="fila2"><center>
																<a href="anular_salida_consumo1.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
															</center></td>

															<?php } ?>



															<?php
														}

														?>
													</tr>
												</table>


												<table width="100%" border="0">
													<tr>
														<td  class="subtitulo">
															<center><STRONG> Salida de elementos de consumo a otras entidades</STRONG></center>
														</td>


													</tr>
												</table>

												<table border="0" class="tabla_2" >

													<tr >
														<p></p>

														<td  class="fila1">ID</td>
														<td  class="fila1">SALIDA N</td>
														<td  class="fila1">FECHA DE SALIDA</td>
														<td  class="fila1">ENTIDAD</td>
														<td class="fila1">NIT</td>
														<td class="fila1">DIRECCIÓN</td>
														<td  class="fila1">TELÉFONO</td>
														<td  class="fila1">CANT ITEMS</td>
														<td  class="fila1">VALOR TOTAL</td>
														<td  class="fila1">ELABORO</td>
														<td  class="fila1">COMENTARIOS</td>
														<td class="fila1">OBSERVACIONES</td>
														<td class="fila1">DETALLES</td>
														<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?>
														<td class="fila1">ELIMINAR</td>
														<?php } ?>

														<?php
														$query1="SELECT  * FROM salidas  

														LEFT  JOIN entidades on salidas.documentoid=entidades.nitentidad


														WHERE salactivo='1' AND tiposalida='Salida de Elementos de consumo a otra Entidad' ORDER BY  salida DESC"  ;
														$t_salidas=mysql_query($query1,$conexion);

														while ($Fila_salidas=mysql_fetch_array($t_salidas)){


															?>
															<tr>
																<td class="fila2"><?php echo $Fila_salidas["idsalida"];?></td>
																<td  class="fila2"><?php echo $Fila_salidas["salida"];?></td>
																<td  class="fila2"><?php echo $Fila_salidas["fechasalida"];?></td>
																<td class="fila2"><?php echo $Fila_salidas["entidad"];?></td>
																<td class="fila2"><?php echo $Fila_salidas["nitentidad"];?></td>
																<td class="fila2"><?php echo $Fila_salidas["direccion_entidad"];?></td>
																<td class="fila2"><?php echo $Fila_salidas["telefono_entidad"];?></td>
																<td class="fila2"><?php echo $Fila_salidas["numitems"];?></td>
																<td  class="fila2">$<?php echo number_format($Fila_salidas["valortotal"],2,',','.');?></td>
																<td  class="fila2"><?php echo $Fila_salidas["elaboradopor"];?></td>

																<td  class="fila2"><?php echo $Fila_salidas["salcomentarios"];?></td>
																<td  class="fila2"><?php echo $Fila_salidas["salobservaciones"];?></td>
																<td class="fila2"><center>
																	<a href="detalles_salida_entidad_consumo.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>
																</center></td>

																<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?>
																<td class="fila2"><center>
																	<a href="anular_salida_consumo1.php?salida=<?php echo $Fila_salidas['salida'];?>"><img src="../imagenes/ANULAR.png" title="Anular Salida " width="28" height="28"/></a>
																</center></td>
																<?php } ?>




																<?php
															}

															?>
														</tr>
													</table>
												</div>




												<?php
												include ("../assets/footer.php");
												?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

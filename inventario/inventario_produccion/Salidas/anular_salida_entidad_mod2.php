<?php
//DESCRIPCION:  METODO PARA ANULAR SALIDAS PARA ENTIDADES  VENTANA2
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
$comentario=$_POST["comentario"];

$salida_a_borrar=$_REQUEST["salida"];
$txt_obs= "Anulada el dia: ";
$fecha_anulada= date('Y-m-d');
$hora= date('H:i:s');


// DATOS PARA INSERTAR Y ACTUALIZAR TODOS LOS ELEMENTOS QUE COMPONEN LA SALIDA Y REIGRESARLOS A LA BODEGA.......EL CODIGO DE BARRAS NO SE QUITA

$idubicacion='1';
$documentoid="";
$fechaasig="0000-00-00";
$numsalidaprod="";
$dependencia="";
$activo='1';

$observaciones=$comentario. " . " .$txt_obs.$fecha_anulada. " A las ".$hora;
echo "<br>";
echo $observaciones;
echo "<br>";
echo "Salida  a borrrar:  ".$salida_a_borrar;
/*
	 ***RUTINA PARA COPIAR DATOS DE LA TABLA DE PRODUCTOS A UNA TABLA DE PRODUCTOS ANULADOS***
	 NOMBRE: Andres Montealegre Giraldo
	 FECHA: 2015-07-24
*/
$queryinsertar="INSERT INTO productos_anulados_salidas (SELECT * FROM productos WHERE (numsalida='$salida_a_borrar'));";

/*
	 ***RUTINA PARA BORRAR LOS DATOS COPIADOS DE LA TABLA PRODUCTOS***
	 NOMBRE: Andres Montealegre Giraldo
	 FECHA: 2015-07-24

*/
//$queryborrar="DELETE FROM productos WHERE (numsalida='$salida_a_borrar');";

/*
	 ***RUTINA PARA ACTUALIZAR LA ENTRADA Y AJUSTAR SU NUEVO ESTADO A ACTIVO=0***
	 EL CUAL INDICA QUE LA ENTRADA FUE ANULADA Y SUS ELEMENTOS NO ESTARAN DENTRO DEL SISTEMA 
	 NOMBRE: Andres Montealegre Giraldo
	 FECHA: 2015-07-24

*/
	 $queryobservac="UPDATE salidas SET salobservaciones='$observaciones' , salactivo='0' WHERE salida='$salida_a_borrar'" ;
/*
$idubicacion=1;
$documentoid="";
$fechaasig="";
$numsalidaprod="";
$dependencia="";
*/
$queryproductos="UPDATE productos 
SET idubicacion='$idubicacion', documentoid='$documentoid', fechaasig='$fechaasig', numsalida='$numsalidaprod' , dependencia='$dependencia', activo='1' 
WHERE numsalida='$salida_a_borrar'   ";

$delhistorial="DELETE FROM historial_productos WHERE numsalida='$salida_a_borrar'";


$justificacion="Anulacion de Salida de activos a entidades	"."permiso usuario: " .$_SESSION['idpermiso'];
$observacionregistro="Salida numero: " .$salida_a_borrar." Acceso por IP:".$ip;
$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
$fecha=date ("Y-m-d");
$hora=date("H:i:s");
$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
mysql_query($registro2, $conexion);

mysql_query($queryinsertar, $conexion);
if (mysql_errno()!=0)
{
	echo "ERROR al insertar los datos...". mysql_errno(). "-". mysql_error();
	mysql_close($conexion);
}

mysql_query($queryobservac, $conexion);
if (mysql_errno()!=0)
{
	echo "ERROR Observaciones en salida de los datos...". mysql_errno(). "-". mysql_error();
	mysql_close($conexion);
}



mysql_query($queryproductos,$conexion);

if (mysql_errno()!=0)
{
	echo "ERROR Actualziar productos ESTADO Y USER...". mysql_errno(). "-". mysql_error();
	mysql_close($conexion);
}

mysql_close();	
header("location: salidas_menu.php?borrado=1");

?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
//DESCRIPCION:  VENTANA PARA AGREGAR ELEMENTOS DE CONSUMO A LA SALIDA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");
$salida=$_GET['salidanum']; //NUMERO DE SALIDA COMPLETO
$docid=$_GET['userid'];
//$numconsec=$_GET['numberconsec']; //SOLAMENTE EL NUMERO CONSECUTIVO
//$contrato=$_GET['contract'];
$id=$_GET['internalid'];



if (isset($_GET['anterior'])){
	$anterior=$_GET['anterior'];
}

if (isset($_GET['id'])){

	if (isset($_GET['numconsec'])){

		$buscarconsecsalida=$_GET['numberconsec'];

	}

}


?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">

	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<link rel="stylesheet" type="text/css" href="../css/paginacion.css"  >
	<link rel="shortcut icon" href="../imagenes/1.ico">





	<style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>

	<title>Salida de elementos</title>

	<body>


				<div id="centro">
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   

					<CENTER>
						<table width="65%" border="0">
							<tr>
								<td colspan="11" class="titulo"><center>
									SALIDA DE ELEMENTOS A USUARIOS
								</center></td>
							</tr>
						</table>
					</CENTER>

					<?php
//MUESTRA UN ANUNCIO SI LA CANTIDAD DE ELEMENTOS QUE SOLICITA EL USUARIO ES SUPERIOR A LA EXISTENTE EN BODEGA Y CANCELA SU PETICION.
					if (isset($_GET['limit'])){
						if ($_GET['limit']==2){
							?>
							<div class="limite" width="500">
								<img src="../imagenes/delete.png" title="Salir" width="24" height="24" align="center"/><strong>&nbsp;Cantidad requerida no permitida.</strong> 
								<p class="textoblanco">   Por favor verifique la cantidad de elementos disponibles en Bodega  </p>
							</div>

							<?php   }    } ?>

							<?php
							if (isset($_GET['quitarok'])){
								if ($_GET['quitarok']==4){
									?>
									<div class="quitarok">
										<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha quitado el elemento de la salida Correctamente!
									</div>

									<?php   }    } ?>

									<div class="container-full" >
										<div class="table-responsive">
											<table   class=" table"  cellspacing="0" >
												<thead>
													<tr>
														<td class="fila1" align="center" colspan="18 "><strong>ELEMENTOS DE CONSUMO DISPONIBLES EN BODEGA</strong></td>
													</tr><tr>
													<td  class="fila1">ID</td>
													<td  class="fila1">CANT. DISPONIBLE</td>
													<td  class="fila1">DESCRIPCION</td>
													<td  class="fila1">UNID MEDIDA</td>
													<td  class="fila1">ID CONTAB</td>
													<td  class="fila1">CODIGO CONTAB</td>
													<td  class="fila1">PRECIO</td>
													<td  class="fila1">CANTIDAD SOLICITADA</td>
													<td  class="fila1">AÑADIR</td>
												</tr>
											</thead>
											<tbody id="myTable2">

												<?php
//SELECCION DE LOS ELEMENTOS DE LA salida PARA ASIGNACION CON LA salida CORRESPONDIENTE
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD



												$tablaaux="SELECT  * FROM consumo
												LEFT  JOIN unidadmedida on consumo.unidmedida=unidadmedida.idunidadmedida
												LEFT  JOIN codigocontable on consumo.codigocontable=codigocontable.codigocontable
												WHERE cantidad>0
												ORDER BY elemento ASC				"; 

												$t_consumo=mysql_query($tablaaux,$conexion);

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09 

//}


//href="salida_consumo_agregar_a_tabla.php?idelemento=<?php echo $fila_consumo['idelemento'];?>

<?php 

while ($fila_consumo=mysql_fetch_array($t_consumo))
{
	?>

	<tr>
		<td class="fila2"><?php echo $fila_consumo["idelemento"];?></td>
		<td class="fila2"><center><?php echo $fila_consumo["cantidad"];?></center></td>
		<td  class="fila2"><?php echo $fila_consumo["elemento"];?></td>         
		<td  class="fila2"><?php echo $fila_consumo["unidadmedida"];?></td>
		<td  class="fila2"><?php echo $fila_consumo["codigocontable"];?></td>
		<td  class="fila2"><?php echo $fila_consumo["codigodescripcion"];?></td>
		<td  class="fila2"align="right">$<?php echo number_format($fila_consumo["precioadqui"],2,',','.');?></td>
		<td class="fila2"><center><form name="cantidadelementos" method="POST" action='salida_consumo_agregar_a_tabla.php' >
			<input CLASS="fila6" type="text" name="cantidad" value="1" onkeypress="return SoloNumeros(event)"></center></td>
			<input type="hidden" name="idelemento" value="<?php echo "$fila_consumo[idelemento]"; ?>" />
			<input type="hidden" name="numsalida" value="<?php echo "$salida"; ?>" />
			<input type="hidden" name="docid" value="<?php echo "$docid"; ?>" />
			<input type="hidden" name="anterior" value="<?php echo "$anterior"; ?>" />

		</td>

		<td class="fila2"> <center> <strong> <input type="submit" class="botonguardar3" name="guardar_producto" title="Guardar producto" value=" + "/></strong></center>
		</td></form>
	</tr>
	<?php } ?>  
</tbody>
</TABLE>
</div>
<div>
	<center>
		<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
		<div class="col-md-12 text-center">

		</div>



		<?php

/*
$salida=$_GET['salidanum']; //NUMERO DE SALIDA COMPLETO
$docid=$_GET['userid'];
$numconsec=$_GET['numberconsec']; //SOLAMENTE EL NUMERO CONSECUTIVO
$contrato=$_GET['contract'];
$id=$_GET['internalid'];
*/
?>
</div>
<DIV ID="CENTRO">
	<?php

	$queryuserdep="SELECT  nombres, apellidos, documentoid, iddependencia, calidadempleado, grado, cargo, idprofesion FROM usuarios  WHERE documentoid='$docid'";
	$userdep=mysql_query($queryuserdep,$conexion);
	while($filuserdep=mysql_fetch_array($userdep))
	{

		$profesion=$filuserdep["idprofesion"];
		$grado=$filuserdep["grado"];
		$cargo=$filuserdep["cargo"];
		$codcalidad=$filuserdep["calidadempleado"]; 
		$codigodependenciausuario=$filuserdep["iddependencia"]; 
		$asignadoa=$filuserdep["nombres"] ." " .$filuserdep["apellidos"] ;
	}

	$selcalidad="SELECT * FROM calidadempleador WHERE idcalidadempleador='$codcalidad'";
	$querycalidad=mysql_query($selcalidad,$conexion);
	while($calidad=mysql_fetch_array($querycalidad))
	{
		$calidadempleador_contrato=$calidad['calidadempleador'];
	}



	$querydep="SELECT iddependencia, codigodependencia, nombredependencia, sigladependencia FROM dependencias  WHERE codigodependencia='$codigodependenciausuario'";
	$dependencia=mysql_query($querydep,$conexion);
	while($filadep=mysql_fetch_array($dependencia))
	{
		$codigodependenciasalida=$filadep["codigodependencia"];
		$nombredependenciasalida=$filadep["nombredependencia"];
		$sigladependenciasalida=$filadep["sigladependencia"];
	}

	?> 

	<table class="tabla_2"   border="0" id="tablaresult"  width="100%">
		<tr>

			<td class="fila1" align="center" colspan="19 "><strong><h3>SALIDA DE ELEMENTOS DEVOLUTIVOS</strong></h3> </td>
		</tr>
		<TR>
			<td  class="fila2" colspan="3">FECHA DE SALIDA</td> 
			<td  class="fila2" colspan="3"><strong><?PHP ECHO date("Y-m-d"); ?></strong></td> 

			<td  class="fila2" colspan="3">SALIDA N°</td> 
			<td  class="fila2" colspan="3"><strong><?PHP ECHO $salida; ?></strong></td> 
			<td  class="fila2" colspan="3">TIPO DE ELEMENTOS</td> 
			<td  class="fila2" colspan="4"><strong>CONSUMO</strong></td>  

			<TR> 
				<td  class="fila2" colspan="3">RESPONSABLE</td> <td  class="fila2" colspan="6"><strong><?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?></strong></td>  
				<td  class="fila2" colspan="4">DOCUMENTO ID</td> <td  class="fila2" colspan="6"><strong><?php echo "$_SESSION[documentoid]"; ?></strong></td>  
			</TR>
			<TR> 
				<td  class="fila2" colspan="3">UNIDAD EJECUTORA</td> <td  class="fila2" colspan="6"><strong><?php echo "$unidadejecutora"; ?></strong></td>  
				<td  class="fila2" colspan="4">NIT UNIDAD EJECUTORA</td> <td  class="fila2" colspan="6"><strong><?php echo "$nitunidadejecutora"; ?></strong></td>  
			</TR>
			<TR>
				<td class="fila2" colspan="3" >ASIGNADO A:</td> <td class="fila2" colspan="3" ><strong><?php echo $asignadoa;?></strong></td>
				<td class="fila2" colspan="2">DOCUMENTO DE ID. N°:</td> <td class="fila2" COLSPAN="4"><strong><?php echo $docid;?></strong></td>
				<td class="fila2" >DEPENDENCIA</td> <td class="fila2" ><strong><?php echo $codigodependenciasalida;?></strong></td><td class="fila2" colspan="2"> <strong><?php echo $nombredependenciasalida;?></strong></td><td class="fila2" > <strong><?php echo $sigladependenciasalida;?></strong></td>
			</TR>
			<TR> <td  class="fila2" colspan="3">CALIDAD EMPLEADO</td> <td  class="fila2" colspan="3"><strong><?php echo $calidadempleador_contrato ; ?></strong></td>  
				<td  class="fila2" colspan="3">PROFESION</td> <td  class="fila2" colspan="3"><strong><?php echo $profesion ; ?></strong></td>  
				<td  class="fila2" colspan="1">CARGO</td> <td  class="fila2" colspan="3"><strong><?php echo $cargo ; ?></strong></td>  
				<td  class="fila2" colspan="1">GRADO</td> <td  class="fila2" colspan="2"><strong><?php echo $grado; ?></strong></td> 
			</TR>

		</Table>
		<div class="container-full">
			<div class="table-responsive">
				<table   class=" table"  cellspacing="0" width="100%" >
					<thead>

						<td class="fila1" align="center" colspan="9" strong><h4>DETALLES</strong></h4> </td>
						<TR>

							<td  class="fila1">ID</td>
							<td  class="fila1">DESCRIPCION</td>
							<td  class="fila1">UNID MEDIDA</td>
							<td  class="fila1">CANTIDAD</td>
							<td  class="fila1">CODIGO CONTAB</td>
							<td  class="fila1">DEPENDENCIA</td>
							<td  class="fila1">VALOR UNIT</td>
							<td  class="fila1">VALOR TOTAL</td>
							<td  class="fila1">QUITAR</td>
						</TR>
					</thead>
					<tbody id="myTable2a">
						<?php

						$tablainf="SELECT  * FROM tabla_aux_consumo_salidas  
						LEFT  JOIN unidadmedida on tabla_aux_consumo_salidas.unidadmedida_aux=unidadmedida.idunidadmedida
						LEFT  JOIN codigocontable on tabla_aux_consumo_salidas.codigocontable_aux=codigocontable.codigocontable
						LEFT  JOIN dependencias on tabla_aux_consumo_salidas.dependencia_aux=dependencias.codigodependencia
						LEFT JOIN  consumo   on tabla_aux_consumo_salidas.idelemento_aux=consumo.idelemento
						WHERE (numsalida_aux='$salida')";

						$t_aux_salida=mysql_query($tablainf,$conexion);                                      

						while ($fila_aux_salida=mysql_fetch_array($t_aux_salida))
						{
							?>

							<tr>
								<td  class="fila2"><?php echo $fila_aux_salida["idelemento_aux"];?></td>
								<td  class="fila2"><?php echo $fila_aux_salida["elemento"];?></td>
								<td  class="fila2"><?php echo $fila_aux_salida["unidadmedida"];?></td>
								<td  class="fila2"><?php echo $fila_aux_salida["cantidad_aux"];?></td>

								<td  class="fila2"><?php echo $fila_aux_salida["codigocontable"];?></td>
								<td  class="fila2"><?php echo $fila_aux_salida["codigodescripcion"];?></td>
								<td  class="fila2" align="right">$<?php echo number_format($fila_aux_salida["valorunit_aux"],2,',','.');?></td>
								<td  class="fila2" align="right">$<?php echo number_format($fila_aux_salida["valortotal_aux"],2,',','.');?></td>
								<td class="fila2"><center>
									<a href="salida_quitar_consumo.php?idtabla_aux=<?php echo $fila_aux_salida['idtabla_aux'];?>&idelemento=<?php echo $fila_aux_salida['idelemento_aux'];?>&iduser=<?php echo $docid;?>&numconsec=<?php echo $numconsec;?>&salidanum=<?php echo $salida;?>&anterior=<?php echo $anterior;?>"><img src="../imagenes/delete.png" title="Quitar" width="28" height="28"/></a>

								</center></td>
								<?php } ?>
							</tr>

							<?php
							$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_salidas WHERE numsalida_aux='$salida'");   
							$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
							$valortotal=$valortotal1["total"];
							?>
						</tbody>
						<TD class="fila2"colspan="6"> &nbsp;</TD>
						<TD class="fila2"colspan="1"><STRONG>VALOR TOTAL</STRONG></TD>
						<td  class="fila3" align="right">$<?php echo number_format($valortotal,2,',','.');?></td>
						<TD class="fila2"colspan="1"> &nbsp;</TD>
					</tABLE>
					<div>
						<center>
							<ul class="pagination pagination-lg pager" id="myPager2a"></ul></center>

							</div>

							<div class="salidaanterior" align="left">
								<?php
								$selectsalanterior="SELECT * FROM salidas WHERE idsalida='$anterior'";
								$querysalidaanterior=mysql_query($selectsalanterior,$conexion);
								while ($salanterior=mysql_fetch_array($querysalidaanterior)){
									?>
									Última salida elaborada: <?PHP echo $salanterior['salida']; ?>  &nbsp;&nbsp;&nbsp; Fecha : <?PHP echo $salanterior['fechasalida']; ?>
									<?PHP } ?>
								</div>
								<div id="centro">

									<table class="tabla_2" width="40%"> 

										<tr>
											<td class="fila1" colspan="8" ><h3>DATOS ADICIONALES DE LA SALIDA DE ELEMENTOS</h3></td>
										</tr>
										<tr>
											<form name="Crearsalida" action="guardar_salida_consumo.php?salida=<?php echo$salida?>&documentoid=&iduser=<?php echo $docid;?>&salidanum=<?PHP ECHO $salida;?>"    title="Guardar Detalles de la salida" method="post" onsubmit="guardar_salida.disabled= true; return true;" >

											</tr>
											<td class="fila2" >FECHA : </td>
											<td class="fila2" colspan="7"><input size="12" id="f_date1" name="f_date1" value="<?php  ECHO date("Y-m-d");?>"><button id="f_btn1">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>


	
</tr>

<tr>
<td class="fila2">TIPO DE SALIDA</td>
<td class="fila2"><select name="salida_tiposalida_sel" required> <option value=""></option>
		<?PHP
		$tipoentradaq=mysql_query("SELECT idtiposalida, tiposalida_sel FROM tiposalida WHERE idtiposalida>=6  AND idtiposalida<=7 ORDER BY idtiposalida");
		while($filtipoen=mysql_fetch_array($tipoentradaq))
		{
		?>
		<option     size="50"value="<?php echo $filtipoen['idtiposalida'];?>"><?php echo $filtipoen['tiposalida_sel'];?></option>
		<?php
		}?>
</select></td>
</tr>


<tr>
	<td class="fila2">COMENTARIO</td><td CLASS="FILA2" COLSPAN="5"><input type="text" class="textinput"  name="salida_comentario" size="120"  onChange="MAY(this)" value="" /></td>

	<td  class="fila2"><p align="center"><button type="submit" class="botonguardar2" name="guardar_salida" title="Guardar salida" value=" Guardar salida" />Guardar Salida</button></p></td>
</tr>

<input type="hidden" name="salida_numsalida" size="30"  onChange="MAY(this)" value="<?php echo $salida;?>" /></td>
<input type="hidden" name="salida_elaboro" size="10"  onChange="MAY(this)" value="<?php echo "$_SESSION[nombres] $_SESSION[apellidos]";?>" /></td>
<input type="hidden" name="salida_dependencia" size="10"  onChange="MAY(this)" value="<?php echo $codigodependenciasalida;?>" /></td>
<input type="hidden" name="salida_valortotal" size="10"  onChange="MAY(this)" value="<?php echo $valortotal;?>" /></td>

<?php
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
//NUMERO DE INTEMS DE LA SALIDA
$consultas="SELECT * FROM tabla_aux_consumo_salidas WHERE numsalida_aux='$salida'";
$consulta_items=mysql_query($consultas,$conexion);

$num_items_salida=mysql_num_rows($consulta_items);
?>
<input type="hidden" name="salida_num_items" size="30" maxlength="50" onChange="MAY(this)" value="<?php echo ($num_items_salida);?>" /></td>

<input type="hidden" name="salida_valortotal" size="10"  onChange="MAY(this)" value="<?php echo ($valortotal);?>" /></td>


</form>
<td>
</table>



</DIV>
</BODY  >

<?php
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>
	

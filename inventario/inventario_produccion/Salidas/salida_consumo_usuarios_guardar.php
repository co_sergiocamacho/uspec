<?php
//DESCRIPCION:  GUARDA SALIDA DE CONSUMO PARA USUARIOS*ACTUALZIA BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
if (isset($_SESSION['idpermiso'])) {
        include("../database/conexion.php");
        $documentoid=$_POST['documento'];
        $jsondata = array();
        $proceso_transaccion = true;


    /**
     * Generar Consecutivo
     */
     ////////////////////////////////////////////////
        $referencia="SAL-";
        $consecutivo="";
        $consulta="SELECT COUNT(idconsecsaldia) AS total_data FROM consecutivosalidas";
        $result=mysql_query($consulta,$conexion);
        $row=mysql_fetch_array($result,MYSQLI_ASSOC);
        $num_resultados=$row["total_data"];
        mysql_free_result($result);

        $tiposalida="";
        $consecutivo=$num_resultados+1;
        $salidanum=$referencia.$consecutivo;
        $fechasalida=date("Y-m-d");
    ///////////////////////////////////////////////
        
	$tipoelementos="ELEMENTOS DE CONSUMO ENTREGADOS";
	$elaboradopor=$_SESSION['nombres']." ". $_SESSION['apellidos'];
           /* Deshabilitar autocommit */
            mysql_query("SET AUTOCOMMIT=0");
            mysql_query("START TRANSACTION");
            /**
             * Insertar consecutivo
             */
            $insertar_consecutivo="INSERT INTO consecutivosalidas (consecsalida) VALUES ('$salidanum')";
            $query_consecutivo=mysql_query($insertar_consecutivo,$conexion);
            if (!$query_consecutivo){
                $proceso_transaccion = false;
            }
            /**
             * Insertar salida
             */
            $insertar_salida="INSERT INTO salidas (salida, fechasalida, tiposalida,  documentoid,  elaboradopor) VALUES ('$salidanum', '$fechasalida', '$tiposalida', '$documentoid',  '$elaboradopor')";
            $query_salida=mysql_query($insertar_salida,$conexion);
            $anterior=mysql_insert_id()-1;
            if (!$query_salida){
                $proceso_transaccion = false;
            }
            
            $insertar_aux_consumo ="SELECT idtabla_aux, numsalida_aux FROM tabla_aux_consumo_salidas WHERE numsalida_aux='$salidanum'";
            $query_aux_consumo=mysql_query($insertar_aux_consumo,$conexion);
            if (!$query_aux_consumo){
                $proceso_transaccion = false;
            }
            
            if ($proceso_transaccion) {
                mysql_query("COMMIT");
                $jsondata["data"] = array(
                        'message' => "Consecutivo creado.",
                        'internalid' => "3",
                        'userid' => $documentoid,
                        'numberconsec' => $consecutivo,
                        'salidanum' => $salidanum,
                        'anterior' => $anterior,
                        'process' => "true"
                );
            } else {

                mysql_query("ROLLBACK");
                $jsondata["data"] = array(
                        'message' => "Consecutivo no creado.",
                        'process' => "false"
                    );
            }           
            mysql_close(); 
            echo json_encode($jsondata);

} 
else {
header("location: ../403.php");
}
?>

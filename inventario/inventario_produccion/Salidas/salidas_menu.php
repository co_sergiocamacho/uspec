<?php
//DESCRIPCION: VENTANA PRINCIPAL  DE SALIDAS PARA ADMINISTRADORES
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
//include("../assets/global.php");

$_SESSION['idpermiso'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="../imagenes/1.ico">



<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Salidas</title>
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<style>	

body {
	background: #eeeeee no-repeat center top;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	background-size: cover;}
	#div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;	}
	.container > header h1,
	.container > header h2 {
		color: #fff;
		text-shadow: 0 1px 1px rgba(0,0,0,0.7);
	}

</style>

<!--IDENTIFICACION USUARIO LOGEADO
<!--NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
<!--FECHA: 2015-07-24	 -->

<!--FIN IDENTIFICACION USUARIO LOGEADO -->

<body>

<div id="centro2"><table class="botonesfila" >
<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td></tr></table></div>


<div id="centro">
	<div id="div_bienvenido">
		<?php echo "Bienvenido"; ?> <BR/>
		<div id="div_usuarios">
			<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
		</div>
		<?php echo "SALIR";?>
		<a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="13" height="13" /></a>
	</div>   

	<center>
		<table width="65%" border="0">
			<tr>
				<td colspan="11" class="titulo"><center>
					SALIDAS
				</center></td>
			</tr>

		</table>

		<?php
		if (isset($_GET['creado'])){  if ($_GET['creado']==5){ ?>
		<div class="quitarok">
			<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado la Salida correctamente!
		</div>

		<?php   }    } ?>

		<?php

		if (isset($_GET['borrado'])){   if ($_GET['borrado']==1){ ?>
		<div class="quitarok">
			<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha anulado la Salida  correctamente!
		</div>

		<?php   }   } ?>


		<table class="menuprincipal"  >
			<tr class="menuactivos">




				<TD colspan="4"><a  class="titulos_menu" align="center"> <h3><center>Activos Devolutivos</center></h3></a></TD>
				<tr class="menuactivos">
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<TD><a  class="titulos_menu" href="salida_usuario_add.php"><img src="../imagenes/salida2.png" title="Menu de Elementos" width="64" height="64"  />Crear salida a usuarios</a></TD>
					<TD><a  class="titulos_menu"  href="salida_entidades_seleccion.php"><img src="../imagenes/establecimientos.png" title="Menu de Elementos" width="64" height="64"  />Crear salida Entidades</a></TD>
					<?php }?>
					<TD><a class="titulos_menu" href="salidas_activos_creadas.php?all1=1&all2=1"><img src="../imagenes/verentradas.png" title="Menu de Elementos" width="64" height="64"  />Ver Salidas de Activos</a></TD>
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<TD><a class="titulos_menu" href="salida_baja_add.php?"><img src="../imagenes/bajas.png" title="Menu de Elementos" width="64" height="64"  />Salida por Bajas</a></TD>
					<?php }?>
<?php 

					//<td><a  class="titulos_menu"href="../activos/editar_activos.php"><img src="../imagenes/documento.png" title="Editar" width="64" height="64"  />Ver Activos</a></td>
?>
				</tr>

				<tr class="menuconsumo" >
					<TD colspan="4" ><a  class="titulos_menu"  align="center" > <h3><center>Elementos de Consumo</center></h3></a></TD>
				</tr>
				<tr class="menuconsumo">
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<TD><a  class="titulos_menu" href="salida_consumo_usuario_add.php"><img src="../imagenes/salida2.png" title="Menu de Elementos" width="64" height="64"  />Crear salida a usuarios</a></TD>
					<TD><a  class="titulos_menu" href="salida_consumo_entidad_seleccion.php"><img src="../imagenes/establecimientos.png" title="Menu de Elementos" width="64" height="64"  />Crear salida Entidades</a></TD>
					<?php }?>
					<TD><a class="titulos_menu" href="salidas_consumo_creadas.php?all1=1&all2=1"><img src="../imagenes/verentradas.png" title="Menu de Elementos" width="64" height="64"  />Ver Salidas de E de Consumo</a></TD>

                    
                    <?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<TD><a class="titulos_menu" href="salida_baja_1.php?"><img src="../imagenes/bajas.png" title="Menu de Elementos" width="64" height="64"  />Baja de E. de Consumo</a></TD>
					<?php }?>
                    
					
<?php 
					//<td><a  class="titulos_menu" href="..consumo/consumo_menu.php"><img src="../imagenes/documento.png" title="Editar" width="64" height="64"  />Ver elementos de consumo</a><?php </td>
?>

				</tr>

				
									<tr class="menugral">
					<TD colspan="4"><a  class="titulos_menu"  align="center"> <h3><center>Bajas</center></h3></a></TD>
					<tr class="menugral">
					<td><a class="titulos_menu" href="bajas_menu.php"><img src="../imagenes/verbajas.png" title="Editar" width="50" height="50"  />Ver Salidas porBajas </a></td>
					<td><a class="titulos_menu" href="../reportes/elementos_bajas.php"><img src="../imagenes/elementosbaja.png" title="Editar" width="50" height="50"  />Ver Elementos  dados de Baja</a></td>

						<TD></TD>
						<TD></TD>
					</tr>

<tr class="menuactivos2">
					<TD colspan="4"><a  class="titulos_menu"  align="center"> <h3><center>Anuladas</center></h3></a></TD>
					<tr class="menuactivos2">
						<?php
//<TD><a class="titulos_menu" href="salidas_creadas.php"><img src="../imagenes/verentradas.png" title="Menu de Elementos" width="64" height="64"  />Ver Todas las salidas</a></TD>
						?>




						<TD><a class="titulos_menu" href="salidas_anuladas.php?all1=1"><img src="../imagenes/anuladas.png" title="Menu de Elementos" width="64" height="64"  />Salidas Activos Anuladas</a></TD>
					
						<TD><a class="titulos_menu" href="salidas_consumo_anuladas.php?all1=1"><img src="../imagenes/anuladas.png" title="Menu de Elementos" width="64" height="64"  />Salidas Consumo Anuladas</a></TD>
	
						<TD></TD>
						<TD></TD>

					</tr>

				</table>
			</center>



		</div>
	</body>
	</html>

	<?php 

	include ("../assets/footer.php");
	?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>



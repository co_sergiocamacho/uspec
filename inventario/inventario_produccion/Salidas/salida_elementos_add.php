<?php
session_start();
//DESCRIPCION:  VENTANA QUE PERMITE  ASIGNAR ELEMENTOS ACTIVOS A LAS SALIDAS PARA USUARIOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");
$salida=$_GET['salidanum']; //NUMERO DE SALIDA COMPLETO
$docid=$_GET['userid'];
$numconsec=$_GET['numberconsec']; //SOLAMENTE EL NUMERO CONSECUTIVO
//$contrato=$_GET['contract'];
$id=$_GET['internalid'];

if (isset($_GET['anterior'])){
	$anterior=$_REQUEST['anterior'];
}


if (isset($_GET['id'])){

	if (isset($_GET['numconsec'])){

		$buscarconsecsalida=$_GET['numberconsec'];
	}

}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>


	<title>Salida de elementos</title>
	<body>

<?php 

/*
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="salidas_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr></table></div>
*/
				?>

				<div id="centro">
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   

					<CENTER>
						<table width="65%" border="0">
							<tr>
								<td colspan="11" class="titulo"><center>
									SALIDA DE ELEMENTOS A USUARIOS
								</center></td>
							</tr>
						</table>
						<th class="fila1" align="center" colspan="18 "  width="100%"><H4><strong>ELEMENTOS DISPONIBLES EN BODEGA</strong></H4></th>
					</CENTER>
				</div>

				<div class="container-full">
					<div class="row">
						<div class="table-responsive">
							<table class="table table-hover">
								<tr>
								</tr>
								<tr>
									<thead>
										<th  class="fila1">ID</th>
										<th  class="fila1">DESCRIPCION</th>
										<th  class="fila1">PLACA N°</th>
										<th  class="fila1">UNID MEDIDA</th>
										<th  class="fila1">PROVEEDOR</th>
										<th  class="fila1">MARCA</th>
										<th  class="fila1">MODELO</th>
										<th  class="fila1">SERIE</th>
										<th  class="fila1">TIPO ENTR.</th>
										<th  class="fila1">ID CAT.</th>
										<th  class="fila1">CATEGORIA</th>
										<th  class="fila1">CONDICION</th>
										<th  class="fila1">PRECIO</th>
										<th  class="fila1">UBICACION</th>
										<th  class="fila1">OBSERVACIONES</th>
										<th  class="fila1">AGREGAR</th>
										<th  class="fila1">PLACA</th>
									</tr>
								</thead>
								<tbody id="myTable2">
									<?php
//SELECCION DE LOS ELEMENTOS DE LA salida PARA ASIGNACION CON LA salida CORRESPONDIENTE
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD



									$tablaaux="SELECT  * FROM productos  
									LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
									LEFT  JOIN condicion on productos.idcondicion=condicion.idcondicion
									LEFT  JOIN proveedores on productos.idproveedor=proveedores.idproveedor
									LEFT  JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
									LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
									WHERE (idubicacion=1 and activo='1')"; 

									$t_productos=mysql_query($tablaaux,$conexion);

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09 

//}

									while ($fila_productos=mysql_fetch_array($t_productos))
									{
										?>

										<tr>
											<td class="fila2"><?php echo $fila_productos["idelemento"];?></td>
											<td  class="fila2"><?php echo $fila_productos["elemento"];?></td>
											<td  class="fila3"align="CENTER"><?php echo $fila_productos["codebar"];?></td>
											<td  class="fila2"><?php echo $fila_productos["unidadmedida"];?></td>
											<td  class="fila2"><?php echo $fila_productos["proveedor"];?></td>
											<td  class="fila2"><?php echo $fila_productos["marca"];?></td>
											<td  class="fila2"><?php echo $fila_productos["modelo"];?></td>
											<td  class="fila2"><?php echo $fila_productos["serie"];?></td>
											<td  class="fila2"><?php echo $fila_productos["tipoentrada"];?></td>
											<td  class="fila2"><?php echo $fila_productos["codigocontable"];?></td>
											<td  class="fila2"><?php echo $fila_productos["codigodescripcion"];?></td>
											<td  class="fila2"><?php echo $fila_productos["condicion"];?></td>
											<td  class="fila2" align="right">$<?php echo number_format($fila_productos["precioadqui"],2,',','.');?></td>
											<td  class="fila2"><?php echo $fila_productos["nombreubicacion"];?></td>
											<td  class="fila2"><?php echo $fila_productos["probservaciones"];?></td>
											<td class="fila2"><center>

												<?php
												if($fila_productos['codebar']!=""){
													?>
													<a href="salida_asignar_elementos.php?idelemento=<?php echo $fila_productos['idelemento'];?>&iduser=<?php echo $docid;?>&numconsec=<?php echo $numconsec;?>&salidanum=<?php echo $salida;?>&anterior=<?php echo $anterior;?>"><img src="../imagenes/add.png" title="AGREGAR A LA SALIDA " width="28" height="28"/></a>


												</center></td>

												<?php
											}
											?>  
											<?php
											if($fila_productos['codebar']==""){
												?>
												<td class="fila2"><center>
													<a href="salida_generar_placa.php?idelemento=<?php echo $fila_productos['idelemento'];?>&salida=<?php echo $salida;?>&userid=<?php echo $docid;?>&numconsec=<?php echo $numconsec;?>&contrato=<?php echo $contrato;?>&anterior=<?php echo $anterior;?>"><img src="../imagenes/codebar.png" title="GENERAR N° PLACA " width="28" height="28"/></a>
												</center></td>

												<?php
												?>

												<?php }
											}
											?>    

										</tr>
									</tbody>
								</table>   
							</div>
							<center>
								<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
								<div class="col-md-12 text-center">

								</div>
							</div>
						</div>
						<DIV ID="CENTRO">



							<?php

							$queryuserdep="SELECT  nombres, apellidos, documentoid, iddependencia, calidadempleado, grado, cargo, idprofesion FROM usuarios
							LEFT  JOIN calidadempleador on usuarios.calidadempleado=calidadempleador.idcalidadempleador
							WHERE documentoid='$docid'";
							$userdep=mysql_query($queryuserdep,$conexion);
							while($filuserdep=mysql_fetch_array($userdep))
							{
								$profesion=$filuserdep["idprofesion"];
								$grado=$filuserdep["grado"];
								$cargo=$filuserdep["cargo"];
								$codigodependenciausuario=$filuserdep["iddependencia"]; $asignadoa=$filuserdep["nombres"] ." " .$filuserdep["apellidos"] ;
								$calidadempleado=$filuserdep["calidadempleado"];

							}

							$selcalidad="SELECT * FROM calidadempleador WHERE idcalidadempleador='$calidadempleado'";
							$querycalidad=mysql_query($selcalidad,$conexion);
							while($calidad=mysql_fetch_array($querycalidad))
							{
								$calidadempleador_contrato=$calidad['calidadempleador'];
							}
							$querydep="SELECT iddependencia, codigodependencia, nombredependencia, sigladependencia FROM dependencias  WHERE codigodependencia='$codigodependenciausuario'";
							$dependencia=mysql_query($querydep,$conexion);
							while($filadep=mysql_fetch_array($dependencia))
							{
								$codigodependenciasalida=$filadep["codigodependencia"];$nombredependenciasalida=$filadep["nombredependencia"];$sigladependenciasalida=$filadep["sigladependencia"];
							}
							?>

							<table class="tabla2" width="97%">
								<tr>

									<td class="fila1" align="center" colspan="19 "><strong><h3>SALIDA DE ELEMENTOS DEVOLUTIVOS</strong></h3> </td>
								</tr>
								<TR>
									<td  class="fila2" colspan="3">FECHA DE SALIDA</td> 
									<td  class="fila2" colspan="3"><strong><?PHP ECHO date("Y-m-d"); ?></strong></td> 

									<td  class="fila2" colspan="3">SALIDA N°</td> 
									<td  class="fila2" colspan="4"><strong><?PHP ECHO $salida; ?></strong></td> 
									<td  class="fila2" colspan="3">TIPO DE ELEMENTOS</td> 
									<td  class="fila2" colspan="4"><strong>ACTIVOS FIJOS DEVOLUTIVOS</strong></td>  

									<TR> 

										<td  class="fila2" colspan="3">RESPONSABLE</td> <td  class="fila2" colspan="6"><strong><?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?></strong></td>  
										<td  class="fila2" colspan="4">DOCUMENTO ID</td> <td  class="fila2" colspan="6"><strong><?php echo "$_SESSION[documentoid]"; ?></strong></td>  
									</TR>
									<TR> 
										<td  class="fila2" colspan="3">CALIDAD EMPLEADO</td> <td  class="fila2" colspan="3"><strong><?php echo $calidadempleador_contrato ; ?></strong></td>  
										<td  class="fila2" colspan="3">PROFESION</td> <td  class="fila2" colspan="3"><strong><?php echo $profesion ; ?></strong></td>  
										<td  class="fila2" colspan="1">CARGO</td> <td  class="fila2" colspan="3"><strong><?php echo $cargo ; ?></strong></td>  
										<td  class="fila2" colspan="1">GRADO</td> <td  class="fila2" colspan="2"><strong><?php echo $grado; ?></strong></td>  


									</TR>
									<TR> 

										<td  class="fila2" colspan="3">UNIDAD EJECUTORA</td> <td  class="fila2" colspan="6"><strong><?php echo "$unidadejecutora"; ?></strong></td>  
										<td  class="fila2" colspan="4">NIT UNIDAD EJECUTORA</td> <td  class="fila2" colspan="6"><strong><?php echo "$nitunidadejecutora"; ?></strong></td>  


										<TR>


											<td class="fila2" colspan="3" >ASIGNADO A:</td> <td class="fila2" colspan="3" ><STRONG><?php echo $asignadoa;?></STRONG></td>
											<td class="fila2" colspan="2">DOCUMENTO DE ID. N°:</td> <td class="fila2" COLSPAN="5"><STRONG><?php echo $docid;?></STRONG></td>

											<td class="fila2" >DEPENDENCIA</td> <td class="fila2" ><strong><?php echo $codigodependenciasalida;?></strong></td><td class="fila2"  colspan="3"> <strong><?php echo $nombredependenciasalida;?></strong></td><td class="fila2" ><strong> <?php echo $sigladependenciasalida;?></strong></td>


										</TR>

									</TR>

									<tr>

										<td class="fila1" align="center" colspan="19 "><strong><h4>DETALLES</strong></h4> </td>
									</tr>
									<TR>
									</Table>
								</div>

								<div class="container-full">
									<div class="table-responsive">

										<div class="container-full">

											<table   class=" table"  cellspacing="0" width="100%" >
												<thead>
													<tr>
														<td  class="fila1">ID</td>
														<td  class="fila1">DESCRIPCION</td>
														<td  class="fila1">PLACA N°</td>
														<td  class="fila1">UNID MEDIDA</td>
														<td  class="fila1">MARCA</td>
														<td  class="fila1">MODELO</td>
														<td  class="fila1">SERIE</td>
		
														<td  class="fila1">FECHA ASIG.</td>
														<td  class="fila1">ID CONTAB</td>
														<td  class="fila1">CODIGO CONTAB</td>
														<td  class="fila1">UBICACION</td>
														<td  class="fila1">CONDICION</td>
														<td  class="fila1">PRECIO</td>
														<td  class="fila1">OBSERVACIONES</td>
														<td  class="fila1">QUITAR</td>
													</tr>
												</thead>
												<tbody id="myTable2a">

													<?php

													$tablainf="SELECT  * FROM productos  
													LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
													LEFT  JOIN condicion on productos.idcondicion=condicion.idcondicion
													LEFT  JOIN proveedores on productos.idproveedor=proveedores.idproveedor
													LEFT  JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
													LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
													LEFT  JOIN dependencias on productos.dependencia=dependencias.codigodependencia
													LEFT JOIN  ubicacion on productos.idubicacion=  ubicacion.id_ubicacion
													WHERE (numsalida='$salida')";

													$t_productos=mysql_query($tablainf,$conexion);                                      

													while ($fila_productos=mysql_fetch_array($t_productos))
													{
														?>

														<tr>
															<td class="fila2"><?php echo $fila_productos["idelemento"];?></td>
															<td  class="fila2"><?php echo $fila_productos["elemento"];?></td>
															<td  class="fila2"><?php echo $fila_productos["codebar"];?></td>
															<td  class="fila2"><?php echo $fila_productos["unidadmedida"];?></td>
															<td  class="fila2"><?php echo $fila_productos["marca"];?></td>
															<td  class="fila2"><?php echo $fila_productos["modelo"];?></td>
															<td  class="fila2"><?php echo $fila_productos["serie"];?></td>
															<td  class="fila2"><?php echo $fila_productos["fechaasig"];?></td>
															<td  class="fila2"><?php echo $fila_productos["codigocontable"];?></td>
															<td  class="fila2"><?php echo $fila_productos["codigodescripcion"];?></td>
															<td  class="fila2"><?php echo $fila_productos["nombreubicacion"];?></td>
															<td  class="fila2"><?php echo $fila_productos["condicion"];?></td>
															<td  class="fila2" Align="right">$<?php echo number_format($fila_productos["precioadqui"],2,',','.');?></td>
															<td  class="fila2"><?php echo $fila_productos["probservaciones"];?></td>
															<td class="fila2"><center>


																<a href="salida_quitar_elementos.php?idelemento=<?php echo $fila_productos['idelemento'];?>&iduser=<?php echo $docid;?>&numconsec=<?php echo $numconsec;?>
																	&salidanum=<?php echo $salida;?>&anterior=<?php echo $anterior;?>"><img src="../imagenes/delete.png" title="Quitar" width="28" height="28"/></a>

																</center></td>
																<?php

															}

															?>
														</tr>

														<?php
														$querysuma = mysql_query("SELECT SUM(precioadqui) as total FROM productos WHERE numsalida='$salida'");   
														$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
														$valortotal=$valortotal1["total"];

														?>
													</tbody>
													<TD class="fila2"colspan="10"> &nbsp;</TD>


													<TD class="fila2"colspan="2"><STRONG>VALOR TOTAL</TD>

													<td  class="fila3" align="right">$<?php echo number_format($valortotal,2,',','.');?></strong></td>
													<TD class="fila2"colspan="2"> &nbsp;</TD>

												</TABLE>


											</div>
											<div>
												<center>
													<ul class="pagination pagination-lg pager" id="myPager2a"></ul></center>
													<div class="col-md-12 text-center">
													</div>

													<div class="salidaanterior" align="left">
														<?php
														$selectsalanterior="SELECT * FROM salidas WHERE idsalida='$anterior'";
														$querysalidaanterior=mysql_query($selectsalanterior,$conexion);
														while ($salanterior=mysql_fetch_array($querysalidaanterior)){
															?>
															Última salida elaborada: <?PHP echo $salanterior['salida']; ?>  &nbsp;&nbsp;&nbsp; Fecha : <?PHP echo $salanterior['fechasalida']; ?>
															<?PHP } ?>
														</div>




														<div id="centro">
															<table class="tabla_2" width="40%"> 
																<tr>
																	<td class="fila1" colspan="6" ><h3>DATOS ADICIONALES DE LA SALIDA DE ELEMENTOS</h3></td>
																</tr>
																<tr>
																	<form name="Crearsalida" action="guardar_salida.php?salida=<?php echo$salida?>&documentoid=&iduser=<?php echo $docid;?>&salidanum=<?PHP ECHO $salida;?>&anterior=<?PHP ECHO $anterior;?>"    title="Guardar Detalles de la salida" method="post"  onsubmit="guardar_salida.disabled= true; return true;"   >

																	</tr>
																	<td class="fila2" >FECHA DE ELABORACIÓN </td>
																	<td class="fila2" colspan="4"><input size="12" id="f_date1" name="f_date1" value="<?php  ECHO date("Y-m-d");?>"><button id="f_btn1">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>
</td>

<tr>
<td class="fila2">TIPO DE SALIDA</td>
<td class="fila2"><select name="salida_tiposalida_sel" required> <option value=""></option>
		<?PHP
		$tipoentradaq=mysql_query("SELECT idtiposalida, tiposalida_sel FROM tiposalida WHERE idtiposalida>=1 AND idtiposalida<=5  ORDER BY idtiposalida");
		while($filtipoen=mysql_fetch_array($tipoentradaq))
		{
		?>
		<option     size="50"value="<?php echo $filtipoen['idtiposalida'];?>"><?php echo $filtipoen['tiposalida_sel'];?></option>
		<?php
		}?></select></td>
</td>
</tr>

</tr>
</tr>
<tr>
	<td class="fila2">COMENTARIO</td><td class="fila2" COLSPAN="3"><input type="text" class="textinput"  name="salida_comentario" size="120"  onChange="MAY(this)" value="" required /></td>
	<td  class="fila2"><p align="center"><button type="submit" class="botonguardar2" name="guardar_salida" title="Guardar salida" value=" Guardar salida" />Guardar Salida</button></p></td>
</tr>

<input type="hidden" name="salida_numsalida" size="30"  onChange="MAY(this)" value="<?php echo $salida;?>" /></td>

<input type="hidden" name="salida_elaboro" size="10"  onChange="MAY(this)" value="<?php echo "$_SESSION[nombres] $_SESSION[apellidos]";?>" /></td>
<input type="hidden" name="salida_dependencia" size="10"  onChange="MAY(this)" value="<?php echo $codigodependenciasalida;?>" /></td>
<?php
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
//NUMERO DE INTEMS DE LA SALIDA
$consultas="SELECT * FROM productos WHERE numsalida='$salida'";
$consulta_items=mysql_query($consultas,$conexion);

$num_productos_salida=mysql_num_rows($consulta_items);
?>
<input type="hidden" name="salida_numproductos_salida" size="30" maxlength="50" onChange="MAY(this)" value="<?php echo ($num_productos_salida);?>" /></td>

<input type="hidden" name="salida_valortotal" size="10"  onChange="MAY(this)" value="<?php echo ($valortotal);?>" /></td>

<tr>






</form>
<td>
</table>



</DIV>
</BODY  >

<?php
include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>
	



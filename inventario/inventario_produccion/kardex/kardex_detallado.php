<?php
//DESCRIPCION: PERMITE VER CADA ELEMENTO DEL KARDEX: SUS DETALLES DE ENTRADAS SALIDAS Y MOVIMIENTOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
include("../assets/global.php");
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
	<style>	


		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>kardex Detallado</title>



	<div id="div_bienvenido">
		<?php echo "Bienvenido"; ?> <BR/>
		<div id="div_usuarios">
			<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
		</div>
	</div>   
	<!--FIN IDENTIFICACION USUARIO LOGEADO -->
</head>
<body>
	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="kardex_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>
			
			
			
			<div id="centro">

				<center>
					<table width="60%" class="tabla_" border="0" style="width:580px">
						<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24
						$idelemento=$_REQUEST["idelemento"];
						$rst_usuarios=mysql_query ("SELECT * FROM consumo
							LEFT JOIN codigocontable on consumo.codigocontable=codigocontable.codigocontable
							LEFT JOIN unidadmedida on consumo.unidmedida=unidadmedida.idunidadmedida
							WHERE idelemento='$idelemento';",$conexion);
//echo $rst_clientes;
						while($filconsumo=mysql_fetch_array($rst_usuarios))
						{
							$elemento=$filconsumo['elemento'];
							$unidadmedida=$filconsumo['unidadmedida'];
							$codigocontable=$filconsumo['codigocontable'];
							$codigodescripcion=$filconsumo['codigodescripcion'];
							$minimo_stock=$filconsumo['minimo_stock'];

						}

						?>


					</tr>
				</table>


				<table width="70%" class="tabla_2" border="0" style="width:100%">

					<tr>
						<td colspan="6" class="fila1">DATOS DEL ELEMENTO</td>
					</tr>
					<tr>
						<td class="fila2"> ID ELEMENTO:</td><td class="fila2"> <?php echo $idelemento ?></td>
						<td class="fila2">ELEMENTO:</td><td class="fila2"> <?php echo $elemento ?></td>
						<td class="fila2" colspan="1">CANT. MINIMA STOCK</td>
						<td class="fila2"><?php echo $minimo_stock ?></td>
					</tr>
					<tr>

						<td class="fila2">CODIGO C:</td><td class="fila2"> <?php echo $codigocontable ?></td>
						<td class="fila2" >CATEGORIA <td class="fila2"> <?php echo $codigodescripcion ?></td>
						<td class="fila2" colspan="1">UNID. MEDIDA:</td><td class="fila2"> <?php echo $unidadmedida ?></td>

					</tr>
				</table>
			</center>

			<table width="70%" class="tabla_2" border="0" style="width:100%">
				<TR>
					<td class="fila1" colspan="3">DETALLE </td>
					<td class="fila1" colspan="3"> ENTRADA</td>
					<td class="fila1"colspan="4"> SALIDA</td>
					<td class="fila1"colspan="4"> SALDO</td>
					<TR>

						<td class="fila1"> COMENTARIO</td>
						<td class="fila1"> VR UNITARIO</td>
						<td class="fila1"> VR TOTAL</td>

						<td class="fila1"> ENTRADA</td>
						<td class="fila1"> FECHA</td>
						<td class="fila1"> CANTIDAD</td>



						<td class="fila1"> SALIDA</td>
						<td class="fila1"> FECHA</td>
						<td class="fila1"> CANTIDAD</td>
						<td class="fila1"> DEPENDENCIA</td>



						<td class="fila1"> FECHA</td>
						<td class="fila1"> CANTIDAD DISPONIBLE</td>

					</TR>
					<tr>
						<?php
						$sel_kardex="SELECT * FROM tabla_aux_KARDEX 
						LEFT JOIN dependencias on tabla_aux_KARDEX.dependencia_out_kardex=dependencias.codigodependencia
						WHERE idelemento_kardex='$idelemento' ORDER BY idtabla_kardex ASC";
						$querykardex=mysql_query($sel_kardex,$conexion);
						while ($f_kardex=mysql_fetch_array($querykardex)){
							?>
							<td class="fila2"><?php echo $f_kardex["comentario_kardex"];?></td>
							<td class="fila2">$<?php echo number_format($f_kardex["valorunit_in_kardex"],2,',','.');?></td>
							<td class="fila2">$<?php echo number_format($f_kardex["valortotal_in_kardex"],2,',','.');?></td>

							<td class="fila2a"><?php echo $f_kardex["entrada_kardex"];?></td>
							<td class="fila2a"><?php if($f_kardex["fechaentrada_kardex"]!="0000-00-00"){   echo $f_kardex["fechaentrada_kardex"];} else{ echo "";} ;?></td>
							<td class="fila2a"><?php if($f_kardex["cantidad_in_kardex"]==0){echo ""; } else {  echo $f_kardex["cantidad_in_kardex"];}?></td>

							<td class="fila2b"><?php echo $f_kardex["salida_kardex"];?></td>

							<td class="fila2b"><?php if($f_kardex["fechasalida_kardex"]!="0000-00-00"){   echo $f_kardex["fechasalida_kardex"];} else{ echo "";} ;?></td>
							<td class="fila2b"><?php if($f_kardex["cantidad_out_kardex"]==0){echo ""; } else {  echo $f_kardex["cantidad_out_kardex"];}?></td>
							<td class="fila2b"><?php echo $f_kardex["dependencia_out_kardex"]."-".$f_kardex["nombredependencia"];?></td>	


							<td class="fila2c"><?php echo $f_kardex["fecha_disponible_kardex"];?></td>
							<td class="fila2c"><?php echo $f_kardex["cantidad_disponible_kardex"];?></td>
							<tr>
								<?php   } ?>

							</table>

						</div>

						<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
						include ("../assets/footer.php");
						?>
					</body>
					</html>

					<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
session_start();
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
/*
GRABA LOS DATOS DEL TRASLADO EN UNA TABLA AUXILIAR PARA SER GUARDADA EN LOS HISTORICOS, ACTUALIZA LOS PRODUCTOS Y CREA UNA TABLA AUX DE LOS TRASLADOS LA CUAL SE COPIA 
AL FINAL A LA TABLA DE TRASLADOS.
NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
FECHA: 2015-09-14
Unidad de Servicios Penitenciarios y Carcelarios
SOLUCIONES DE PRODUCTIVIDAD
*/
//----------VARIABLES DE TRASLADO_USUARIO_CREAR.PHP
$idelemento=$_REQUEST['idelemento'];
$docid_recibe=$_REQUEST['docid_rec'];
$docid_entrega=$_REQUEST['docid_ent'];
$trasladonum3=$_REQUEST['traslado'];
$fecha_traslado3= date("Y-m-d");
$anterior=$_REQUEST['anterior'];
$elaboro=$_SESSION['nombres'] ."  ". $_SESSION['apellidos'];
//-------------------------------------------------------------------------------------------------------------------------
//------------------DATOS DEL USUARIO ENTREGA
$datos_user_entrega="SELECT usuario, nombres, apellidos, iddependencia, documentoid FROM usuarios

WHERE documentoid='$docid_entrega' ";
$queryusuarioentrega=mysql_query($datos_user_entrega,$conexion);
while ($datos_entrega=mysql_fetch_array($queryusuarioentrega)){
	$nombre_entrega=$datos_entrega['nombres']. " ". $datos_entrega['apellidos'];
	$dependencia_entrega=$datos_entrega['iddependencia'];
}
//----------------------------------------DATOS USUARIO RECIBE------------------------------------------------------------------
$datos_user_recibe="SELECT usuario, nombres, apellidos, iddependencia, documentoid FROM usuarios

WHERE documentoid='$docid_recibe' ";
$queryusuariorecibe=mysql_query($datos_user_recibe,$conexion);
while ($datos_recibe=mysql_fetch_array($queryusuariorecibe)){
	$nombre_recibe=$datos_recibe['nombres']. " ". $datos_recibe['apellidos'];
	$dependencia_recibe=$datos_recibe['iddependencia'];
}
//----------------------------------------DATOS DEL PRODUCTO--------------------------------------------------------------------
//-----------3A------Correcion Proceso de Agrear salidas-----------------------------------------------------------------------

$sel_producto="SELECT * FROM productos WHERE idelemento='$idelemento'";
$query_producto=mysql_query($sel_producto,$conexion);
while ($datos_producto=mysql_fetch_array($query_producto)){
	$unidadmedida=$datos_producto['idunidadmedida'];
	$precioadqui=$datos_producto['precioadqui'];
	$salida=$datos_producto['numsalida'];
	$codigocontable=$datos_producto['codigocontable'];
	$condicion=$datos_producto['idcondicion'];
$traslado2=$datos_producto['numtraspaso']; //traspaso anterior
$fecha_traslado2=$datos_producto['fechaultimanov'];
$traslado1=$datos_producto['ultimotraslado']; //Traspaso anterior guardado 
$fecha_traslado1=$datos_producto['fecha_ultimo_traspaso'];
}

if (empty($fecha_traslado2)){

	$fecha_traslado2='0000-00-00';
}

if (empty($fecha_traslado1)){

	$fecha_traslado1='0000-00-00';
}

//-------------------------INSERTAR DATOS A LA TABLA AUXILIAR DE TRASLADOS----------------------------------------------------------------
//Trasladonum_AUX =    |3|
//ultimotraslado_aux=  |2|
//ultimotraslado2_aux= |1|

$insertar_tabla_aux="INSERT INTO tabla_aux_traslados 
(	docid_entrega_aux, iddependencia_entrega_aux, docid_recibe_aux, iddependencia_recibe_aux, id_unidadmedida_aux, idelemento_tr_aux, codigocontable_tr_aux, 
	precioadqui_tr_aux, salidanum_tr, elaboro_aux, condicion_aux, trasladonum_aux, fecha_traslado_aux, ultimotraslado2_aux, fechaultimo_traslado2_aux,
	ultimotraslado_aux, fechaultimotraslado_aux
)          

VALUES (
	'$docid_entrega', '$dependencia_entrega', '$docid_recibe', '$dependencia_recibe', '$unidadmedida', '$idelemento','$codigocontable', 
	'$precioadqui', '$salida','$elaboro' ,'$condicion', '$trasladonum3',  '$fecha_traslado3', '$traslado2' ,'$fecha_traslado2', 
	'$traslado1' ,'$fecha_traslado1'
	)                    ";

//traslado Num =   |3|
//ultimotraslado = |2|
//ultimotraslado2 =|1| /Guardado en TABLA
$query_tabla_aux=mysql_query($insertar_tabla_aux,$conexion);
if (mysql_errno()!=0)
{
	echo "ERROR al insertar los datos...". mysql_errno(). "-". mysql_error();
	}
//----------------------------------------ACTUALIZAR PRODUCTO A NUEVO USUARIO-------------------------------------------------------------
//Poner el traslado actual , y el ultimo en ultimo traslado


$upd_producto="UPDATE productos SET 

documentoid='$docid_recibe',
dependencia='$dependencia_recibe',
numtraspaso='$trasladonum3',
fechaultimanov='$fecha_traslado3',
ultimotraslado='$traslado2',
fecha_ultimo_traspaso='$fecha_traslado2'
WHERE idelemento='$idelemento'";
$query_upd_producto=mysql_query($upd_producto,$conexion);


header ("location: traslado_usuarios_crear.php?internalid=5&docid_ent=$docid_entrega&docid_rec=$docid_recibe&traslado=$trasladonum3&anterior=$anterior");


/*
if(empty($ultimotraslado_preg)){
$upd_producto="UPDATE productos SET 
ultimotraslado='$trasladonum',
fechaultimanov='$fecha_traslado'
WHERE idelemento='$idelemento'";
$query_upd_producto=mysql_query($upd_producto,$conexion);

$upd_producto2="UPDATE tabla_aux_traslados SET 
ultimotraslado_aux='$trasladonum',
fechaultimotraslado_aux='$fecha_traslado'
WHERE (idelemento_tr_aux='$idelemento' AND trasladonum_aux='$trasladonum')";
$query_upd_producto=mysql_query($upd_producto2	,$conexion);

}

$primer_anterior="SELECT * FROM tabla_aux_traslados WHERE idelemento_tr_aux='$idelemento' AND trasladonum_aux='$trasladonum'";
$query_primer_anterior=mysql_query($primer_anterior,$conexion);

while ($datos_tabla=mysql_fetch_array($query_primer_anterior)){
$ultimotraslado_preg2=$datos_tabla['ultimotraslado_aux'];
}

*/



//------------------------------------------------------------------------------------------------------------------------------------------------------



/*
if(empty($ultimotraslado_preg2)){
$upd_producto="UPDATE tabla_aux_traslados SET 
ultimotraslado_aux='$trasladonum',
fechaultimotraslado_aux='$fecha_traslado'
WHERE idelemento='$idelemento' AND trasladonum_aux='$trasladonum' ";
$query_upd_producto=mysql_query($upd_producto,$conexion);
}
*/

/*       3A ANTIGUO
$sel_producto="SELECT * FROM productos WHERE idelemento='$idelemento'";
$query_producto=mysql_query($sel_producto,$conexion);
while ($datos_producto=mysql_fetch_array($query_producto)){
$unidadmedida=$datos_producto['idunidadmedida'];
$precioadqui=$datos_producto['precioadqui'];
$salidanum=$datos_producto['numsalida'];
$codigocontable=$datos_producto['codigocontable'];
$condicion=$datos_producto['idcondicion'];
$ultimotraslado=$datos_producto['numtraspaso']; //traspaso anterior
$fechaultimotraslado=$datos_producto['fechaultimanov'];
}
*/
/*
echo "ultimo----->". $ultimotraslado. " <-- ";
echo "<br/>";
echo $fechaultimotraslado;
echo "<br/>";
*/
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
//DESCRIPCION: VENTANA PRINCIPAL PARA ADMINISTRADORES

//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
//include("../assets/global.php");

$_SESSION['idpermiso'];



if (isset($_GET['creado'])){  if ($_GET['creado']==5){ ?>
<div class="quitarok">
	<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado El traspaso  correctamente!
</div>

<?php   }    } ?>

<?php
if (isset($_GET['borrado'])){   if ($_GET['borrado']==1){ ?>
<div class="quitarok">
	<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha anulado la El traspaso  correctamente!
</div>

<?php   }   } ?>


<?php  
if (isset($_REQUEST['zeroasign'])) { ?>
<div  class="quitarok"> EL usuario seleccionado no tiene elementos asignados</div>

<?php }?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="shortcut icon" href="../imagenes/1.ico">
	

		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Traslados</title>
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<style>	

			body {
				background: #eeeeee no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;}
				#div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;	}
				.container > header h1,
				.container > header h2 {
					color: #fff;
					text-shadow: 0 1px 1px rgba(0,0,0,0.7);
				}

			</style>

<!--IDENTIFICACION USUARIO LOGEADO
<!--NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
	<!--FECHA: 2015-07-24	 -->
	<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	<body>
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td></tr></table></div>
			<header>  
				<div id="centro">
					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						<a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="15" height="15" /></a>
					</div>   
					<center>
						<table width="65%" border="0">
							<tr>
								<td colspan="11" class="titulo"><center>
									TRASLADOS
								</center></td>
							</tr>
						</table>
					</center>
					<CENTER>
						<TABLE class="menuprincipal" width="55%" PADDING="50">
							<tr class="menuactivos" align="center"> 
								<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
								<TD><a class="titulos_menu" href="traslado_usuario1.php"><img src="../imagenes/trasladousuario.png" title="Menu de Elementos" width="50" height="50"  />Cambio de Usuario</TD>
								<TD><a class="titulos_menu" href="traslado_usuario2.php"><img src="../imagenes/trasladodependencia.png" title="Salidas" width="50" height="50"/>Cambio Dependencia</a></TD>
								<?php }?>
							</tr>
							<TR class="menugral" align="center">
								<TD><a class="titulos_menu" href="ver_traslados.php?all=1"><img src="../imagenes/vertraslados.png" title="Inventario " width="50" height="50"  />Ver Traslados</a></TD>
								<TD><a  class="titulos_menu"href="ver_traslados_anulados.php?all=1"><img src="../imagenes/bajas.png" title="Activos" width="50" height="50"  />Traslados Anulados</a></TD>



							</TR>







						</center>

					</table>


				</div>
			</body>
			</html>

			<?php 

			include ("../assets/footer.php");
			?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");
/*
GRABA LOS DATOS DEL TRASLADO EN UNA TABLA AUXILIAR PARA SER GUARDADA EN LOS HISTORICOS, ACTUALIZA LOS PRODUCTOS Y CREA UNA TABLA AUX DE LOS TRASLADOS LA CUAL SE COPIA 
AL FINAL A LA TABLA DE TRASLADOS.
NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
FECHA: 2015-09-14
Unidad de Servicios Penitenciarios y Carcelarios
SOLUCIONES DE PRODUCTIVIDAD
-------------------------------------------------------------------------------------------------------------------------------------------------------
*/
//--------OBTENER DATOS ADICIONALES PARA REALIZAR EL REGISTRO DEL TRASLADO A PARTIR DE LAS TABLAS AUXILIARES DE TRASLADOS---------------------
$docid_recibe=$_REQUEST['docid_recibe'];
$docid_entrega=$_REQUEST['docid_entrega'];
$fecha_traslado=$_REQUEST['f_date1'];
$trasladonum=$_REQUEST['trasladonum'];
$comentario=$_REQUEST['traslado_comentario'];
$elaboradopor=$_SESSION['nombres'] ."  ". $_SESSION['apellidos'];
$valortotal_traslado=$_REQUEST['traslado_valortotal'];
$tipotraslado="Traslado a otro usuario";
//-----------------------------------------------------------------------------------------------------

//------------------DATOS DEL USUARIO ENTREGA
$datos_user_entrega="SELECT usuario, nombres, apellidos, iddependencia, documentoid FROM usuarios

WHERE documentoid='$docid_entrega' ";
$queryusuarioentrega=mysql_query($datos_user_entrega,$conexion);
while ($datos_entrega=mysql_fetch_array($queryusuarioentrega)){
	$nombres_entrega=$datos_entrega['nombres'];
	$apellidos_entrega=$datos_entrega['apellidos'];
	$dependencia_entrega=$datos_entrega['iddependencia'];
}

//----------------------------------------DATOS USUARIO RECIBE------------------------------------------------------------------
$datos_user_recibe="SELECT usuario, nombres, apellidos, iddependencia, documentoid FROM usuarios

WHERE documentoid='$docid_recibe' ";
$queryusuariorecibe=mysql_query($datos_user_recibe,$conexion);
while ($datos_recibe=mysql_fetch_array($queryusuariorecibe)){
	$nombres_recibe=$datos_recibe['nombres'];
	$apellidos_recibe=$datos_recibe['apellidos'];
	$dependencia_recibe=$datos_recibe['iddependencia'];
}

//------------------OBTENER DATOS DE TABLA AUX CON NUM DE TRASLADO, CANTIDAD DE ITEMS---------------------
$peticion_tabla_aux="SELECT * FROM tabla_aux_traslados WHERE trasladonum_aux='$trasladonum'";
$query_tabla_aux=mysql_query($peticion_tabla_aux,$conexion);
$num_items=mysql_num_rows($query_tabla_aux);
/*
echo "Datos ". "<br/>";
echo "<br/>";
echo "traslado ".$trasladonum;echo "<br/>";
echo "Fecha t ".$fecha_traslado;echo "<br/>";
echo "Fecha t ".$docid_entrega;echo "<br/>";
echo "Fecha t ".$nombres_entrega;echo "<br/>";
echo "Fecha t ".$apellidos_entrega;echo "<br/>";
echo "Fecha t ".$dependencia_entrega;echo "<br/>";
echo "Fecha t ".$docid_recibe;echo "<br/>";
echo "Fecha t ".$nombres_recibe;echo "<br/>";
echo "Fecha t ".$apellidos_recibe;echo "<br/>";
echo "Fecha t ".$dependencia_recibe;echo "<br/>";
echo "Fecha t ".$elaboradopor;echo "<br/>";
echo "Fecha t ".$num_items;echo "<br/>";
echo "Fecha t ".$valortotal_traslado;echo "<br/>";
echo "Fecha t ".$tipotraslado;echo "<br/>";
echo "Fecha t ".$comentario;echo "<br/>";
*/

//---------------CREAR TRASACCIÓN CON DATOS DE SALIDA---------------------------------------
$insertar_traslado="INSERT INTO traslados (traslado, fecha_traslado, docid_entrega, nombres_entrega, apellidos_entrega, dependencia_entrega, docid_recibe, nombres_recibe, apellidos_recibe, dependencia_recibe, elaboradopor, num_items, valor_total_traslado, tipo_traslado, traslado_comentario) 
VALUES ('$trasladonum', '$fecha_traslado', '$docid_entrega', '$nombres_entrega', '$apellidos_entrega', '$dependencia_entrega', '$docid_recibe', '$nombres_recibe', '$apellidos_recibe', '$dependencia_recibe', '$elaboradopor', '$num_items', '$valortotal_traslado', '$tipotraslado', '$comentario')";
$guardar_traslado=mysql_query($insertar_traslado,$conexion);
//--------------------actualziar TABLA AUXLIAR DE TRASLADOS------------------------------------------------------------------------------------
$upd_tabla_aux="UPDATE tabla_aux_traslados SET 
fecha_traslado_aux='$fecha_traslado' WHERE trasladonum_aux='$trasladonum'";
$query_upd_tabla_aux=mysql_query($upd_tabla_aux,$conexion);


$justificacion="Guardado de Traslado de elementos "." permiso usuario: " .$_SESSION['idpermiso'];
$observacionregistro="Traslado numero: " .$trasladonum." Acceso por IP:".$ip;
$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
$fecha=date ("Y-m-d");
$hora=date("H:i:s");
$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
$queryreg2=mysql_query($registro2,$conexion);





header ("location: ver_traslados.php?add=1&all=1");
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

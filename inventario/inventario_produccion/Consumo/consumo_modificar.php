<?php
//DESCRIPCION:MODIFICAR ELEMENTOS DE CONSUMO -EVNTANA PARA EL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	


			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuario Modificar</title>



		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
		</div>   
		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<body>
		<br>
		<div id="centro">
			<a href="../principal.php?num=1"><input type="image" src="../imagenes/inicio6.png" width="38" height="38" name="regresar" title="Inicio" value="Regresar" class="botonf" /></a>
			<center>
				<table width="60%" class="tabla_" border="0" style="width:580px">
					<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24
					$idelemento=$_REQUEST["idelemento"];
					$rst_usuarios=mysql_query ("SELECT
						*
						FROM consumo
						WHERE idelemento='$idelemento';",$conexion);
//echo $rst_clientes;
					while($filconsumo=mysql_fetch_array($rst_usuarios))
					{

						?>

						<tr>
							<td class="fila1">MODIFICAR ELEMENTOS</td>
						</tr>
						<tr>
							<td class="fila2">Realizar la modificacion del elemento seleccionado...</td>
						</tr>
					</table>




					<table width="70%" class="tabla_2" border="0" style="width:580px">
						<form name="clientes" action="consumo_update.php?idelemento= <?php echo $idelemento;?>" title="Nuevo Usuario" method="post">
							<tr>
								<td colspan="6" class="fila1">DATOS DEL ELEMENTO</td>
							</tr>
							<tr>
								<td class="fila2">ELEMENTO:</td>
								<td class="fila2" colspan="4"><input type="text" class="textinput"  name="consumo_elemento" size="90"  class="input_descripcion" onChange="MAY(this)" value="<?php echo $filconsumo["elemento"] ?>"  /></td>

							</tr>
							<tr>

								<td class="fila2" colspan="1">UNID. MEDIDA:</td>
								<td class="fila2" c><select name="consumo_unidadmedida" >

									<?php
									$unidades=mysql_query("SELECT idunidadmedida, unidadmedida FROM unidadmedida WHERE (unidactivo = '1') ");
									while($filunidades=mysql_fetch_array($unidades))
									{
										if ($filunidades["idunidadmedida"]==$filconsumo["unidadmedida"])
											echo "<option selected='' value='". $filunidades['idunidadmedida'] ."'>". $filunidades['unidadmedida'] ."</option>";

										else
											echo "<option value='". $filunidades['idunidadmedida'] ."'>". $filunidades['unidadmedida'] ."</option>";
									}
									?>

								</select></td>


								<td class="fila2" colspan="1">CANT. MINIMA STOCK</td>
								<td class="fila2"><input type="text" class="textinput"  name="consumo_minimostock" size="7" maxlength="80" onChange="MAY(this)" value="<?php echo $filconsumo["minimo_stock"] ?>" /></td>

							</tr>
							<tr>
								<td class="fila2">CODIGO CONTABLE</td>
								<td class="fila2"colspan="4"><select name="consumo_codigocontable"  required="required">


									<option selected="" value=""  required="required">
										<?php
										$codigos=mysql_query("SELECT idcodigocontable, codigocontable, codigodescripcion FROM codigocontable WHERE (codactivo = '1') ");
										while($filcodigos=mysql_fetch_array($codigos))
										{
											echo "<option selected='' value='". $filcodigos['codigocontable'] ."'>". $filcodigos['codigocontable']. " &nbsp;&nbsp; ". $filcodigos['codigodescripcion'] ."</option>";

										}
										?>
									</select></td>
								</tr>
								<tr>
									<td class="fila2">VALOR UNIT:</td>
									<td class="fila2"><input type="text" class="textinput"  name="consumo_valorunit" size="12"  onChange="MAY(this)" value="<?php echo $filconsumo["precioadqui"] ?>" /></td>
									<td class="fila2">OBSERVACIONES:</td>
									<td colspan="3" class="fila2"><input type="text" class="textinput"  name="consumo_observaciones" size="65" maxlength="300"  onChange="MAY(this)" value="<?php echo $filconsumo["consobservaciones"] ?>" /></td> 
								</tr>

								<tr>

									<?php
								}
								?></td>
								<tr>
									<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
										<input type="hidden" name="idusu" value="<?php echo "ID usuario modificar $_REQUEST[id_usu]"; ?>" /></td>
										<td clas "fila2">&nbsp;</td>   <td class="fila2"><p align="center">
										<input type="submit" class="botonguardar" name="guardar_usuario" title="Guardar usuario" value="GUARDAR"/>
									</p></td>

									<td class="fila2">
										
									</a> </td> <td class="fila2" colspan="2">&nbsp;</td> 
								</tr>
							</table>
						</form></center>

						<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
						include ("../assets/footer.php");
						?>
					</body>
					</html>



					<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

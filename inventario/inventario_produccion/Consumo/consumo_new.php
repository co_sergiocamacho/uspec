<?php

//DESCRIPCION: NUEVO ELEMENTO DE CONSUMO-VENTANA DEL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/encabezado.php");

	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
		<script language="javascript">

			var cursor;
			if (document.all) {
// Está utilizando EXPLORER
cursor='hand';
} else {
// Está utilizando MOZILLA/NETSCAPE
cursor='pointer';
}

var miPopup
function nueva_categoria(){
	miPopup = window.open("categoria_new.php","miwin","width=900,height=380,scrollbars=yes");
	miPopup.focus();
}
</script>

<style> 
	<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
	body {
		background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		background-size: cover;
	}
	.container > header h1,
	.container > header h2 {
		color: #fff;
		text-shadow: 0 1px 1px rgba(0,0,0,0.7);
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nuevo Elemento de consumo</title>


<BR />
<?php 
//include('menu.php');

?>
</head>
<script src="js/calendario/src/js/jscal2.js"></script>
<script src="js/calendario/src/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

<body>
	<BR />
	<BR />
	<div id="centro">



		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
			<?php echo "SALIR";?>
			'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
		</div>   
		<table>
			<tr>
				<td>
					<a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="48" height="48" name="regresar" title="Inicio" value="Regresar" class="botonf" />INICIO</a>
				</td>


				<td>


					<a href="consumo_menu.php"><input type="image" src="../imagenes/atras.png" width="48" height="48" name="regresar" title="Inicio" value="Regresar" class="botonf" />ATRAS</a>
				</td>

			</tr>
		</table>
		<center>
			<table width="60%" class="tabla_2" border="0" style="width:580px">
				<tr>
					<td class="fila1">NUEVO ELEMENTO DE CONSUMO</td>
				</tr>
				<tr>
					<td class="fila2"> Permite el ingreso un  ELEMENTO DE CONSUMO al sistema.</td>
				</tr>
			</table>

			<table width="100%" class="tabla_2" border="0" style="">
				<form name="clientes" action="consumo_guardar.php" title="Nuevo Elemento" method="post">
					<tr>
						<td colspan="8" class="fila1">DATOS DEL ELEMENTO</td>
					</tr>
					<tr>
						<td class="fila2">ELEMENTO:</td>
						<td class="fila2" colspan="6"><input type="text" class="textinput"  name="consumo_elemento" size="126"  class="input_descripcion" onChange="MAY(this)" value=""  required/></td>

					</tr>
					<tr>

						<td class="fila2" colspan="1">UNID. MEDIDA:</td>
						<td class="fila2" colspan="1" ><select name="consumo_unidadmedida" required>

							<option selected="" value= ""colspan="2"></option>
							<?php
							$unidades=mysql_query("SELECT idunidadmedida, unidadmedida FROM unidadmedida WHERE (unidactivo = '1') ");
							while($filunidades=mysql_fetch_array($unidades))
							{
								if ($filunidades["idunidadmedida"]==$filconsumo["unidadmedida"])
									echo "<option selected='' value='". $filunidades['idunidadmedida'] ."'>". $filunidades['unidadmedida'] ."</option>";

								else
									echo "<option value='". $filunidades['idunidadmedida'] ."'>". $filunidades['unidadmedida'] ."</option>";
							}
							?>

						</select></td>


						<td class="fila2" colspan="1">CANT. MINIMA STOCK</td>
						<td class="fila2"><input type="text" class="textinput"  name="consumo_minimostock" size="7" maxlength="80" onkeypress="return SoloNumeros(event)" value="" /></td>
						<td class="fila2"colspan="2">VALOR UNIT:</td>
						<td class="fila2"colspan="6"><input type="text" class="textinput"  name="consumo_valorunit" size="20"  onkeypress="return SoloNumeros(event)" value="" /></td>
					</tr>
					<tr>
						<td class="fila2">CODIGO CONTABLE</td>
						<td class="fila2"colspan="3"><select name="consumo_codigocontable" required>


							<option selected="" value="">
								<?php
								$codigos=mysql_query("SELECT idcodigocontable, codigocontable, codigodescripcion FROM codigocontable",$conexion);
								while($filcodigos=mysql_fetch_array($codigos))
								{


									echo "<option selected='' value='". $filcodigos['codigocontable'] ."'>". $filcodigos['codigocontable']. " &nbsp;&nbsp;  ". $filcodigos['codigodescripcion'] ."</option>";

								}
								?>
							</select></td><td class="fila2" colspan="3">&nbsp;</td> 
						</tr>
						<tr>

							<td class="fila2">OBSERVACIONES:</td>
							<td colspan="5" class="fila2"><input type="text" class="textinput"  name="consumo_observaciones" size="65" maxlength="300"  onChange="MAY(this)" value="" /></td> 
							<td class="fila2" colspan="3">&nbsp;</td> 
						</tr>


						<tr>
							<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
								<input type="hidden" name="idusu" value="<?php echo "ID usuario modificar $_REQUEST[id_usu]"; ?>" /></td>
								<td class= "fila2" colspan="1">&nbsp;</td>   <td class="fila2"><p align="center">
								<input type="submit" class="botonguardar" name="guardar_usuario" title="Guardar usuario" value="GUARDAR"/>
							</p></td>

							<td class="fila2">
								<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="CANCELAR"/>
							</a> </td> <td class="fila2" colspan="3">&nbsp;</td> 
						</tr>
					</table>

				</form>
			</center>
		</div>
	</body>
	</html>
	<?php

	include ("../assets/footer.php");
	?>


	<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

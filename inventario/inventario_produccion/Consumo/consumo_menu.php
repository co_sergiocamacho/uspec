<?php
//DESCRIPCION: MENU  DE ELEMENTOS DE CONSUMO PARA EL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
	include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<script type="text/javascript" src="../js/buscar.js"></script>
		<script type="text/javascript" src="../js/dataTables.min.js"></script>
		<script type="text/javascript" src="../js/jquery.js"></script>
		<?php
// <script type="text/javascript" src="../js/tablas.js"></script>
		?>

		<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
		<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
		<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
		<script src="../js/calendario/src/js/jscal2.js"></script>
		<script src="../js/calendario/src/js/lang/en.js"></script>
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

		<style>	
			<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
			body {
				background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}
		</style>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>ELEMENTOS DE CONSUMO</title>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".search").keyup(function () {
					var searchTerm = $(".search").val();
					var listItem = $('.results tbody').children('tr');
					var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
					
					$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
						return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
					}
				});
					
					$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','false');
					});

					$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
						$(this).attr('visible','true');
					});

					var jobCount = $('.results tbody tr[visible="true"]').length;
					$('.counter').text(jobCount + ' Elementos');

					if(jobCount == '0') {$('.no-result').show();}
					else {$('.no-result').hide();}
				});
			});

		</script>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />

	</head>

	<body>


		<?php
		if (isset($_GET['creado'])){ if ($_GET['creado']==5){?>
		<div class="quitarok">
			<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado el elemento correctamente!
		</div>

		<?php   }    } ?>
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="../entradas/elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>
				<?php 
/*
<td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>

*/
?>
<div id="centro">
	<div id="div_bienvenido">
		<?php echo "Bienvenido"; ?> <BR/>
		<div id="div_usuarios">
			<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
		</div>
		<?php echo "SALIR";?>
		<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
	</div>
	<div id="centro">
		<center>
			
		</center>
		<?php

		if (isset($_GET['add'])){ if ($_GET['add']==1){?>
		<div class="quitarok">
			<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Elemento creado correctamente!
		</div>

		<?php   }    } ?> 
	</center></td>
</tr>
<tr>

	<table width="100%" border="0">
		<tr>
			<td class="titulo"><center>
				ELEMENTOS DE CONSUMO</center></td>
			</tr>
		</table>
		<td class="texto">
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
	<?php
	if ($_SESSION['idpermiso']==1){
		?>
		<td><a href="consumo_new.php"></td></a>



		<?php
	}
	?>


	<?php

//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
	$t_consumo=mysql_query("SELECT
		*
		FROM CONSUMO 
		LEFT JOIN unidadmedida on consumo.unidmedida=unidadmedida.idunidadmedida  
		LEFT JOIN  codigocontable on consumo.codigocontable=codigocontable.codigocontable
		WHERE consactivo='1'
		", $conexion);
//echo $t_clientes;
	$num_registro=mysql_num_rows($t_consumo);
	?>

</div>
<div id="centro">

	<center>
		<table class="menuactivos" width="900">
			<tr class="menuactivos">
				<td coslpan="1"> <a class="titulos_menu"  href="consumo_new.php">  

					<img src="../imagenes/add.png" title="Nuevo Usuario" width="38" height="38" alt="nuevo"  align="left" />Agregar Elementos</a>


				</table></center>
				

				<center>
					<div id="centro">
						<div class="form-group pull-left">
							<h4>Escriba su busqueda aqui</h4>
							<input type="text" class="search form-control" placeholder="Consulta por cedula, id, serial, placa, entrada, salida etc..." size="80">
						</div>
						<br>
						<span class="counter pull-left"></span>
						<div class="container-full">
							<br>
							<div class="table-responsive " >


								<a href="../reportes/registro_excel_consumo.php"><input type="image" src="../imagenes/excel.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">	Descargar Inventario de consumo</a>
								<table   class=" table table-bordered results"  cellspacing="0" width="100%" >
									<thead>
										
										<th  class="fila1">ELEMENTO</th>
										<th  class="fila1">CANTIDAD DISPONIBLE</th>
										<th  class="fila1">UNID. MEDIDA</th>
										<th  class="fila1">CODIGO CONTABLE</th>
										<th  class="fila1">DESCRIPCION CODIGO</th>
										<th  class="fila1">VALOR UNITARIO</th>
										<th  class="fila1">VALOR TOTAL</th>
										<th  class="fila1">CANTIDAD MIN STOCK</th>
										<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
										<th  class="fila1">ACCIONES</th>
										<?php }?>

										<th  class="fila1">AVISO</th>
									</thead>
									<tbody id="myTable2">

										<?php
										while ($fila_consumo=mysql_fetch_array($t_consumo))
										{
											?><TR>
											<td class="fila2"><?php echo $fila_consumo["elemento"];?></td>
											<td class="fila2"><?php echo $fila_consumo["cantidad"];?></td>
											<td class="fila2"><?php echo $fila_consumo["unidadmedida"];?></td>
											<td class="fila2"><?php echo $fila_consumo["codigocontable"];?></td>
											<td class="fila2"><?php echo $fila_consumo["codigodescripcion"];?></td>
											<td class="fila2"align="right">$<?php echo number_format($fila_consumo["precioadqui"],2,',','.');;?></td>
											<td class="fila2"align="right">$<?php echo number_format(($fila_consumo["precioadqui"]*$fila_consumo["cantidad"]),2,',','.');?></td>
											<td class="fila2" align="center"><?php echo $fila_consumo["minimo_stock"];?></td>
											<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
										</center></td>
										<td class="fila2"><center>
											<a href="consumo_modificar.php?idelemento=<?php echo $fila_consumo['idelemento'];?>"><img src="../imagenes/editar1.png" title="Modificar Elemento" width="28" height="28"/></a>
										</center></td>
										<?php }?>
										<?php 
										if ($fila_consumo["minimo_stock"]>=$fila_consumo["cantidad"]  ){

											?>   <td class="fila2" > <p class="alerta2">Baja cantidad de<br> este elemento</p></td>   <?php } else {
												?> <td class="fila2"></td>
												<TR>


													<?php
												}
											}?>   

											<?php
											$querysuma = mysql_query("SELECT SUM(precioadqui*cantidad) as total FROM consumo WHERE consactivo='1'");   
											$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
											$valortotal=$valortotal1["total"];
											?>
											<TR>
												<TD class="fila2"colspan="5"> &nbsp;</TD>
												<TD class="fila2"colspan="1"><STRONG>VALOR TOTAL</STRONG></TD>

												<td  class="fila2"align="right"><strong>$<?php echo number_format($valortotal,2,',','.');?></strong></td>
												<TD class="fila2"colspan="3"> &nbsp;</TD>
											</tr>

										</tbody>
										<tr class="warning no-result">
											<td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
										</tr>
									</TABLE>
								</div>
								<center>
									<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
									<div class="col-md-12 text-center">

									</div>
								</div>
							</div>

						</center>
					</table>
				</div>


			</body>
			<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
			include ("../assets/footer.php");

			?>
		</body>			</html>

		<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

<?php
/* --------------------------------------------------------------------------
//DESCRIPCION: CREACION DE PDF DE HISTORIAL DE ELEMENTOS ACTIVOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//---------------------------------------------------------------------------
*/
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
	require("../pdf/fpdf.php");
	include ("../assets/datosgenerales.php");
	include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------
	$idelemento=$_REQUEST['idelemento'];


	$justificacion="Descarga de PDF de historial del elemento "."-  permiso usuario: " .$_SESSION['idpermiso'];
	$observacionregistro="Elemento ID: " .$idelemento." Acceso por IP:".$ip;
	$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
	$fecha=date ("Y-m-d");
	$hora=date("H:i:s");
	$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
	$queryreg2=mysql_query($registro2,$conexion);



	if(isset($_REQUEST['idelemento'])){


		$query2="SELECT  * FROM historial_productos  
		LEFT JOIN codigocontable on historial_productos.codigocontable=codigocontable.codigocontable
		LEFT JOIN dependencias on historial_productos.dependencia=dependencias.codigodependencia	
		LEFT  JOIN usuarios on historial_productos.documentoid=usuarios.documentoid
		LEFT  JOIN ubicacion on historial_productos.idubicacion=ubicacion.id_ubicacion
		WHERE activo='1'  AND  idelemento='$idelemento' ORDER BY  numsalida DESC";
		$t_prod=mysql_query($query2,$conexion);
		$pregun0=mysql_num_rows($t_prod);





		$fecha=date('Y-m-d');
		class PDF extends FPDF
		{
//Page header
			function Header()
			{
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(185, -5, "HISTORIAL DEL ELEMENTO", 0, 0, 'C');
$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,160,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);
$this->Line(10, 25 , 200, 25);  
}


function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',8);
    //Pagina numero
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}
$pdf=new PDF();
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(30);

//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(30, 5, "FECHA", 0);
$pdf->Cell(50, 5, $fecha,0 , 'C');
$pdf->Ln(5);
//------------------| DATOS UNIDAD EJECUTORA------------>
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "U Ejecutora:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(120, 5, $unidadejecutora,0 , 'C');
$pdf->Cell(15, 5, "NIT", 0);
$pdf->Cell(30, 5,$nitunidadejecutora, 0);
$pdf->Ln(5);
//-----------------DATOS DE CONTACTO Y FACTURA  

//-------------------COMENTARIO----------------



$query12="SELECT  * FROM productos 
LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia
WHERE idelemento='$idelemento'"  ;
$t_entradas2=mysql_query($query12,$conexion);
$inicial2=mysql_num_rows($t_entradas2);

if(!empty($inicial2)){
	while ($fen2=mysql_fetch_array($t_entradas2)){
		$docufun=$fen2['documentoid'];
		$nombrefun=$fen2['nombres'];
		$apellidofun=$fen2['apellidos'];
		$dependenciafun=$fen2['dependencia'];
		$ubifun=$fen2['nombreubicacion'];
		$fecha_origen=$fen2['fecha_origen'];
		$entrada_origen=$fen2['numentrada_origen'];


	}
}
$query1="SELECT  * FROM productos 
LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia
LEFT JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
WHERE idelemento='$idelemento'"  ;
$t_entradas=mysql_query($query1,$conexion);
$inicial=mysql_num_rows($t_entradas);


if(!empty($inicial)){
//$pdf->setFillColor(100, 100, 100);
	$pdf->Cell(65,5, "",0,'C'); 
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Cell(21, 5, "USUARIO ACTUAL", 0, 'C'); 
	$pdf->Ln(5);    
	$pdf->SetFont('Helvetica','',8);
	$pdf->Cell(21, 5, "Documento ID", 0, 'C');
	$pdf->Cell(25,5,$docufun,0,'C'); 
	$pdf->Cell(50, 5, " Nombres y Apellidos", 0, 'C');
	$pdf->Cell(60, 5, utf8_decode($nombrefun) . " ". utf8_decode($apellidofun), 0, 'C');
	$pdf->Ln(5); 
	$pdf->Cell(21, 5, "Dependencia", 0, 'C');
	$pdf->Cell(21,5,$dependenciafun,0,'C'); 
	$pdf->Cell(21, 5, " Ubicacion", 0, 'C');
	$pdf->Cell(15,5,$ubifun,0,'C'); 
	$pdf->Ln(6);   

	$pdf->Cell(25, 5, "Entrada Num: ", 0, 'C');
	$pdf->Cell(21,5,$entrada_origen,0,'C'); 
	$pdf->Cell(28, 5, "Fecha Ingreso: ", 0, 'C');	
	$pdf->Cell(21,5,$fecha_origen,0,'C'); 
	$pdf->Ln(6);   
}  

$pdf->SetFont('Helvetica','B',8);
$pdf->Cell(95 , 5, "     DETALLES DEL ELEMENTO", 0, 'C');


$query1="SELECT  * FROM productos 
LEFT  JOIN usuarios on productos.documentoid=usuarios.documentoid
LEFT  JOIN ubicacion on productos.idubicacion=ubicacion.id_ubicacion
LEFT JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
LEFT JOIN dependencias on productos.dependencia=dependencias.codigodependencia
LEFT JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
WHERE idelemento='$idelemento'"  ;
$t_entradas=mysql_query($query1,$conexion);
$inicial=mysql_num_rows($t_entradas);








$pdf->SetFont('Helvetica','B',8);
$pdf->Cell(15, 5, " CAT.", 0, 'C');
$pdf->Cell(23, 5, "   SERIE", 0, 'C');
$pdf->Cell(16, 5, "   PLACA", 0, 'C');
$pdf->Cell(16, 5, "  UNIDAD", 0, 'C');
$pdf->Cell(18, 5, " VALOR", 0, 'C');
$pdf->SetFont('Helvetica','',8, 'C');
$pdf->Ln(5);  
$pdf->Ln(3); 
$pdf->SetFont('Helvetica','',8);


while ($fen=mysql_fetch_array($t_entradas)){
//---------------------------------------------

	$y = $pdf->GetY();
	$pdf->SetFont('Helvetica','',7, 'C');
	$pdf->MultiCell(90,4,utf8_decode($fen['elemento']),0,'J'); 
	$y1=$pdf->GetY();
	$x1=$pdf->GetX();
	$pdf->SetXY(105,$y+5);
	$pdf->Line(10, $y-1, 210-10, $y-1);
	$pdf->Cell(14, 4, $fen['codigocontable'], 0, 'C');
	$pdf->SetFont('Helvetica','',6, 'C');
	$pdf->Cell(28, 4, $fen['serie'], 0, 'C');
	$pdf->SetFont('Helvetica','',7, 'C');	
	$pdf->Cell(16, 4, $fen['codebar'], 0, 'C');
	$pdf->Cell(16,4,$fen['unidadmedida'],0,'L'); 
	$pdf->Cell(2,4,"$",0,'L'); 
	$pdf->Cell(18,4,number_format($fen['precioadqui'],2,',','.'),0,'R'); 
	$pdf->Ln(8); 


//------------------------------------------------------------------------------------------------------

	$query2="SELECT  * FROM historial_productos  
	LEFT JOIN codigocontable on historial_productos.codigocontable=codigocontable.codigocontable
	LEFT JOIN dependencias on historial_productos.dependencia=dependencias.codigodependencia	
	LEFT  JOIN usuarios on historial_productos.documentoid=usuarios.documentoid
	LEFT  JOIN ubicacion on historial_productos.idubicacion=ubicacion.id_ubicacion
	WHERE activo='1'  AND  idelemento='$idelemento' ORDER BY  numsalida DESC";
	$t_prod=mysql_query($query2,$conexion);
	$pregun0=mysql_num_rows($t_prod);

	if(!empty($pregun0))	{
		$pdf->Ln(12);  
		$pdf->SetFont('Helvetica','b',9, 'C');
		$pdf->Cell(60 , 5, " ", 0, 'C');
		$pdf->Cell(105 , 5, "HISTORIAL DE SALIDAS DEL  ELEMENTO", 0, 'C');
		$pdf->SetFont('Helvetica','b',8, 'C');
		$pdf->Ln(7);  

		$pdf->Cell(17, 5, "FECHA", 0, 'C');
		$pdf->Cell(13, 5, "SALID.", 0, 'C');
		$pdf->Cell(80, 5, "DEPENDENCIA", 0, 'C');
		$pdf->Cell(22, 5, "DOCUMENTO ", 0, 'C');
		$pdf->Cell(45, 5, "NOMBRES Y APELLIDOS", 0, 'C');
		$pdf->Ln(5);  
		$pdf->SetFont('Helvetica','',7, 'C');
	}
	while ($f_productos=mysql_fetch_array($t_prod)){

		$pdf->Cell(17, 5,$f_productos["fechaasig"] , 0, 'C');
		$pdf->Cell(14, 5,$f_productos["numsalida"] , 0, 'C');
		$pdf->Cell(80, 5,$f_productos["nombredependencia"]  , 0, 'C');
		$pdf->Cell(22, 5,$f_productos["documentoid"] , 0, 'C');
		$pdf->Cell(45, 5,$f_productos["nombres"]." " .$f_productos["apellidos"] , 0, 'C');
		$pdf->Ln(5);  
		$y2 = $pdf->GetY();
		$y3 = $pdf->GetY();
		if ($y2>255){
			$pdf-> AddPage();
		}
	}
	$y4 = $pdf->GetY();
	if ($y4>255){
		$pdf-> AddPage();
	}
}
$pdf->Ln(9);
//------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------

$query2tr="SELECT  * FROM tabla_aux_traslados  
LEFT JOIN usuarios on tabla_aux_traslados.docid_entrega_aux=usuarios.documentoid
LEFT JOIN dependencias on tabla_aux_traslados.iddependencia_entrega_aux=dependencias.codigodependencia
WHERE activo_aux_tr='1' AND idelemento_tr_aux='$idelemento'  ORDER BY  trasladonum_aux DESC"  ;
$t_prod=mysql_query($query2tr,$conexion);
$pregun1=mysql_num_rows($t_prod);

if(!empty($pregun1)){
	$pdf->SetFont('Helvetica','b',9, 'C');
	$pdf->Cell(30 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE TRASLADOS DEL  ELEMENTO (Datos de Funcionario que entrega)", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');

	$pdf->Cell(17, 5, "FECHA", 0, 'C');
	$pdf->Cell(18, 5, "TRASL", 0, 'C');
	$pdf->Cell(80, 5, "  DEPENDENCIA ", 0, 'C');
	$pdf->Cell(22, 5, "DOCUMENTO ", 0, 'C');
	$pdf->Cell(45, 5, "NOMBRES Y APELLIDOS entrega", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','',7, 'C');

	while ($f_trasl=mysql_fetch_array($t_prod)){

		$pdf->Cell(17, 5,$f_trasl["fecha_traslado_aux"] , 0, 'C');
		$pdf->Cell(18, 5,$f_trasl["trasladonum_aux"] , 0, 'C');
		$pdf->Cell(80, 5,$f_trasl["nombredependencia"] , 0, 'C');
		$pdf->Cell(22, 5,$f_trasl["docid_entrega_aux"] , 0, 'C');
		$pdf->Cell(45, 5,$f_trasl["nombres"]." ".$f_trasl["apellidos"] , 0, 'C');

		$pdf->Ln(5);

		$y5 = $pdf->GetY();
		if ($y5>255){
			$pdf-> AddPage();
		}
	}
	$y6 = $pdf->GetY();
	if ($y6>255){
		$pdf-> AddPage();
	} 
//Cierra IF De Datos vacios
}

//------------------------------------------------------------------------------------------------------

$pdf->Ln(4);
$query2tr="SELECT  * FROM tabla_aux_traslados  
LEFT JOIN usuarios on tabla_aux_traslados.docid_recibe_aux=usuarios.documentoid
LEFT JOIN dependencias on tabla_aux_traslados.iddependencia_recibe_aux=dependencias.codigodependencia
WHERE activo_aux_tr='1' AND idelemento_tr_aux='$idelemento'  ORDER BY  trasladonum_aux DESC"  ;
$t_prod=mysql_query($query2tr,$conexion);
$pregun1=mysql_num_rows($t_prod);

if(!empty($pregun1)){
	$pdf->SetFont('Helvetica','b',9, 'C');
	$pdf->Cell(30 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE TRASLADOS DEL  ELEMENTO (Datos de Funcionario que recibe)", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');


	$pdf->Cell(17, 5, "FECHA", 0, 'C');
	$pdf->Cell(18, 5, "TRASL.", 0, 'C');
	$pdf->Cell(80, 5, "DEPENDENCIA ", 0, 'C');
	$pdf->Cell(22, 5, "DOCUMENTO ", 0, 'C');
	$pdf->Cell(45, 5, "NOMBRES Y APELLIDOS RECIBE", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','',7, 'C');


	while ($f_trasl=mysql_fetch_array($t_prod)){

		$pdf->Cell(17, 5,$f_trasl["fecha_traslado_aux"] , 0, 'C');
		$pdf->Cell(16, 5,$f_trasl["trasladonum_aux"] , 0, 'C');
		$pdf->Cell(80, 5,$f_trasl["nombredependencia"] , 0, 'C');
		$pdf->Cell(22, 5,$f_trasl["docid_recibe_aux"] , 0, 'C');
		$pdf->Cell(45, 5,$f_trasl["nombres"]." ".$f_trasl["apellidos"] , 0, 'C');

		$pdf->Ln(5);

		$y5 = $pdf->GetY();
		if ($y5>255){
			$pdf-> AddPage();
		}
	}
	$y6 = $pdf->GetY();
	if ($y6>255){
		$pdf-> AddPage();
	} 
//Cierra IF De Datos vacios
}

//------------------------------------------------------------------------------------------------------

$pdf->Ln(8);
$query2tr="SELECT  * FROM tabla_aux_reintegros  
LEFT JOIN usuarios on tabla_aux_reintegros.documentoid_aux=usuarios.documentoid
LEFT JOIN dependencias on tabla_aux_reintegros.dependencia_aux=dependencias.codigodependencia
WHERE  idelemento_r_aux='$idelemento'  ORDER BY  fecha_entrada_aux DESC" ;
$t_prod2=mysql_query($query2tr,$conexion);
$pregun2=mysql_num_rows($t_prod2);

if(!empty($pregun2)){

	$pdf->SetFont('Helvetica','b',9, 'C');
	$pdf->Cell(60 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE RENTEGROS", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');


	$pdf->Cell(17, 5, "FECHA", 0, 'C');
	$pdf->Cell(16, 5, "ENTR.", 0, 'C');
	$pdf->Cell(80, 5, "DEPENDENCIA ", 0, 'C');
	$pdf->Cell(22, 5, "DOCUMENTO ", 0, 'C');
	$pdf->Cell(45, 5, "NOMBRES Y APELLIDOS", 0, 'C');
	$pdf->Ln(5);
	while ($f_rein=mysql_fetch_array($t_prod2)){
		$pdf->SetFont('Helvetica','',7, 'C');
		$pdf->Cell(17, 5,$f_rein["fecha_entrada_aux"] , 0, 'C');
		$pdf->Cell(16, 5,$f_rein["entrada_r_aux"] , 0, 'C');
		$pdf->Cell(80, 5,$f_rein["dependencia_aux"] ."-".$f_rein["nombredependencia"] , 0, 'C');
		$pdf->Cell(20, 5,$f_rein["documentoid_aux"] , 0, 'C');
		$pdf->Cell(45, 5,$f_rein["nombres"]." ".$f_rein["apellidos"] , 0, 'C');
		$pdf->Ln(5);

		$y6 = $pdf->GetY();
		if ($y6>255){
			$pdf-> AddPage();
		}
	}

	$y7 = $pdf->GetY();
	if ($y7>255){
		$pdf-> AddPage();
	}
// CierreIF datos vacios
}
$pdf->Ln(8);





$nombresalida="USPEC_Elem"."-".$idelemento."-".$fecha. ".pdf";
$pdf->Output($nombresalida,'D');


} else {
	$pdf->Ln(18);

	$pdf->SetFont('Helvetica','B',12, 'C');

	$pdf->Cell(45 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "NO SE ENCONTRARON DATOS DE ESTE ELEMENTO", 0, 'C');
	$nombresalida="USPEC_Elem"."-".$idelemento."-".$fecha. ".pdf";
	$pdf->Output($nombresalida,'D');


}

}


?>


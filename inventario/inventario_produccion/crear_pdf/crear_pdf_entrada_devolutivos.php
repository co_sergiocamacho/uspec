
<?php
/* --------------------------------------------------------------------------
//DESCRIPCION: CREACION DE PDF DE ENTRADA DE ACTIVOS FIJOS DEVOLUTIVOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//---------------------------------------------------------------------------
*/

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
	require("../pdf/fpdf.php");
	include ("../assets/datosgenerales.php");
	include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------




	$numentrada=$_REQUEST['entrada'];
//$valortotal=$_REQUEST['valortotal'];
//$cant_items=$_REQUEST['cant_items'];
//$tipoelementos=$_REQUEST['tipoelementos'];
//$new=$_REQUEST['new'];


	$justificacion="Descarga de PDF de entrada de activos "."-  permiso usuario: " .$_SESSION['idpermiso'];
	$observacionregistro="entrada numero: " .$numentrada." Acceso por IP:".$ip;
	$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
	$fecha=date ("Y-m-d");
	$hora=date("H:i:s");
	$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
	$queryreg2=mysql_query($registro2,$conexion);



	$selentrada="SELECT  * FROM entradas 
	LEFT  JOIN tipoentrada on entradas.tipoentrada_sel=tipoentrada.idtipoentrada
	where entrada='$numentrada'";
	$queryselentrada=mysql_query($selentrada,$conexion);
	while ($datos_entrada=mysql_fetch_array($queryselentrada)){
		$fecha=$datos_entrada['fecha'];
		$tipomovimiento=$datos_entrada['tipoelementos'];
		$idproveedor=$datos_entrada['idproveedor'];
		$cant_items=$datos_entrada['numitems'];
		$comentario=$datos_entrada['comentario'];
		$contrato=$datos_entrada['contrato'];
		$valortotal=number_format($datos_entrada['valortotal'],2,',','.');
		$factura=$datos_entrada['entnumfactura'];
		$tipoentrada=$datos_entrada['tipoelementos'];
		$tipoentrada_sel=$datos_entrada['tipoentrada_sel'];
		$elaboradopor=$datos_entrada['elaboradopor'];
	}
	$entrada=$numentrada;

	


	if($cant_items!=0){
//--------------------------OBTENER DATOS DEL PROVEEDOR SEGUN LA INFORMACION ALMACENADA EN LA ENTRADA--
		$selproveedor="SELECT proveedor, nit FROM proveedores where idproveedor='$idproveedor'";
		$queryselproveedor=mysql_query($selproveedor,$conexion);
		while ($datos_proveedor=mysql_fetch_array($queryselproveedor)){

			$responsable=$datos_proveedor['proveedor'];
			$nit=$datos_proveedor['nit'];
		}
//$descripcion="COMPUTADOR PORTATIL DE 17 PULGADAS PROCESADOR INTEL MEMORIA 4GB DISCO DURO DE 1 GB CON CARGADOR, CABLES TECLADO Y MOUSE SE ENTREGA PARA CAMBIO DE EQUIPO
//POR REPOSICION Y ACTUALIZACION DE SISTEMA  OPERATIVO, SE DEBE INSTALAR Y CONFIGURAR ADAPTADORES DE RED";
//$valorunitario=1900000;
	}
//-----------OBTENER DATOS DE LOS PRODUCTOS ANEXOS A LA ENTRADA------------------------

//----------------------------
	$date=date('Y-m-d');
	class PDF extends FPDF
	{
//Page header
		function Header()
		{

			global $numentrada;
			global $fecha;
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(185, -5, "COMPROBANTE DE ENTRADA", 0, 0, 'C');

$this->Ln(5);
$this->SetFont('Helvetica','B',12);
$this->Cell(185, -5, $numentrada ."    Fecha: " . $fecha, 0, 0, 'C');
$this->Line(10, 28 , 200, 28); 

$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,160,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);

}


function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',8);
    //Pagina numero
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}
$pdf=new PDF();
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(35);


//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(40, 5, "FECHA", 0);
$pdf->Cell(50, 5, $fecha,0 , 'C');
$pdf->Cell(45, 5, "No", 0);
$pdf->SetFont('Helvetica','B',11);
$pdf->Cell(60, 5, $entrada, 0, 'B');
$pdf->SetFont('Helvetica','',9);
$pdf->Ln(5);
$pdf->Cell(40, 5, "Tipo Comprobante", 0);
$pdf->SetFont('Helvetica','B',10);
$pdf->Cell(50, 5, $tipoentrada, 0);
$pdf->Ln(5);
$pdf->Cell(40, 5, "Tipo de Movimiento", 0);
$pdf->Cell(50, 5, $tipoentrada_sel, 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(45, 5, "Tipo de Movimiento", 0);
$pdf->SetFont('Helvetica','B',10);
//$pdf->Cell(60, 5, $tipomovimiento, 0);
$pdf->Ln(5);
if($cant_items!=0){
//---------------DATOS DE PROVEEDOR
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Cell(30, 5, "Responsable:", 0);
	$pdf->SetFont('Helvetica','',9);
	$pdf->Cell(120, 5, utf8_decode($responsable),0 , 'C');
	$pdf->Cell(15, 5, "NIT", 0);
	$pdf->Cell(30, 5,$nit, 0);
	$pdf->Ln( 5);
}

//------------------| DATOS UNIDAD EJECUTORA------------>
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "U Ejecutora:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(120, 5, $unidadejecutora,0 , 'C');
$pdf->Cell(15, 5, "NIT", 0);
$pdf->Cell(30, 5,$nitunidadejecutora, 0);
$pdf->Ln(5);

if($cant_items!=0){
//-----------------DATOS DE CONTACTO Y FACTURA  
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Cell(30, 5, "Contrato No:", 0);
	$pdf->SetFont('Helvetica','',9);
	$pdf->Cell(105, 5,$contrato, 0);
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Ln(5);
	$pdf->Cell(30, 5, "Factura No:", 0);
	$pdf->SetFont('Helvetica','',9);
	$pdf->Cell(60, 5,$factura, 0);
	$pdf->Ln(2);
//-------------------COMENTARIO----------------
	$pdf->Ln(6);
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Cell(0, 5, "Comentario:",0);
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','',7);
	$pdf->Multicell(195, 3,$comentario, 0, "L");
	$pdf->Ln(2);
	$yl = $pdf->GetY();
	$pdf->Line(10,  $yl, 210-10, $yl);
	$pdf->Line(10,  $yl+0.2, 210-10, $yl+0.2);
	$pdf->Ln(6);

	$pdf->SetFont('Helvetica','B',8, 'C');
//$pdf->setFillColor(100, 100, 100);
	$pdf->Cell(105 , 5, "                  DESCRIPCION DE LA MERCANCIA", 0, 'C');
	$pdf->Cell(22, 5, " CATEGORIA", 0, 'C');
	$pdf->Cell(28, 5, "   SERIE", 0, 'C');
	$pdf->Cell(21, 5, "  UNIDAD", 0, 'C');
	$pdf->Cell(25, 5, " VALOR", 0, 'C');
	$pdf->SetFont('Helvetica','',8, 'C');
	$pdf->Ln(5);  
	$pdf->Ln(3); 
	$query1="SELECT  * FROM productos 
	LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida WHERE numentrada='$numentrada'"  ;
	$t_entradas=mysql_query($query1,$conexion);


	while ($fen=mysql_fetch_array($t_entradas)){
//---------------------------------------------
		$y = $pdf->GetY();
		$pdf->SetFont('Helvetica','',7, 'C');
		$pdf->MultiCell(105,3,utf8_decode($fen['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(120,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(14, 3, $fen['codigocontable'], 0, 'C');
		$pdf->Cell(33, 3, $fen['serie'], 0, 'C');

		$pdf->Cell(19,3,$fen['unidadmedida'],0,'L'); 
		$pdf->Cell(2,3,"$",0,'L'); 

		$pdf->Cell(25,3,number_format($fen['precioadqui'],2,',','.'),0,'R'); 

//$pdf->MultiCell(22,5,$y,0,'L')  ;
//$pdf->MultiCell(22,5,$y1,0,'L')  ;

		$pdf->SetY($y1);
		$pdf->Ln();  
		$y2 = $pdf->GetY();

		if ($y2>255){
			$pdf-> AddPage();

		}

	}

	$y3 = $pdf->GetY();
	if ($y2<250){
	//----------------------FIRMA Y --VALOR TOTAL--------------------------

		$pdf->SetY($y3);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2);  
		$yl = $pdf->GetY();
		$pdf->SetY($yl+1);
		$pdf->Line(10, $yl-0.9, 210-10, $yl-0.9);
		$pdf->Line(10, $yl-1, 210-10, $yl-1);


//$pdf->Cell(190,1,'    ',1,'L'); 
		$pdf->SetFont('Helvetica','B',9, 'C');
		$pdf->Ln(2); 
		$pdf->SetFont('Helvetica','B',9, 'C');
		$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
		$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 
		$pdf->Cell(50,5,'    ',0,'L'); 
		$pdf->Cell(32,5,'   VALOR TOTAL  ',0,'L'); 

		$pdf->Cell(7,5,'  $',0,'L'); 
		$pdf->Cell(32,5,$valortotal,0,'L'); 

		$pdf->Ln(10);  
		$pdf->SetFont('Helvetica','B',1, 'C');
		$pdf->Cell(77,20,'  ',1,'L'); 
		$pdf->Cell(27,5,'                         ',0,'L'); 
		$pdf->Cell(77,20,'                         ',1,'L'); 
		$pdf->Ln(12);  
		$pdf->Cell(5,5,'    ',0,'L'); 
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(67,5,'   COORDINADOR    GRUPO ADMINISTRATIVO  ',0,'L'); 
		$pdf->Cell(45,5,'                         ',0,'L'); 
		$pdf->Cell(67,5,'   RESPONSABLE RECURSOS FISICOS  ',0,'L'); 
//----------------------FIRMA Y --VALOR TOTAL--------------------------
	} else {
		$pdf-> AddPage();

		$pdf->SetY(100);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2);  
		$pdf->Cell(190,1,'    ',1,'L'); 
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2); 
		$pdf->SetFont('Helvetica','B',9, 'C');
		$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
		$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 
		$pdf->Cell(50,5,'    ',0,'L'); 
		$pdf->Cell(32,5,'   VALOR TOTAL  ',0,'L'); 
		$pdf->Cell(37,5,"$ ".$valortotal,0,'L'); 

		$pdf->Ln(10);  
		$pdf->SetFont('Helvetica','B',1, 'C');
		$pdf->Cell(77,20,'  ',1,'L'); 
		$pdf->Cell(27,5,'                         ',0,'L'); 
		$pdf->Cell(77,20,'                         ',1,'L'); 
		$pdf->Ln(22);  
		$pdf->Cell(5,5,'    ',0,'L'); 
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(67,5,'   COORDINADOR    GRUPO ADMINISTRATIVO  ',0,'L'); 
		$pdf->Cell(45,5,'                         ',0,'L'); 
		$pdf->Cell(67,5,'   RESPONSABLE RECURSOS FISICOS  ',0,'L'); 
	}


}

if($cant_items==0){
	$pdf->SetFont('Helvetica','B',10, 'C');
	$pdf->Ln(20); 
	$pdf->Cell(70,5,'',0,'L'); 
	$pdf->Cell(67,5,'ENTRADA SIN ELEMENTOS REGISTRADOS  ',0,'L'); 
	$pdf->Ln(12); 
	$pdf->Ln(10);  
	$pdf->SetFont('Helvetica','B',1, 'C');

	$pdf->Cell(27,5,'                         ',0,'L'); 

	$pdf->Ln(22);  
	$pdf->Cell(5,5,'    ',0,'L'); 
	$pdf->SetFont('Helvetica','B',8, 'C');



}


$nombresalida=$numentrada."-".$fecha.".pdf";

$pdf->Output($nombresalida,'D');
//header("location:../elementos_menu.php?");




} else {
	header("location: ../403.php");
}
?>

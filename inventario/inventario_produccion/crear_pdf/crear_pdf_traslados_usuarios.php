
<?php
/* -----------------------------------------------------------------------------------
//DESCRIPCION: CREACION DE PDF DE TRASLADO A USUARIOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//------------------------------------------------------------------------------------
*/
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	require("../pdf/fpdf.php");
	include ("../assets/datosgenerales.php");
	include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------
	$numtraslado=$_REQUEST['traslado'];
//$valortotal=$_REQUEST['valortotal'];
//$cant_items=$_REQUEST['cant_items'];
//$tipoelementos=$_REQUEST['tipoelementos'];
//$new=$_REQUEST['new'];

	$justificacion="Descarga de PDF de traslado de elementos Activos  "."-  permiso usuario: " .$_SESSION['idpermiso'];
	$observacionregistro="Salida Num: " .$numtraslado." Acceso por IP:".$ip;
	$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
	$fecha=date ("Y-m-d");
	$hora=date("H:i:s");
	$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
	$queryreg2=mysql_query($registro2,$conexion);


	$seltraslado="SELECT * FROM traslados where traslado='$numtraslado'";
	$queryseltraslado=mysql_query($seltraslado,$conexion);
	while ($datos_traslado=mysql_fetch_array($queryseltraslado)){
		$fecha_traslado=$datos_traslado['fecha_traslado'];
		$docid_entrega=$datos_traslado['docid_entrega'];
		$nombres_entrega=$datos_traslado['nombres_entrega'];
		$apellidos_entrega=$datos_traslado['apellidos_entrega'];
		$dependencia_entrega=$datos_traslado['dependencia_entrega'];

		$docid_recibe=$datos_traslado['docid_recibe'];
		$nombres_recibe=$datos_traslado['nombres_recibe'];
		$apellidos_recibe=$datos_traslado['apellidos_recibe'];
		$dependencia_recibe=$datos_traslado['dependencia_recibe'];

		$elaboradopor=$datos_traslado['elaboradopor'];
		$numitems=$datos_traslado['num_items'];
		$valortotal=number_format($datos_traslado['valor_total_traslado'],2,',','.');
		$tipo_traslado=$datos_traslado['tipo_traslado'];
		$comentario=$datos_traslado['traslado_comentario'];
	}

	$nombrecompleto_entrega=$nombres_entrega. " ". $apellidos_entrega;
	$nombrecompleto_recibe=$nombres_recibe. " ". $apellidos_recibe;
	$traslado=$numtraslado;

//--------------------------OBTENER DATOS DEL PROVEEDOR SEGUN LA INFORMACION ALMACENADA EN LA ENTRADA--

//Datos dependencia entrega
	$seldependencia="SELECT nombredependencia, sigladependencia FROM dependencias where codigodependencia='$dependencia_entrega'";
	$queryseldependencia=mysql_query($seldependencia,$conexion);
	while ($datos_dependencia=mysql_fetch_array($queryseldependencia)){

		$nombredependencia_entrega=$datos_dependencia['nombredependencia'];
		$sigladependencia_entrega=$datos_dependencia['sigladependencia'];
	}

//Datos dependencia Recibe
	$seldependencia="SELECT nombredependencia, sigladependencia FROM dependencias where codigodependencia='$dependencia_recibe'";
	$queryseldependencia=mysql_query($seldependencia,$conexion);
	while ($datos_dependencia=mysql_fetch_array($queryseldependencia)){

		$nombredependencia_recibe=$datos_dependencia['nombredependencia'];
		$sigladependencia_recibe=$datos_dependencia['sigladependencia'];
	}


//Datos Adicionales Usuario que Entrega

	$selproveedor="SELECT nombres, apellidos, iddependencia, cargo, idprofesion, grado, calidadempleado FROM usuarios where documentoid='$docid_entrega'";
	$queryselproveedor=mysql_query($selproveedor,$conexion);
	while ($datos_usuario=mysql_fetch_array($queryselproveedor)){

		$profesion_entrega=$datos_usuario['idprofesion'];
		$cargo_entrega=$datos_usuario['cargo'];
		$grado_entrega=$datos_usuario['grado'];
		$codcalidadempleado_entrega=$datos_usuario['calidadempleado'];
	}

	$selcalidad="SELECT * FROM calidadempleador where idcalidadempleador='$codcalidadempleado_entrega'";
	$queryselc=mysql_query($selcalidad,$conexion);
	while ($cal=mysql_fetch_array($queryselc)){
		$calidadempleado_entrega=$cal['calidadempleador'];
	}

//Datos Adicionales Usuario que Recibe

	$selproveedor="SELECT nombres, apellidos, iddependencia, cargo, idprofesion, grado, calidadempleado FROM usuarios where documentoid='$docid_recibe'";
	$queryselproveedor=mysql_query($selproveedor,$conexion);
	while ($datos_usuario=mysql_fetch_array($queryselproveedor)){

		$profesion_recibe=$datos_usuario['idprofesion'];
		$cargo_recibe=$datos_usuario['cargo'];
		$grado_recibe=$datos_usuario['grado'];
		$codcalidadempleado_recibe=$datos_usuario['calidadempleado'];
	}

	$selcalidad="SELECT * FROM calidadempleador where idcalidadempleador='$codcalidadempleado_recibe'";
	$queryselc=mysql_query($selcalidad,$conexion);
	while ($cal=mysql_fetch_array($queryselc)){
		$calidadempleado_recibe=$cal['calidadempleador'];
	}





//-----------OBTENER DATOS DE LOS PRODUCTOS ANEXOS A LA ENTRADA------------------------

//----------------------------
	$date=date('Y-m-d');
	class PDF extends FPDF
	{
//Page header
		function Header()
		{
			global $numtraslado;
			global $fecha_traslado;
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(185, -5, "COMPROBANTE DE TRASLADOS", 0, 0, 'C');

$this->Ln(5);
$this->SetFont('Helvetica','B',12);
$this->Cell(185, -5, $numtraslado ."    Fecha: " . $fecha_traslado, 0, 0, 'C');


$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,160,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);
$this->Line(10, 28 , 200, 28);  
}

function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',8);
    //Pagina numero
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}
$pdf=new PDF();
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(35);
//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(40, 5, "FECHA", 0);
$pdf->Cell(50, 5, $fecha_traslado,0 , 'C');
$pdf->Cell(50, 5, "No", 0);
$pdf->SetFont('Helvetica','B',11);
$pdf->Cell(60, 5, $traslado, 0, 'B');
$pdf->SetFont('Helvetica','',9);
$pdf->Ln(5);

$pdf->Cell(40, 5, "Tipo Comprobante", 0);
$pdf->SetFont('Helvetica','B',10);
$pdf->Cell(50, 5, $tipo_traslado, 0);

//$pdf->Cell(40, 5, "Tipo de Traslado", 0);
//$pdf->SetFont('Helvetica','B',10);
//$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(50, 5,"Traslado por cambio de usuario", 0);

$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(45, 5, "Tipo de Movimiento", 0);
$pdf->SetFont('Helvetica','B',10);
//$pdf->Cell(60, 5, $tipomovimiento, 0);
$pdf->Ln(5);

//------------------| DATOS UNIDAD EJECUTORA------------>
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "U Ejecutora:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(110, 5, $unidadejecutora,0 , 'C');
$pdf->Cell(25, 5, "NIT:", 0);
$pdf->Cell(30, 5,$nitunidadejecutora, 0);
$pdf->Ln( 3);
$ylin2 = $pdf->GetY();
$pdf->Ln( 3);
$pdf->Line(10,  $ylin2+1.5, 210-10, $ylin2+1.5);
$pdf->Ln( 2);
//---------------DATOS DE QUIEN ENTREGA

$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "ENTREGA:", 0);
$pdf->Ln( 5);
$pdf->Cell(30, 5, "Funcionario:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(110, 5, utf8_decode($nombrecompleto_entrega),0 , 'C');
$pdf->Cell(35, 5, "DOCUMENTO:", 0);
$pdf->Cell(30, 5,$docid_entrega, 0);
$pdf->Ln( 5);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "Dependencia:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30, 5,utf8_decode($nombredependencia_entrega), 0);
$pdf->Ln( 5);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "Cod. Dependencia:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30, 5,$dependencia_entrega, 0);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(20, 5, "Sigla:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30, 5,$sigladependencia_entrega, 0);

$pdf->Ln(5);
$pdf->Cell(20, 5, "Grado:", 0);
$pdf->Cell(30, 5,$grado_entrega, 0);

$pdf->Cell(30, 5, "Calidad de emp.:  ", 0);
$pdf->Cell(65, 5, $calidadempleado_entrega, 0);

$pdf->Cell(20, 5, "Profesion:", 0);
$pdf->Cell(40, 5,$profesion_entrega, 0);

$pdf->Ln(5);
$pdf->Cell(30, 5, "Cargo: ", 0);
$pdf->Cell(40, 5,utf8_decode($cargo_entrega), 0);

$pdf->Ln(7);

//---------------DATOS DE QUIEN RECIBE
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "RECIBE:", 0);
$pdf->Ln( 5);
$pdf->Cell(30, 5, "Funcionaro:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(110, 5, utf8_decode($nombrecompleto_recibe),0 , 'C');
$pdf->Cell(35, 5, "DOCUMENTO:", 0);
$pdf->Cell(30, 5,$docid_recibe, 0);
$pdf->Ln( 5);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "Dependencia:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30, 5,utf8_decode($nombredependencia_recibe), 0);
$pdf->Ln( 5);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "Cod. Dependencia:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30, 5,$dependencia_recibe, 0);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(20, 5, "Sigla:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30, 5,$sigladependencia_recibe, 0);
$pdf->Ln(5);

$pdf->Cell(20, 5, "Grado:", 0);
$pdf->Cell(30, 5,$grado_recibe, 0);

$pdf->Cell(30, 5, "Calidad de emp.:  ", 0);
$pdf->Cell(65, 5, $calidadempleado_recibe, 0);

$pdf->Cell(20, 5, "Profesion:", 0);
$pdf->Cell(40, 5,$profesion_recibe, 0);

$pdf->Ln(5);
$pdf->Cell(30, 5, "Cargo: ", 0);
$pdf->Cell(40, 5,utf8_decode($cargo_recibe), 0);
$pdf->Ln(5);




$ylin = $pdf->GetY();
$pdf->Line(10,  $ylin+0.5, 210-10, $ylin+0.5);


//-----------------DATOS DE CONTACTO Y FACTURA  
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(30, 5, "Contrato No:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(60, 5,$contrato, 0);
$pdf->SetFont('Helvetica','B',9);
//$pdf->Cell(45, 5, "Factura No:", 0);
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(60, 5,$factura, 0);
$pdf->Ln(2);
//-------------------COMENTARIO----------------
$pdf->Ln(2);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(0, 5, "Comentario:",0);
$pdf->Ln(6);
$pdf->SetFont('Helvetica','',7);
$pdf->Multicell(195, 3,$comentario, 0, "L");
$pdf->Ln(2);
$yl = $pdf->GetY();
$pdf->Line(10,  $yl, 210-10, $yl);
$pdf->Line(10,  $yl+0.2, 210-10, $yl+0.2);
$pdf->Ln(6);



if($numitems!=0){
	$pdf->SetFont('Helvetica','B',8, 'C');
//$pdf->setFillColor(100, 100, 100);
	$pdf->Cell(105 , 5, "                  DESCRIPCION DE LA MERCANCIA", 0, 'C');
	$pdf->Cell(12, 5, " CATEG.", 0, 'C');
	$pdf->Cell(25, 5, "   SERIE", 0, 'C');
	$pdf->Cell(17, 5, "PLACA", 0, 'C');
	$pdf->Cell(15, 5, "  UNID.", 0, 'C');
	$pdf->Cell(25, 5, " VALOR", 0, 'C');
	$pdf->SetFont('Helvetica','',8, 'C');
	$pdf->Ln(5);  
	$pdf->Ln(3); 
	$query1="SELECT  * FROM productos 
	LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida WHERE numtraspaso='$numtraslado'"  ;
	$t_entradas=mysql_query($query1,$conexion);


	while ($fen=mysql_fetch_array($t_entradas)){
//---------------------------------------------
		$y = $pdf->GetY();
		$pdf->SetFont('Helvetica','',7, 'C');
		$pdf->MultiCell(105,3,utf8_decode($fen['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(120,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(9, 3, $fen['codigocontable'], 0, 'C');
		$pdf->Cell(25, 3, $fen['serie'], 0, 'C');
		$pdf->Cell(16, 3, $fen['codebar'], 0, 'C');

		$pdf->Cell(16,3,$fen['unidadmedida'],0,'L'); 
		$pdf->Cell(2,3,"$",0,'L'); 

		$pdf->Cell(25,3,number_format($fen['precioadqui'],2,',','.'),0,'R'); 

//$pdf->MultiCell(22,5,$y,0,'L')  ;
//$pdf->MultiCell(22,5,$y1,0,'L')  ;

		$pdf->SetY($y1);
		$pdf->Ln();  
		$y2 = $pdf->GetY();

		if ($y2>270){
			$pdf-> AddPage();
		}
	}

	$y3 = $pdf->GetY();
	if ($y2<255){
	//----------------------FIRMA Y --VALOR TOTAL--------------------------

		$pdf->SetY($y3+2);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2);  
		$yl = $pdf->GetY();
		$pdf->SetY($yl+1);
		$pdf->Line(10, $yl-0.9, 210-10, $yl-0.9);
		$pdf->Line(10, $yl-1, 210-10, $yl-1);
//$pdf->Cell(190,1,'    ',1,'L'); 
		$pdf->SetFont('Helvetica','B',9, 'C');
		$pdf->Ln(2); 
		$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
		$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 

		$pdf->Cell(74,5,'    ',0,'L'); 
		$pdf->Cell(32,5,'   VALOR TOTAL  ',0,'L'); 

		$pdf->Cell(7,5,'  $',0,'L'); 
		$pdf->Cell(32,5, $valortotal,0,'L'); 

		$pdf->Ln(10);  
		$pdf->SetFont('Helvetica','B',1, 'C');

//-------------------------------------------------
		$pdf->Ln(8);  

//------------firma Entrega--------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
		$yfirmas = $pdf->GetY();
		$pdf->SetX(30);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(30,5,'ENTREGADO POR:',0,'L'); 
		$pdf->Ln(12);  
		$pdf->SetX(30);
		$pdf->SetX(30);
		$pdf->SetFont('Helvetica','',8, 'C');
		$pdf->Cell(30,5,'FIRMA:',0,'L'); 
		$pdf->Ln(7);  

//-------------FIRMAS*---------------------------
		$pdf->SetX(30);
		$pdf->Cell(20,5,'NOMBRE: _____________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(30);
		$pdf->Cell(20,5,'CC: __________________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(30);
		$pdf->Cell(20,5,'FECHA: _____ - _____ - _________',0,'L'); 
		$pdf->SetX(10);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------firma recibe---------------------------------------
		$pdf->SetY($yfirmas);
		$pdf->SetX(130);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(130,5,'RECIBIDO POR:',0,'L'); 
		$pdf->Ln(12);  
		$pdf->SetX(130);$pdf->SetX(130);
		$pdf->SetFont('Helvetica','',8, 'C');
		$pdf->Cell(30,5,'FIRMA:',0,'L'); 
		$pdf->Ln(7);  

//-------------FIRMAS*---------------------------
		$pdf->SetX(130);
		$pdf->Cell(20,5,'NOMBRE: _____________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'CC: __________________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'FECHA: _____ - _____ - _________',0,'L'); 
		$pdf->SetX(10);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//----------------------FIRMA Y --VALOR TOTAL--------------------------
	} else {
		$pdf-> AddPage();

		$pdf->SetY(50);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Ln(2);  

		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(25,5,'   Elaborado por:  ',0,'L'); 
		$pdf->Cell(22,5,"   ".$elaboradopor,0,'L'); 
		$pdf->Cell(75,5,'     ',0,'L'); 

		$pdf->Cell(37,5,'   VALOR TOTAL  ',0,'R'); 
		$pdf->Cell(37,5,"$ ".$valortotal,0,'L'); 
		$pdf->Ln(6); 
		$yfirmas2 = $pdf->GetY();
//-------------FIRMAS*---------------------------
//------------firma Entrega--------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

		$pdf->SetX(30);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(30,5,'ENTREGADO POR:',0,'L'); 

		$pdf->Ln(12);  
		$pdf->SetX(30);
		$pdf->SetX(30);
		$pdf->SetFont('Helvetica','',8, 'C');
		$pdf->Cell(30,5,'FIRMA:',0,'L'); 
		$pdf->Ln(7);  

//-------------FIRMAS*---------------------------------------------------------
		$pdf->SetX(30);
		$pdf->Cell(20,5,'NOMBRE: _____________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(30);
		$pdf->Cell(20,5,'CC: __________________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(30);
		$pdf->Cell(20,5,'FECHA: _____ - _____ - _________',0,'L'); 
		$pdf->SetX(10);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------firma recibe---------------------------------------
		$pdf->SetY($yfirmas2);
		$pdf->SetX(130);
		$pdf->SetFont('Helvetica','B',8, 'C');
		$pdf->Cell(130,5,'RECIBIDO POR:',0,'L'); 
		$pdf->Ln(12);  
		$pdf->SetX(130);
		$pdf->SetFont('Helvetica','',8, 'C');
		$pdf->Cell(30,5,'FIRMA:',0,'L'); 
		$pdf->Ln(7);  

//-------------FIRMAS*---------------------------
		$pdf->SetX(130);
		$pdf->Cell(20,5,'NOMBRE: _____________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'CC: __________________________________',0,'L'); 
		$pdf->Ln(7);  
		$pdf->SetX(130);
		$pdf->Cell(20,5,'FECHA: _____ - _____ - _________',0,'L'); 
		$pdf->SetX(10);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

	}

}

if($numitems==0){

	$pdf->SetFont('Helvetica','B',10, 'C');
	$pdf->Ln(20); 
	$pdf->Cell(60,5,'',0,'L'); 
	$pdf->Cell(67,5,'TRASLADO SIN ELEMENTOS REGISTRADOS  ',0,'L'); 
	$pdf->Ln(12); 
	$pdf->Ln(10);  
	$pdf->SetFont('Helvetica','B',1, 'C');

	$pdf->Cell(27,5,'                         ',0,'L'); 

	$pdf->Ln(22);  
	$pdf->Cell(5,5,'    ',0,'L'); 
	$pdf->SetFont('Helvetica','B',8, 'C');




}


$nombresalida=$numtraslado."-".$fecha_traslado.".pdf";

$pdf->Output($nombresalida,'D');
//header("location:../salidas_menu.php?");


} else {
	header("location: ../403.php");
}
?>


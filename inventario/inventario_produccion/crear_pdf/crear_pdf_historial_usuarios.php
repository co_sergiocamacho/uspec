<?php
/* --------------------------------------------------------------------------
//DESCRIPCION: CREACION DE PDF DE HISTORIAL DE ELEMENTOS ASIGNADOS A USUARIOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
//---------------------------------------------------------------------------
*/
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
	require("../pdf/fpdf.php");
	include ("../assets/datosgenerales.php");
	include ("../database/conexion.php");
//-----------OBTENER DATOS DE LA ENTRADA------------------------
	$documentoid=$_REQUEST['documentoid'];


	$justificacion="Descarga de PDF de historial del Usuario "."-  permiso usuario: " .$_SESSION['idpermiso'];
	$observacionregistro="Usuario con documento ID : " .$documentoid." Acceso por IP:".$ip;
	$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
	$fecha=date ("Y-m-d");
	$hora=date("H:i:s");
	$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
	$queryreg2=mysql_query($registro2,$conexion);




	if(isset($_REQUEST['documentoid'])){

		$selproveedor="SELECT nombres, apellidos, iddependencia, cargo, idprofesion, grado, calidadempleado FROM usuarios where documentoid='$documentoid'";
		$queryselproveedor=mysql_query($selproveedor,$conexion);
		while ($datos_usuario=mysql_fetch_array($queryselproveedor)){

			$nombres=$datos_usuario['nombres'];
			$apellidos=$datos_usuario['apellidos'];
			$iddependencia=$datos_usuario['iddependencia'];

			$profesion=$datos_usuario['idprofesion'];
			$cargo=$datos_usuario['cargo'];
			$grado=$datos_usuario['grado'];
			$codcalidadempleado=$datos_usuario['calidadempleado'];
		}

		$selcalidad="SELECT * FROM calidadempleador where idcalidadempleador='$codcalidadempleado'";
		$queryselc=mysql_query($selcalidad,$conexion);
		while ($cal=mysql_fetch_array($queryselc)){
			$calidadempleado=$cal['calidadempleador'];
		}

		$seldependencia="SELECT nombredependencia, sigladependencia FROM dependencias where codigodependencia='$iddependencia'";
		$queryseldependencia=mysql_query($seldependencia,$conexion);
		while ($datos_dependencia=mysql_fetch_array($queryseldependencia)){

			$nombredependencia=$datos_dependencia['nombredependencia'];
			$sigladependencia=$datos_dependencia['sigladependencia'];
		}



		$query2="SELECT  * FROM productos  
		LEFT JOIN codigocontable on historial_productos.codigocontable=codigocontable.codigocontable
		LEFT JOIN dependencias on historial_productos.dependencia=dependencias.codigodependencia	
		LEFT  JOIN usuarios on historial_productos.documentoid=usuarios.documentoid
		LEFT  JOIN ubicacion on historial_productos.idubicacion=ubicacion.id_ubicacion
		WHERE activo='1'  AND  documentoid='$documentoid' ORDER BY  idelemento DESC";
		$t_prod=mysql_query($query2,$conexion);
//$pregun0=mysql_num_rows($t_prod);

		$nombres_apellidos=$nombres. "  " .$apellidos;	
		

		$fecha=date('Y-m-d');
		class PDF extends FPDF
		{
//Page header
			function Header()
			{
$this->Image('../imagenes/logos/logo1.png' ,15,11, 35,14, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->SetFont('Helvetica','B',13);
$this->Cell(185, -5, "HISTORIAL DE USUARIO", 0, 0, 'C');
$this->SetFont('Helvetica','',9);
$this->Image('../imagenes/logos/logo2.png' ,160,11, 35,13, 'PNG', 'USPEC'); // Relacion escala 3.6 W Y H
$this->Ln(5);
$this->Line(10, 25 , 200, 25);  
}


function Footer()
{
	$fechaimpreso=date('Y-m-d');
    //Posicion at 1.5 cm from bottom
	$this->SetY(-15);
    //Arial 
	$this->SetFont('Arial','',8);
    //Pagina numero
	$this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
}
}
$pdf=new PDF();
$pdf->SetMargins(10, 22 , 20,10); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(30);

//----------------------------------------------------------------------
//---------------DATOS GENERALES -------------------------------------------- 
$pdf->SetFont('Helvetica','',9);
$pdf->SetXY(10,30);
$pdf->Cell(30, 5, "FECHA", 0);
$pdf->Cell(50, 5, $fecha,0 , 'C');
$pdf->Ln(5);
//------------------| DATOS UNIDAD EJECUTORA------------>
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "U Ejecutora:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(120, 5, $unidadejecutora,0 , 'C');
$pdf->Cell(15, 5, "NIT", 0);
$pdf->Cell(30, 5,$nitunidadejecutora, 0);
$pdf->Ln(5);
//-----------------DATOS DE CONTACTO Y FACTURA  


//---------------DATOS DE PROVEEDOR
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(30, 5, "Funcionario:", 0);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(100, 5, utf8_decode($nombres_apellidos),0 , 'C');
$pdf->Cell(35, 5, "DOCUMENTO:", 0);
$pdf->Cell(30, 5,$documentoid, 0);
$pdf->Ln( 5);
$pdf->Cell(30, 5, "Dependencia:", 0);
$pdf->Cell(30, 5,$nombredependencia, 0);
$pdf->Ln( 5);
$pdf->Cell(30, 5, "Cod. Dependencia:", 0);
$pdf->Cell(30, 5,$iddependencia, 0);

$pdf->Cell(20, 5, "Sigla:", 0);
$pdf->Cell(30, 5,$sigladependencia, 0);

$pdf->Cell(20, 5, "Grado:", 0);
$pdf->Cell(30, 5,$grado, 0);
$pdf->Ln(5);

$pdf->Cell(30, 5, "Calidad de emp.:  ", 0);
$pdf->Cell(70, 5, $calidadempleado, 0);

$pdf->Cell(20, 5, "Profesion:", 0);
$pdf->Cell(60, 5,$profesion, 0);

$pdf->Ln(5);
$pdf->Cell(30, 5, "Cargo: ", 0);
$pdf->Cell(40, 5,utf8_decode($cargo), 0);

$pdf->Ln(5);

//-------------------COMENTARIO----------------

$query1="SELECT  * FROM productos 
LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida WHERE documentoid='$documentoid' and activo='1'"  ;
$t_entradas33=mysql_query($query1,$conexion);
$inicial=mysql_num_rows($t_entradas33);

if(!empty($inicial)){
//$pdf->setFillColor(100, 100, 100);
	$pdf->SetFont('Helvetica','B',9);
	$pdf->Cell(50, 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "             ELEMENTOS ACTUALMENTE ASIGNADOS", 0, 'C');
	$pdf->Ln(5);  
	$pdf->Cell(110 , 5, "                  DETALLES DEL ELEMENTO", 0, 'C');
	$pdf->SetFont('Helvetica','B',8);
	$pdf->Cell(22, 5, " PLACA", 0, 'C');
	$pdf->Cell(33, 5, "   SERIE", 0, 'C');
	$pdf->Cell(15, 5, "  UNIDAD", 0, 'C');
	$pdf->Cell(25, 5, " VALOR", 0, 'C');
	$pdf->SetFont('Helvetica','',8, 'C');
	$pdf->Ln(5);  
	$pdf->Ln(3); 
	$pdf->SetFont('Helvetica','',8);


	while ($fen=mysql_fetch_array($t_entradas33)){
//---------------------------------------------
		$y = $pdf->GetY();
		$pdf->SetFont('Helvetica','',7, 'C');
		$pdf->MultiCell(105,3,utf8_decode($fen['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(120,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(14, 3, $fen['codebar'], 0, 'C');
		$pdf->Cell(39, 3, $fen['serie'], 0, 'C');
		$pdf->Cell(12,3,$fen['unidadmedida'],0,'L'); 
		$pdf->Cell(2,3,"$",0,'L'); 
		$pdf->Cell(25,3,number_format($fen['precioadqui'],2,',','.'),0,'R'); 
		
		$pdf->SetY($y1);
		$pdf->Ln(5); 

		$y2 = $pdf->GetY();
		$y3 = $pdf->GetY();
		if ($y2>245){
			$pdf-> AddPage();
		}
	}

}
$pdf->Ln(3); 
$y2 = $pdf->GetY();
$y3 = $pdf->GetY();
if ($y2>245){
	$pdf-> AddPage();
}
		//------------------------------------------------------------------------------------------------------

$query22="SELECT  * FROM historial_productos  
WHERE activo='1'  AND  documentoid='$documentoid' ORDER BY  numsalida DESC";
$t_prod22=mysql_query($query22,$conexion);    


$pregun0=mysql_num_rows($t_prod22);

if(!empty($pregun0))	{
	$pdf->Ln(6);  
	$pdf->SetFont('Helvetica','b',9, 'C');
	$pdf->Cell(60 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE ELEMENTOS ASIGNADOS AL USUARIO", 0, 'C');
	$pdf->SetFont('Helvetica','b',8, 'C');
	$pdf->Ln(7);  
	$pdf->Cell(100, 5, "ELEMENTO", 0, 'C');
	$pdf->Cell(16, 5, "SALIDA", 0, 'C');
	$pdf->Cell(26, 5, "SERIE", 0, 'C');
	$pdf->Cell(22, 5, "PLACA", 0, 'C');
	$pdf->Cell(25, 5, "VALOR", 0, 'C');
	$pdf->Ln(5);  
	$pdf->SetFont('Helvetica','',7, 'C');

	$query2="SELECT  * FROM historial_productos  
	LEFT JOIN codigocontable on historial_productos.codigocontable=codigocontable.codigocontable
	WHERE documentoid='$documentoid' and activo='1' ";

	$t_prod=mysql_query($query2,$conexion);
	while ($f_productos=mysql_fetch_array($t_prod)){
		$pdf->SetFont('Helvetica','',7, 'C');
		$y = $pdf->GetY();
		$pdf->MultiCell(90,3,utf8_decode($f_productos['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(105,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);

		$pdf->Cell(13, 5,$f_productos["numsalida"] , 0, 'C');
		$pdf->Cell(33	, 5,$f_productos["serie"] , 0, 'C');
		$pdf->Cell(22, 5,$f_productos["codebar"] , 0, 'C');
		$pdf->Cell(22, 5,"$ ".number_format($f_productos["precioadqui"],2,',','.') , 0, 'C');
			//$pdf->Cell(22, 5,$documentoid , 0, 'C');
			//$pdf->Cell(45, 5,$f_productos["nombres"]." " .$f_productos["apellidos"] , 0, 'C');
		$pdf->SetY($y1);
		$pdf->Ln(5);  
		$y2 = $pdf->GetY();
		$y3 = $pdf->GetY();
		if ($y2>245){
			$pdf-> AddPage();
		}
	}
}

$pdf->Ln(5);
if ($y2>245){
	$pdf-> AddPage();
}
//------------------------------------------------------------------------------------------------------
$pdf->SetFont('Helvetica','',7, 'C');

//------------------------------------------------------------------------------------------------------

$query2tr="SELECT  * FROM tabla_aux_traslados  
LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
LEFT JOIN codigocontable on tabla_aux_traslados.codigocontable_tr_aux=codigocontable.codigocontable
LEFT JOIN usuarios ON tabla_aux_traslados.docid_entrega_aux= usuarios.documentoid
LEFT JOIN condicion on tabla_aux_traslados.condicion_aux=condicion.idcondicion
left join unidadmedida on tabla_aux_traslados.id_unidadmedida_aux=unidadmedida.idunidadmedida
WHERE activo_aux_tr='1' AND docid_entrega_aux='$documentoid'  ORDER BY  trasladonum_aux DESC"  ;

$t_prod=mysql_query($query2tr,$conexion);
$pregun1=mysql_num_rows($t_prod);

if(!empty($pregun1)){
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');
	$pdf->Cell(30 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE TRASLADOS DEL  ELEMENTO (Datos de Funcionario que entrega)", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',7, 'C');
	$pdf->Cell(80, 5, "ELEMENTO", 0, 'C');
	$pdf->Cell(20, 5, "FECHA", 0, 'C');
	$pdf->Cell(18, 5, "TRASLADO", 0, 'C');
	$pdf->Cell(30, 5, "SERIE", 0, 'C');
	$pdf->Cell(25, 5, "PLACA", 0, 'C');
	$pdf->Cell(25, 5, "VALOR", 0, 'C');
	$pdf->Ln(5);  
	$pdf->SetFont('Helvetica','',7, 'C');

	while ($f_trasl=mysql_fetch_array($t_prod)){
		$y = $pdf->GetY();
		$pdf->MultiCell(90,5,utf8_decode($f_trasl['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(105,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(15, 5,$f_trasl["fecha_traslado_aux"] , 0, 'C');
		$pdf->Cell(13, 5,$f_trasl["trasladonum_aux"] , 0, 'C');
		$pdf->Cell(30, 5,$f_trasl["serie"] , 0, 'C');
		$pdf->Cell(25, 5,$f_trasl["codebar"] , 0, 'C');
		$pdf->Cell(25, 5,"$".number_format($f_trasl["precioadqui"],2,',','.') , 0, 'C');

		$pdf->Ln(5);
		$pdf->SetY($y1);

		$y5 = $pdf->GetY();
		if ($y5>240){
			$pdf-> AddPage();
		}
	}
	$y6 = $pdf->GetY();
	if ($y6>240){
		$pdf-> AddPage();
	} 
//Cierra IF De Datos vacios
}



//------------------------------------------------------------------------------------------------------

$pdf->Ln(8);

$query2tr="SELECT  * FROM tabla_aux_traslados  
LEFT JOIN  productos on tabla_aux_traslados.idelemento_tr_aux=productos.idelemento
LEFT JOIN codigocontable on tabla_aux_traslados.codigocontable_tr_aux=codigocontable.codigocontable
LEFT JOIN usuarios ON tabla_aux_traslados.docid_entrega_aux= usuarios.documentoid
LEFT JOIN condicion on tabla_aux_traslados.condicion_aux=condicion.idcondicion
left join unidadmedida on tabla_aux_traslados.id_unidadmedida_aux=unidadmedida.idunidadmedida
WHERE activo_aux_tr='1' AND docid_recibe_aux='$documentoid'  ORDER BY  trasladonum_aux DESC"  ;
$t_prod=mysql_query($query2tr,$conexion);
$pregun2=mysql_num_rows($t_prod);

if(!empty($pregun2)){

	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');
	$pdf->Cell(30 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE TRASLADOS DEL  ELEMENTO (Datos de Funcionario que recibe)", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');

	$pdf->Cell(80, 5, "ELEMENTO", 0, 'C');
	$pdf->Cell(20, 5, "FECHA", 0, 'C');
	$pdf->Cell(18, 5, "TRASLADO", 0, 'C');
	$pdf->Cell(30, 5, "SERIE", 0, 'C');
	$pdf->Cell(25, 5, "PLACA", 0, 'C');
	$pdf->Cell(25, 5, "VALOR", 0, 'C');

	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','',7, 'C');


	while ($f_trasl=mysql_fetch_array($t_prod)){
		$y = $pdf->GetY();
		$pdf->MultiCell(90,5,utf8_decode($f_trasl['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(105,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(15, 5,$f_trasl["fecha_traslado_aux"] , 0, 'C');
		$pdf->Cell(13, 5,$f_trasl["trasladonum_aux"] , 0, 'C');
		$pdf->Cell(30, 5,$f_trasl["serie"] , 0, 'C');
		$pdf->Cell(25, 5,$f_trasl["codebar"] , 0, 'C');
		$pdf->Cell(25, 5,"$".number_format($f_trasl["precioadqui"],2,',','.') , 0, 'C');

		$pdf->Ln(5);
		$pdf->SetY($y1);

		$y5 = $pdf->GetY();
		if ($y5>240){
			$pdf-> AddPage();
		}
	}
	$pdf->Ln(8);
	$y6 = $pdf->GetY();
	if ($y6>240){
		$pdf-> AddPage();
	} 
//Cierra IF De Datos vacios
}

//------------------------------------------------------------------------------------------------------

$query2tr="SELECT  * FROM tabla_aux_reintegros  
LEFT JOIN usuarios on tabla_aux_reintegros.documentoid_aux=usuarios.documentoid
LEFT JOIN  productos on tabla_aux_reintegros.idelemento_r_aux=productos.idelemento
LEFT JOIN dependencias on tabla_aux_reintegros.dependencia_aux=dependencias.codigodependencia
WHERE  documentoid_aux='$documentoid'  ORDER BY  fecha_entrada_aux DESC" ;
$t_prod2=mysql_query($query2tr,$conexion);
$pregun3=mysql_num_rows($t_prod2);

if(!empty($pregun3)){

	$pdf->SetFont('Helvetica','b',9, 'C');
	$pdf->Cell(60 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "HISTORIAL DE RENTEGROS", 0, 'C');
	$pdf->Ln(5);
	$pdf->SetFont('Helvetica','b',8, 'C');

	$pdf->Cell(100, 5, "ELEMENTO", 0, 'C');
	$pdf->Cell(16, 5, "ENTRADA", 0, 'C');
	$pdf->Cell(26, 5, "SERIE", 0, 'C');
	$pdf->Cell(22, 5, "PLACA", 0, 'C');
	$pdf->Cell(25, 5, "VALOR", 0, 'C');
	$pdf->Ln(5);
	while ($f_rein=mysql_fetch_array($t_prod2)){
		$pdf->SetFont('Helvetica','',7, 'C');
		$y = $pdf->GetY();
		$pdf->MultiCell(90,5,utf8_decode($f_rein['elemento']),0,'J'); 
		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$pdf->SetXY(105,$y);
		$pdf->Line(10, $y-1, 210-10, $y-1);
		$pdf->Cell(18, 5,$f_rein["entrada_r_aux"] , 0, 'C');
		$pdf->Cell(30, 5,$f_rein["serie"] , 0, 'C');
		$pdf->Cell(25, 5,$f_rein["codebar"] , 0, 'C');
		$pdf->Cell(25, 5,"$".number_format($f_rein["precioadqui"],2,',','.'), 0, 'C');
		$pdf->SetY($y1);


		$y6 = $pdf->GetY();
		if ($y6>240){
			$pdf-> AddPage();
		}
	}
	$y6 = $pdf->GetY();
	if ($y6>240){
		$pdf-> AddPage();
	}
	
// CierreIF datos vacios
}
$pdf->Ln(8);



if ($pregun0==0 AND $pregun1==0 and $pregun2==0 and $pregun3==0){
	$pdf->Ln(18);

	$pdf->SetFont('Helvetica','B',12, 'C');

	$pdf->Cell(45 , 5, " ", 0, 'C');
	$pdf->Cell(105 , 5, "NO SE ENCONTRARON ELEMENTOS ASIGNADOS", 0, 'C');

}


$nombresalida="USPEC_user_ID"."-".$documentoid."-".$fecha. ".pdf";
$pdf->Output($nombresalida,'D');


} else { }








}


?>



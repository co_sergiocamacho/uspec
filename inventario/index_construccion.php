<?php
//DESCRIPCION: VENTANA PRINCIPAL DE LOGIN
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
?>



<?php

if(isset ($_REQUEST['exit'])){

header("location: logout.php");
}

if(!isset ($_REQUEST['exit'])){

}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<link href="css/estilos.css" rel="stylesheet" type="text/css" />    
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<title>Login</title>
  <style> 
            body {
background: #eff2f9; /* Old browsers */
background: -moz-radial-gradient(center, ellipse cover,  #eff2f9 0%, #808e93 87%, #4f6372 95%); /* FF3.6-15 */
background: -webkit-radial-gradient(center, ellipse cover,  #eff2f9 0%,#808e93 87%,#4f6372 95%); /* Chrome10-25,Safari5.1-6 */
background: radial-gradient(ellipse at center,  #eff2f9 0%,#808e93 87%,#4f6372 95%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eff2f9', endColorstr='#4f6372',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */

                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;}
                #div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px; }
                .container > header h1,
                .container > header h2 {
                    color: #fff;
                    text-shadow: 0 1px 1px rgba(0,0,0,0.7);
                }
            </style>
</head>


<body>
	<div class="container">

		<header>	
			<h1>Sistema de Administracion de <strong>USPEC</strong> Unidad de Servicios Penitenciarios</h1>
			<h2>Bienvenido esperamos que sea util tu visita en este lugar</h2>

			<div class="support-note">
				<span class="note-ie">Lo sentimos, por favor actualiza tu navegador.</span>
			</div>
		</div>
	</header>

	<section>
		<form id="formlogin" name="form1" method="POST" action="verificar.php" class="form-4">   
			<center>
				<img src="imagenes/logo.png" width="577"  />
			</center>
			<center>
				<h1 align="left">Ingreso al sistema </h1>
			</center>

			<table border="0" align="center">
				<tr></tr>
				<tr>
					<td align="right" class="texto">Usuario:</td>
					<td align="center"><input type="text" class="textinput"  name="txt_usuario" id="txt_usuario" placeholder="Usuario" required /></td>
				</tr>
				<tr>
					<td align="right" class="texto">Clave:</td>
					<td align="center"><input type="password" name="txt_password" id="txt_password" placeholder="Password" required /></td>
				</tr>
				<tr>
					<td colspan="2" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" name="btn_login" id="btn_login" value="Ingresar" /></td>
				</tr>
			</table>
		</center>
	</form>
</section>
</div>
</body>
<div class="quality" align="right">
	<img src="imagenes/logo.png" width="360" height="59" /><br />
	<span style="font-size:13px;text-align:right; color:#FFFFFF">USPEC<br />
		Realizado por ANDRES MONTEALEGRE GIRALDO<br />
		Ing. Electr&oacutenico<br />
		Oficina de Tecnolog&iacute;a<br />
		Derechos Reservados  &reg;
		<?php echo date('Y');?>
		<br />
		webcontrol version 0.1<br />       
	</body>
	</html>
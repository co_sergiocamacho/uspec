<?php
//DESCRIPCION: VENTANA PRINCIPAL PARA ADMINISTRADORES

//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
if (isset($_SESSION['idpermiso'])) {
    require_once '../database/conexion_pdo.php';
    include("../assets/encabezado.php");
    include("../Security/LevelSecurityModules.php");;
?>
<!doctype html>
<html lang="es">

<head>
  <link rel="shortcut icon" href="../imagenes/1.ico" />
  <link href="../css/paginacion.css" type="text/css" rel="stylesheet" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Reintegros</title>
  <link href="../css/estilos.css" rel="stylesheet" type="text/css" />
  <style>
    body {
      background: #eeeeee no-repeat center top;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
    }
    #div_bienvenido {
      padding-right: 100px;
      text-align: right;
      padding-top: 20px;
    }
    .container > header h1,
    .container > header h2 {
      color: #fff;
      text-shadow: 0 1px 1px rgba(0, 0, 0, 0.7);
    }
  </style>


  <body>

    <div id="centro2">
      <table class="botonesfila">
        <tr>
          <td>
            <a href="../principal.php">
              <img src="../imagenes/inicio6.png" border="0" width="52" height="52"> INICIO
            </a>
          </td>
        </tr>
      </table>
    </div>


    <div id="centro">
      <div id="div_bienvenido">
        <?php echo "Bienvenido"; ?>
        <BR/>
        <div id="div_usuarios">
          <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
        </div>
        <?php echo "SALIR";?>
        <a href="../index.php"><img src="../imagenes/apagar.png" title="Salir" width="13" height="13" />
        </a>
      </div>

      <center>
        <table width="65%" border="0">
          <tr>
            <td colspan="11" class="titulo">
              <center>MENÚ REINTEGROS </center>
            </td>
          </tr>

        </table>

        <?php if (isset($_GET[ 'creado'])){ if ($_GET[ 'creado']==5){ ?>
        <div class="quitarok">
          <img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center" /> Se ha creado la Entrada por Reintegro correctamente!
        </div>

        <?php } } ?>

        <?php if (isset($_GET[ 'borrado'])){ if ($_GET[ 'borrado']==1){ ?>
        <div class="quitarok">
          <img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center" /> Se ha anulado la Entrada por Reintegro correctamente!
        </div>

        <?php } } ?>


        <?php if (isset($_REQUEST[ 'zeroasign'])) { ?>
        <div class="quitarok"> EL usuario seleccionado no tiene elementos asignados</div>

        <?php }?>

        <table class="menuprincipal">
            <tr class="menuactivos">
              <td>
                <?php
                if (in_array("10", $DataSecurityLevel)) {
                ?>
                <a class="titulos_menu" href="reintegros_sel_usuario.php"><img src="../imagenes/salida.png" title="Menu de Elementos" width="36" height="36" />Crear Reintegro</a>
                <?php
                } else {
                    ?>
                    &#160;
                    <?php
                }
                ?>
              
              </td>
              <td>
                <?php
                if (in_array("11", $DataSecurityLevel)) {
                ?>
                <a class="titulos_menu" href="ver_reintegros.php?all=1"><img src="../imagenes/verentradas.png" title="Menu de Elementos" width="36" height="36" />Ver Reintegros</a>
                 <?php
                } else {
                    ?>
                    &#160;
                    <?php
                }
                ?>
              
              </td>
              <td>
                <?php
                if (in_array("12", $DataSecurityLevel)) {
                ?>
                <a class="titulos_menu" href="ver_reintegros_anulados.php?all=1"><img src="../imagenes/anuladas.png" title="Inventario " width="50" height="50" />Reintegros Anulados</a>
                 <?php
                } else {
                    ?>
                    &#160;
                    <?php
                }
                ?>
              
              </td>

            </tr>
        </table>
      </center>
   </div>
  </body>

</html>

<?php 
include ("../assets/footer.php");
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {


include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");


?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <script type="text/javascript" src="../js/buscar.js"></script>

  <script type="text/javascript" src="../js/dataTables.min.js"></script>
  <script type="text/javascript" src="../js/jquery.js"></script>

  <?php
// <script type="text/javascript" src="../js/tablas.js"></script>
  ?>


  <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  <link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
  <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
  <script src="../js/calendario/src/js/jscal2.js"></script>
  <script src="../js/calendario/src/js/lang/en.js"></script>
  <link href="../css/estilos.css" type="text/css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
  <link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
  <link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

  <style> 
    <!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
    body {
      background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
    }
    .container > header h1,
    .container > header h2 {
      color: #fff;
      text-shadow: 0 1px 1px rgba(0,0,0,0.7);
    }
  </style>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Buscar usuario</title>


  <script type="text/javascript">
    $(document).ready(function() {
      $(".search").keyup(function () {
        var searchTerm = $(".search").val();
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
        
        $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
          return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
      });
        
        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
          $(this).attr('visible','false');
        });

        $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
          $(this).attr('visible','true');
        });

        var jobCount = $('.results tbody tr[visible="true"]').length;
        $('.counter').text(jobCount + ' Elementos');

        if(jobCount == '0') {$('.no-result').show();}
        else {$('.no-result').hide();}
      });
    });

  </script>
</head>

  <body>



    <div id="centro">
      <div id="div_bienvenido">
        <?php echo "Bienvenido"; ?> <BR/>
        <div id="div_usuarios">
          <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
        </div>
        <?php echo "SALIR";?>
        '<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
      </div>   




      <table width="65%" border="0">
        <tr>
          <td colspan="11" class="titulo"><center>

          </center></td>
        </tr>
        <td>       &nbsp;       </td>
      </table>


      <table class="tabla_3" width="50%">
        <th class="fila1" colspan="4">Buscar Usuarios</th>
        <tr>


<div id="centro">
    <div class="form-group pull-left">
      <h4>Escriba su busqueda aqui</h4>
      <input type="text" class="search form-control" placeholder="Escribe aqui el nombre, apellido, dependencia o el documento de identificación del usuario a buscar..." size="80">
    </div>
    <br>
    <span class="counter pull-left"></span>
    <div class="container-full">
    <br>
      <div class="table-responsive " >
    <table   class=" table table-bordered results"  cellspacing="0" width="90%" >
<thead>
       <th width="30px"class="fila1"  >ID</th>
       <th class="fila1"  >DOCUMENTO ID</th>
       <th class="fila1"  >NOMBRES</th>
       <th class="fila1"  >APELLIDOS</th>
       <th class="fila1" colspan="2"  >DEPENDENCIA</th>
       </thead>
       <tbody id="myTable2">
       <?PHP


    //CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
    //NOMBRE: Andres Montealegre Giraldo
    //FECHA: 2015-01-09     
    //Rutina: Si los campos estan Vacios Muestra todos los Activos
       
       $sqlquery="SELECT *  FROM usuarios  LEFT JOIN  dependencias on usuarios.iddependencia=dependencias.codigodependencia ";

     $t_activos=mysql_query($sqlquery, $conexion);
     while ($fila_usuarios=mysql_fetch_array($t_activos))
     {

      ?>
      <tr><h3>
        <td class="fila2"><h3><?php echo $fila_usuarios["idusuario"];?></h3></td>
        <td class="fila2"><h3><?php echo $fila_usuarios["documentoid"];?></h3></td>
        <td class="fila2"><h3><?php echo $fila_usuarios["nombres"];?></h3></td>
        <td class="fila2"><h3><?php echo $fila_usuarios["apellidos"];?></h3></td>
        <td class="fila2" align="center"><h3><?php echo $fila_usuarios["codigodependencia"];?></h3></td>
        <td class="fila2"><h3><?php echo $fila_usuarios["nombredependencia"];?></h3></td>

        <tr>
        </tr>
        </center>
        </td>
        </h3>
        </tr>
           <?php
        }
        ?>
</tbody>
                  <tr class="warning no-result">
                    <td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
                  </tr>
                </TABLE>
              </div>
              <center>
                <ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
                <div class="col-md-12 text-center">

                </div>

              </div>
            </div>

       
      </h4>


      <?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

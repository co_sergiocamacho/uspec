<?php
//VARIABLE PARA LA IDENTIFICACION DEL USUARIO
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS

$entrada=$_REQUEST['entrada'];

$query3="SELECT  * FROM entradas  WHERE entrada='$entrada' "  ;
$t_entradas3=mysql_query($query3,$conexion);

while ($fila_datos3=mysql_fetch_array($t_entradas3)){

	$fechaentrada=$fila_datos3['fecha'];
	$elaboradopor=$fila_datos3['elaboradopor'];

}
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Detalles de Reintegro</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />

</head>

<body>
	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="ver_reintegros.php?all=1.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>

			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> DETALLES DEL REINTEGRO</STRONG></center>
						</td>

					</tr>
				</table>

				<table border="0" class="tabla_2" >

					<tr >
						<tr>
							<td  class="fila1">ENTRADA N</td> <td class="fila2"> <?php echo $entrada;?></td>


							<td  class="fila1">FECHA DE ENTRADA</td><td class="fila2"><?php echo $fechaentrada;?></td>
							<td  class="fila1">ELABORADO POR</td><td class="fila2"><?php echo $elaboradopor;?></td>
						</tr>
						<tr>

							<?php
							$query2="SELECT  * FROM tabla_aux_reintegros  

							LEFT JOIN  usuarios on tabla_aux_reintegros.documentoid_aux=usuarios.documentoid
							ORDER BY  entrada_r_aux DESC"  ;
							$t_entradas2=mysql_query($query2,$conexion);

							while ($fila_datos2=mysql_fetch_array($t_entradas2)){
								$documentoid=$fila_datos2['documentoid_aux'];
								$nombres=$fila_datos2['nombres'];
								$apellidos=$fila_datos2['apellidos'];
								$dependencia_aux=$fila_datos2['dependencia_aux'];


							}
							?>

							<td  class="fila1">DOCUMENTO ID</td><td class="fila2"> <?php echo $documentoid;?></td>
							<td  class="fila1">NOMBRE</td> <td class="fila2"> <?php echo $nombres;?></td>
							<td  class="fila1">APELLIDOS</td> <td class="fila2"> <?php echo $apellidos;?></td>
							<td  class="fila1">DEPENDENCIA</td><td class="fila2"> <?php echo $dependencia_aux;?></td>

						</tr>
						<tr>


						</tr>
					</table>
					<tr>
						<table class="tabla_2">
							<p></p>
							<td  class="fila1">ID</td>
							<td  class="fila1">DESCRIPCION</td>
							<td  class="fila1">PLACA</td>
							<td  class="fila1">MARCA</td>
							<td  class="fila1">MODELO</td>
							<td  class="fila1">SERIE</td>
							<td  class="fila1">VALOR ELEMENTO</td>
							<td  class="fila1">OBSERVACIONES</td>

							<tr>

								<?php

								$query1="SELECT  * FROM tabla_aux_reintegros  

								LEFT JOIN  productos on tabla_aux_reintegros.idelemento_r_aux=productos.idelemento
								WHERE entrada_r_aux='$entrada' ORDER BY  entrada_r_aux DESC"  ;
								$t_entradas=mysql_query($query1,$conexion);

								while ($fila_entradas=mysql_fetch_array($t_entradas)){

									?>
									<tr>
										<td class="fila2"><?php echo $fila_entradas["idelemento_r_aux"];?></td>
										<td class="fila2"><?php echo $fila_entradas["elemento"];?></td>
										<td class="fila2"><?php echo $fila_entradas["codebar"];?></td>
										<td class="fila2"><?php echo $fila_entradas["marca"];?></td>
										<td class="fila2"><?php echo $fila_entradas["modelo"];?></td>
										<td class="fila2"><?php echo $fila_entradas["serie"];?></td>
										<td class="fila2" align="right">$<?php echo number_format($fila_entradas["precioadqui"],2,',','.');?></td>
										<td class="fila2"><?php echo $fila_entradas["probservaciones"];?></td>



										<?php
									}
									?>
<TR>
<td class="fila2" colspan="4"></td>
<td class="fila2" colspan="2"><b>VALOR TOTAL</b></td>
<?PHP
$query2="SELECT  * FROM entradas  
								WHERE entrada='$entrada' "  ;
								$t_entradas2=mysql_query($query2,$conexion);

								while ($fila_entradas2=mysql_fetch_array($t_entradas2)){
 ?>
									<td class="fila2" ALIGN="RIGHT">$<?php echo number_format($fila_entradas2["valortotal"],2,',','.'); }?></td>
									<td class="fila2" colspan="1"></td>
									</TR>

</table></body>
									<?php

include("../assets/footer.php");
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>


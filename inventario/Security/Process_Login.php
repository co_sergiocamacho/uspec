<?php
include_once '../database/conexion_pdo.php';
include_once 'Functions.php';
//sec_session_start(); 
 
if (isset($_POST['data_user'], $_POST['data_password'])) {
    $data_user = $_POST['data_user'];
    $data_password = $_POST['data_password']; 
 
    echo login($data_user, $data_password, $conexion);
   
} else {
    // Las variables POST correctas no se enviaron a esta página.
    $jsondata = array(
                        'success' => true,
                        'message' => sprintf("Datos no válidos"),
                        'complete'=>false
                    );
    echo  json_encode($jsondata);
}
?>

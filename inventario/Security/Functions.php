<?php
session_start();
function sec_session_start() {
    $session_name = 'sec_session_id';   // Configura un nombre de sesión personalizado.
    $secure = SECURE;
    // Esto detiene que JavaScript sea capaz de acceder a la identificación de la sesión.
    $httponly = true;
    // Obliga a las sesiones a solo utilizar cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Obtiene los params de los cookies actuales.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"], 
        $cookieParams["domain"], 
        $secure,
        $httponly);
    // Configura el nombre de sesión al configurado arriba.
    session_name($session_name);
    session_start();            // Inicia la sesión PHP.
    session_regenerate_id();    // Regenera la sesión, borra la previa. 
}
function login($data_user, $password, $mysqli) {
    // Usar declaraciones preparadas significa que la inyección de SQL no será posible.
    $sql="SELECT idusuario, usuario, password, salt, usuactivo, idpermiso FROM usuarios WHERE usuario = ? ";
    $stmt = $mysqli->prepare($sql);
    $stmt->bindParam(1,$data_user);
    $stmt->execute();
    $total = $stmt->rowCount();
    $jsondata = array();
    if ($total>0){
        
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $user_id=$row->idusuario;
            $username=$row->usuario;
            $db_password=$row->password;
            $salt=$row->salt;
            $active=$row->usuactivo;
            $idpermiso=$row->idpermiso;
        }
        
        if($idpermiso==1||$idpermiso==2||$idpermiso==3){
            // Hace el hash de la contraseña con una sal única.
        $password = hash('sha1', $password . $salt);
       
            // Si el usuario existe, revisa si la cuenta está bloqueada
            // por muchos intentos de conexión.
 
            if (checkbrute($user_id, $mysqli) == true) {
                // La cuenta está bloqueada.
                // Envía un correo electrónico al usuario que le informa que su cuenta está bloqueada.
                
                 $jsondata = array(
                        'success' => true,
                        'message' => sprintf("Lo sentimos, su cuenta ha sido bloqueada por demasidos intentos de inicio de sesión no válidos. </BR> Por favor intenta de nuevo más tarde."),
                        'complete'=>false
                    ); 
                 return json_encode($jsondata);
            } else {
                // Revisa que la contraseña en la base de datos coincida 
                // con la contraseña que el usuario envió.
                if($active==1){
                        if ($db_password == $password) {
                        // ¡La contraseña es correcta!
                        // Obtén el agente de usuario del usuario.
                        $user_browser = $_SERVER['HTTP_USER_AGENT'];
                        //  Protección XSS ya que podríamos imprimir este valor.
                        $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                        $_SESSION['user_id'] = $user_id;
                        // Protección XSS ya que podríamos imprimir este valor.
                        $username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                    "", 
                                                                    $username);
                        $_SESSION['username'] = $username;
                        $_SESSION['login_string'] = hash('sha1', 
                                  $password . $user_browser);


                        $sqlu="SELECT * FROM usuarios WHERE usuario = ? ";
                        $stmtu = $mysqli->prepare($sqlu);
                        $stmtu->bindParam(1,$data_user);
                        $stmtu->execute();
                        $total = $stmtu->rowCount();
                        if ($total>0) {

                            while ($row = $stmtu->fetch(PDO::FETCH_OBJ)){
                                $_SESSION['idusuario']=$row->idusuario;
                                $_SESSION['nombres']=$row->nombres;
                                $_SESSION['apellidos']=$row->apellidos;
                                $_SESSION['usuario']=$row->usuario;
                                $_SESSION['idpermiso']=$row->idpermiso;
                                $_SESSION['documentoid']=$row->documentoid;
                            }

                        }
                        // Inicio de sesión exitoso
                        $jsondata = array(
                            'success' => true,
                            'message' => sprintf("Éxito en el inicio de sesión."),
                            'complete'=>true,
                            'permiso'=>$_SESSION['idpermiso']
                        );
                        return json_encode($jsondata);
                    } else {
                        // La contraseña no es correcta.
                        // Se graba este intento en la base de datos.
                        $now = time();
                        $slqinsertusert="INSERT INTO login_attempts(user_id, time)";
                        $slqinsertusert.="VALUES (?,?)";
                        $mysqli->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $mysqli->beginTransaction();
                        $gsent = $mysqli->prepare($slqinsertusert);
                        $gsent->bindParam(1, $user_id);
                        $gsent->bindParam(2, $now);
                        $gsent->execute();
                        $mysqli->commit();
                        $gsent->closeCursor();
                        $gsent=null;
                        $mysqli=null;

                        $jsondata = array(
                            'success' => true,
                            'message' => sprintf("La contraseña es incorrecta. Inténtalo de nuevo."),
                            'complete'=>false
                        ); 
                        return json_encode($jsondata);
                    }
                }else{
                    // Inicio de sesión exitoso
                        $jsondata = array(
                            'success' => true,
                            'message' => sprintf("Lo sentimos, el usuario no esta activo. Por favor contacte al administrador."),
                            'complete'=>false
                        );
                        return json_encode($jsondata);
                }
                
            }
        }else{
            $jsondata = array(
                        'success' => true,
                        'message' => sprintf("Lo sentimos, usuario no autorizado. Por favor contacte al administrador."),
                        'complete'=>false
                ); 
            return json_encode($jsondata);
        }
     
    }else {
           
           $jsondata = array(
                        'success' => true,
                        'message' => sprintf("Lo sentimos, el usuario no existe. Indica una cuenta diferente."),
                        'complete'=>false
                ); 
           return json_encode($jsondata);
    }
}


function checkbrute($user_id, $mysqli) {
    // Obtiene el timestamp del tiempo actual.
    $now = time();
    // Todos los intentos de inicio de sesión se cuentan desde las 2 horas anteriores.
    $valid_attempts = $now - (2 * 60 * 60);
    $sql="SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'";
    $stmt = $mysqli->prepare($sql);
    $stmt->bindParam(1,$user_id);
    $stmt->execute();
    $total = $stmt->rowCount();
    if ($total>0) {
 
        // Si ha habido más de 5 intentos de inicio de sesión fallidos.
        if ($total > 5) {
            return true;
        } else {
            return false;
        }
    }
}

function login_check($mysqli) {
    // Revisa si todas las variables de sesión están configuradas.
    if (isset($_SESSION['user_id'], 
                        $_SESSION['username'], 
                        $_SESSION['login_string'])) {
 
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];
 
        // Obtiene la cadena de agente de usuario del usuario.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
 
        if ($stmt = $mysqli->prepare("SELECT password 
                                      FROM members 
                                      WHERE id = ? LIMIT 1")) {
            // Une “$user_id” al parámetro.
            $stmt->bind_param('i', $user_id);
            $stmt->execute();   // Ejecuta la consulta preparada.
            $stmt->store_result();
 
            if ($stmt->num_rows == 1) {
                // Si el usuario existe, obtiene las variables del resultado.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
 
                if ($login_check == $login_string) {
                    // ¡¡Conectado!! 
                    return true;
                } else {
                    // No conectado.
                    return false;
                }
            } else {
                // No conectado.
                return false;
            }
        } else {
            // No conectado.
            return false;
        }
    } else {
        // No conectado.
        return false;
    }
}


function esc_url($url) {
 
    if ('' == $url) {
        return $url;
    }
 
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
 
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
 
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
 
    $url = str_replace(';//', '://', $url);
 
    $url = htmlentities($url);
 
    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);
 
    if ($url[0] !== '/') {
        // Solo nos interesan los enlaces relativos de  $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}

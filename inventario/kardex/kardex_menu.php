<?php
//DESCRIPCION: MENU PRINCIPAL DEL KARDEX, POR ELEMENTO DE CONSUMO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="../js/buscar.js"></script>

	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>

	<?php
// <script type="text/javascript" src="../js/tablas.js"></script>
	?>


	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>KARDEX</title>
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
		$(document).ready(function() {
			$(".search").keyup(function () {
				var searchTerm = $(".search").val();
				var listItem = $('.results tbody').children('tr');
				var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
				
				$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
					return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
				}
			});
				
				$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
					$(this).attr('visible','false');
				});

				$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
					$(this).attr('visible','true');
				});

				var jobCount = $('.results tbody tr[visible="true"]').length;
				$('.counter').text(jobCount + ' Elementos');

				if(jobCount == '0') {$('.no-result').show();}
				else {$('.no-result').hide();}
			});
		});

	</script>

</head>

<body>

	<?php
	if (isset($_GET['creado'])){ if ($_GET['creado']==5){?>
	<div class="quitarok">
		<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado el elemento correctamente!
	</div>

	<?php   }    } ?>
<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td></tr></table></div>



			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   

				<center>
					<table width="70%" border="0">
						<tr>
							<td class="titulo"><center>
								KARDEX ELEMENTOS DE CONSUMO</center></td>
							</tr>
						</table>
						<?php
						if (isset($_GET['add'])){ if ($_GET['add']==1){?>
						<div class="quitarok">
							<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Elemento creado correctamente!
						</div>

						<?php   }    } ?> 
					</center></td>
				</tr>
				<tr>


					<td class="texto">
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1){
		?>
		<td>





			<?php
		}
		?>





		<?php

//CONSULTA DE LOS USUARIOS ACTIVOS EN EL SISTEMA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09     
		$t_consumo=mysql_query("SELECT
			*
			FROM CONSUMO 
			LEFT JOIN unidadmedida on consumo.unidmedida=unidadmedida.idunidadmedida  
			LEFT JOIN  codigocontable on consumo.codigocontable=codigocontable.codigocontable
			WHERE consactivo='1'
			", $conexion);
//echo $t_clientes;
		$num_registro=mysql_num_rows($t_consumo);
		?>

		<center>
<div id="centro">
		<div class="form-group pull-left">
			<h4>Escriba su busqueda aqui</h4>
			<input type="text" class="search form-control" placeholder="Consulta por Elemento, Descripcion, ..." size="80">
		</div>
		<br>
		<span class="counter pull-left"></span>
		<div class="container-full">
		<br>
			<div class="table-responsive " >
				<table   class=" table table-bordered results"  cellspacing="0" width="100%" >
					<thead>
					<th width="57" class="fila1">ID</th>
					<th  class="fila1">ELEMENTO</th>
					<th width="70" class="fila1">CANTIDAD DISPONIBLE</th>
					<th  class="fila1">UNID. MEDIDA</th>
					<th width="96" class="fila1">CATEGORIA</th>

					<th  class="fila1">VALOR UNIT</th>
					<th  class="fila1">VALOR TOTAL</th>
					<th width="79" class="fila1">CANTIDAD MIN STOCK</th>
					<th width="79" class="fila1">DETALLES</th>
					<th width="79" class="fila1">PDF</th>

				</tr>
				</thead>
				<tbody>
				<?php



				while ($fila_consumo=mysql_fetch_array($t_consumo))
				{

					?>

					<tr>
						<td class="fila2"><?php echo $fila_consumo["idelemento"];?></td>
						<td class="fila2"><?php echo $fila_consumo["elemento"];?></td>
						<td class="fila2"><?php echo $fila_consumo["cantidad"];?></td>
						<td class="fila2"><?php echo $fila_consumo["unidadmedida"];?></td>
						<td class="fila2"><?php echo $fila_consumo["codigocontable"]. "  :" .$fila_consumo["codigodescripcion"];?></td>

						<td class="fila2"align="right">$<?php echo number_format($fila_consumo["precioadqui"],2,',','.');?></td>
						<td class="fila2"align="right">$<?php echo number_format(($fila_consumo["precioadqui"]*$fila_consumo["cantidad"]),2,',','.');?></td>
						<td class="fila2" align="center"><?php echo $fila_consumo["minimo_stock"];?></td>


					</center></td>
					<td class="fila2"><center>
						<a href="kardex_detallado.php?idelemento=<?php echo $fila_consumo['idelemento'];?>"><img src="../imagenes/detalles.png" title="Modificar Elemento" width="28" height="28"/></a>
					</center></td>


<?php 

if($fila_consumo["cantidad"]!=0){

 ?>
					<td class="fila2"><center>
						<a href="../crear_pdf/crear_pdf_kardex_horizontal.php?idelemento=<?php echo $fila_consumo['idelemento'];?>"><img src="../imagenes/pdf.png" title="Descarga PDF " width="28" height="28"/></a>
					</center></td>

		<?php } else {

?>
<td class="fila2">No disponible en PDF <br>Cantidad en cero<center>
		<?php }		?>

					<?php

				}

				?> 

			</tr>
			<?php
			$querysuma = mysql_query("SELECT SUM(precioadqui*cantidad) as total FROM consumo WHERE consactivo='1'");   
			$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
			$valortotal=$valortotal1["total"];


			?>
			<TR>
				<TD class="fila2"colspan="5">&nbsp; </TD>
				<TD class="fila2"colspan="1" align="right"><STRONG>VALOR TOTAL</STRONG></TD>

				<td  class="fila2"align="right"><strong><?php echo number_format($valortotal,2,',','.');?></strong></td>
				<TD class="fila2"colspan="3">&nbsp; </TD>

				<tr>
					<td colspan="14" class="fila2"><center>

					</center> </td>	
				</tr>
</tbody>
									<tr class="warning no-result">
										<td colspan="20"><i class="fa fa-warning"></i>¡¡ No hay resultados !!</td>
									</tr>
								</TABLE>
		</center>

	</div>
	<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
	include ("../assets/footer.php");

	?>
</body>
</html>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

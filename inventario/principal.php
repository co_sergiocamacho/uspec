<?php
//DESCRIPCION: VENTANA PRINCIPAL DEL APLICATIVO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
if (isset($_SESSION['idpermiso'])) {
    require_once 'database/conexion_pdo.php';
    include("encabezado.php");
    include 'Security/LevelSecurityModules.php';
    $idusuario = $_SESSION['idusuario'];

?>
<!DOCTYPE html>
<html lang="es">
<head>
        <link rel="shortcut icon" href="imagenes/1.ico"/>
        <link href="css/paginacion.css" type="text/css" rel="stylesheet"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/estilos.css" rel="stylesheet" type="text/css" />
        <title>Principal</title>
        <style>    
            body {
                background: #eeeeee no-repeat center top;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;}
                #div_bienvenido{ padding-right:100px; text-align: right; padding-top: 20px;    }
                .container > header h1,
                .container > header h2 {
                    color: #fff;
                    text-shadow: 0 1px 1px rgba(0,0,0,0.7);
                }
            </style>

    <body>

  <div id="centro">
    <div id="div_bienvenido">
<?php
    echo "Bienvenido";
?> <BR/>
                    <div id="div_usuarios">
<?php
    echo "$_SESSION[nombres] $_SESSION[apellidos]" . "   ";
?>
<a  href="cambio/usuario_clave.php?idusuario=<?php
    echo $idusuario;
?>"><img src="imagenes/clave.png" title="Activos" width="15" height="15"  /> </a>
                    <br >
<?php
    echo "SALIR";
?>
      <a href="index.php?exit=1"><img src="imagenes/apagar.png" title="Salir" width="18" height="18" />
      </a>
    </div>
  </div>
  <center>
    <table width="65%" border="0">
      <tr>
        <td colspan="11" class="titulo">
          <center>
            PRINCIPAL
          </center>
        </td>
      </tr>

    </table>
  </center>
  <center>
    <table class="menuprincipal" width="55%">

      <tr class="menuactivos" align="center">

        <td>
          <?php
             if (in_array("1", $DataSecurityLevel)||in_array("2", $DataSecurityLevel)||in_array("3", $DataSecurityLevel)||in_array("4", $DataSecurityLevel)||in_array("5", $DataSecurityLevel)||in_array("6", $DataSecurityLevel)||in_array("7", $DataSecurityLevel)||in_array("8", $DataSecurityLevel)||in_array("9", $DataSecurityLevel)||in_array("10", $DataSecurityLevel)||in_array("11", $DataSecurityLevel)||in_array("12", $DataSecurityLevel)) {
          ?>
          <a class="titulos_menu" href="Entradas/elementos_menu.php"><img src="imagenes/entrada.png" title="Menu de Elementos" width="50" height="50" />Entradas</td>
          <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        <td>
          <?php
             if (in_array("13", $DataSecurityLevel)||in_array("14", $DataSecurityLevel)||in_array("15", $DataSecurityLevel)||in_array("16", $DataSecurityLevel)||in_array("17", $DataSecurityLevel)||in_array("18", $DataSecurityLevel)||in_array("19", $DataSecurityLevel)||in_array("20", $DataSecurityLevel)||in_array("21", $DataSecurityLevel)||in_array("22", $DataSecurityLevel)||in_array("23", $DataSecurityLevel)) {
          ?>
          <a class="titulos_menu" href="Salidas/salidas_menu.php"><img src="imagenes/inventario3.png" title="Salidas" width="50" height="50" />Salidas</a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>
        <td>
            <?php
             if (in_array("24", $DataSecurityLevel)||in_array("25", $DataSecurityLevel)||in_array("26", $DataSecurityLevel)||in_array("27", $DataSecurityLevel)||in_array("28", $DataSecurityLevel)) {
          ?>
          <a class="titulos_menu" href="Traslados/traslados_menu.php"><img src="imagenes/traspaso.png" title="Salidas" width="50" height="50" />Traslados</a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        
        </td>
        <td>
            <?php
                if (in_array("29", $DataSecurityLevel)) {
            ?>
          <a class="titulos_menu" href="Reportes/inventario_menu.php"><img src="imagenes/inventario2.png" title="Salidas" width="50" height="50" />Reporte Inventario</a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>

      </tr>

      <tr class="menuactivos" align="center">
        <td>
          
            <?php
                if (in_array("30", $DataSecurityLevel)) {
            ?>
            <a class="titulos_menu" href="kardex/kardex_menu.php">
            <img src="imagenes/kardex2.png" title="Inventario " width="50" height="50" />Reporte KARDEX
            </a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
          
        </td>
        <td>
            <?php
                if (in_array("31", $DataSecurityLevel)) {
            ?>
          <a class="titulos_menu" href="Reportes/reportes_activos_menu.php">
            <img src="imagenes/activos1.png" title="Activos" width="50" height="50" /> Consulta de Activos
          </a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>
        <td>
            <?php
                if (in_array("5", $DataSecurityLevel)||in_array("34", $DataSecurityLevel)||in_array("36", $DataSecurityLevel)) {
            ?>
          <a class="titulos_menu" href="consumo/consumo_menu.php">
            <img src="imagenes/consumo.png" title="Activos" width="50" height="50" />Ver Elementos de Consumo
          </a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>
        <td>
            <?php
                if (in_array("35", $DataSecurityLevel)) {
            ?>
          <a class="titulos_menu" href="reportes/reportes_menu.php">
            <img src="imagenes/boletin.png" title="Activos" width="50" height="50" />Información del Sistema
          </a>
            <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>

      </tr>

      <tr class="menuactivos" align="center">
            <?php
                if (in_array("32", $DataSecurityLevel)) {
            ?>
        <td>
          <a class="titulos_menu" href="ajustes/ajustes_menu.php"><img src="imagenes/ajustes.png" title="Editar" width="50" height="50" />Ajustes del Sistema</a>
           <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>

        <td>
            <?php
                if (in_array("33", $DataSecurityLevel)) {
            ?>
          <a class="titulos_menu" href="ajustes/registro.php"><img src="imagenes/log.png" title="Editar" width="50" height="50" />Registro del sistema</a>
          <?php
            } else {
                ?>
                &#160;
                <?php
            }
            ?>
        </td>

        <td>
          <a class="titulos_menu" href="#"></a>
        </td>

      </tr>

    </table>
  </center>
  </div>
</body>
</html>
<?php
    include("footer.php");
?>
<?php
    /*
    @Cerrar Sesion
    */
} else {
    header("location: 403.php");
}
?>
<?php
//DESCRIPCION: MENU PRINCIPAL DE LAS ENTRADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
    //CONEXION A LA BASE DE DATOS
    require_once '../database/conexion_pdo.php';
    include("../assets/encabezado.php");
    include("../Security/LevelSecurityModules.php");
    ?>
    <!doctype html>
    <html lang="es">
        <head>
            <link href="../css/paginacion.css" type="text/css" rel="stylesheet"/>
            <link href="../css/styles.css" type="text/css" rel="stylesheet"/>
            <link href="../css/estilos.css" type="text/css" rel="stylesheet"/>
            <link rel="shortcut icon" href="../imagenes/1.ico"/>
            <style>	
                body {
                    background: #eaeaea no-repeat center top;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    background-size: cover;
                }
                .container > header h1,
                .container > header h2 {
                    color: #fff;
                    text-shadow: 0 1px 1px rgba(0,0,0,0.7);
                }
            </style>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Menu Elementos</title>
            <link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
        </head>
        <body>
            <div id="centro2">
                <table class="botonesfila" >
                    <tr>
                        <td>  
                            <a href="../principal.php">
                                <img src="../imagenes/inicio6.png"  border="0" width="52" height="52">
                                INICIO
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="centro">
                <div id="div_bienvenido">
                    <?php echo "Bienvenido"; ?> <BR/>
                    <div id="div_usuarios">
                        <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
                    </div>
                    <?php echo "SALIR"; ?>
                    '<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
                </div>
                <center>
                    <table width="100%" border="0">
                        <tr>
                            <td colspan="11" class="titulo">
                        <center>
                            <STRONG> ENTRADAS DE ELEMENTOS ACTIVOS Y DE CONSUMO</STRONG>
                        </center>
                        </td>
                        </tr>
                    </table>
                    <?php if (isset($_GET['creado'])) {
                        if ($_GET['creado'] == 5) { ?>
                            <div class="quitarok">
                                <img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha creado la Entrada correctamente!
                            </div>
                        <?php }
                    } ?> 
                    <?php if (isset($_GET['borrado'])) {
                        if ($_GET['borrado'] == 1) { ?>
                            <div class="quitarok">
                                <img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Se ha Borrado la Entrada de correctamente!
                            </div>
                        <?php }
                    } ?>
                    <table class="menu2">
                        <tr class="menuactivos">
                            <td colspan="4">
                                <h3>
                                    Activos Devolutivos
                                </h3>
                            </td>
                        <tr class="menuactivos">

                            <td>
                                <?php
                                if (in_array("2", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="entradas_add.php">
                                        <img src="../imagenes/add_file.png" title="Activos" width="64" height="64" align="LEFT" />
                                        Crear Entrada de Activos
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>

                            <td></td>
                            <td>
                                <?php
                                if (in_array("3", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="../activos/editar_activos.php">
                                        <img src="../imagenes/documento.png" title="Editar" width="64" height="64" align="left" />
                                        Ver Activos
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>
                            <td><a  class="titulos_menu"></a></td>
                        </tr>
                        <tr class="menuconsumo">
                            <td colspan="4">
                                <h3>
                                    Crear Entrada Elementos de consumo
                                </h3>
                            </td>
                        <tr class="menuconsumo">

                            <td>
                                <?php
                                if (in_array("4", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="entrada_consumo_add.php">
                                        <img src="../imagenes/add_file.png" title="Activos" width="64" height="64" align="LEFT" />
                                        Crear Entrada Elementos de consumo
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>
                            <td></td>

                            <td>
                                <?php
                                if (in_array("5", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="../consumo/consumo_menu.php">
                                        <img src="../imagenes/documento.png" title="Editar" width="64" height="64" align="left" />
                                        Ver elementos de consumo</a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>

                        </tr>
                        <tr class="menugral" >
                            <td colspan="4">
                                <h3>
                                    GENERAL
                                </h3>
                            </td>
                        <TR class="menugral" >


                            <td>
                                <?php
                                if (in_array("6", $DataSecurityLevel) || in_array("10", $DataSecurityLevel) || in_array("11", $DataSecurityLevel) || in_array("12", $DataSecurityLevel)) {
                                    ?>
                                    <a class="titulos_menu" href="../Reintegros/reintegros_menu.php">
                                        <img src="../imagenes/reintegro.png" title="Salidas" width="50" height="50"/>
                                        Reintegros
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (in_array("7", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="entradas_anuladas.php?all=1">
                                        <img src="../imagenes/anuladas.png" title="Eliminar" width="64" height="64" align="left" />
                                        Ver Entradas Anuladas
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (in_array("8", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="ver_entradas_incompletas.php?all=1">
                                        <img src="../imagenes/verentradas.png" title="Eliminar" width="64" height="64" align="left" />
                                        Ver Entradas sin Terminar
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <tr class="menugral" >
                            <td colspan="4">
                                <h3>
                                    Entradas Reportes
                                </h3>
                            </td>
                        <tr class="menugral" >
                            <td>
                                <?php
                                if (in_array("9", $DataSecurityLevel)) {
                                    ?>
                                    <a  class="titulos_menu" href="ReportEntry.php">
                                        <img src="../imagenes/documento.png" title="Reportes" width="64" height="64" align="left" />
                                        Reporte de Entradas
                                    </a>
                                    <?php
                                } else {
                                    ?>
                                    &#160;
                                    <?php
                                }
                                ?>
                            </td>
                            <td>  
                            </td>
                            <td> 
                            </td>
                        </tr>
                    </table>
            </div>
        </body>
    </html>
    <?php
    include ("../assets/footer.php");
    ?>
    <?php
    /*
      @Cerrar Sesion
     */
} else {
    header("location: ../403.php");
}
?>
<?php
session_start();
require("../database/conexion_pdo.php");
$KeyWord=$_POST['search'];
$ExtraParameter=$_POST['ExtraParameter'];
$start=$_POST['start'];
$length=$_POST['length'];
$draw=$_POST['draw'];
$KeyWord_End=$KeyWord["value"];

class Entry{
    
  private $KeyWord;
  private $Connection;
  private $Start;
  private $Length;
  private $Draw;
  
  public function __construct($conn,$ExtraParameter,$start,$length,$draw)
  {
       $this->Connection = $conn;
       $this->KeyWord=$ExtraParameter;
       $this->Start=$start;
       $this->Length=$length;
       $this->Draw=$draw;
  }
  public function FormatDateSql($infoDate){
      $DateFormat = new DateTime($infoDate);
      return $DateFormat->format('Y-m-d');
  }
   public function FormatDate($infoDate){
      $DateFormat = new DateTime($infoDate);
      return $DateFormat->format('d-m-Y');
  }
  public function SearchListEntry(){ 
    
    $sql="SELECT * FROM entradas ";
    $sql.="LEFT JOIN proveedores ON entradas.idproveedor=proveedores.idproveedor ";
    $sql.="WHERE identrada IS NOT NULL ";
  
    if($this->KeyWord["DateStart"]!=""&&$this->KeyWord["DateEnd"]!=""){
        
         $sql.="AND (fecha>=:datestart AND fecha<=:dateend) ";
    }
    if($this->KeyWord["TypeElement"]!="-1"){
        
         $sql.="AND tipoelementos=:idtypelement ";
    }
    $sql.="ORDER BY identrada ASC ";
    $sql_limit="LIMIT $this->Length OFFSET ".$this->Start;
    $stmt_total = $this->Connection->prepare($sql);
    $stmt = $this->Connection->prepare($sql.$sql_limit);
 
    
    if($this->KeyWord["DateStart"]!=""&&$this->KeyWord["DateEnd"]!=""){
   
        $DateStart_R=$this->FormatDateSql($this->KeyWord["DateStart"]);
        $DateEnd_R=$this->FormatDateSql($this->KeyWord["DateEnd"]);
        
        $stmt_total->bindParam(':datestart',$DateStart_R);
        $stmt_total->bindParam(':dateend',$DateEnd_R);
        $stmt->bindParam(':datestart',$DateStart_R);
        $stmt->bindParam(':dateend',$DateEnd_R);
        
        
    }
    if($this->KeyWord["TypeElement"]!="-1"){
        
        $TypeElement = $this->KeyWord["TypeElement"];
        $stmt_total->bindParam(':idtypelement',$TypeElement,PDO::PARAM_STR);
        $stmt->bindParam(':idtypelement',$TypeElement,PDO::PARAM_STR);
    }
    $stmt_total->execute();
    $total_datos = $stmt_total->rowCount();
    $stmt->execute();
    $total_limit = $stmt->rowCount();
  
    //$result = $stmt->fetchAll();
    //$obj->filmName;
    $jsondata = array();
        if($total_datos>0){
            $jsondata = array(
                    'success' => true,
                    'message' => sprintf("Se han encontrado %d resultados", $total_datos),
                    'draw'=> $this->Draw,
                    'recordsTotal'=> $total_limit,
                    'recordsFiltered'=> $total_datos,
                    'data'=>array()
            );
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
                  //$documento_user = (empty($row->documentoid) || is_null($row->documentoid)) ? "--" : $row->documentoid;
                    $detail="";
                    $action="";
                    $idpdf="";
                    if($row->tipoelementos=="ELEMENTOS DE CONSUMO"){
                        $detail='<a href="detalles_entrada_consumo.php?entrada='.$row->entrada.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                       if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                           
                        $action='<a href="anular_entrada_consumo1.php?entrada='.$row->entrada.'"><img src="../imagenes/ANULAR.png" title="Anular Entrada " width="28" height="28"/></a>';
                       }       
                        $idpdf='<a href="../crear_pdf/crear_pdf_entrada_consumo.php?entrada='.$row->entrada.'"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>';
                    }
                    if($row->tipoelementos=="ACTIVOS DEVOLUTIVOS"){
                        $detail='<a href="detalles_entrada.php?entrada='.$row->entrada.'"><img src="../imagenes/detalles.png" title="Ver Detalles " width="28" height="28"/></a>';
                        if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){
                        $action='<a href="anular_entrada_mod.php?entrada='.$row->entrada.'"><img src="../imagenes/ANULAR.png" title="Anular Entrada " width="28" height="28"/></a>';
                        }
                        $idpdf='<a href="../crear_pdf/crear_pdf_entrada_devolutivos.php?entrada='.$row->entrada.'"><img src="../imagenes/pdf.png" title="Ver Detalles " width="28" height="28"/></a>';
                        
                    }
                  $jsondata['data'][]= array(
                    'identrada' => $row->identrada,
                    'entrada' => $row->entrada,
                    'fecha' => $this->FormatDate($row->fecha),
                    'tipoelementos' => htmlentities($row->tipoelementos),
                    'proveedor' => htmlentities(utf8_encode($row->proveedor)),
                    'numitems' => $row->numitems,
                    'valortotal' => number_format($row->valortotal,2,',','.'),
                    'elaboradopor' => htmlentities(utf8_encode($row->elaboradopor)),
                    'comentario' => htmlentities(utf8_encode($row->comentario)),
                    'entobservaciones' => htmlentities(utf8_encode($row->entobservaciones)),
                    'detalle' => $detail,
                    'acciones' => $action,
                    'pdf' => $idpdf
                );
           
            }
          
        }else {
            $jsondata = array(
                    'success' => true,
                    'message' => 'No se encontró ningún resultado.',
                    'draw'=> $this->Draw,
                    'recordsTotal'=> $total_limit,
                    'recordsFiltered'=> $total_datos,
                     'data'=>array()
            );
        }
        return json_encode($jsondata);
        $stmt = null;
        $stmt_total = null;
  }
}

$data = new Entry($conexion,$ExtraParameter,$start,$length,$draw);
$resultado = $data->SearchListEntry();
echo $resultado;
/**
 * Close connection
 */
$conn = null;

?>

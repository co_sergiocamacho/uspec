<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
//DESCRIPCION: VENTANA PARA CREACION DE ENTRADAS DE ACTIVOS FIJOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
if (isset($_SESSION['idpermiso'])) {
include("../assets/encabezado.php");
include("../database/conexion.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");

if (isset($_GET['entrada'])){
	if (isset($_GET['consec'])){
		$buscarconsec=$_GET['consec'];
		//$idanterior=$_GET['anterior'];

	}
	else {
		header ("location: elementos_menu.php");
/*
			$consultanum="SELECT * FROM consecutivoentrada_temp ORDER BY idconsecentrada";
			$leerconsec=mysql_query($consultanum);
			$num_entrada=mysql_num_rows($leerconsec);
	// este es el numero para grabar los datos en el formulario
	//echo $num_entrada;   
			$consultaconsecutivo= "SELECT * FROM consecutivoentrada_temp WHERE  idconsecentrada=$num_entrada";
			$asignarconsecutivo=mysql_query($consultaconsecutivo);
	//   echo $num_entrada;

			if ($consecutivo=mysql_fetch_array($asignarconsecutivo)){
				$buscarconsec=$consecutivo['consecentrada'];
	//      echo $consecutivo['consecentrada'];
				echo $buscarconsec;
				*/

		}
	}



?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">

	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>

	<link href="../css/estilos.css" type="text/css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


	<style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ENTRADA DE ELEMENTOS</title>




	<?php 
//include('menu.php');

	?>
</head>

<body>
	<div id="centro2">
	<?php

if(!isset($_REQUEST['entrada'])) {
?>
		<table class="botonesfila" >


		
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr>
			</table>

<?php } ?>
		</div>






		<div id="centro">
			<div id="div_bienvenido">
				<?php echo "Bienvenido"; ?> <BR/>
				<div id="div_usuarios">
					<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
				</div>
				<?php echo "SALIR";?>
				'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
			</div>   

			<center>
				<table width="65%" border="0">
					<tr>
						<td colspan="11" class="titulo"><center>ENTRADA DE ELEMENTOS</center></td>
					</tr>
				</table>
			</center>

			<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){ 
				if (!isset($_GET['entrada'])){

					?><center>
					<div id="generador">
						<table align="center">
							<tr>
								<center>
									<form name="CrearEntrada" action="nuevonumentrada.php"       title="Consecutivo de Entrada" method="post">
										<td align="center"><input type="image" src="../imagenes/entradanew.png" width="48" height="48"  value="GENERAR">Generar Consecutivo</input></td>
									</form>
								</tr>
							</table>
						</div>
					</center>
					<?php

//solicitud de confirmacion de consecutivo
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
				} elseif(isset($_GET['entrada'])){

					if ($_GET['entrada']==2){
//Activar generador de Consecutivos
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
						?> 



						<div id="centro2">
							<form name="productos" action="entrada_elemento_guardar.php"       title="Nuevo Elemento" method="post" onsubmit="guardar_producto.disabled= true; return true;" >
								<table width="100%" class="tabla_2" border="0" style="">
									<tr>
										<td colspan="6" class="fila1" style="font-size:14px;">AGREGAR ELEMENTO</td>
									</tr>

									<tr> 
										<input type="hidden" name="producto_consecutivo" value="<?php echo $buscarconsec;?>"/>  

										<td class="fila2" >CANTIDAD:</td>
										<td class="fila2"><input type="text" class="textinput"  name="producto_cantidad" size="5" maxlength="50" onkeypress="return SoloNumeros(event)" value="1" /></td>
										<td class="fila2">UNIDAD DE MEDIDA</td>
										<td class="fila2"><select name="producto_unidadmedida" required><option value=""></option>

											<?php
											echo $num_entrada;
											$unidadmedida=mysql_query("SELECT idunidadmedida, unidadmedida FROM unidadmedida WHERE (unidactivo = '1') ORDER BY idunidadmedida");
											while($filpro=mysql_fetch_array($unidadmedida))
											{
												?>
												<option size="70"  value="<?php echo $filpro['idunidadmedida'];?>"><?php echo $filpro['unidadmedida'];?></option> <?php } ?></select></td>

												<td class="fila3">CODIGO CONTABLE  &nbsp; &nbsp; &nbsp; &nbsp; <a href="codigos_contables_lista.php" target="_blank"><input type="image" src="../imagenes/ayuda.png" width="16" height="16&" name="regresar" title="Inicio" value="Regresar"  />&nbsp;&nbsp;Ver codigos</a></td>
												<td class="fila2"><select colspan="2"name="producto_codigocontable" required>
													<?php
													$codigocont=mysql_query("SELECT idcodigocontable, codigocontable, codigodescripcion  FROM codigocontable  ORDER BY codigocontable ");
													?>
													<option value=""></option>
													<?php

													while($filcodigo=mysql_fetch_array($codigocont)) { ?>

													<option  value="<?php echo $filcodigo['codigocontable'];?>"><?php echo	$filcodigo['codigocontable'] ." " . $filcodigo['codigodescripcion'] ;?></option> <?php $codigoelegido=$filcodigo['codigodescripcion']; }?>  </td>

												</tr>
												<td class="fila2">DESCRIPION:</td>
												<td colspan="5" ><input type="text" class="textinput"  name="producto_elemento"  rows="5"  SIZE="200" onChange="MAY(this)" value="" class="input_descripcion" required /></td>

											</tr>
											<tr>
											</TR>
											<td class="fila2">MARCA</td>
											<td class="fila2"><input type="text" class="textinput"  name="producto_marca" size="30" maxlength="50" onChange="MAY(this)" value=""  /></td>
										</select></td>
										<td class="fila2">MODELO</td>
										<td class="fila2"><input type="text" class="textinput"  name="producto_modelo" size="30" maxlength="50" onChange="MAY(this)" value="" /></td> 
									</td>
									<td class="fila2">SERIE</td>
									<td class="fila2"><input type="text" class="textinput"  name="producto_serie" size="30" maxlength="50"  onChange="MAY(this)" value=""  /></td>

								</tr>
								
									<td class="fila2">CONDICION</td>
									<td class="fila2"><select name="producto_condicion" required> <option value=""></option>
										<?php
										$unidadmedida=mysql_query("SELECT idcondicion,condicion FROM condicion WHERE (condactivo = '1') ORDER BY idcondicion");
										while($filpro=mysql_fetch_array($unidadmedida))
										{
											?>
											<option   size="50"value="<?php echo $filpro['idcondicion'];?>"><?php echo $filpro['condicion'];?></option>
											<?php
										}?>  </td>

										<td class="fila2">PRECIO ADQUISICION&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;$</td>
										<td class="fila2"><input type="text" class="textinput"  name="producto_precioadqui" size="30" maxlength="50" onkeypress="return SoloNumeros(event)" value="" required/></td>
										<tr>
											<td class="fila2">OBSERVACIONES:</td>
											<td colspan="5" class="fila2"><input type="text" class="textinput"  name="producto_observaciones" size="200" maxlength="3000"  onChange="MAY(this)" /></td>
										</tr>

										<tr>

											<td class="fila2"><input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
											<td class="fila2"><input type="hidden" name="anterior" value="<?php echo $idanterior; ?>" /> </td>

											<td  class="fila2" colspan="4"><p align="center"><button type="submit" class="botonguardar" name="guardar_producto" title="Guardar producto" value=" Agregar " />Guardar</BUTTON></p></td>

										</tr>
									</form>
								</table>
							</div>
							<?php
						}
					}

					if (isset($_GET['entrada'])){?>


					<?php

//echo $buscarconsec;
//DESCRIPCION: TABLA DE ELEMENTOS AGREGADOS  A LA ENTRADA (datos generales de la entrada )
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
					?>

					<?php
					if (isset($_GET['entrada'])){
//PERMITE AGREGAR ELEMENTOS A LA ENTRADA SOLAMENTE SI SE TIENE CONSECUTIVO GENERADO DE LA MISMA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD


						?>




						<?php 

						if (isset($_GET['entrada'])){

							?>
							<center>

								<DIV ID="CENTRO2">
									<div class="container-full">
										<div class="table-responsive"> 
											<table   class=" table table-hover"  cellspacing="0" width="100%" >

												<h4><CENTER>ELEMENTOS AGREGADOS A LA ENTRADA</h4></CENTER>
												<tr >
													<thead>

														<td  class="fila1">ID</td>
														<td  class="fila1">DESCRIPCION</td>
														<td  class="fila1">UNID MEDIDA</td>

														<td class="fila1">MARCA</td>
														<td  class="fila1">MODELO</td>
														<td  class="fila1">SERIE</td>

														<td class="fila1">ID CONTAB</td>
														<td class="fila1">CODIGO CONTAB</td>
														<td  class="fila1">CONDICION</td>
														<td   class="fila1">PRECIO</td>
														<td  class="fila1">OBSERVACIONES</td>
														<td  class="fila1">QUITAR</td>
														<td  class="fila1">EDITAR</td>
													</thead>
													<tbody id="myTable">



														<?php
//SELECCION DE LOS ELEMENTOS DE LA ENTRADA PARA ASIGNACION CON LA ENTRADA CORRESPONDIENTE
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD


														$tablaaux="SELECT  * FROM productos  
														LEFT  JOIN unidadmedida on productos.idunidadmedida=unidadmedida.idunidadmedida
														LEFT  JOIN condicion on productos.idcondicion=condicion.idcondicion
														LEFT  JOIN proveedores on productos.idproveedor=proveedores.idproveedor
														LEFT  JOIN codigocontable on productos.codigocontable=codigocontable.codigocontable
														WHERE (numentrada='$buscarconsec')"; 

														$t_productos=mysql_query($tablaaux,$conexion);

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09 

//}
														while ($fila_productos=mysql_fetch_array($t_productos))
														{
															?>

															<tr>

																<td class="fila2"><?php echo $fila_productos["idelemento"];?></td>
																<td  class="fila2"><?php echo $fila_productos["elemento"];?></td>
																<td  class="fila2"><?php echo $fila_productos["unidadmedida"];?></td>

																<td  class="fila2"><?php echo $fila_productos["marca"];?></td>
																<td  class="fila2"><?php echo $fila_productos["modelo"];?></td>
																<td  class="fila2"><?php echo $fila_productos["serie"];?></td>

																<td  class="fila2"><?php echo $fila_productos["codigocontable"];?></td>
																<td  class="fila2"><?php echo $fila_productos["codigodescripcion"];?></td>
																<td  class="fila2"><?php echo $fila_productos["condicion"];?></td>
																<td  class="fila2" align="right">$<?php echo number_format($fila_productos["precioadqui"],2,',','.');?></td>
																<td  class="fila2"><?php echo $fila_productos["probservaciones"];?></td>
																<td class="fila2"><center>
																	<a href="activos_entrada_modificar.php?idelemento=<?php echo $fila_productos['idelemento'];?>&entrada=2&consec=<?php echo $buscarconsec;?>&anterior=<?php echo $idanterior;?> "><img src="../imagenes/delete.png" title="Quitar" width="28" height="28"/></a>
																</center></td>


																<td class="fila2"><center>
																	<a href="activos_entrada_serie.php?idelemento=<?php echo $fila_productos['idelemento'];?>&entrada=2&consec=<?php echo $buscarconsec;?>&anterior=<?php echo $idanterior;?> "><img src="../imagenes/editar1.png" title="Editar" width="28" height="28"/></a>
																</center></td>

															</tr>
															<?php

														}
														?>
														<?php
														$querysuma = mysql_query("SELECT SUM(precioadqui) as total FROM productos WHERE numentrada='$buscarconsec'");   
														$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
														$valortotal=$valortotal1["total"];

														?>
													</tbody>
													<TR>
														<TD class="fila2"colspan="8"> &nbsp;</TD>
														<TD class="fila2"colspan="1"><STRONG>VALOR TOTAL</STRONG></TD>

														<td  class="fila2"align="right"><strong>$<?php echo number_format($valortotal,2,',','.');?></strong></td>
														<TD class="fila2"colspan="3"> &nbsp;</TD>

														<?php

													}
												}
											}

											?>
										</TR>

									</table>   
								</div>
								<div>
									<center>
										<ul class="pagination pagination-lg pager" id="myPager"></ul></center>
										<div class="col-md-12 text-center">
										</div>

										
									</div>
									<?php
									if (isset($_GET['entrada'])){ 

//PERMITE AGREGAR ELEMENTOS A LA ENTRADA SOLAMENTE SI SE TIENE CONSECUTIVO GENERADO DE LA MISMA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

										$consultas="SELECT * FROM productos WHERE numentrada='$buscarconsec'";
										$consulta_items=mysql_query($consultas,$conexion);
//    while($filpro=mysql_fetch_array($unidadmedida)){
//
//  }


//PERMITE AGREGAR DATOS ADICIONALES A LA ENTRADA, LOS DATOS QUEDAN GUARDADOS EN TABL ADE ENTRADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

										$num_productos_entrada=mysql_num_rows($consulta_items);
										$elaboro=$_SESSION['nombres'] ."  ". $_SESSION['apellidos'];
										$fecha_entrada= date("Y-m-d");

										?>

									</DIV>

									<DIV ID="CENTRO">


										<table border="0" class="tabla_3" WIDTH="100%" >
											<th class="fila1" colspan="8 " style="font-size:14px;"><strong>INFORMACION ADICIONAL DE LA ENTRADA</strong></th>
											<tr>
												<td width="120" class="fila2">UNIDAD EJECUTORA</td>
												<td width="230" class="fila2" colspan="7"> <h5><?php   if (isset($_GET['entrada'])){ echo $unidadejecutora;}?></h5></td>
											</tr>

											<tr>
												<td width="120" class="fila2">NIT </td>
												<td width="230" class="fila2" colspan="3"> <h5><?php   if (isset($_GET['entrada'])){ echo $nitunidadejecutora;}?></h5></td>

												<td width="120" class="fila2">ENTRADA DE ELEMENTOS N°</td>
												<td width="230" class="fila2"> <h5><?php   if (isset($_GET['entrada'])){ echo $buscarconsec;}?></h5></td>
											</tr>
											<td width="120" class="fila2">FECHA DE ELABORACION</td>
											<td width="120" class="fila2"colspan="3"><h5><?php  ECHO date("Y-m-d");?></h5></td>

											<td width="90" class="fila2">TIPO DE ELEMENTOS</td>
											<td width="157" class="fila2" colspan="2" ><h5>DEVOLUTIVOS<h5></td>  
										</tr>
									</tr>
								</table>

								<center>
									<table class="tabla_3" width="60%"> 
										<tr>

										</tr>
										<tr>
											<form name="CrearEntrada" action="guardar_entrada.php?entrada=$buscarconsec"    title="Guardar Detalles de la entrada" method="post" onsubmit="guardar_entrada.disabled= true; return true;" >
											</center>
											<div class="entradaanterior" align="left">

												<?php

												$selectentanterior="SELECT identrada, entrada, fecha FROM entradas ";
												$queryant1=mysql_query($selectentanterior,$conexion);
												$numfilas=mysql_num_rows($queryant1);

												$selectentanterior2="SELECT identrada, entrada, fecha FROM entradas WHERE identrada='$numfilas' ";

												$queryentradaanterior=mysql_query($selectentanterior2,$conexion);
												while ($entanterior=mysql_fetch_array($queryentradaanterior)){
													?>

													Ultima entrada elaborada: <?PHP echo $entanterior['entrada']; ?>  &nbsp;&nbsp;&nbsp; Fecha : <?PHP echo $entanterior['fecha']; ?>

													<?PHP } ?>
												</div>
												<center>
												</tr>
												<td class="fila2" >FECHA DE ELABORACIÓN </td>
												<td class="fila2"><input size="12" id="f_date1" name="f_date1" value="<?php  ECHO date("Y-m-d");?>"><button id="f_btn1">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>


	<td class="fila2">DOCUMENTO/CONTRATO N°<td class="fila2"><input type="text" class="textinput"  colspan="4"name="entrada_contrato" size="30"  onChange="MAY(this)" value="" /></td></td>

	<tr>
		<td class="fila2">PROVEEDOR</td>
		<td class="fila2"><select name="entrada_idproveedor" required><option value=""></option>
			<?php
			$proveedor=mysql_query("SELECT idproveedor,proveedor,nit, direccion FROM proveedores WHERE (provactivo = '1') ORDER BY proveedor ASC");
			while($filapro=mysql_fetch_array($proveedor))
			{
				?>
				<option  value="<?php echo $proveedorid=$filapro['idproveedor'];?>"><?php echo $idprov=$filapro['proveedor'];?></option>
				<?php
			}?>
			?>
		</select></td>


		<td  class="fila2">FACTURA N°</td>
		<td class="fila2"><input type="text" class="textinput"  name="entrada_numfactura" size="30"  onChange="MAY(this)" value=""  /></td>
	</tr>

	<tr>
		<td class="fila2" colspan="4"><a href="Agregar_proveedor_entradas_add.php?entrada=2&consec=<?php echo $buscarconsec;?>&anterior=<?php echo $idanterior;?>"  align="center"><img align="center" src="../imagenes/proveedor_add.png"   title="Agregar Proveedor" width="24" height="24" align="left"/>Agreger Provedor</a></TD>
		</tr>
		<tr>
					<td   class="fila2">TIPO DE ENTRADA</td>
					<td class="fila2"><select name="entrada_tipoentrada_sel" required> <option value=""></option>
						<?PHP
						$tipoentradaq=mysql_query("SELECT idtipoentrada, tipoentrada_sel FROM tipoentrada WHERE (activo = '1' AND idtipoentrada>=1 AND idtipoentrada<=8 )  ORDER BY idtipoentrada");
						while($filtipoen=mysql_fetch_array($tipoentradaq))
						{
							?>
							<option     size="50"value="<?php echo $filtipoen['idtipoentrada'];?>"><?php echo $filtipoen['tipoentrada_sel'];?></option>
							<?php
						}?>

					</td>

		<tr>

			<td class="fila2" >COMENTARIO</td><td class="fila2" COLSPAN="4"><input type="text" class="textinput"  colspan="5"name="entrada_comentario" size="150"  onChange="MAY(this)" value=""  required/></td></td>


			<input type="hidden" name="entrada_numentrada" size="30"  onChange="MAY(this)" value="<?php echo ($buscarconsec);?>" /></td>
			<input type="hidden" name="entrada_elaboro" size="10"  onChange="MAY(this)" value="<?php echo ($elaboro);?>" /></td>
			<input type="hidden" name="entrada_numproductosentrada" size="30" maxlength="50" onChange="MAY(this)" value="<?php echo ($num_productos_entrada);?>" /></td>

			<input type="hidden""hidden" name="entrada_fecha" size="20" maxlength="50" onChange="MAY(this)" value="<?php echo ($fecha_entrada);?>" /></td>
			<input type="hidden" name="entrada_idproveedor2" size="10"  onChange="MAY(this)" value="<?php echo $proveedorid;?>" /></td>
			<input type="hidden" name="entrada_valortotal" size="10"  onChange="MAY(this)" value="<?php echo ($valortotal);?>" /></td>
			<tr>
				<td  class="fila2" colspan="4"><p align="center"><button type="submit" class="botonguardar2" name="guardar_entrada" title="Guardar Entrada" value=" Guardar Entrada" />Guardar Entrada</button></p></td>

			</form>
		</table>
	</div>

</body>



</html>
<?php 
}
}
?>
<?php

include ('../assets/footer.php');
?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

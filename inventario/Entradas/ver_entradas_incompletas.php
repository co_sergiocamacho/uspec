<?php
//DESCRIPCION: PERMITE VER ENTRADAS INCOMPLETAS-ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {
//CONEXION A LA BASE DE DATOS
include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
//INCLUYO LA HOJA DE ESTILOS
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	
	
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<style>	

		body {
			background: #eaeaea no-repeat center top;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Ver Entradas</title>

</head>

<body>
	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>



			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>

				<table width="100%" border="0">
					<tr>
						<td  class="titulo">
							<center><STRONG> ENTRADAS INCOMPLETAS</STRONG></center>
						</td>
					</tr>
				</table>

<div class="container-full">
	<div class="table-responsive">
		<table   class=" table"  cellspacing="0" width="100%" >
			<thead>
				<tr>
					<th  class="fila1">ID</th>
					<th  class="fila1">ENTRADA N</th>
					<th  class="fila1">TIPO DE ELEMENTOS</th>
					<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 
					<th class="fila1">TERMINAR</th>
					<?php }?>
	
				</tr>
			</thead>
			<tbody id="myTable">
				<?php

					$fecha1=date("Y-m-d");
					$fecha2=date("Y-m-d");
					$query1="SELECT  * FROM consecutivoentrada_temp
					
					WHERE activo='0' ORDER BY consecentrada  DESC"  ;
		

 /*if (empty($fecha1) and empty($fecha2)){ 
$query1="SELECT  * FROM entradas  LEFT  JOIN proveedores on entradas.idproveedor=proveedores.idproveedor   WHERE activo='1' AND tipoelementos='ACTIVOS DEVOLUTIVOS' AND entobservaciones<>'Entrada por reintegro' ORDER BY identrada  DESC"  ;
} */
$t_entradas=mysql_query($query1,$conexion);
while ($fila_entradas=mysql_fetch_array($t_entradas)){
	?>
	<tr>
		<td class="fila2"><?php echo $fila_entradas["idconsecentrada"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["consecentrada"];?></td>
		<td  class="fila2"><?php echo $fila_entradas["observaciones"];?></td>


		<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){?> 

		<td class="fila2"><center>
			<a href="eleccion_entrada_incompleta.php?entrada=2&consec=<?php echo $fila_entradas['consecentrada'];?>"><img src="../imagenes/editar1.png" title="Editar Entrada " width="28" height="28"/></a>
		</center></td>
		<?php }?>





	</tr>


	<?php
}
?>
</tbody>
</TABLE>


</div>
<div>
	<center>
		<ul class="pagination pagination-lg pager" id="myPager"></ul></center>
		<div class="col-md-12 text-center">
		</div>
		
	</div>
</div>
</div>
</div>




<?php
include ('../assets/footer.php');
?>

<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>




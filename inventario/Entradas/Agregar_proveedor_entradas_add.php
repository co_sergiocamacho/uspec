<?php
//DESCRIPCION: VENTANA PARA AGREGAR PROVEEDOR A UNA ENTRADA EN EJECUCION
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/datosgenerales.php");


	include("../assets/global.php");
	include("../assets/encabezado.php");
	$consec=$_REQUEST['consec'];
	$anterior=$_REQUEST['anterior'];




	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="../js/calendario/src/js/lang/en.js"></script>
		<script language="javascript">

			var cursor;
			if (document.all) {
// Está utilizando EXPLORER
cursor='hand';
} else {
// Está utilizando MOZILLA/NETSCAPE
cursor='pointer';
}

var miPopup
function nueva_categoria(){
	miPopup = window.open("categoria_new.php","miwin","width=900,height=380,scrollbars=yes");
	miPopup.focus();
}
</script>

<style>	
	<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
	body {
		background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		background-size: cover;
	}
	.container > header h1,
	.container > header h2 {
		color: #fff;
		text-shadow: 0 1px 1px rgba(0,0,0,0.7);
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nuevo PROVEEDOR</title>



<?php 
//include('menu.php');

?>
</head>
<script src="calendario/src/js/jscal2.js"></script>
<script src="calendario/src/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calendario/src/css/steel/steel.css" />

<body>


	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="#"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="#"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>




			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   
				<BR />
				<center>
					<table width="60%" class="tabla_2" border="0" style="width:580px">
						<tr>
							<td class="fila1">NUEVO PROVEEDOR</td>
						</tr>
						<tr>
							<td class="fila2"> Permite el ingreso un nuevo PROVEEDOR al sistema.</td>
						</tr>
					</table>

					<br>
					<form name="productos" action="proveedores_entrada_guardar.php" title="Nuevo Proveedor" method="post">
						<table width="60%" class="tabla_2" border="0" style="width:580px">
							<tr>
								<td colspan="5" class="fila1">&nbsp;</td>
							</tr>
							<tr>
								<td class="fila2">NOMBRE DE PROVEEDOR</td>
								<td class="fila2"><input type="text" class="textinput"  name="proveedores_nombre" size="50" maxlength="80"  onChange="MAY(this)" value=""  required/></td>
								<td class="fila2">NIT</td>
								<td class="fila2"><input type="text" class="textinput"  name="proveedores_nit" size="30" maxlength="80" onChange="MAY(this)" value="" required/></td>
							</tr>
							<tr>

							</tr>
							<tr>
								<td class="fila2">EMAIL</td>
								<td class="fila2"><input type="text" class="textinput"  name="proveedores_email" size="30" maxlength="80" value=""   required/></td>
								<td class="fila2">TELEFONO</td>
								<td class="fila2"><input type="t  ext" name="proveedores_telefono" size="30" maxlength="80" value="" required /></td>


							</tr>
							<tr>
								<td class="fila2">DIRECCION:</td>
								<td class="fila2"><input type="text" class="textinput"  name="proveedores_direccion" size="30" maxlength="80" onChange="MAY(this)" value="" /></td>
								<td class="fila2">FECHA CREACIÓN </td>
								<td class="fila2"><input size="10" id="f_date1" name="f_date1" value="<?php echo date("Y-m-d") ?>" /></td>
							</tr>
							<tr>
							</tr>
							<tr>
								<td class="fila2">OBSERVACIONES:</td>
								<td colspan="3" class="fila2"><input type="text" class="textinput"  name="proveedores_observaciones" size="100" maxlength="300"  onChange="MAY(this)" /></td>
							</tr>
							<tr>
								<td class="fila2">&nbsp;</td>
								<td colspan="1" class="fila2"><p align="right"> ACTIVO:</p></td>
								<td colspan="2" class="fila2"><input type="radio" name="proveedores_activo" value="1" class="check" checked="checked"/>
									SI
									<input type="radio" name="proveedores_activo" value="0" class="check" />
									NO </td>
								</tr>
								<tr>
									<input type="hidden" name="consec"   onChange="MAY(this)" value="<?php echo ($consec);?>" /></td>
									<input type="hidden" name="anterior"   onChange="MAY(this)" value="<?php echo ($anterior);?>" /></td>

									<td class="fila2"><input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
									<td class="fila2"><p align="center">
										<input type="submit" class="botonguardar" name="guardar_proveedor" title="Guardar proveedor" value="GUARDAR"/>
									</p></td>
									<td class="fila2">&nbsp;</td>
									<td class="fila2"><p align="center">


									</td>
								</tr>
							</table>
						</form>
					</center>
				</div>

				<?php

				include ('../assets/footer.php');
				?>
			</body>
			</html>









			<?php
//entrada=2&consec=<?php echo $buscarconsec;&anterior=<?php echo $idanterior;

/*

echo $consec;
echo "<br/>";

echo $anterior;
echo "<br/>";


*/

?>

<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>






<?php

//DESCRIPCION: VALIDACION PARA VERIFICAR TODOS LOS ELEMENTOS QUE HAY EN LA ENTRADA DE E. DE CONSUMO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");

	$idtabla_aux=$_REQUEST['idtabla_aux'];

	$idelemento=$_REQUEST['idelemento'];
	$cantidad=$_REQUEST['cant'];
	$numentrada=$_REQUEST["numentrada"];
	$observaciones="Entrada anulada el dia: " . date("Y-m-d");

	echo "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";
	echo "<br/>";
	echo "LA CANTIDAD DE ELEMENTOS:". $cantidad;
	echo "<br/>";
	echo "Entrada a modificar". $numentrada;
	echo "<br/>";
	echo "ID del Elemento:". $idelemento;

	echo "<br/>";

	echo "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";

//******************************************************************************************************************************************************
//SELECCION DE LOS DATOS A MODIFICAR POR EL ELEMENTO EN LA TABLA DE ENTRADA , solamente se utiliza paraobtener informacion general del elemento. no para modificar ni sobreescribir sus atributos

	$queryelemento="SELECT * FROM consumo WHERE idelemento='$idelemento' ";
	$datoselemento=mysql_query($queryelemento,$conexion);
	$filaelemento=mysql_fetch_array($datoselemento);
	$unidmedida= $filaelemento['unidmedida'];
	$valorunit=$filaelemento['precioadqui'];
	$valortotal=$valorunit*$cantidad;
	$codigocontable=$filaelemento['codigocontable'];
	$elaboro=$_SESSION['nombres'] ."  ". $_SESSION['apellidos'];
//*****************************************************************************

	$queryentrada="SELECT fecha FROM entradas WHERE entrada='$numentrada'";

	$datosentrada=mysql_query($queryentrada,$conexion);
	$filaentrada=mysql_fetch_array($datosentrada);
	$fechaentrada=$filaentrada['fecha'];
//*****************************************************************************
//DATOS PARA CREAR ELEMENTOS DE CONSUMO  DE ENTRADA ANULADAS....

/*
******************************************************************************************************************************************************
CREACION DE ELEMENTOS EN LA TABLA DE ENTRADAS ANULADAS. SE COPIAN DE LA TABLA AUXILAIR Y SE PASA A LA TABLA DE ENTRADAS ANULADAS
SE DEBE ACTUALIZAR los campos basicos de LA TABLA AUX DE ENTRADAS ANULADAS::

ID DEL ELEMENTO -CANTIDAD -NUMERO DE ENTRADA, 
ID DE UNIDAD DE MEDIDA, VALOR UNITARIO, VALOR TOTAL 
ELABORADO POR, CODIGO CONTABLE,  FECHA CREACION DE ENTRADA QUE SE ANULA

*/	
$query_tabla_aux_anulada="INSERT INTO tabla_aux_consumo_entradas_anuladas (idelemento_aux, cantidad_aux, unidadmedida_aux, valorunit_aux, valortotal_aux, codigocontable_aux, numentrada_aux, elaboro_aux, fecha_entrada_aux) 
VALUES ('$idelemento', '$cantidad', '$unidmedida',   '$valorunit', '$valortotal', '$codigocontable', '$numentrada', '$elaboro', '$fechaentrada')";
mysql_query($query_tabla_aux_anulada,$conexion);

//*******************************************************************************************************************************************************
//DESPUES DE CREAR TABLA AUXILIAR CON ENTRADAS ANULADAS SE PROCEDE A 
//REESTABLECER CANTIDADES DE LOS ELEMENTOS DE CONSUMO EN BODEGA

$queryconsumo="UPDATE  consumo SET cantidad=cantidad-'$cantidad' WHERE idelemento='$idelemento'";
$resta_consumo=mysql_query($queryconsumo, $conexion);

//*******************************************************************************************************************************************************
//ELIMINAR ELEMENTO DE LA TABLA AUXILIAR Y SE QUITA CONSECUTIVO DEL NUMERO DE ENTRADA

$delete_kardex=" DELETE FROM tabla_aux_kardex WHERE idelemento_kardex='$idelemento' AND entrada_kardex='$numentrada' AND idtabla_aux_ent_sal='$idtabla_aux' ";
$querydelete_kardex=mysql_query($delete_kardex,$conexion);

$queryborrar_de_tabla="DELETE FROM tabla_aux_consumo_entradas WHERE (idelemento_aux='$idelemento' AND numentrada_aux='$numentrada' AND idtabla_aux='$idtabla_aux')";
$borrar_de_entrada=mysql_query($queryborrar_de_tabla, $conexion);


$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_entradas WHERE numentrada_aux='$numentrada'", $conexion);   
$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
$valortotal=$valortotal1["total"];

$consultas="SELECT * FROM tabla_aux_consumo_entradas WHERE numentrada_aux='$numentrada'";
$consulta_items=mysql_query($consultas,$conexion);
$num_items_salida=mysql_num_rows($consulta_items);

$queryconsumo="UPDATE entradas SET numitems='$num_items_salida', valortotal='$valortotal' WHERE entrada='$numentrada'";
mysql_query($queryconsumo, $conexion);



//ACTUALIZAR ENTRADA ANULADA DESPUES DE BORRAR LOS DATOS
//$queryactivocero= "UPDATE entradas SET activo='0', entobservaciones='$observaciones' WHERE numentrada='$numentrada'  ";
//$inactiva=mysql_query($queryactivocero, $conexion);

//CUANDO NO SE TENGA ELEMENTOS EN LA ENTRADA SE PROCEDE A POENR COMENTARIO A LA ENTRADA ANULADA
header("location: anular_entrada_consumo1.php?entrada=$numentrada&borrado=1");

?>

<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

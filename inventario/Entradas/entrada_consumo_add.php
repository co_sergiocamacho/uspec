<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
//DESCRIPCION: VENTANA PARA AGREGAR ENTRADAS DE ELEMENTOS DE CONSUMO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

include("../database/conexion.php");
include("../assets/encabezado.php");
include("../assets/global.php");
include("../assets/datosgenerales.php");

if (isset($_GET['entrada'])){
	if (isset($_GET['consec'])){
		$buscarconsec=$_GET['consec'];
		//$idanterior=$_GET['anterior'];
	}
	else {
		header ("location: elementos_menu.php");

	}
}

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="text/javascript" src="../js/dataTables.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/tablas.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link  type="text/css" href="../css/dataTables.min.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/datatables.css"  >
	<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">

	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<link rel="stylesheet" type="text/css" href="../css/paginacion.css"  >
	<link rel="shortcut icon" href="../imagenes/1.ico">





	<style>	
		<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
		body {
			background: #eaeaea url(images/fondo_2.jpg) no-repeat center telefonop;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			background-size: cover;
		}
		.container > header h1,
		.container > header h2 {
			color: #fff;
			text-shadow: 0 1px 1px rgba(0,0,0,0.7);
		}
	</style>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ENTRADA DE ELEMENTOS DE CONSUMO

	</title>



	<BR />

	<?php 
//include('menu.php');

	?>
</head>


<body>



<?php  


if(!isset($_REQUEST['intro'])){
?>
<div id='ventana-flotante'>

   <a class='cerrar' href='javascript:void(0);' onclick='document.getElementById(&apos;ventana-flotante&apos;).className = &apos;oculto&apos;'>x</a>

   <div id='contenedor'>

       <div class='contenido'>
<center><h3>Instrucciones</h3></center>
<p align="left" class="blanco">1 Para agregar un elemento a la entrada debe estar previamente creado, para ello puede utilizar la función "Crear Elemento". </p>
<p align="left" class="blanco">2 A continuación se selecciona este elemento utilizando la función agregar elemento </p>

       </div>
   </div>
</div>

<style>
#ventana-flotante {
width: 360px;  /* Ancho de la ventana */
height: 300px;  /* Alto de la ventana */
background: rgba(100,190,230,.6);
border-radius:10px;
position: fixed;
top: 200px;
left: 50%;
margin-left: -180px;
border: 1px solid #adcffa;  /* Borde de la ventana */
box-shadow: 10px 5px 25px rgba(0,0,0,.1);  /* Sombra */
z-index:999;
}
#ventana-flotante #contenedor {
padding: 25px 10px 10px 10px;
}
#ventana-flotante .cerrar {
float: right;
border-bottom: 1px solid #ddd;
border-left: 1px solid #ddd;
color: #aaa ;
background: white;
line-height: 19px;
text-decoration: none;
padding: 0px 14px;
font-family: Arial;
border-radius: 0 0 0 8px;

font-size: 18px;
-webkit-transition: .3s;
-moz-transition: .3s;
-o-transition: .3s;
-ms-transition: .3s;
}
#ventana-flotante .cerrar:hover {
background: #e17171;
color: white;
text-decoration: none;
border-bottom:1px solid #efa1a1;
border-left: 1px solid #efa1a1;
}
#ventana-flotante #contenedor .contenido {
padding: 15px;
box-shadow: inset 1px 1px white;
background: rgba(46,127,230,.88);  /* Fondo del mensaje */
border: 0px solid #9efffc;  /* Borde del mensaje */
font-size: 14px;  /* Tamaño del texto del mensaje */
color: #fefefe;  /* Color del texto del mensaje */
margin: 0 auto;
border-radius: 10px;
}
.oculto {-webkit-transition:1.5s;-moz-transition:1.5s;-o-transition:1.5s;-ms-transition:1s;opacity:0;-ms-opacity:0;-moz-opacity:0;visibility:hidden;}
</style>

</div>




<?php  }

?>





	<div id="centro2">
	<?php
if(!isset($_REQUEST['entrada'])) {  ?>
		<table class="botonesfila" >
		
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
		<td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr>
		</table>
		<?php } ?>
		</div>

		<div id="centro">
		<div id="div_bienvenido">
		<?php echo "Bienvenido"; ?> <BR/>
		<div id="div_usuarios">
		<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
		</div>
		<?php echo "SALIR";?>
		'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
		</div>   

		<center>
		<table width="65%" border="0">
		<tr>
		<td colspan="11" class="titulo"><center>ENTRADA DE ELEMENTOS</center></td>
		</tr>
		</table>
		</center>

		<?php if ($_SESSION['idpermiso']==1 or $_SESSION['idpermiso']==2 ){ 
		if (!isset($_GET['entrada'])){

		?><center>
		<div id="generador">
		<table align="center">
		<tr>
		<center>
																			<div id="generador" width="30%" align="">
											<form name="CrearEntrada" action="nuevonumentrada_consumo.php"       title="Consecutivo de Entrada" method="post">
												<center><input type="image" src="../imagenes/entradanew.png" width="48" height="48"  value="GENERAR"><H3>Generar Consecutivo</H3></input></center>
											</center>
										</tr>
									</form>
								</div>
							</center>
						</TABLE>
						</DIV>
						<?php
//solicitud de confirmacion de consecutivo
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
					}
					if (isset($_GET['entrada'])){
						if ($_GET['entrada']==2){
//Activar generador de Consecutivos
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD
							?> 
						</center>
						<center>



							<?php
//**********************************************************************************************************************************************************************************************
//**********************************************************************************************************************************************************************************************
//**********************************************************************************************************************************************************************************************
//********Codigo para crear elementos y volver a la entrada?>
<br>
<table width="100%" class="tabla_2" border="0" style="">
	<form name="clientes" action="consumo_guardar_include.php" title="Nuevo Elemento" method="post"   onsubmit="guardar_elem_nuevo.disabled= true; return true;">
		<tr><td colspan="110" class="fila1"><h2 CLASS="TEXTOBLANCO">CREAR ELEMENTO</h2> (Crea elementos nuevos)</td></tr>
		<tr>
			<td class="fila2">ELEMENTO:</td>
			<tr>
			<td class="fila2" colspan="10"><input type="text" class="textinput"  name="consumo_elemento" size="150"  onChange="MAY(this)" value=""  required/></td>
			<tr>
			<td class="fila2" colspan="1">UNID. MEDIDA:</td>
			<td class="fila2" colspan="1" ><select name="consumo_unidadmedida" required>
				<option selected="" value= ""colspan="2"></option>
				<?php
				$unidades=mysql_query("SELECT idunidadmedida, unidadmedida FROM unidadmedida WHERE (unidactivo = '1') ");
				while($filunidades=mysql_fetch_array($unidades))
					{ if ($filunidades["idunidadmedida"]==$filconsumo["unidadmedida"])
				echo "<option selected='' value='". $filunidades['idunidadmedida'] ."'>". $filunidades['unidadmedida'] ."</option>";

				else
					echo "<option value='". $filunidades['idunidadmedida'] ."'>". $filunidades['unidadmedida'] ."</option>";}?>

			</select></td><td class="fila2">CODIGO CONTABLE</td><td class="fila2"colspan="87"><select name="consumo_codigocontable" required>
			<option selected="" value="">
				<?php
				$codigos=mysql_query("SELECT idcodigocontable, codigocontable, codigodescripcion FROM codigocontable",$conexion);
				while($filcodigos=mysql_fetch_array($codigos)){
					echo "<option selected='' value='". $filcodigos['codigocontable'] ."'>". $filcodigos['codigocontable']. " &nbsp;&nbsp;  ". $filcodigos['codigodescripcion'] ."</option>";}?>
				</select></td></tr>
				<tr>
					<td class="fila2" colspan="1">CANT. MINIMA STOCK</td>
					<td class="fila2"><input type="text" class="textinput"  name="consumo_minimostock" size="7" maxlength="80" onkeypress="return SoloNumeros(event)" value="" /></td>
					<td class="fila2"colspan="1">VALOR UNIT:</td>
					<td class="fila2"colspan="2"><input type="text" class="textinput"  name="consumo_valorunit" size="20"  onkeypress="return SoloNumeros(event)" value="" required/></td>

					<td class="fila2">OBSERVACIONES:</td><td colspan="2" class="fila2"><input type="text" class="textinput"  name="consumo_observaciones" size="65" maxlength="2000"  onChange="MAY(this)" value="" /></td> 
					<input type="hidden" name="numentrada" value="<?php echo $buscarconsec;?>" />
					<td class="fila2">
						<input type="hidden" name="anterior" value="<?php echo $anterior; ?>" /> 
						<td class="fila2"><button type="submit" class="botonguardarmini" name="guardar_elem_nuevo" title="Guardar" value="GUARDAR"/>Guardar</BUTTON>
						</p></td>


					</a> </td>
				</tr>

				<tr>

				</tr>
			</table>
		</form>

		<?php
//**********************************************************************************************************************************************************************************************
//**********************************************************************************************************************************************************************************************?>


<?php 

//*************************************************NUEVO CODIGO


?>

<div class="container-full" >
<div class="table-responsive">
	<table   class=" table"  cellspacing="0" >
		<thead>
			<tr>
				<td class="fila1" align="center" colspan="18 "><strong>ELEMENTOS DE CONSUMO DISPONIBLES PARA AGREGAR A LA ENTRADA</strong></td>
			</tr><tr>
			<td  class="fila1">ID</td>
			<td  class="fila1">CANT. DISPONIBLE</td>
			<td  class="fila1">DESCRIPCION</td>
			<td  class="fila1">UNID MEDIDA</td>
			<td  class="fila1">ID CONTAB</td>
			<td  class="fila1">CODIGO CONTAB</td>
			<td  class="fila1">PRECIO</td>
			<td  class="fila1">CANTIDAD SOLICITADA</td>
			<td  class="fila1">AÑADIR</td>
		</tr>
	</thead>
	<tbody id="myTable2">
		<?php

			$tablaaux="SELECT  * FROM consumo
			LEFT  JOIN unidadmedida on consumo.unidmedida=unidadmedida.idunidadmedida
			LEFT  JOIN codigocontable on consumo.codigocontable=codigocontable.codigocontable
			ORDER BY elemento ASC"; 

			$t_consumo=mysql_query($tablaaux,$conexion);

//INICIO DEL PAGINADOR DE LA CONSULTA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09 

//}


//href="salida_consumo_agregar_a_tabla.php?idelemento=<?php echo $fila_consumo['idelemento'];?>

<?php 

while ($fila_consumo=mysql_fetch_array($t_consumo))
{
	?>
	<tr>
		<td class="fila2"><?php echo $fila_consumo["idelemento"];?></td>
		<td class="fila2">
                    <center>
                    <?php echo $fila_consumo["cantidad"];?>
                    </center></td>
		<td  class="fila2"><?php echo $fila_consumo["elemento"];?></td>         
		<td  class="fila2"><?php echo $fila_consumo["unidadmedida"];?></td>
		<td  class="fila2"><?php echo $fila_consumo["codigocontable"];?></td>
		<td  class="fila2"><?php echo $fila_consumo["codigodescripcion"];?></td>
		<td  class="fila2"align="right">$<?php echo $fila_consumo["precioadqui"];?></td>
		
		<td class="fila2"><center><form name="consumo_cantidad_agregar" method="POST" action='entrada_consumo_guardar.php' >
		<input class="fila6" type="text" name="consumo_cantidad" value="1" onkeypress="return SoloNumeros(event)"></center>
		<input type="hidden" name="consumo_idelemento" value="<?php echo "$fila_consumo[idelemento]"; ?>" />
		<input type="hidden" name="anterior" value="<?php echo "$anterior"; ?>" />
		<input type="hidden" name="consumo_consecutivo" value="<?php echo $buscarconsec;?>"/>  
		<input type="hidden" name="anterior" value="<?php echo $anterior;?>"/>  </td>

			


		</td>

		<td class="fila2"> <center> <strong> <input type="submit" class="botonguardar3" name="guardar_producto" title="Guardar producto" value=" + "/></strong></center>
		</td></form>
	</tr>
	<?php } ?>  
</tbody>
</TABLE>



</div>
<div>
	<center>
		<ul class="pagination pagination-lg pager" id="myPager2"></ul></center>
		<div class="col-md-12 text-center">

		</div>


<?php //******************FIN NUEVO CODIGO ************?>


<?php 

if (isset($_GET['tabla_aux'])){

	$tabla_aux_actual=$_GET['tabla_aux'];
} else {

	$tabla_aux_actual=0;
}
?>

<div id="centro">
	<table class="tabla_2" width="100%">
		<th class="FILA1" style="font-size:14px;" COLSPAN="9">ELEMENTOS AGREGADOS A LA ENTRADA</th>
		<TR>

			<td  class="fila1">ID</td>
			<td  class="fila1">DESCRIPCION</td>
			<td   class="fila1">CANTIDAD</td>
			<td  class="fila1">UNID MEDIDA</td>
			<td  class="fila1">COD. CONTABLE</td>
			<td  class="fila1">CATEGORIA</td>
			<td   class="fila1">VALOR UNIT.</td>
			<td   class="fila1">VALOR TOTAL</td>
			<td   class="fila1">ELIMINAR</td>


			<?PHP
			$tablaaux_consumo="SELECT  * FROM tabla_aux_consumo_entradas  
			LEFT  JOIN consumo on tabla_aux_consumo_entradas.idelemento_aux=consumo.idelemento
			LEFT  JOIN codigocontable on tabla_aux_consumo_entradas.codigocontable_aux=codigocontable.codigocontable
			LEFT  JOIN unidadmedida on tabla_aux_consumo_entradas.unidadmedida_aux=unidadmedida.idunidadmedida

			WHERE (numentrada_aux='$buscarconsec')"; 
//BUSCARCONSEC = ENTRADA NUMERO  ENT-XXX

			$t_consumo_aux=mysql_query($tablaaux_consumo,$conexion);

			while ($fila_tabla_aux=mysql_fetch_array($t_consumo_aux)){
				?>
				<tr>
					<td class="fila2"><?php echo $fila_tabla_aux["idelemento_aux"];?></td>
					<td class="fila2"><?php echo $fila_tabla_aux["elemento"];?></td>
					<td class="fila2"><?php echo $fila_tabla_aux["cantidad_aux"];?></td>
					<td class="fila2"><?php echo $fila_tabla_aux["unidadmedida"];?></td>
					<td class="fila2"><?php echo $fila_tabla_aux["codigocontable"];?></td>
					<td class="fila2"><?php echo $fila_tabla_aux["codigodescripcion"];?></td>
					<td class="fila2" align="right">$<?php echo $fila_tabla_aux["valorunit_aux"];?></td>
					<td class="fila2" align="right">$<?php echo $fila_tabla_aux["valortotal_aux"];?></td>
					<td class="fila2" align="center">
						<a href="consumo_quitar_elementos_entrada.php?idtabla_aux=<?php echo $fila_tabla_aux['idtabla_aux'];?>&idelemento=<?php echo $fila_tabla_aux['idelemento'];?>&numentrada=<?php echo $buscarconsec;?>&anterior=<?php echo $anterior;?>"><img src="../imagenes/delete.png" title="Quitar de la Entrada " width="28" height="28"/></a></td>
					</tr>
					<?php }  ?>

					<?php
					$querysuma = mysql_query("SELECT SUM(valortotal_aux) as total FROM tabla_aux_consumo_entradas WHERE numentrada_aux='$buscarconsec'");   
					$valortotal1 = mysql_fetch_array($querysuma,  MYSQL_ASSOC);
					$valortotal=$valortotal1["total"];

					?>
					<TR>
						<TD class="fila2"colspan="6"> &nbsp;</TD>


						<TD class="fila2"colspan="1"><STRONG>VALOR TOTAL</STRONG></TD>

						<td  class="fila3" align="right">$<?php echo number_format($valortotal,3,',','.');?></td>
						<TD class="fila2"colspan="2"> &nbsp;</TD>

						<?php
						if (isset($_GET['entrada'])){ 

//PERMITE AGREGAR ELEMENTOS A LA ENTRADA SOLAMENTE SI SE TIENE CONSECUTIVO GENERADO DE LA MISMA
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

							$consultas="SELECT * FROM tabla_aux_consumo_entradas WHERE numentrada_aux='$buscarconsec'";
							$consulta_items=mysql_query($consultas,$conexion);
//    while($filpro=mysql_fetch_array($unidadmedida)){
//
//  }


//PERMITE AGREGAR DATOS ADICIONALES A LA ENTRADA, LOS DATOS QUEDAN GUARDADOS EN TABL ADE ENTRADAS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD

							$num_productos_entrada=mysql_num_rows($consulta_items);
							$elaboro=$_SESSION['nombres'] ."  ". $_SESSION['apellidos'];
							$fecha_entrada= date("Y-m-d");
						}
						?>

					</TR>

				</DIV>
			</tABLE>
		</div>

		<table border="0" class="tabla_3" WIDTH="100%" >
			<th class="fila1" colspan="8 " style="font-size:14px;"><strong>INFORMACION ADICIONAL DE LA ENTRADA</strong></th>
			<tr>
				<td width="120" class="fila2">UNIDAD EJECUTORA</td>
				<td width="230" class="fila2" colspan="7"> <h6><?php   if (isset($_GET['entrada'])){ echo $unidadejecutora;}?></h6></td>
			</tr>

			<tr>
				<td width="120" class="fila2">NIT </td>
				<td width="230" class="fila2" colspan="3"> <h6><?php   if (isset($_GET['entrada'])){ echo $nitunidadejecutora;}?></h6></td>

				<td width="120" class="fila2">ENTRADA DE ELEMENTOS N°</td>
				<td width="230" class="fila2"> <h6><?php   if (isset($_GET['entrada'])){ echo $buscarconsec;}?></h6></td>
			</tr>
			<td width="120" class="fila2">FECHA DE ELABORACION</td>
			<td width="120" class="fila2"><h6><?php  ECHO date("Y-m-d");?></h6></td>

			<td width="90" class="fila2">TIPO DE ELEMENTOS</td>
			<td width="157" class="fila2"><h6>CONSUMO<h6></td>  <TD class="fila2"colspan="2"> &nbsp;</TD>
		</tr>

	</tr>
</table>
<center>

	<div class="entradaanterior" align="left">

		<?php

			$selectentanterior="SELECT identrada, entrada, fecha FROM entradas ";
			$queryant1=mysql_query($selectentanterior,$conexion);
			$numfilas=mysql_num_rows($queryant1);

			$selectentanterior2="SELECT identrada, entrada, fecha FROM entradas WHERE identrada='$numfilas' ";

			$queryentradaanterior=mysql_query($selectentanterior2,$conexion);
			while ($entanterior=mysql_fetch_array($queryentradaanterior)){
			?>
			Ultima entrada elaborada: <?PHP echo $entanterior['entrada']; ?>  &nbsp;&nbsp;&nbsp; Fecha : <?PHP echo $entanterior['fecha']; ?>

			<?PHP } ?>
		</div>

		<table class="tabla_3" width="60%"> 

			<tr>
				<form name="CrearEntrada" action="guardar_entrada_consumo.php?entrada=$buscarconsec"    title="Guardar Detalles de la entrada" method="post"  onsubmit="guardar_Entrada.disabled= true; return true;">

				</tr>
				<td class="fila2" >FECHA DE ELABORACIÓN </td>
				<td class="fila2"><input size="12" id="f_date1" name="f_date1" value="<?php  ECHO date("Y-m-d");?>"><button id="f_btn1">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>


	<td class="fila2">DOCUMENTO/CONTRATO N°<td  class="fila2" colspan="2"><input type="text" class="textinput"  name="entrada_contrato" size="30"  onChange="MAY(this)" value="" /></td></td>

	<tr>
		<td class="fila2">PROVEEDOR</td>

		<td class="fila2"><select name="entrada_idproveedor" required><option value=""></option>
			<?php
			$proveedor=mysql_query("SELECT idproveedor,proveedor,nit, direccion FROM proveedores WHERE (provactivo = '1') ORDER BY proveedor ASC");
			while($filapro=mysql_fetch_array($proveedor))
			{
				?>
				<option  value="<?php echo $proveedorid=$filapro['idproveedor'];?>"><?php echo $idprov=$filapro['proveedor'];?></option>
				<?php
			}?>
			?>
		</select></td>

		<td  class="fila2">FACTURA N°</td>
		<td class="fila2"><input type="text" class="textinput"  name="entrada_numfactura" size="30"  onChange="MAY(this)" value=""  /></td>
	</tr>

			<tr>
			<td   class="fila2">TIPO DE ENTRADA</td>
			<td class="fila2"><select name="entrada_tipoentrada_sel" required> <option value=""></option>
			<?PHP
			$tipoentradaq=mysql_query("SELECT idtipoentrada, tipoentrada_sel FROM tipoentrada WHERE (activo = '1' AND idtipoentrada>=1 AND idtipoentrada<=8) ORDER BY idtipoentrada");
			while($filtipoen=mysql_fetch_array($tipoentradaq))
			{
			?>
			<option     size="50"value="<?php echo $filtipoen['idtipoentrada'];?>"><?php echo $filtipoen['tipoentrada_sel'];?></option>
			<?php
			}?>

			</td>
		<tr>
	<tr>
		<td class="fila2" colspan="4"><a href="Agregar_proveedor_entradas_consumo_add.php?entrada=2&consec=<?php echo $buscarconsec;?>&anterior=<?php echo $anterior;?>"  align="center"><img align="center" src="../imagenes/proveedor_add.png"   title="Agregar Proveedor" width="24" height="24" align="left"/>Agreger Provedor</a></TD>
		</tr>
		<tr>
			<td class="fila2" >COMENTARIO</td><td class="fila2" COLSPAN="3"><input type="text" class="textinput"  colspan="5"name="entrada_comentario" size="150"  onChange="MAY(this)" value=""  required/></td></td>



		</tr>

		<input type="hidden" name="entrada_numentrada" size="30"  onChange="MAY(this)" value="<?php echo ($buscarconsec);?>" /></td>
		<input type="hidden" name="entrada_elaboro" size="10"  onChange="MAY(this)" value="<?php echo ($elaboro);?>" /></td>
		<input type="hidden" name="entrada_numproductosentrada" size="30" maxlength="50" onChange="MAY(this)" value="<?php echo ($num_productos_entrada);?>" /></td>

		<input type="hidden" name="entrada_fecha" size="20" maxlength="50" onChange="MAY(this)" value="<?php echo ($fecha_entrada);?>" /></td>
		<input type="hidden" name="entrada_idproveedor2" size="10" onChange="MAY(this)"  value="<?php echo $proveedorid;?>" /></td>
		<input type="hidden" name="entrada_valortotal" size="10"  onChange="MAY(this)" value="<?php echo $valortotal;?>" /></td>
		<tr>
			<td  class="fila2" colspan="4"> <p align="center"><button type="submit" class="botonguardar2" name="guardar_Entrada" title="Guardar Entrada" value=" Guardar Entrada" />Guardar Entrada</button></p></td>

		</form>
	</tr>
</table>
</div>
</BODY>
</DIV>
</DIV>







<?php


}
}
}




include("../assets/footer.php");

?>


<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

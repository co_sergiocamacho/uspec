<?php
//DESCRIPCION: Reporte de entradas
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
if (isset($_SESSION['idpermiso'])) {
    include("../database/conexion_pdo.php");
    include("../assets/encabezado.php");
    ?>
    <!doctype html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <title>Reporte de Entradas</title>
            <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="../bootstrap/css/dataTables.bootstrap.min.css" />
            <link rel="stylesheet" href="../js/jquery-ui.css" />
            <script src="../js/jquery-3.1.1.min.js"></script>
            <script src="../js/jquery-ui.js"></script>
            <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
            <script src="../bootstrap/js/dataTables.bootstrap.min.js"></script>
            <script src="../bootstrap/js/bootstrap.min.js"></script>
            <link href="../css/estilos.css" type="text/css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="../css/LoadScreen.css"/>
            <script type="text/javascript" src="../js/ReportEntry.js"></script>
            <script type="text/javascript" src="../js/ConfigurationCalendar.js"></script>
            <link href="../css/modal.css" type="text/css" rel="stylesheet">
            <link href="../css/table.css" type="text/css" rel="stylesheet">
            <link href="../css/switch.css" type="text/css" rel="stylesheet">
            <script>
                /**
                 * Datepicker
                 * @returns {undefined}
                 */
                $(function () {
                    $("#DateStart").datepicker({
                        showWeek: true,
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'dd-mm-yy',
                        onClose: function (selectedDate) {
                            $("#DateEnd").datepicker("option", "minDate", selectedDate);
                        }
                    });

                    $("#DateEnd").datepicker({
                        showWeek: true,
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'dd-mm-yy',
                        onClose: function (selectedDate) {
                            $("#DateStart").datepicker("option", "maxDate", selectedDate);
                        }
                    });
                });
            </script>
        </head>
        <body>
            <div id="centro2">
                <table class="botonesfila" >
                    <tr>
                        <td><a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" title="Inicio">INICIO</a>
                        </td>
                        <td><a href="elementos_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" title="Inicio">ATRAS</a></td>
                    </tr>
                </table>
            </div>
            <div id="centro">
                <div id="div_bienvenido">
                    <?php echo "Bienvenido"; ?> <BR/>
                    <div id="div_usuarios">
                        <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
                    </div>
                    <?php echo "SALIR"; ?>
                    '<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
                </div>
                <center>
                    <table width="50%" border="0">
                        <tr>
                            <td class="titulo">
                        <center>Reporte de Entradas</center>
                        </td>
                        </tr>
                    </table>
                    <input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
                </center>
                <div class="container">
                    <div class="row">

                        <div class="col-sm-10 col-sm-offset-1">

                            <div class="panel panel-default panel-table">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col col-sm-4">
                                            <form role="form" method="post" >
                                                <div class="form-group">
                                                    <label>Fecha de inicio (Día-Mes-Año)</label>
                                                    <input type="text" class="form-control" id="DateStart" name="DateStart">
                                                </div>
                                                <div class="form-group">
                                                    <label>Fecha de fin (Día-Mes-Año)</label>
                                                    <input type="text" class="form-control" id="DateEnd" name="DateEnd">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo de entrada</label>
                                                    <select class="selectpicker" id="TypeElement" name="TypeElement">
                                                        <option value="-1">Todos</option>
                                                        <option value="ELEMENTOS DE CONSUMO">Elementos de consumo</option>
                                                        <option value="ACTIVOS DEVOLUTIVOS">Activos devolutivos</option>
                                                    </select>
                                                </div>
                                                <div class="alert alert-warning" role="alert" id="Message_AlertSearchListEntry"></div>
                                            </form>
                                            <p></p>
                                            <div class="col col-sm-12 text-right">
                                                <button type="button" id="Button_SearchListEntry" class="btn btn-sm btn-primary btn-create"><em class="glyphicon glyphicon-search"></em>  Buscar</button>
                                            </div>
                                        </div>
                                        <div class="col col-sm-4 text-left">

                                        </div>
                                        <div class="col col-sm-4 text-left">

                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table id="ListEntrySearch" class="table table-condensed table-striped table-bordered">
                                        <thead>
                                            <tr>

                                                <th>Entrada</th>
                                                <th>Fecha</th>
                                                <th>Tipo de entrada</th>
                                                <th>Proveedor </th>
                                                <th>Cantidad</th>
                                                <th>Valor Total</th>
                                                <th>Elaborado por</th>
                                                <th>Comentario</th>
                                                <th>Detalle</th>
                                                <th>Acciones</th>
                                                <th>PDF</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>

                                                <th>Entrada</th>
                                                <th>Fecha</th>
                                                <th>Tipo de entrada</th>
                                                <th>Proveedor </th>
                                                <th>Cantidad</th>
                                                <th>Valor Total</th>
                                                <th>Elaborado por</th>
                                                <th>Comentario</th>
                                                <th>Detalle</th>
                                                <th>Acciones</th>
                                                <th>PDF</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div></div></div>
            </div>
            <?php
            include ("../assets/footer.php");
            ?> 
        </body>
    </html>
    <?php
} else {
    header("location: ../403.php");
}
?>




<?php
//DESCRIPCION: CAMBIO DE CLAVE POR PARTE DEL USUARIO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>



	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>


			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuario Modificar</title>



		<div id="div_bienvenido">
			<?php echo "Bienvenido"; ?> <BR/>
			<div id="div_usuarios">
				<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
			</div>
		</div>   
		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<body>
		<br>
		<div id="centro">
			<a href="../principal.php?num=1"><input type="image" src="../imagenes/inicio6.png" width="38" height="38" name="regresar" title="Inicio" value="Regresar" class="botonf" /></a>
			<center>
				<table width="60%" class="tabla_2" border="0" style="width:580px">
					<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24
					$idusuario=$_REQUEST["idusuario"];
					$rst_usuarios=mysql_query ("SELECT *
						FROM usuarios
						WHERE idusuario=". $_REQUEST["idusuario"].";",$conexion);
//echo $rst_clientes;
					while($filusu=mysql_fetch_array($rst_usuarios))
					{

						?>

						<tr>
							<td class="fila1">CAMBIAR CONTRASEÑA</td>
						</tr>
						<tr>
						</tr>
					</table>
					<script> 
						function comprobarClave(){ 	
							clave1 = document.f1.password1.value;
							clave2 = document.f1.password2.value;

							if (clave1!= clave2) {
								return false;
							}else {

								if (clave1=="" || clave2=="") {
									alert("¡Campo vacio!");
									document.f1.password1.focus();
									return false;
								}
								alert("¡Su clave ha sido cambiada con éxito!");
								return true;	
							}
						} 
					</script> 
					<br>
					<form name="f1" action="usuario_update_clave.php?idusuario=<?php echo $idusuario;?>" title="Nuevo Usuario" method="post">
						<table width="60%" class="tabla_2" border="0" style="width:580px">

							<tr>
								<?php

								if(isset($_REQUEST['error'])){



									?>
									<td colspan="2" class="fila2 rojoerror"><center>

										<style>

											.rojoerror {

												color:#fe6b6c;
												font-size: 16px;
											}

										</style>
										La contraseña no coincide, verifique nuevamente</center></td>


										<?php 
									}
									?>
								</tr>
								<tr>
									<td class="fila2">Contraseña nueva:</td>
									<td class="fila2"><input type="password" name="password1" size="40" maxlength="80" value="" required  /></td>
								</tr>
								<tr>

									<td class="fila2">Escriba nuevamente su contraseña:</td>
									<td class="fila2"><input type="password" name="password2" size="40" maxlength="80" value="" required  /></td>
								</tr>




							</tr>

							<tr>
								<td class="fila2"><input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
								<td class="fila2"><p align="center">
									<input type="submit" class="botonguardar" name="guardar_producto" title="Guardar producto" value="GUARDAR" onClick="comprobarClave()"/>
								</p></td>



							</td>
						</tr>
					</table>
				</form></center>

				<?php
			}
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
			include ("../assets/footer.php");
			?>
			<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

</body>
</html>
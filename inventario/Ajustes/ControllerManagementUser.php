<?php
session_start();
include("../database/conexion_pdo.php");
if (isset($_SESSION['idpermiso'])) {
    date_default_timezone_set('America/Bogota');
    $hoy_dia_reg=date('Y-m-d');
    
    /**
     * Generate password
     */
    $random_salt="";
    $password="";
    if($_POST["PasswordUser"]!=""){
       $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
       $password=$_POST["PasswordUser"];
       $password=sha1($password);
       $password=hash('sha1', $password . $random_salt); 
    }
    
     /********************/

    
    $EditUser = $_POST["EditUser"];
    $ContractDateUser = $_POST['ContractDateUser'];
    if($ContractDateUser==""){
        $ContractDateUser=NULL;
    }
    
    if($EditUser=="0"){
        /**
         * Create new user
         */
        $slqinsertusert="INSERT INTO usuarios ";
        $slqinsertusert.="(";
        $slqinsertusert.="nombres, ";
        $slqinsertusert.="apellidos, ";
        $slqinsertusert.="usuario, ";
        $slqinsertusert.="password, ";
        $slqinsertusert.="email, ";
        $slqinsertusert.="documentoid, ";
        $slqinsertusert.="fechacrea, ";
        $slqinsertusert.="idprofesion, ";
        $slqinsertusert.="grado, ";
        $slqinsertusert.="cargo, ";
        $slqinsertusert.="fechafincontrato, ";
        $slqinsertusert.="calidadempleado, ";
        $slqinsertusert.="iddependencia, ";
        $slqinsertusert.="usuactivo, ";
        $slqinsertusert.="salt, ";
        $slqinsertusert.="idpermiso  ";
        $slqinsertusert.=") ";
        $slqinsertusert.="VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
        /* Deshabilitar autocommit */

        try { 
            $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conexion->beginTransaction();
            $gsent = $conexion->prepare($slqinsertusert);
            $gsent->bindParam(1, $_POST['NameUser'], PDO::PARAM_STR);
            $gsent->bindParam(2, $_POST['LastNameUser'], PDO::PARAM_STR);
            $gsent->bindParam(3, $_POST['IdentityCard'], PDO::PARAM_STR);
            $gsent->bindParam(4, $password, PDO::PARAM_STR);
            $gsent->bindParam(5, $_POST['EmailUser'], PDO::PARAM_STR);
            $gsent->bindParam(6, $_POST['IdentityCard'], PDO::PARAM_STR);
            $gsent->bindParam(7, $hoy_dia_reg);
            $gsent->bindParam(8, $_POST['OccupationUser'], PDO::PARAM_STR);
            $gsent->bindParam(9, $_POST['GradeUser'], PDO::PARAM_STR);
            $gsent->bindParam(10, $_POST['PositionUser'], PDO::PARAM_STR);
            $gsent->bindParam(11, $ContractDateUser);
            $gsent->bindParam(12, $_POST['TypeContractUser'], PDO::PARAM_STR);
            $gsent->bindParam(13, $_POST['OfficeUser'], PDO::PARAM_STR);
            $gsent->bindParam(14, $_POST['StatusUser'], PDO::PARAM_STR);
            $gsent->bindParam(15, $random_salt, PDO::PARAM_STR);
            $gsent->bindParam(16, $_POST['TypeLevelUser'], PDO::PARAM_STR);
            $gsent->execute();
            $jsondata = array();
            $lastInsertIdUser=$conexion->lastInsertId();
            if($gsent->rowCount() > 0){
                       /**
                        * Insert Node Module User
                        */
               
                        $Node_Permission = $_POST['SecurityLevelUser'];
                        if($Node_Permission!=""){
                            $Array_Node_Permission = explode(",", $Node_Permission);
                            for($i=0;$i<count($Array_Node_Permission);$i++){

                                $slqinsertnode_module_user="INSERT INTO node_module_user ";
                                $slqinsertnode_module_user.="(";
                                $slqinsertnode_module_user.="id_user , ";
                                $slqinsertnode_module_user.="id_node_module ";
                                $slqinsertnode_module_user.=") ";
                                $slqinsertnode_module_user.="VALUES (?,?) ";
                                $gsentn = $conexion->prepare($slqinsertnode_module_user);

                                $gsentn->bindParam(1, $lastInsertIdUser, PDO::PARAM_INT);
                                $gsentn->bindParam(2, $Array_Node_Permission[$i]);
                                $gsentn->execute();


                            }  
                        }
                        
                        
                        
                        
               
                $jsondata = array(
                        'success' => true,
                        'message' => sprintf("Se registró con éxito el nuevo usuario."),
                        'total'=> $gsent->rowCount(),
                        'complete'=>true
                ); 
            }
            $conexion->commit();
            $gsent->closeCursor();
            $gsent=null;
             if($Node_Permission!=""){
                $gsentn->closeCursor();
                $gsentn=null;
             }
            $conexion=null;

            echo json_encode($jsondata);


        } catch (PDOException $e) {
            $jsondata = array();
            $conexion->rollBack();
            $conexion=null;
            $jsondata = array(
                        'success' => true,
                        'message' => sprintf("No se registro el usuario. Por favor informe al administrador."),
                        'infoerror'=>$e->getMessage(),
                        'total'=> "0",
                        'complete'=>false
            ); 
            echo json_encode($jsondata);

        } 
    }
    if($EditUser=="1"){
        
        
        
        /**
         * Edit user
         */
        $slqinsertusert="UPDATE usuarios ";
        $slqinsertusert.="SET ";
        $slqinsertusert.="nombres=?, ";
        $slqinsertusert.="apellidos=?, ";
        $slqinsertusert.="usuario=?, ";
        $slqinsertusert.="email=?, ";
        $slqinsertusert.="documentoid=?, ";
        $slqinsertusert.="fechacrea=?, ";
        $slqinsertusert.="idprofesion=?, ";
        $slqinsertusert.="grado=?, ";
        $slqinsertusert.="cargo=?, ";
        $slqinsertusert.="fechafincontrato=?, ";
        $slqinsertusert.="calidadempleado=?, ";
        $slqinsertusert.="iddependencia=?, ";
        $slqinsertusert.="usuactivo=?, ";
        $slqinsertusert.="idpermiso=? ";
        $slqinsertusert.="WHERE documentoid=? ";
        
        
        /**
         * Edit password
         */
        $slqinsertusertp="UPDATE usuarios ";
        $slqinsertusertp.="SET ";
        $slqinsertusertp.="password=?, ";
        $slqinsertusertp.="salt=? ";
        $slqinsertusertp.="WHERE documentoid=? ";
        /* Deshabilitar autocommit */

        try { 
            $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conexion->beginTransaction();
            $gsent = $conexion->prepare($slqinsertusert);
            $gsent->bindParam(1, $_POST['NameUser'], PDO::PARAM_STR);
            $gsent->bindParam(2, $_POST['LastNameUser'], PDO::PARAM_STR);
            $gsent->bindParam(3, $_POST['IdentityCard'], PDO::PARAM_STR);
            $gsent->bindParam(4, $_POST['EmailUser'], PDO::PARAM_STR);
            $gsent->bindParam(5, $_POST['IdentityCard'], PDO::PARAM_STR);
            $gsent->bindParam(6, $hoy_dia_reg);
            $gsent->bindParam(7, $_POST['OccupationUser'], PDO::PARAM_STR);
            $gsent->bindParam(8, $_POST['GradeUser'], PDO::PARAM_STR);
            $gsent->bindParam(9, $_POST['PositionUser'], PDO::PARAM_STR);
            $gsent->bindParam(10, $ContractDateUser);
            $gsent->bindParam(11, $_POST['TypeContractUser'], PDO::PARAM_STR);
            $gsent->bindParam(12, $_POST['OfficeUser'], PDO::PARAM_STR);
            $gsent->bindParam(13, $_POST['StatusUser'], PDO::PARAM_STR);
            $gsent->bindParam(14, $_POST['TypeLevelUser'], PDO::PARAM_STR);
            $gsent->bindParam(15, $_POST['IdentityCard'], PDO::PARAM_STR);
            $gsent->execute();
            
            $jsondata = array();
          
                $jsondata = array(
                        'success' => true,
                        'message' => sprintf("Se actualizó con éxito el nuevo usuario."),
                        'total'=> $gsent->rowCount(),
                        'complete'=>true
                ); 
            $gsentp=""; 
            if($password!=""){
                $gsentp = $conexion->prepare($slqinsertusertp);
                $gsentp->bindParam(1, $password, PDO::PARAM_STR);
                $gsentp->bindParam(2, $random_salt, PDO::PARAM_STR);
                $gsentp->bindParam(3, $_POST['IdentityCard'], PDO::PARAM_STR);
                $gsentp->execute();
            }
            
            
            /**
             * Delete Node Module User
             */
            
            $IdUser=$_POST['IdSerialNumber'];
            $slqdelete_module_user="DELETE FROM node_module_user WHERE id_user=:documentoid ";
            $delete_module_user = $conexion->prepare($slqdelete_module_user);
            $delete_module_user->bindParam(':documentoid', $IdUser, PDO::PARAM_INT);
            $delete_module_user->execute();
             $Node_Permission = $_POST['SecurityLevelUser'];
             if($Node_Permission!=""){
            /**
             * Insert Node Module User
             */
            
             $Array_Node_Permission = explode(",", $Node_Permission);
             for($i=0;$i<count($Array_Node_Permission);$i++){
                 
                 $slqinsertnode_module_user="INSERT INTO node_module_user ";
                 $slqinsertnode_module_user.="(";
                 $slqinsertnode_module_user.="id_user , ";
                 $slqinsertnode_module_user.="id_node_module ";
                 $slqinsertnode_module_user.=") ";
                 $slqinsertnode_module_user.="VALUES (?,?) ";
                 $gsentn = $conexion->prepare($slqinsertnode_module_user);

                 $gsentn->bindParam(1, $IdUser, PDO::PARAM_INT);
                 $gsentn->bindParam(2, $Array_Node_Permission[$i]);
                 $gsentn->execute();


             }
             
             }
            
            $conexion->commit();
            if($gsentp!=""){
                $gsentp->closeCursor();
                $gsentp=null;
            }
            $gsent->closeCursor();
            $gsent=null;
             if($Node_Permission!=""){
                $gsentn->closeCursor();
                $gsentn=null;
             }
            $delete_module_user->closeCursor();
            $delete_module_user=null;
            $conexion=null;
            

            echo json_encode($jsondata);


        } catch (PDOException $e) {
            $jsondata = array();
            $conexion->rollBack();
            $conexion=null;
            $jsondata = array(
                        'success' => true,
                        'message' => sprintf("No se actualizo el usuario. Por favor informe al administrador."),
                        'infoerror'=>$e->getMessage(),
                        'total'=> "0",
                        'complete'=>false
            ); 
            echo json_encode($jsondata);

        } 
        
    }
 
  
}else{
     header("location: ../403.php");
}
?>

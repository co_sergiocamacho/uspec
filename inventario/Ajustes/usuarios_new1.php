<?php
session_start();
//DESCRIPCION: CREAR  USUARIOS *VENTANA DE ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS

if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/encabezado.php");

	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="../js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />


		<style>	
			<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
			body {
				background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Nuevo Usuario</title>



		<?php 
//include('menu.php');

		?>
	</head>
	<script src="../js/calendario/src/js/jscal2.js"></script>
	<script src="../js/calendario/src/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

	<body>

		<div id="centro2">
			<table class="botonesfila" >
				<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
					<td><a href="usuarios_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td></tr>
				</table>




			</div>

			<div id="centro">
				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   





				<center>
					<table width="60%" class="tabla_2" border="0" style="width:580px">
						<tr>
							<td class="fila1">NUEVO USUARIO</td>
						</tr>
						<tr>
							<td class="fila2"> Permite el ingreso un nuevo USUARIO al sistema.</td>
						</tr>
					</table>

					<br>
					<form name="USUARIOS" action="usuario_guardar.php" title="Nuevo producto" method="post">
						<table width="60%" class="tabla_2" border="0" style="width:580px">
							<tr>
								<td colspan="5" class="fila1">&nbsp;</td>
							</tr>
							<tr>
								<td class="fila2">NOMBRES:</td>
								<td class="fila2"><input type="text" class="textinput"  name="nomusu" size="30" maxlength="80"  onChange="MAY(this)" value=""  required/></td>
								<td class="fila2">APELLIDOS:</td>
								<td class="fila2"><input type="text" class="textinput"  name="apeusu" size="40" maxlength="80" onChange="MAY(this)" value="" required/></td>
							</tr>
							<tr>
								<td class="fila2">USUARIO:</td>
								<td class="fila2"><input type="text" class="textinput"  name="usuario" size="30" maxlength="80" value="" required  /></td>
								<td class="fila2">PASSWORD:</td>
								<td class="fila2"><input type="password" name="password" size="40" maxlength="80" value="" required /></td>
							</tr>
							<tr>
								<td class="fila2">EMAIL</td>
								<td class="fila2"><input type="text" class="textinput"  name="emailusu" size="30" maxlength="80" value=""   required/></td>
								<td class="fila2">DIRECCION:</td>
								<td class="fila2"><input type="text" class="textinput"  name="dirusu" size="40" maxlength="80" onChange="MAY(this)" value="" /></td>
							</tr>
							<tr>
								<td class="fila2">TELEFONO:</td>
								<td class="fila2"><input type="text" class="textinput"  name="telusu" size="20" maxlength="60" onChange="MAY(this)" value="" required/></td>
								<td class="fila2">FECHA NACIMIENTO:</td>
								<td class="fila5a"><input size="10" id="f_date1" name="f_date1" value="" /><button id="f_btn1" class="botonmas">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_date1",
		trigger    : "f_btn1",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"
	});
	//]]></script>
</tr>
<tr>
	<td class="fila2">DOCUMENTO:</td>
	<td class="fila2"><input type="text" class="textinput"  name="docusu" size="30" maxlength="80" onChange="MAY(this)" value="" required /></td>
	<td class="fila2">PROFESION:</td>
	<td class="fila2"><input type="text" class="textinput"  name="profesion" size="30" maxlength="80" onChange="MAY(this)" value="" required /></td>
	<tr>
		<td class="fila2">GRADO:</td>
		<td class="fila2" colspan="1"><input type="text" class="textinput"  name="grado" size="30" maxlength="80" onChange="MAY(this)" value="" required /></td>


		<td class="fila2">CARGO:</td>
		<td class="fila2"><input type="text" class="textinput"  name="carusu" size="30" maxlength="80"  onChange="MAY(this)" value=""  /></td>




	</tr>
	<tr>
		<td class="fila2">SEXO:</td>
		<td class="fila2"><select name="sexo">
			<option value="">............</option>
			<?php
			$sexos=mysql_query("SELECT idsexo,sexo FROM sexo WHERE (activo = '1') ORDER BY sexo");
			while($filsex=mysql_fetch_array($sexos))
			{
				?>
				<option value="<?php echo $filsex['idsexo'];?>"><?php echo $filsex['sexo'];?></option>
				<?php
			}
			?>
		</select></td>

		<td class="fila2">FECHA FIN CONTRATO:</td>
		<td class="fila2" colspan="3"><input size="10" id="f_fin" name="f_fin" value="" /><button id="f_btn2">+</button></td>
<script type="text/javascript">//<![CDATA[
	Calendar.setup({
		inputField : "f_fin",
		trigger    : "f_btn2",
		onSelect   : function() { this.hide() },
		showTime   : 12,
		dateFormat : "%Y-%m-%d"	
	});
	//]]></script></td>


</tr>
<tr>
	<td class="fila2">CALIDAD DEL EMPLEADOR:</td>
	<td class="fila2"><select name="calidadempleado">
		<option value="">............</option>
		<?php
		$calidad=mysql_query("SELECT idcalidadempleador, calidadempleador FROM calidadempleador WHERE (activo = '1') ORDER BY idcalidadempleador");
		while($filcalidad=mysql_fetch_array($calidad))
		{
			?>
			<option value="<?php echo $filcalidad['idcalidadempleador'];?>"><?php echo $filcalidad['calidadempleador'];?></option>
			<?php
		}
		?>
	</select></td>

	<td class="fila2">PERMISO:</td>
	<td class="fila2"><select name="permiso">
		<option value="">............</option>
		<?php
		$permisos=mysql_query("SELECT idpermiso,permiso FROM permisos WHERE (activo = '1') ORDER BY permiso");
		while($filper=mysql_fetch_array($permisos))
		{
			?>
			<option  value="<?php echo $filper['idpermiso'];?>"><?php echo $filper['permiso'];?></option>
			<?php
		}
		?>
	</select></td>


</tr>

<tr>
	<td class="fila2">DEPENDENCIA:</td>
	<td class="fila2" colspan="3"><select name="dependencia">
		<option value="">............</option>
		<?php
		$dependencia=mysql_query("SELECT iddependencia, codigodependencia, nombredependencia FROM dependencias WHERE (depactivo = '1') ORDER BY codigodependencia");
		while($filadependencia=mysql_fetch_array($dependencia))
		{
			?>
			<option value="<?cophp echo $filadependencia['codigodependencia'];?>"><?php echo $filadependencia['codigodependencia']."  ".$filadependencia['nombredependencia'];?></option>
			<?php
		}
		?>
	</select></td></tr>


	<tr>
		<td class="fila2">OBSERVACIONES:</td>
		<td colspan="3" class="fila2"><input type="text" class="textinput"  name="obsusu" size="63" maxlength="300"  onChange="MAY(this)" /></td>
	</tr>
	<tr>
		<td class="fila2">&nbsp;</td>
		<td colspan="1" class="fila2"><p align="right"> ACTIVO:</p></td>
		<td colspan="2" class="fila2"><input type="radio" name="activo" value="1" class="check" checked="checked"/>
			SI
			<input type="radio" name="activo" value="0" class="check" />
			NO </td>
		</tr>
		<tr>
			<td class="fila2"><input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
			<td class="fila2"><p align="center">
				<input type="submit" class="botonguardar" name="guardar_producto" title="Guardar producto" value="GUARDAR"/>
			</p></td>
			<td class="fila2">&nbsp;</td>
			<td class="fila2"><p align="center">

				<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="CANCELAR"/></a>	
			</td>
		</tr>
	</table>
</form>
</center>
</div>
</body>
</html>
<?php

include ("../assets/footer.php");
?>
<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>


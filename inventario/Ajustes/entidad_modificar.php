<?php
//DESCRIPCION:  MODIFICAR ENTIDADES POR PARTE DEL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
		<style>	



			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Entidad Modificar</title>



		<!--FIN IDENTIFICACION USUARIO LOGEADO -->
	</head>
	<body>
		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td><a href="entidades_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>


				<div id="centro">

					<div id="div_bienvenido">
						<?php echo "Bienvenido"; ?> <BR/>
						<div id="div_usuarios">
							<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
						</div>
						<?php echo "SALIR";?>
						'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
					</div>   






					<center>
						<table width="60%" class="tabla_2" border="0" style="width:580px">
							<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24
							$rst_entidades=mysql_query ("SELECT *
								FROM entidades
								WHERE identidad=". $_REQUEST["id_entidad"].";",$conexion);
//echo $rst_clientes;
							while($fil_entidad=mysql_fetch_array($rst_entidades))
							{

								?>

								<tr>
									<td class="fila1">MODIFICAR ENTIDADES</td>
								</tr>
								<tr>
									<td class="fila2">Realizar la modificacion de la entidad seleccionada ...</td>
								</tr>
							</table>

							<br>
							<form name="clientes" action="entidad_update.php?id_entidad= <?php echo $_REQUEST["id_entidad"];?>" title="Nuevo Usuario" method="post">
								<table width="60%" class="tabla_2" border="0" style="width:580px">
									<tr>
										<td colspan="5" class="fila1">DATOS DE LA ENTIDAD</td>
									</tr>
									<tr>
										<td class="fila2">ENTIDAD</td>
										<td class="fila2"><input type="text" class="textinput"  name="nombreenti" size="110" maxlength="80"  onChange="MAY(this)" value="<?php echo $fil_entidad["entidad"] ?>"  /></td>
										<td class="fila2">NIT</td>
										<td class="fila2"><input type="text" class="textinput"  name="nitenti" size="30" maxlength="80"  onChange="MAY(this)" value="<?php echo $fil_entidad["nitentidad"] ?>"  /></td>



										<tr>
											<td class="fila2">DIRECCION:</td>
											<td class="fila2" colspan="3"><input type="text" class="textinput"  name="direnti" size="160" maxlength="80" onChange="MAY(this)" value="<?php echo $fil_entidad["direccion_entidad"] ?>" /></td>

										</tr>
										<tr>
											<td class="fila2">EMAIL</td>
											<td class="fila2"><input type="text" class="textinput"  name="correoenti" size="50" maxlength="80" value="<?php echo $fil_entidad["correo_entidad"] ?>"  /></td>
											<td class="fila2">TELEFONO:</td>
											<td class="fila2"><input type="text" class="textinput"  name="telenti" size="20" maxlength="60" onChange="MAY(this)" value="<?php echo $fil_entidad["telefono_entidad"] ?>" /></td>

										</tr>
										<tr>

											<?php
										}
										?></td>
									</tr>
									<tr>
										<td class="fila2"><input type="hidden" name="identidad" value="<?php echo "Bienvenido $_SESSION[identidad]"; ?>" />
											<input type="hidden" name="identidad" value="<?php echo "ID entidad modificar $_REQUEST[id_entidad]"; ?>" /></td>
											<td class="fila2"><p align="center">
												<input type="submit" class="botonguardar" name="guardar_usuario" title="Guardar usuario" value="GUARDAR"/>
											</p></td>
											<td class="fila2" colspan="2"> 

												<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar usuario" value="CANCELAR"/></td>
											</a> </td>
										</tr>
									</table>
								</form></center>

								<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
								include ("../assets/footer.php");
								?>
							</body>
							</html>
							<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

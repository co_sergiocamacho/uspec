<?php
class Validate_User{
    
  private $KeyWord;
  private $Connection;
  private $Start;
  private $Length;
  private $Draw;
  
  public function __construct($KeyWord,$conn,$start,$length,$draw)
  {
       $this->Connection = $conn;
       $this->KeyWord=$KeyWord;
       $this->Start=$start;
       $this->Length=$length;
       $this->Draw=$draw;
  }
  public function SearchListUser(){ 
    
    $sql="SELECT user.idusuario, user.nombres, user.apellidos, user.usuario, user.documentoid, user.idpermiso, permit.permiso, user.usuactivo, ";
    $sql.="user.email, user.idprofesion, user.grado, user.calidadempleado, user.fechafincontrato, user.cargo, ";
    $sql.="user.idpermiso, user.calidadempleado, user.iddependencia ";
    $sql.="FROM usuarios AS user ";
    $sql.="LEFT JOIN permisos AS permit ON user.idpermiso=permit.idpermiso ";
    if($this->KeyWord!=""){
        $sql.="WHERE (user.documentoid LIKE :documentoid OR user.nombres LIKE :nombres OR user.apellidos LIKE :apellidos)  ";
    }
    $sql.="ORDER BY user.nombres , user.apellidos ASC ";
    $sql_limit="LIMIT $this->Length OFFSET ".$this->Start;
    $stmt_total = $this->Connection->prepare($sql);
    $stmt = $this->Connection->prepare($sql.$sql_limit);
    if($this->KeyWord!=""){
        $KeyWord = $this->KeyWord.'%';
        
        $stmt_total->bindParam(':documentoid', $KeyWord, PDO::PARAM_STR);
        $stmt->bindParam(':documentoid', $KeyWord, PDO::PARAM_STR);
        
        $stmt_total->bindParam(':nombres', $KeyWord, PDO::PARAM_STR);
        $stmt->bindParam(':nombres', $KeyWord, PDO::PARAM_STR);
        
        $stmt_total->bindParam(':apellidos', $KeyWord, PDO::PARAM_STR);
        $stmt->bindParam(':apellidos', $KeyWord, PDO::PARAM_STR);
    }
    $stmt_total->execute();
    $total_datos = $stmt_total->rowCount();
    $stmt->execute();
    $total_limit = $stmt->rowCount();
  
    //$result = $stmt->fetchAll();
    //$obj->filmName;
    $jsondata = array();
        if($total_datos>0){
            $jsondata = array(
                    'success' => true,
                    'message' => sprintf("Se han encontrado %d usuarios", $total_datos),
                    'draw'=> $this->Draw,
                    'recordsTotal'=> $total_limit,
                    'recordsFiltered'=> $total_datos,
                    'data'=>array()
            );
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
                  $documento_user = (empty($row->documentoid) || is_null($row->documentoid)) ? "--" : $row->documentoid;
                  $userstatus="";
                  if($row->usuactivo=="1"){
                     $userstatus="Activo"; 
                  }
                  if($row->usuactivo=="0"){
                      $userstatus="Inactivo";
                  }
                  $jsondata['data'][]= array(
                    'idusuario' => $row->idusuario,
                    'documentoid' => $documento_user,
                    'permiso' => $row->permiso,
                    'nombres' => htmlentities($row->nombres),
                    'apellidos' => htmlentities($row->apellidos),
                    'usuario' => htmlentities($row->usuario),
                    'email' => $row->email,
                    'idprofesion' => htmlentities($row->idprofesion),
                    'grado' => htmlentities($row->grado),
                    'cargo' => htmlentities($row->cargo),
                    'idpermiso' => $row->idpermiso,
                    'fechafincontrato'=> $row->fechafincontrato,
                    'calidadempleado' => $row->calidadempleado,
                    'iddependencia' => $row->iddependencia,
                    'codeusuactivo' => $row->usuactivo,
                    'usuactivo' => $userstatus
                );
           
            }
          
        }else {
            $jsondata = array(
                    'success' => true,
                    'message' => 'No se encontró ningún resultado.',
                    'draw'=> $this->Draw,
                    'recordsTotal'=> $total_limit,
                    'recordsFiltered'=> $total_datos,
                     'data'=>array()
            );
        }
        return json_encode($jsondata);
        $stmt = null;
        $stmt_total = null;
  }
}

?>

                            


<?php
session_start();


//DESCRIPCION: CREACION DE NUEVA CATEGORIA CONTABLE
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

	include("../database/conexion.php");
	include("../assets/encabezado.php");

	include("../assets/global.php");
	?>

	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<script src="js/calendario/src/js/lang/en.js"></script>

		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
		<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
		<script language="javascript">

			var cursor;
			if (document.all) {
				// Está utilizando EXPLORER
				cursor='hand';
				} else {
				// Está utilizando MOZILLA/NETSCAPE
				cursor='pointer';
				}

				var miPopup
				function nueva_categoria(){
					miPopup = window.open("categoria_new.php","miwin","width=900,height=380,scrollbars=yes");
					miPopup.focus();
				}
				</script>

<style>	
	<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
	body {
		background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		background-size: cover;
	}
	.container > header h1,
	.container > header h2 {
		color: #fff;
		text-shadow: 0 1px 1px rgba(0,0,0,0.7);
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Unidad</title>




<?php 
//include('menu.php');

?>
</head>
<script src="js/calendario/src/js/jscal2.js"></script>
<script src="js/calendario/src/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

<body>

	<div id="centro2"><table class="botonesfila" >
		<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
			<td><a href="categorias_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>







			<div id="centro">


				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   


				<center>
					<table width="40%" class="tabla_2" border="0" style="width:680px">
						<tr>
							<td colspan="3" class="fila1">NUEVA CATEGORIA</td>
						</tr>
						<form name="Unidades" action="categoria_guardar.php" title="Guardar Unidad" method="post">


							<tr>
								<td class="fila2" COLSPAN="2" ALIGN="CENTER">CATEGORIA</td>    
								<td class="fila2"><input type="text" class="textinput"  name="codigocontable" size="30" maxlength="80"  onkeypress="return SoloNumeros(event)" value=""  required=""/></td>
							</tr>

							<tr>
								<td class="fila2" COLSPAN="2" ALIGN="CENTER">DESCRIPCIÓN </td>    
								<td class="fila2"><input type="text" class="textinput"  name="codigodescripcion" size="80"    value=""  required=""/></td>
							</tr>

							<tr>
								<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
								<td class="fila2" colspan="3"><p align="center">
									<input align="center" type="submit" class="botonguardar" name="guardar_categoria" title="Guardar Unidad" value="GUARDAR"/>
								</p></td>



							</td>

							<tr>

								<tr>
								</table>

							</body>
							</html>
							<?php

							include ("../assets/footer.php");
							?>

							<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

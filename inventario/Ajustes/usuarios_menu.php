<?php
//DESCRIPCION: MENU DE USUARIOS PARA EL ADMINISTRADOR
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
if (isset($_SESSION['idpermiso'])) {
	include("../database/conexion.php");
	include("../assets/encabezado.php");
?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Gesti&#243;n de Usuarios</title>

<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="../bootstrap/css/dataTables.bootstrap.min.css" />
<link rel="stylesheet" href="../css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="../js/jquery-ui.css" />
<script src="../js/jquery-3.1.1.min.js"></script>
<script src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
<script src="../bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<link href="../css/estilos.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/LoadScreen.css"/>
<script type="text/javascript" src="../js/usuarios_menu.js"></script>
<link href="../css/modal.css" type="text/css" rel="stylesheet">
<link href="../css/table.css" type="text/css" rel="stylesheet">
<link href="../css/switch.css" type="text/css" rel="stylesheet">
</head>
<body>

 <!-- Start Modal -->
  <div class="modal fade" id="ModalUser" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" id="FormValidUser">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4>Validar Usuario</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="usuarios_new.php" method="post" id="Create_User">
            <div class="form-group">
              <label for="usrname"> Identificaci&#243;n del usuario</label>
              <input type="text" class="form-control" id="idusuario" name="idusuario" placeholder="Identificaci&#243;n del usuario.">
            </div>
            <div class="alert alert-warning" role="alert" id="Message_AlertSearchUser"></div>
          </form>
        </div>
         <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            <button type="button" id="button_validate" class="btn btn-primary">Consultar</button>
          </div>
      </div>
      
      <!-- Modal content-->
      <div class="modal-content" id="FormCreateUser">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4>Crear Usuario</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
            <div id="Load_Form_Create_User"></div>
          
        </div>
         <div class="modal-footer">
             <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
          </div>
      </div>
      <!-- Modal content-->
      <div class="modal-content" id="FormEditUser">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4>Editar Usuario</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
            <div id="Load_Form_Edit_User"></div>
          
        </div>
         <div class="modal-footer">
             <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
          </div>
      </div>
    </div>
  </div>
  <!-- End Modal content-->
 <div id="centro2">
   <table class="botonesfila" >
      <tr>
         <td><a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a>
         </td>
         <td><a href="ajustes_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td>
      </tr>
   </table>
</div>
<div id="centro">
   <div id="div_bienvenido">
      <?php echo "Bienvenido"; ?> <BR/>
      <div id="div_usuarios">
         <?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
      </div>
      <?php echo "SALIR";?>
      '<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
   </div>
   <center>
      <table width="50%" border="0">
         <tr>
            <td class="titulo">
               <center>USUARIOS</center>
            </td>
         </tr>
       </table>
        <input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
   </center>
   <div class="container">
    <div class="row">
    
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                      <h3 class="panel-title">
                          
                      </h3>
                  </div>
                  <div class="col col-xs-6 text-right"> 
                    <button type="button" id="button_ModalCreateUser" class="btn btn-sm btn-primary btn-create"><em class="glyphicon glyphicon-book"></em>  Nuevo Usuario</button>
                    <button type="button" id="button_ModalEditUser" class="btn btn-sm btn-primary btn-create"><em class="glyphicon glyphicon-pencil"></em>  Editar Usuario</button>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table id="ListUserSearch" class="table table-striped table-bordered">
                   <thead>
                      <tr>
                         <th class="hidden-xs">Documento</th>
                         <th>Nombre</th>
                         <th>Apellido</th>
                         <th>Usuario</th>
                         <th>Rol</th>
                         <th>Estado</th>
                      </tr>
                   </thead>
                   <tfoot>
                        <tr>
                            <th class="hidden-xs">Documento</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Usuario</th>
                            <th>Rol</th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>
              </div>
            </div>

</div></div></div>
</div>
<?php
      include ("../assets/footer.php");
?> 
</body>
</html>
<?php
}else{
   	header("location: ../403.php");
}
?>

<?php

//DESCRIPCION:  AGREGAR DEPENDENCIAS EN BASE DE DATOS
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

include("../database/conexion.php");
include("../assets/encabezado.php");

include("../assets/global.php");
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
	<link href="../css/styles.css" type="text/css" rel="stylesheet">
	<link href="../css/estilos.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="../imagenes/1.ico">
	<script src="js/calendario/src/js/lang/en.js"></script>

	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />
	<script language="javascript">

		var cursor;
		if (document.all) {
// Está utilizando EXPLORER
cursor='hand';
} else {
// Está utilizando MOZILLA/NETSCAPE
cursor='pointer';
}

var miPopup
function nueva_categoria(){
	miPopup = window.open("categoria_new.php","miwin","width=900,height=380,scrollbars=yes");
	miPopup.focus();
}
</script>

<style>	
	<!--@import url(http://fonts.googleapis.com/css?family=Raleway:400,700);
	body {
		background: #eaeaea url(images/fondo_2.jpg) no-repeat center top;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		background-size: cover;
	}
	.container > header h1,
	.container > header h2 {
		color: #fff;
		text-shadow: 0 1px 1px rgba(0,0,0,0.7);
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nueva Dependencia</title>


<div id="div_bienvenido">
	<?php echo "Bienvenido"; ?> <BR/>
	<div id="div_usuarios">
		<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
	</div>
	<?php echo "SALIR";?>
	'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
</div>   
<BR />
<?php 
//include('menu.php');

?>
</head>
<script src="js/calendario/src/js/jscal2.js"></script>
<script src="js/calendario/src/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../js/calendario/src/css/steel/steel.css" />

<body>
	<BR />
	<BR />
	<div id="centro">
		<table>
			<tr>
				<td>
					<a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="48" height="48" name="regresar" title="Inicio" value="Regresar" class="botonf" />INICIO</a>
				</td>
				<td>

					<a href="unidades_menu.php"><input type="image" src="../imagenes/atras.png" width="48" height="48" name="regresar" title="Inicio" value="Regresar" class="botonf" />ATRAS</a>
				</td>
			</tr>
		</table>
		<center>
			<table width="60%" class="tabla_2" border="0" style="width:580px">
				<tr>
					<td class="fila1">NUEVA DEPENDENCIA</td>
				</tr>

			</table>

			<form name="Unidades" action="dependencia_guardar.php" title="Guardar Unidad" method="post">
				<table width="50%" class="tabla_2" border="0" style="width:580px">
					<tr>
					</tr>
					<tr>
						<td class="fila2">CODIGO DEPENDENCIA</td>  
						<td class="fila2"><input type="text" class="textinput"  name="codigodependencia" size="30" maxlength="80"  onChange="MAY(this)" value=""  required/></td>

						<td class="fila2">SIGLA DEPENDENCIA</td>    
						<td class="fila2"><input type="text" class="textinput"  name="sigladependencia" size="30" maxlength="80"  onChange="MAY(this)" value=""  required/></td>
					</tr>
					<TR>

						<td class="fila2">NOMBRE DEPENDENCIA</td>  
						<td class="fila2"  colspan="3"><input type="text" class="textinput"  name="nombredependencia" size="90" maxlength="80"  onChange="MAY(this)" value=""  required/></td>
					</TR>
					<tr>

						<td class="fila2"><input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> </td>
						<td class="fila2"><p align="center">
							<input type="submit" class="botonguardar" name="guardar_unidad" title="Guardar Unidad" value="GUARDAR"/>
						</p></td>
						<td class="fila2">&nbsp;</td>
						<td class="fila2"><p align="center">

							<input type="submit" class="botoncancelar" name="cancelar" title="Cancelar Unidad" value="cancelar"/></a>  
						</td>

						<tr>

							<tr>
							</table>

						</body>
						</html>
						<?php
						include ("../assets/footer.php");
						?>
<?php
/*
@Cerrar Sesion
*/
} else {
header("location: ../403.php");
}
?>

<?php
//DESCRIPCION: VENTANA CON LISTA DE LOS ULTIMOS RESPONSABLES DE BODEGA PARA EL ADMINISTRADOR
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
//Verificación de sesion
if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
	include("../database/conexion.php");
	include("../assets/encabezado.php");
//INCLUYO LA HOJA DE ESTILOS
	?>


	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="../css/paginacion.css" type="text/css" rel="stylesheet">
		<link href="../css/styles.css" type="text/css" rel="stylesheet">
		<link href="../css/estilos.css" type="text/css" rel="stylesheet">
		<link rel="shortcut icon" href="../imagenes/1.ico">
		<style>	

			body {
				background: #eaeaea no-repeat center top;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
			}
			.container > header h1,
			.container > header h2 {
				color: #fff;
				text-shadow: 0 1px 1px rgba(0,0,0,0.7);
			}

		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Usuarios</title>
		<link href="estilos/estilos.css" rel="stylesheet" type="text/css" />



	</head>

	<body>


		<div id="centro2"><table class="botonesfila" >
			<tr><td>  <a href="ajustes_menu.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
				<td>  <a href="responsable_bodega.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Atras" value="Regresar">ATRAS</a></td>
			</tr></table></div>




			<div id="centro">


				<div id="div_bienvenido">
					<?php echo "Bienvenido"; ?> <BR/>
					<div id="div_usuarios">
						<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
					</div>
					<?php echo "SALIR";?>
					'<a href="../index.php?exit=1"><img src="../imagenes/apagar.png" title="Salir" width="18" height="18" /></a>
				</div>   


				<center>
					<table width="50%" border="0">
						<tr>
							<td class="titulo"><center>Tabla Responsable de Bodega</td></center>

						</tr>
					</center>
				</table>
				<?php
				if (isset($_GET['add'])){ if ($_GET['add']==1){?>
				<div class="quitarok">
					<img src="../imagenes/ok.png" title="Salir" width="24" height="24" align="center"/>  Usuario creado correctamente!
				</div>

				<?php   }    } ?> 
<!--Andres Montealegre Giraldo
	IDENFICIACION DEL ID DEL USUARIO EN EL SISTEMA MEDIANTE UN hiddEN-->
	<input type="hidden" name="idcliente" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" /> 
	<?php
	if ($_SESSION['idpermiso']==1){
		?>


		<?php  }	  ?>
	</center>
	<center>
		

	</center>

	<center>

		<table border="0" class="tabla_2" style="width:900"><center>
			<tr>
				<td  class="fila1">ID</td>
				<td  class="fila1">NOMBRES</td>
				<td class="fila1">APELLIDOS</td>
				<td  class="fila1">FECHA INICIO </td>
				<td  class="fila1">FECHA FIN</td>
				<td  class="fila1">COMENTARIO</td>
				
				
			</tr>
			<?php

//CONSULTA PARA UBICAR EL PAGINADOR
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09	 
			$t_usuarios=mysql_query("SELECT * FROM responsable_bodega  ORDER BY fecha_desde DESC", $conexion);
//echo $t_clientes;

			?>	

			<?php
			while ($fila_usuarios=mysql_fetch_array($t_usuarios))
			{

				?>

				<tr>
					<td class="fila2"><?php echo $fila_usuarios["id_responsable"];?></td>
					<td class="fila2"><?php echo $fila_usuarios["nombres"];?></td>
					<td class="fila2"><?php echo $fila_usuarios["apellidos"];?></td>
					<td class="fila2" align="center"><?php echo $fila_usuarios["fecha_desde"];?></td>
					<td class="fila2" align="center">

						<?php 
						$hasta=$fila_usuarios["fecha_hasta"];

						if($hasta=='0000-00-00'){
							echo "Fecha Actual"	;

						} else{

							echo $hasta; }?>
							



						</td>
						<td class="fila2"><?php echo $fila_usuarios["resp_comentario"];?></td>

						<?php 				} 					?>
						
					</table>
				</br>
			</center>
		</div>
	</body>
	<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
	include ("../assets/footer.php");
	?>

	<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>


</html>
<?php
   session_start();
   //DESCRIPCION: Validar usuarios
   //NOMBRE: Fredy Duvan Velasquez Naranjo
   //Unidad de Servicios Penitenciarios y Carcelarios
   //SOLUCIONES DE PRODUCTIVIDAD SAS
   if (isset($_SESSION['idpermiso'])){
?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>Validar Usuario</title>
<link rel="stylesheet" type="text/css" href="../css/LoadScreen.css"/>
<link rel="stylesheet" href="../css/bootstrap_2.min.css">
<script src="../js/jquery-3.1.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<style>
  .modal-header, h4, .close {
      background-color: #5cb85c;
      color:white !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-footer {
      background-color: #f9f9f9;
  }
  </style>
<script>
        $(document).ready(function () {
                /**
                 * Hide LoadScreen
                 * @returns {Boolean}
                 */
                function HideLoadScreen(){
                    $("#loading").hide();
                }
				$('#myModal').modal('show');
                
                HideLoadScreen();
               
                $("#button_validate").click(function(){ 
                    var  RegExPattern = /^[0-9a-zA-Z]+$/;
                    var id_usuario = $("#idusuario").val();
                    if ((RegExPattern.test(id_usuario)) && (id_usuario.value!='')){
 
                        $.ajax({
                            beforeSend: function(){
                                $("#loading").show();
                            },
                            complete: function(jqXHR, estado){
                                $("#loading").hide();
                            },
                            data: {"iduser" : id_usuario},
                            type: "POST",
                            dataType: "json",
                            url: "service_valide_User.php",
                            success:  function (data) {
                                
                                var code = data[0].code;
                                var message =data[0].message;
                                if(code=="1"){
                                    alert(message);
                                }
                                if(code=="2"){
                                    $( "#Create_User" ).submit();
                                }
                            }
                        });
                    }else{
                        alert("Por favor ingrese el n\u00FAmero de identificaci\u00F3n del usuario.");
                        return false;
                    }
                });
        });
    </script>
</head>
<body>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4>Validar Usuario</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="usuarios_new.php" method="post" id="Create_User">
            <div class="form-group">
              <label for="usrname"> Identificaci&#243;n del usuario</label>
              <input type="text" class="form-control" id="idusuario" name="idusuario" placeholder="Identificaci&#243;n del usuario.">
            </div>
          </form>
        </div>
         <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button type="button" id="button_validate" class="btn btn-primary">Consultar</button>
  </div>
      </div>
      
    </div>
  </div> 
</body>
</html>
<?php
} else {
   	header("location: ../403.php");
}
?>

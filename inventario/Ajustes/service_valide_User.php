<?php
require("../database/conexion_pdo.php");
class Validate_User{
    
  private $ID_USER;
  private $Connection;
  public function __construct($value,$conn)
  {
        $this->ID_USER = $value;
        $this->Connection = $conn;
  }
  public function SearchUser(){     
    $sql="SELECT documentoid FROM usuarios WHERE documentoid=?";
    $gsent = $this->Connection->prepare($sql);
    $gsent->bindParam(1,$this->ID_USER,PDO::PARAM_STR, 20);
    $gsent->execute();
    $total = $gsent->rowCount();
    $obj = $gsent->fetchObject();
    //$obj->filmName;
        if($total>0){
           $jsonarray = array(
                array('success' => true,'code' => "1",'message' => 'Este usuario ya esta registrado en el sistema.')
              );
        }else{
            $jsonarray = array(
                array('success' => true,'code' => "2",'message' => 'Este usuario no esta registrado en el sistema. Por favor registrelo.'),
            );
        }
        echo json_encode($jsonarray);
  }
}
$Filter_ID_User =$_POST['iduser'];
$data = new Validate_User($Filter_ID_User,$conexion);
$resultado = $data->SearchUser();
echo $resultado;
exit();
?>
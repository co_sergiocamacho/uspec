<?php
//DESCRIPCION: MENU DE USUARIOS PARA EL ADMINISTRADOR
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS
session_start();
if (isset($_SESSION['idpermiso'])) {
    
    $IdentityCard = $_GET["IdentityCard"];
    $EditUser = $_GET["EditUser"];
    $IdSerialNumber = $_GET["IdSerialNumber"];
    require("../database/conexion_pdo.php");
    require("SearchListUser.php");
    if($EditUser=="1"){
        
       $start="0";
       $length="1";
       $draw="";
       $KeyWord_End=$IdentityCard;
       $data = new Validate_User($KeyWord_End,$conexion,$start,$length,$draw);
       $resultado = $data->SearchListUser(); 
       $data = json_decode($resultado);
       $data_end = $data->data;
       $NameUser = $data_end[0]->nombres;
       $LastNameUser = $data_end[0]->apellidos;
       $EmailUser = $data_end[0]->email;
       $OccupationUser = $data_end[0]->idprofesion;
       $GradeUser = $data_end[0]->grado;
       $PositionUser = $data_end[0]->cargo;
       $ContractDateUser = $data_end[0]->fechafincontrato;
       $TypeContractUser = $data_end[0]->calidadempleado;
       $OfficeUser = $data_end[0]->iddependencia;
       $SecurityLevelUser = $data_end[0]->idpermiso;
       $StatusUser = $data_end[0]->codeusuactivo;
   
 
    }else{
       $NameUser = "";
       $LastNameUser = "";
       $EmailUser = "";
       $OccupationUser = "";
       $GradeUser = "";
       $PositionUser = "";
       $ContractDateUser = "";
       $TypeContractUser = "";
       $OfficeUser = "";
       $SecurityLevelUser = "";
       $StatusUser = "";
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Gesti&#243;n de Usuarios</title>
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="../bootstrap/css/dataTables.bootstrap.min.css" />
<link rel="stylesheet" href="../js/jquery-ui.css" />
<script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery-validation-1.15.1/dist/jquery.validate.js"></script>
<script type="text/javascript" src="../js/ValidateFormCreateEditUser.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../jstree/dist/themes/default/style.min.css" />
<script type="text/javascript" src="../jstree/dist/jstree.min.js"></script>
<script type="text/javascript" src="../js/ConfigTreeCreateEditUser.js"></script>
<script>
 $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
     $( function() {
       $( "#ContractDateUser" ).datepicker({
         showWeek: true,
         firstDay: 1,
         changeMonth: true,
         changeYear: true,
         dateFormat: 'yy-mm-dd'
       });
  } );
  </script>
  <style>
    .error {
       color: red;
    }
  </style>
</head>
<body> 
     
<div id="LayerMessageSuccess" class="alert alert-success">
  <strong>Success!</strong> Indicates a successful or positive action.
</div>
<div id="LayerForm">
   <form action="#" id="Form_Create_Edit_User" method="post">
       <input type="hidden" id="IdUser" name="IdUser" value="<?php echo $IdentityCard;?>">
       <input type="hidden" id="EditUser" name="EditUser" value="<?php echo $EditUser;?>">
       <input type="hidden" id="IdSerialNumber" name="IdSerialNumber" value="<?php echo $IdSerialNumber;?>">
       
    <div class="form-group">
         <label>Documento de identidad</label>
         <input type="text" class="form-control"  placeholder="Documento de identidad" id="IdentityCard" name="IdentityCard" value="<?php echo $IdentityCard;?>" readonly>
     </div>
     <div class="form-group">
         <label>Contraseña</label>
         <input type="password" class="form-control" id="PasswordUser" name="PasswordUser">
     </div>
     <div class="form-group">
         <label>Nombres</label>
         <input type="text" class="form-control"  placeholder="Nombres" id="NameUser" name="NameUser" autofocus value="<?php echo $NameUser;?>">
     </div>
    <div class="form-group">
         <label>Apellidos</label>
         <input type="text" class="form-control"  placeholder="Apellidos" id="LastNameUser" name="LastNameUser" value="<?php echo $LastNameUser;?>">
     </div>
     <div class="form-group">
         <label for="inputEmail">Email</label>
         <input type="email" class="form-control"  placeholder="Email" id="EmailUser" name="EmailUser" value="<?php echo $EmailUser;?>">
     </div>
    <div class="form-group">
         <label>Profesi&#243;n</label>
         <input type="text" class="form-control"  placeholder="Profesi&#243;n" id="OccupationUser" name="OccupationUser" value="<?php echo $OccupationUser;?>">
     </div>
    <div class="form-group">
         <label>Grado</label>
         <input type="text" class="form-control"  placeholder="Grado" id="GradeUser" name="GradeUser" value="<?php echo $GradeUser;?>">
     </div>
    <div class="form-group">
         <label>Cargo</label>
         <input type="text" class="form-control"  placeholder="Cargo" id="PositionUser" name="PositionUser" value="<?php echo $PositionUser;?>">
     </div>
    <div class="form-group">
         <label>Fecha fin contrato</label>
         <input type="text" class="form-control" id="ContractDateUser" name="ContractDateUser" value="<?php echo $ContractDateUser;?>">
     </div>
    <div class="form-group">
         <label>Calidad del empleador</label>
         <select class="form-control" id="TypeContractUser" name="TypeContractUser">
            <option value="">Seleccione</option>
            <?php
           
               $sql_calidad="SELECT idcalidadempleador, calidadempleador ";
               $sql_calidad.="FROM calidadempleador ";            
               $sql_calidad.="WHERE ";     
               $sql_calidad.="activo = '1' ORDER BY idcalidadempleador";
               $stmt_calidad = $conexion->prepare($sql_calidad);
               $stmt_calidad->execute();
            ?>
            <?php
               while($filcalidad=$stmt_calidad->fetch(PDO::FETCH_OBJ))
               {
                    if($TypeContractUser!="" && $TypeContractUser==$filcalidad->idcalidadempleador){
            ?>
                    <option selected value="<?php echo $filcalidad->idcalidadempleador;?>"><?php echo $filcalidad->calidadempleador;?></option>
            <?php
                    }else{
            ?>
                    <option value="<?php echo $filcalidad->idcalidadempleador;?>"><?php echo $filcalidad->calidadempleador;?></option>
            <?php
                    }
               }
               $stmt_calidad=null;
            
            ?>
          </select>

     </div>
    <div class="form-group">
         <label>Dependencia</label>
         <select class="form-control" id="OfficeUser" name="OfficeUser">
            <option value="">Seleccione</option>
            <?php
            
               $sql_dependencia ="SELECT iddependencia, codigodependencia, nombredependencia ";
               $sql_dependencia.="FROM dependencias ";
               $sql_dependencia.="WHERE ";  
               $sql_dependencia.="depactivo = '1' ORDER BY codigodependencia";
               $stmt_dependencia = $conexion->prepare($sql_dependencia);
               $stmt_dependencia->execute();
               while($filadependencia=$stmt_dependencia->fetch(PDO::FETCH_OBJ))
               {
                   if($OfficeUser!="" && $OfficeUser==$filadependencia->codigodependencia){
           ?>
                   <option selected value="<?php echo $filadependencia->codigodependencia;?>"><?php echo $filadependencia->codigodependencia."  ".$filadependencia->nombredependencia;?></option>
            <?php
                   }else{
            ?>
                   <option value="<?php echo $filadependencia->codigodependencia;?>"><?php echo $filadependencia->codigodependencia."  ".$filadependencia->nombredependencia;?></option>
            <?php
                   }
               }
               $stmt_dependencia=null;
   
            ?>
          </select>

     </div>
    <div class="form-group">
         <label>Tipo de Usuario</label>
        <select class="form-control" id="TypeLevelUser" name="TypeLevelUser">
            <option value="">Seleccione</option>
            <?php
               $sql_permisos="SELECT idpermiso,permiso FROM permisos WHERE activo = '1' ORDER BY permiso";
               $stmt_permisos = $conexion->prepare($sql_permisos);
               $stmt_permisos->execute();
               while($filper=$stmt_permisos->fetch(PDO::FETCH_OBJ))
               {
                   if($SecurityLevelUser!="" && $SecurityLevelUser==$filper->idpermiso){
            ?>
                   <option  selected value="<?php echo $filper->idpermiso;?>"><?php echo $filper->permiso;?></option>
            <?php
                   }else{
            ?> 
                   <option value="<?php echo $filper->idpermiso;?>"><?php echo $filper->permiso;?></option>
            <?php
                   }
               }
               $stmt_permisos=null;
               ?>
        </select>
     </div>
    <div class="form-group">
        <label>Permiso Modulo</label>
        <input type="hidden" id="SecurityLevelUser" name="SecurityLevelUser" value="">
        <div id="ContainerTree"></div>
        
     </div>
    <div class="form-group">
         <label>Activo</label>
         <select class="form-control" id="StatusUser" name="StatusUser">
            <?php
            if($StatusUser!="" && ($StatusUser=="1" || $StatusUser=="0")){
                    if($StatusUser=="1"){
                    ?>
             <option selected value="1">Si</option>
              <option value="0">No</option>
                    <?php
                    }
                ?>
                 <?php
                    if($StatusUser=="0"){
                    ?>
                     <option value="1">Si</option>
                     <option selected value="0">No</option>
                    <?php
                    }
            }
            else{
            ?>
                <option value="1">Si</option>
                <option value="0">No</option>
           <?php
            }
            ?>
         </select>
     </div>
     <?php
      if($EditUser=="1"){
    ?>
       <button type="submit" id="Button_Edit_User" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em> Editar</button>
    <?php
      }else{
    ?>
    <button type="submit" id="Button_Create_User" class="btn btn-primary"><em class="glyphicon glyphicon-floppy-disk"></em> Guardar</button>
    <?php
      }
    ?>
</form> 
</div>
</body>
</html>
<?php
      $conexion = null;
}else{
   	header("location: ../403.php");
}
?>

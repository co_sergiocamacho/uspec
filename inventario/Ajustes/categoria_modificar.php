	<?php

//DESCRIPCION: VENTANA PARA MODIFICAR LA CATEGORÍA POR PARTE DEL USUARIO
//NOMBRE: ANDRÉS MONTEALEGRE GIRALDO
//FECHA: 2015-07-24
//Unidad de Servicios Penitenciarios y Carcelarios
//SOLUCIONES DE PRODUCTIVIDAD SAS


	session_start();
//Verificación de sesion
	if (isset($_SESSION['idpermiso'])) {

//CONEXION A LA BASE DE DATOS
		include("../database/conexion.php");
		include("../assets/encabezado.php");
//INCLUYE VARIABLE GLOBAL PARA LA FUNCION DE MAYUSCULAS
		include("../assets/global.php");
		?>

		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<link rel="shortcut icon" href="../imagenes/1.ico">
			<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
			<style>	



				body {
					background: #eaeaea no-repeat center top;
					-webkit-background-size: cover;
					-moz-background-size: cover;
					background-size: cover;
				}
				.container > header h1,
				.container > header h2 {
					color: #fff;
					text-shadow: 0 1px 1px rgba(0,0,0,0.7);
				}

			</style>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Usuario Modificar</title>



			<div id="div_bienvenido">
				<?php echo "Bienvenido"; ?> <BR/>
				<div id="div_usuarios">
					<?php echo "$_SESSION[nombres] $_SESSION[apellidos]"; ?>
				</div>
			</div>   
			<!--FIN IDENTIFICACION USUARIO LOGEADO -->
		</head>
		<body>
			<div id="centro2"><table class="botonesfila" >
				<tr><td>  <a href="../principal.php"><input type="image" src="../imagenes/inicio6.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">INICIO</a></td>
					<td><a href="categorias_menu.php"><input type="image" src="../imagenes/atras.png" width="52" height="52" name="regresar" title="Inicio" value="Regresar">ATRAS</a></td></tr></table></div>



					<div id="centro">

						<center>
							<table width="60%" class="tabla_2" border="0" style="width:620px">
								<?php
//***CONSULTA A LA BASE DE DATOS PARA TRAER LOS DATOS DEL USUARIOS A MODIFICAR***
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-07-24

								$rst_unidades=mysql_query ("SELECT *
									FROM codigocontable
									WHERE idcodigocontable=". $_REQUEST["idcodigocontable"].";",$conexion);
//echo $rst_clientes;
								while($filunidad=mysql_fetch_array($rst_unidades))
								{

									?>
									<tr>
										<td class="fila1">MODIFICAR CATEGORIA</td>
									</tr>
									<tr>
										<td class="fila2">Realizar la modificacion de la Categoria...</td>
									</tr>
								</table>

								<br>
								<form name="clientes" action="categoria_update.php?idcodigocontable=<?php echo $_REQUEST["idcodigocontable"];?>" title="Nuevo codigo" method="post">
									<table width="90%" class="tabla_2" border="0" style="width:580px">
										<tr>

										</tr>
										<tr>
											<td class="fila2">CODIGO :</td>
											<td class="fila2"><input type="text" class="textinput"  name="codigocontable" size="30" maxlength="80"   onkeypress="return SoloNumeros(event)" value="<?php echo $filunidad["codigocontable"] ?>"  /></td>
										</tr>
										<tr>
											<td class="fila2">CATEGORIA:</td>
											<td class="fila2"><input type="text" class="textinput"  name="codigodescripcion" size="100"  value="<?php echo $filunidad["codigodescripcion"] ?>"  /></td>


										</tr>
										<tr>

											<?php
										}
										?></td>
									</tr>
									<tr>
										<td class="fila2"><input type="hidden" name="idusuario" value="<?php echo "Bienvenido $_SESSION[idusuario]"; ?>" />
											<input type="hidden" name="idusu" value="<?php echo "ID usuario modificar $_REQUEST[id_usu]"; ?>" /></td>
											<td class="fila2"><p align="center">
												<input type="submit" class="botonguardar" name="guardar_categoria" title="Guardar Categoria" value="GUARDAR"/>
											</p></td>

											<td class="fila2"><p align="center"> 

											</a> </td>
										</tr>
									</table>
								</form></center>

								<?php
//INCLUYE EL FOOTER DE LA PAGINA
//NOMBRE: Andres Montealegre Giraldo
//FECHA: 2015-01-09
								include ("../assets/footer.php");
								?>
							</body>
							</html>

							<?php
/*
@Cerrar Sesion
*/
} else {
	header("location: ../403.php");
}
?>

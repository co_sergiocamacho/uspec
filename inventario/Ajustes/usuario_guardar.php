<?php
session_start();
include("../database/conexion");
if (isset($_SESSION['idpermiso'])) {
    date_default_timezone_set('America/Bogota');
    $hoy_dia_reg=date('Y-m-d');
    $clave_ = $_POST["valuepassword"];
    $clave=md5($clave_);
 
    /**
     * Create new user
     */
    $slqinsertusert="INSERT INTO usuarios ";
    $slqinsertusert.="(";
    $slqinsertusert.="nombres, ";
    $slqinsertusert.="apellidos, ";
    $slqinsertusert.="usuario, ";
    $slqinsertusert.="password, ";
    $slqinsertusert.="email, ";
    $slqinsertusert.="direccion, ";
    $slqinsertusert.="telefono, ";
    $slqinsertusert.="fechanac, ";
    $slqinsertusert.="documentoid, ";
    $slqinsertusert.="idprofesion, ";
    $slqinsertusert.="grado, ";
    $slqinsertusert.="cargo, ";
    $slqinsertusert.="idsexo, ";
    $slqinsertusert.="fechafincontrato, ";
    $slqinsertusert.="calidadempleado, ";
    $slqinsertusert.="idpermiso, ";
    $slqinsertusert.="iddependencia, ";
    $slqinsertusert.="observaciones, ";
    $slqinsertusert.="usuactivo ";
    $slqinsertusert.=") ";
    $slqinsertusert.="VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    /* Deshabilitar autocommit */
    
    try { 
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conexion->beginTransaction();
        $gsent = $conexion->prepare($slqinsertusert);
        $gsent->bindParam(1, $_POST['nomusu'], PDO::PARAM_STR);
        $gsent->bindParam(2, $_POST['apeusu'], PDO::PARAM_STR);
        $gsent->bindParam(3, $_POST['valueusuario'], PDO::PARAM_STR);
        $gsent->bindParam(4, $clave, PDO::PARAM_STR);
        $gsent->bindParam(5, $_POST['emailusu'], PDO::PARAM_STR);
        $gsent->bindParam(6, $_POST['dirusu'], PDO::PARAM_STR);
        $gsent->bindParam(7, $_POST['telusu'], PDO::PARAM_STR);
        $gsent->bindParam(8, $hoy_dia_reg, PDO::PARAM_STR);
        $gsent->bindParam(9, $_POST['docusu'], PDO::PARAM_STR);
        $gsent->bindParam(10, $_POST['profesion'], PDO::PARAM_STR);
        $gsent->bindParam(11, $_POST['grado'], PDO::PARAM_STR);
        $gsent->bindParam(12, $_POST['carusu'], PDO::PARAM_STR);
        $gsent->bindParam(13, $_POST['sexo'], PDO::PARAM_STR);
        $gsent->bindParam(14, $_POST['f_fin'], PDO::PARAM_STR);
        $gsent->bindParam(15, $_POST['calidadempleado'], PDO::PARAM_STR);
        $gsent->bindParam(16, $_POST['permiso'], PDO::PARAM_STR);
        $gsent->bindParam(17, $_POST['dependencia'], PDO::PARAM_STR);
        $gsent->bindParam(18, $_POST['obsusu'], PDO::PARAM_STR);
        $gsent->bindParam(19, $_POST['activo'], PDO::PARAM_STR);
        $gsent->execute();
        $count_process_1 = $gsent->rowCount();
        if($insertar->rowCount() > 0){
        $conexion->commit();
        $gsent->closeCursor();
        $gsent=null;
        $conexion=null;
        header("location:usuarios_menu.php?add=1");
        }
        /*$justificacion="creacion de usuario nuevo"."-  permiso usuario: " .$_SESSION['idpermiso'];
		$observacionregistro="Usuario creado : " .$_POST['nomusu'] ." " .$_POST['apeusu']." Acceso por IP: ".$ip ;
		$usuarioregistro= $_SESSION['nombres']. "  ".  $_SESSION['apellidos'];
		$fecha=date ("Y-m-d");
		$hora=date("H:i:s");
		$registro2="INSERT INTO registro (registro, usuario, fecha, hora, observacion) VALUES ('$justificacion' , '$usuarioregistro', '$fecha' ,'$hora' ,'$observacionregistro' ) 	";
		$queryreg2=mysql_query($registro2,$conexion);
		header("location:usuarios_menu.php?add=1");*/
  
    } catch (PDOException $e) {
        $conexion->rollBack();
        $conexion=null;
        echo "Fallo: " . $e->getMessage();
    }
}else{
     header("location: ../403.php");
}
?>

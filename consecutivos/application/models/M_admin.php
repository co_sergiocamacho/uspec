<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {
	public function update($data, $id) {
		$this->db->where("id", $id);
		$this->db->update("admin", $data);

		return $this->db->affected_rows();
	}

	public function select($id = '') {
		if ($id != '') {
			$this->db->where('id', $id);
		}

		$data = $this->db->get('admin');

		return $data->row();
	}
	public function informeMenu(){


		// $this->db->select('Voto_Propietario,Voto_Pregunta,Voto_Respuesta, Voto.at_create as time');
        // $this->db->from('Voto');

        // $this->db->join(
        //     'Pregunta',
        //     'Pregunta.Preg_id = Voto.Voto_Pregunta'
        // );
        // $this->db->where('Preg_Setup');

        // $query = $this->db->get();
        // return $query;
	}
	public function getTableTotalRow($tabla){

		$this->db->select('count('.$tabla.'.id) as theRows');		
        $this->db->from($tabla);
        $query = $this->db->get();
        return $query;
	}

		
	public function getUsersArea($tabla){
		$this->db->select('usuario');	
		$this->db->order_by('usuario', 'ASC');									
		$this->db->distinct();
        $this->db->from($tabla);
        $query = $this->db->get();
        return $query;
	}

	public function countUsersArea($tabla,$user){
		$this->db->select('count('.$tabla.'.id) as theRows');					
		$this->db->where('usuario', $user);
        $this->db->from($tabla);
        $query = $this->db->get();
        return $query;
	}

}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */
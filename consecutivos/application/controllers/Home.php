<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_pegawai');
		$this->load->model('M_posisi');
		$this->load->model('M_kota');
		$this->load->model('M_acta');
		$this->load->model('M_solcdp');
		$this->load->model('M_mocdp');
		$this->load->model('M_traspre');
	}

	public function index()
	{
		$data['jml_pegawai'] 	= $this->M_pegawai->total_rows();
		$data['jml_acta'] 	= $this->M_acta->total_rows();
		$data['jml_solcdp'] 	= $this->M_solcdp->total_rows();
		$data['jml_mocdp'] 	= $this->M_mocdp->total_rows();
		$data['jml_posisi'] 	= $this->M_posisi->total_rows();
		$data['jml_kota'] 		= $this->M_kota->total_rows();
		$data['jml_traspre']    = $this->M_traspre->total_rows();
		$data['userdata'] 		= $this->userdata;

		$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');

		$posisi 				= $this->M_posisi->select_all();
		$index = 0;
		foreach ($posisi as $value) {
			$color = '#' . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)];

			$pegawai_by_posisi = $this->M_pegawai->select_by_posisi($value->id);

			$data_posisi[$index]['value'] = $pegawai_by_posisi->jml;
			$data_posisi[$index]['color'] = $color;
			$data_posisi[$index]['highlight'] = $color;
			$data_posisi[$index]['label'] = $value->nama;

			$index++;
		}

		$kota 				= $this->M_kota->select_all();
		$index = 0;
		foreach ($kota as $value) {
			$color = '#' . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)] . $rand[rand(0, 16)];

			$pegawai_by_kota = $this->M_pegawai->select_by_kota($value->id);

			$data_kota[$index]['value'] = $pegawai_by_kota->jml;
			$data_kota[$index]['color'] = $color;
			$data_kota[$index]['highlight'] = $color;
			$data_kota[$index]['label'] = $value->nama;

			$index++;
		}

		$data['data_posisi'] = json_encode($data_posisi);
		$data['data_kota'] = json_encode($data_kota);

		$data['page'] 			= "Inicio";
		$data['judul'] 			= "Inicio Consecutivos";
		$data['deskripsi'] 		= "Unidad de Servicios Penitenciarios y Carcelarios USPEC";
		$this->template->views('home', $data);
	}


	public function informe()
	{
		$tablas = array("acta", "solcdp", "mocdp", "traspre");
		$collection = [];

		for ($i = 0; $i < count($tablas); $i++) {
			$totalRows = $this->M_admin->getTableTotalRow($tablas[$i])->result()[0]->theRows;
			$collection['general'][$tablas[$i]] = $totalRows;
			$usuarios = $this->M_admin->getUsersArea($tablas[$i])->result();			
			foreach ($usuarios as $row) {
				$count = $this->M_admin->countUsersArea($tablas[$i], $row->usuario)->result()[0]->theRows;
				$collection[$tablas[$i]][] = array('usuario' => $row->usuario, 'cantidad' => $count);
			}
		}
		echo json_encode($collection);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */
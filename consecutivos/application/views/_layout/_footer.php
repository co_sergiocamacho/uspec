<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		Consecutivos USPEC
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2020 <a href="#">Unidad de Servicios Penitenciarios y Carcelarios</a>.</strong>
</footer>
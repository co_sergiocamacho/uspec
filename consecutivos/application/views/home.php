﻿<div class="row">
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo $jml_acta; ?></h3>
        <p>Número de actas</p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-contact"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $jml_solcdp; ?></h3>

        <p>Número de CDP</p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-briefcase-outline"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $jml_mocdp; ?></h3>

        <p>Número de modificaciones a CDP</p>
      </div>
      <div class="icon">
        <i class="ion ion-location"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-purple">
      <div class="inner">
        <h3><?php echo $jml_traspre; ?></h3>
        <p> Traslados Presupuestales</p>
      </div>
      <div class="icon">
        <i class="ion ion-android-archive"></i>
      </div>
    </div>
    
  </div>
  <div class="col-lg-2 col-md-4 col-xs-6">
    <div class="small-box bg-white">
    <canvas id="myChart"></canvas>
    </div>

  </div>

  <div class="col-lg-12 col-xs-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-briefcase"></i>
        <h3 class="box-title">¿Cómo generar consecutivos? <small>Mire el siguiente video</small></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">

        <video width="" height="" controls>
          <source src="http://localhost/AdminLTE-CRUD-With-Codeigniter-master/assets/img/Consecutivos.mp4" type="video/mp4">
        </video>

      </div>
    </div>
  </div>


</div>

<script src="<?php echo base_url(); ?>assets/Chart.min.js"></script>
<script>
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener("readystatechange", function() {
    if (this.readyState === 4) {
      const obj = JSON.parse(this.responseText)
      loadPie1([obj.general.acta, obj.general.mocdp, obj.general.solcdp, obj.general.traspre])
      // actas(labels,cantidad)
    }
  });

  xhr.open("GET", "<?php echo base_url(); ?>/Home/informe");
  xhr.send();


  function loadPie1(ladata) {
    const data = {
      labels: [
        'Acta','Solicitud CDP',' Modificación CDP','Traslados Presupuestales'
      ],
      datasets: [{
        label: 'My First Dataset',
        data: ladata,
        backgroundColor: [
          '#00c0ef',
          '#00a65a',
          '#f39c12',
          '#605ca8'
        ],
        hoverOffset: 4
      }]
    };

    const config = {
      type: 'pie',
      data: data,
      options: {}
    };
    const myChart = new Chart(
      document.getElementById('myChart'),
      config
    );
  }
</script>
<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">
    <div class="col-md-6">
      <button class="form-control btn btn-primary" data-toggle="modal" data-target="#tambah-traspre"><i class="glyphicon glyphicon-plus-sign"></i> Generar</button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">

    Fecha Inicial
    <input type="date" id="dateStart" value="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" />
    Fecha Final
    <input type="date" id="dateEnd" value="<?php echo date('Y-m-d', strtotime('+1 day')); ?>" />
    <button onclick="searchDate('traspre')">Buscar</button>
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Consecutivo</th>
          <th>Descripción</th>
          <th>Usuario</th>
        </tr>
      </thead>
      <tbody id="data-traspre">

      </tbody>
    </table>
    <canvas id="traspre"></canvas>
  </div>
</div>

<?php echo $modal_tambah_kota; ?>

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataKota', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>
<?php

$data['judul'] = 'Kota';
$data['url'] = 'Kota/import';
echo show_my_modal('modals/modal_import', 'import-kota', $data);
?>


<script src="<?php echo base_url(); ?>assets/Chart.min.js"></script>
<script>
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener("readystatechange", function() {
    if (this.readyState === 4) {
      const obj = JSON.parse(this.responseText)
      tablas = ["traspre"]
      colors = ['#00c0ef', '#00a65a', '#f39c12', '#605ca8']
      title = ['Traslados Presupuestales']
      for (let y = 0; y < tablas.length; y++) {
        var labels = []
        var cantidad = []

        for (let i = 0; i < obj[tablas[y]].length; i++) {
          const element = obj[tablas[y]][i].usuario;
          const count = obj[tablas[y]][i].cantidad;
          labels.push(element)
          cantidad.push(count)
        }
        crearBar(tablas[y], labels, cantidad, colors[y], title[y])
      }
      // actas(labels,cantidad)
    }
  });

  xhr.open("GET", "<?php echo base_url(); ?>/Home/informe");
  xhr.send();

  function crearBar(tabla, theLabel, theData, theColor, theTitle) {
    const data = {
      labels: theLabel,
      datasets: [{
        axis: 'y',
        label: theTitle,
        data: theData,
        fill: true,
        backgroundColor: [
          theColor
        ],
        borderColor: [
          theColor
        ],
        borderWidth: 1
      }]
    };
    const config = {
      type: 'bar',
      data
    };
    const myChart = new Chart(
      document.getElementById(tabla),
      config
    );
  }
</script>